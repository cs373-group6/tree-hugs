import { EventParamType, ParkParamType, ThingsToDoParamType } from "../components/Types";

export const API_ENDPOINT = "https://treehugs.me/api/";
export const PARKS = "parks";
export const EVENTS = "events";
export const TTD = "thingstodo";
export const GOOGLE_API_KEY = "AIzaSyDZfonsQI2mQYnOfYJEYQEIyO1QOR9AODY";

/* eslint-disable @typescript-eslint/no-explicit-any */
export async function getParks(page: number, param: ParkParamType): Promise<any> {
  let query = `${API_ENDPOINT}${PARKS}?page=${page}`;
  if (param.day) {
    query += `&day=${param.day}`;
  }
  if (param.world_heritage) {
    query += `&world_heritage=${param.world_heritage}`;
  }
  if (param.min_budget) {
    query += `&min_budget=${param.min_budget}`;
  }
  if (param.max_budget) {
    query += `&max_budget=${param.max_budget}`;
  }
  if (param.name) {
    query += `&name=${param.name}`;
  }
  if (param.state) {
    query += `&state=${param.state}`;
  }
  if (param.page_size) {
    query += `&page_size=${param.page_size}`;
  }
  const res = await fetch(query);
  const data = await res.json();
  return data;
}

export async function getParkById(id: number): Promise<any> {
  const res = await fetch(`${API_ENDPOINT}${PARKS}/id=${id}`);
  const data = await res.json();
  return data;
}

export async function getEvents(page: number, param: EventParamType): Promise<any> {
  let query = `${API_ENDPOINT}${EVENTS}?page=${page}`;
  if (param.category) {
    query += `&category=${param.category}`;
  }
  if (param.free) {
    query += `&free=${param.free}`;
  }
  if (param.registration) {
    query += `&registration=${param.registration}`;
  }
  if (param.date) {
    query += `&date=${param.date}`;
  }
  if (param.title) {
    query += `&title=${param.title}`;
  }
  const res = await fetch(query);
  const data = await res.json();
  return data;
}

export async function getEventById(id: number): Promise<any> {
  const res = await fetch(`${API_ENDPOINT}${EVENTS}/id=${id}`);
  const data = await res.json();
  return data;
}

export async function getThingsToDo(page: number, param: ThingsToDoParamType): Promise<any> {
  let query = `${API_ENDPOINT}${TTD}?page=${page}`;
  if (param.seasons) {
    query += `&seasons=${param.seasons}`;
  }
  if (param.reservation) {
    query += `&reservation=${param.reservation}`;
  }
  if (param.fees) {
    query += `&fees=${param.fees}`;
  }
  if (param.pets) {
    query += `&pets=${param.pets}`;
  }
  if (param.title) {
    query += `&title=${param.title}`;
  }
  const res = await fetch(query);
  const data = await res.json();
  return data;
}

export async function getThingsToDoById(id: number): Promise<any> {
  const res = await fetch(`${API_ENDPOINT}${TTD}/id=${id}`);
  const data = await res.json();
  return data;
}
export async function getEventsPerDay(): Promise<any> {
  const res = await fetch(`${API_ENDPOINT}events-dates`);
  const data = await res.json();
  return data;
}
export async function getGitLabCommits(): Promise<any> {
  const res = await fetch(
    "https://gitlab.com/api/v4/projects/cs373-group6%2Ftree-hugs/repository/commits?ref_name=develop&per_page=9999",
  );
  const data = await res.json();
  return data;
}

export async function getGitLabIssues(): Promise<any> {
  let data: Array<any> = [];
  let page = 1;
  // eslint-disable-next-line no-constant-condition
  while (true) {
    /* eslint-disable no-await-in-loop */
    const res = await fetch(
      `https://gitlab.com/api/v4/projects/cs373-group6%2Ftree-hugs/issues?per_page=100&page=${page}`,
    );
    const pageData = await res.json();
    /* eslint-enable no-await-in-loop */
    data = data.concat(pageData);
    page += 1;
    if (pageData.length === 0) break;
  }
  return data;
}
