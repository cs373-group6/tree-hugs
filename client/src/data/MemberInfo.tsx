import yichenImage from "../images/yichen.jpg";
import sophieImage from "../images/sophie.jpg";
import reginaImage from "../images/regina.jpeg";
import stephanieImage from "../images/stephanie.jpeg";
import eugeneImage from "../images/eugene.jpeg";

const members = [
  {
    name: "Eugene Chang",
    id: "eugeneccc",
    role: "Frontend Developer",
    leadership: "|   Phase 3 Leader",
    commits: 0,
    issues: 0,
    unitTests: 0,
    image: eugeneImage,
    bio:
      "I am a fourth year CS student at UT Austin. I have interest in data science. I do guided meditation every day.",
    linkedin: "https://www.linkedin.com/in/eugene-chang-127a30175/",
  },
  {
    name: "Regina Chen",
    id: "reginaxchen",
    role: "Frontend Developer",
    leadership: "|   Phase 4 Leader",
    commits: 0,
    issues: 0,
    unitTests: 0,
    image: reginaImage,
    bio:
      "I am a third year CS major at UT Austin from Houston, Texas. I am interested in full stack development. My hobbies include baking and powerlifting.",
    linkedin: "https://www.linkedin.com/in/reginaxchen/",
  },
  {
    name: "Yichen Liu",
    id: "tt0suzy",
    role: "Frontend Developer",
    leadership: "",
    commits: 0,
    issues: 0,
    unitTests: 0,
    image: yichenImage,
    bio:
      "I'm a fourth year CS student at University of Texas at Austin. I'm very into web apps and games development, and I love playing dota2 in my free time.",
    linkedin: "https://www.linkedin.com/in/yichen-liu-44253a157/",
  },
  {
    name: "Stephanie Tran",
    id: "stepokness",
    role: "Backend Developer",
    leadership: "|   Phase 2 Leader",
    commits: 0,
    issues: 0,
    unitTests: 0,
    image: stephanieImage,
    bio:
      "I'm a third year studying CS at UT Austin, and I'm interested in back end development. Some hobbies I have are reading and drawing.",
    linkedin: "https://www.linkedin.com/in/stephctran/",
  },
  {
    name: "Sophie Zhao",
    id: "SophieZhao00",
    role: "Fullstack Developer",
    leadership: "|   Phase 1 Leader",
    commits: 0,
    issues: 0,
    unitTests: 0,
    image: sophieImage,
    bio:
      "I’m a third year CS major and business minor at UT Austin. I'm interested in full-stack development. I like watching TikTok in my free time.",
    linkedin: "https://www.linkedin.com/in/xuefei-zhao/ ",
  },
];

export default members;
