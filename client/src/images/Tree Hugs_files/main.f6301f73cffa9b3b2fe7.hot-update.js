webpackHotUpdate("main",{

/***/ "./src/pages/ThingstodoInstancePage.tsx":
/*!**********************************************!*\
  !*** ./src/pages/ThingstodoInstancePage.tsx ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(__react_refresh_utils__, __react_refresh_error_overlay__) {/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-bootstrap */ "./node_modules/react-bootstrap/esm/index.js");
/* harmony import */ var react_player__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-player */ "./node_modules/react-player/lib/index.js");
/* harmony import */ var react_player__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_player__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _data_api__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../data/api */ "./src/data/api.tsx");
/* harmony import */ var _styles_pages_css__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../styles/pages.css */ "./src/styles/pages.css");
/* harmony import */ var _styles_pages_css__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_styles_pages_css__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _styles_thingstodo_module_css__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../styles/thingstodo.module.css */ "./src/styles/thingstodo.module.css");
/* harmony import */ var _styles_thingstodo_module_css__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_styles_thingstodo_module_css__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _images_yichen_jpg__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../images/yichen.jpg */ "./src/images/yichen.jpg");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_8__);
__webpack_require__.$Refresh$.runtime = __webpack_require__(/*! ./node_modules/react-refresh/runtime.js */ "./node_modules/react-refresh/runtime.js");
__webpack_require__.$Refresh$.setup(module.i);

var _jsxFileName = "/tree-hugs/client/src/pages/ThingstodoInstancePage.tsx",
    _s = __webpack_require__.$Refresh$.signature();



 // import { GoogleMap, useLoadScript } from "@react-google-maps/api";
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore






/* eslint-disable camelcase */



function ThingsToDoInstance() {
  _s();

  var _thingsToDo$media, _videoFeed$items, _videoFeed$items$, _videoFeed$items$$id, _thingsToDo$parks, _thingsToDo$park, _thingsToDo$descripti, _thingsToDo$accessibi, _thingsToDo$media2, _thingsToDo$media2$im, _thingsToDo$media3, _thingsToDo$media3$, _thingsToDo$tags;

  const [thingsToDo, setThingsToDo] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])({});
  const [loading, setLoading] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false);
  const [videoFeed, setVideoFeed] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])({});
  const [searchQuery, setSearchQuery] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])("things to do in national parks");
  const thingsToDoId = parseInt(window.location.pathname.split("/").pop(), 10);
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
    const getThingsToDoData = async () => {
      var _apiThingsToDoData$pa;

      const apiThingsToDoData = await Object(_data_api__WEBPACK_IMPORTED_MODULE_4__["getThingsToDoById"])(thingsToDoId);
      setThingsToDo(apiThingsToDoData);
      setSearchQuery(`${apiThingsToDoData.title} ${(_apiThingsToDoData$pa = apiThingsToDoData.parks) === null || _apiThingsToDoData$pa === void 0 ? void 0 : _apiThingsToDoData$pa[0].name}`);
      console.log("search query is ");
      console.log(searchQuery);
    };

    setLoading(true);
    getThingsToDoData();
  }, []);
  const imgLength = (_thingsToDo$media = thingsToDo.media) === null || _thingsToDo$media === void 0 ? void 0 : _thingsToDo$media.length;
  const hasImg = imgLength > 0;
  const imgId = Math.floor(Math.random() * imgLength); // const searchQuery = `${thingsToDo.title} ${thingsToDo.parks?.[0].name}`;

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
    const getVideoData = async () => {
      const videoData = await Object(_data_api__WEBPACK_IMPORTED_MODULE_4__["getVideoFeedByQuery"])(searchQuery);
      setVideoFeed(videoData);
    };

    getVideoData();
  }, []);
  const videoId = videoFeed === null || videoFeed === void 0 ? void 0 : (_videoFeed$items = videoFeed.items) === null || _videoFeed$items === void 0 ? void 0 : (_videoFeed$items$ = _videoFeed$items[0]) === null || _videoFeed$items$ === void 0 ? void 0 : (_videoFeed$items$$id = _videoFeed$items$.id) === null || _videoFeed$items$$id === void 0 ? void 0 : _videoFeed$items$$id.videoId;
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_8__["jsxDEV"])("div", {
    className: "page",
    children: loading ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_8__["jsxDEV"])("div", {
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_8__["jsxDEV"])("h1", {
        children: thingsToDo.title
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 65,
        columnNumber: 11
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_8__["jsxDEV"])("p", {
        children: ["Park: ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_8__["jsxDEV"])(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
          to: `/parks/${(_thingsToDo$parks = thingsToDo.parks) === null || _thingsToDo$parks === void 0 ? void 0 : _thingsToDo$parks[0].id}`,
          children: (_thingsToDo$park = thingsToDo.park) === null || _thingsToDo$park === void 0 ? void 0 : _thingsToDo$park[0].name
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 67,
          columnNumber: 19
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_8__["jsxDEV"])("br", {}, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 68,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_8__["jsxDEV"])("br", {}, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 69,
          columnNumber: 13
        }, this), "Description: ", (_thingsToDo$descripti = thingsToDo.description) === null || _thingsToDo$descripti === void 0 ? void 0 : _thingsToDo$descripti.replace(/<\/?[^>]+>/gi, ""), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_8__["jsxDEV"])("br", {}, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 71,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_8__["jsxDEV"])("br", {}, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 72,
          columnNumber: 13
        }, this), "Duration: ", thingsToDo.duration, /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_8__["jsxDEV"])("br", {}, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 74,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_8__["jsxDEV"])("br", {}, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 75,
          columnNumber: 13
        }, this), "Fee Description: ", thingsToDo.fee_description, /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_8__["jsxDEV"])("br", {}, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 77,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_8__["jsxDEV"])("br", {}, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 78,
          columnNumber: 13
        }, this), "Accessibility Information: ", (_thingsToDo$accessibi = thingsToDo.accessibility_info) === null || _thingsToDo$accessibi === void 0 ? void 0 : _thingsToDo$accessibi.replace(/<\/?[^>]+>/gi, ""), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_8__["jsxDEV"])("br", {}, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 80,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_8__["jsxDEV"])("img", {
          src: hasImg ? (_thingsToDo$media2 = thingsToDo.media) === null || _thingsToDo$media2 === void 0 ? void 0 : (_thingsToDo$media2$im = _thingsToDo$media2[imgId]) === null || _thingsToDo$media2$im === void 0 ? void 0 : _thingsToDo$media2$im.url : _images_yichen_jpg__WEBPACK_IMPORTED_MODULE_7__["default"],
          alt: (_thingsToDo$media3 = thingsToDo.media) === null || _thingsToDo$media3 === void 0 ? void 0 : (_thingsToDo$media3$ = _thingsToDo$media3[0]) === null || _thingsToDo$media3$ === void 0 ? void 0 : _thingsToDo$media3$.altText,
          className: _styles_thingstodo_module_css__WEBPACK_IMPORTED_MODULE_6___default.a.image
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 81,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_8__["jsxDEV"])("br", {}, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 86,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_8__["jsxDEV"])("br", {}, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 87,
          columnNumber: 13
        }, this), "Tags:", " ", (_thingsToDo$tags = thingsToDo.tags) === null || _thingsToDo$tags === void 0 ? void 0 : _thingsToDo$tags.map(tag => /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_8__["jsxDEV"])("h4", {
          children: tag === null || tag === void 0 ? void 0 : tag.name
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 90,
          columnNumber: 15
        }, this)), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_8__["jsxDEV"])("hr", {}, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 92,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_8__["jsxDEV"])("br", {}, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 93,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_8__["jsxDEV"])(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
          to: "/thingstodo1",
          children: "Click here to see realted things to do"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 94,
          columnNumber: 13
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 66,
        columnNumber: 11
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_8__["jsxDEV"])(react_player__WEBPACK_IMPORTED_MODULE_3___default.a, {
        url: `https://www.youtube.com/watch?v=${videoId}`
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 96,
        columnNumber: 11
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 64,
      columnNumber: 9
    }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_8__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Spinner"], {
      animation: "border"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 99,
      columnNumber: 9
    }, this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 62,
    columnNumber: 5
  }, this);
}

_s(ThingsToDoInstance, "b7yV/pBHqPOqNhJCRS7KlA9jRtA=");

_c = ThingsToDoInstance;
/* harmony default export */ __webpack_exports__["default"] = (ThingsToDoInstance);

var _c;

__webpack_require__.$Refresh$.register(_c, "ThingsToDoInstance");

const currentExports = __react_refresh_utils__.getModuleExports(module.i);
__react_refresh_utils__.registerExportsForReactRefresh(currentExports, module.i);

if (true) {
  const isHotUpdate = !!module.hot.data;
  const prevExports = isHotUpdate ? module.hot.data.prevExports : null;

  if (__react_refresh_utils__.isReactRefreshBoundary(currentExports)) {
    module.hot.dispose(
      /**
       * A callback to performs a full refresh if React has unrecoverable errors,
       * and also caches the to-be-disposed module.
       * @param {*} data A hot module data object from Webpack HMR.
       * @returns {void}
       */
      function hotDisposeCallback(data) {
        // We have to mutate the data object to get data registered and cached
        data.prevExports = currentExports;
      }
    );
    module.hot.accept(
      /**
       * An error handler to allow self-recovering behaviours.
       * @param {Error} error An error occurred during evaluation of a module.
       * @returns {void}
       */
      function hotErrorHandler(error) {
        if (
          typeof __react_refresh_error_overlay__ !== 'undefined' &&
          __react_refresh_error_overlay__
        ) {
          __react_refresh_error_overlay__.handleRuntimeError(error);
        }

        if (typeof __react_refresh_test__ !== 'undefined' && __react_refresh_test__) {
          if (window.onHotAcceptError) {
            window.onHotAcceptError(error.message);
          }
        }

        __webpack_require__.c[module.i].hot.accept(hotErrorHandler);
      }
    );

    if (isHotUpdate) {
      if (
        __react_refresh_utils__.isReactRefreshBoundary(prevExports) &&
        __react_refresh_utils__.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)
      ) {
        module.hot.invalidate();
      } else {
        __react_refresh_utils__.enqueueUpdate(
          /**
           * A function to dismiss the error overlay after performing React refresh.
           * @returns {void}
           */
          function updateCallback() {
            if (
              typeof __react_refresh_error_overlay__ !== 'undefined' &&
              __react_refresh_error_overlay__
            ) {
              __react_refresh_error_overlay__.clearRuntimeErrors();
            }
          }
        );
      }
    }
  } else {
    if (isHotUpdate && __react_refresh_utils__.isReactRefreshBoundary(prevExports)) {
      module.hot.invalidate();
    }
  }
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js"), __webpack_require__(/*! ./node_modules/react-dev-utils/refreshOverlayInterop.js */ "./node_modules/react-dev-utils/refreshOverlayInterop.js")))

/***/ })

})
//# sourceMappingURL=main.f6301f73cffa9b3b2fe7.hot-update.js.map