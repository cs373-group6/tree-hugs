(this["webpackJsonptree-hugs"] = this["webpackJsonptree-hugs"] || []).push([["main"],{

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/styles/about.module.css":
/*!****************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--5-oneOf-5-1!./node_modules/postcss-loader/src??postcss!./src/styles/about.module.css ***!
  \****************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
// Imports

var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(true);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".about_aboutPage__31FSf {\n  text-align: center;\n}\n.about_carddeck__pkQOG {\n  display: flex;\n  flex-direction: row;\n  flex: 1 1;\n  justify-content: center;\n}\n.about_biocard__3zZHV {\n  text-align: center;\n  max-width: 30%;\n  min-width: 300px;\n  border: 20px;\n}\n.about_headshot__R_wcq {\n  /* object-fit: cover !important; */\n  width: 100%;\n  /* height: 300px; */\n}\n.about_toolcard__1L9Rf {\n  text-align: center;\n  border: 20px;\n  max-width: 15%;\n  min-width: 150px;\n}\n.about_logo__1vogH {\n  max-width: 200px;\n  height: 150px;\n}\n", "",{"version":3,"sources":["webpack://src/styles/about.module.css"],"names":[],"mappings":"AAAA;EACE,kBAAkB;AACpB;AACA;EACE,aAAa;EACb,mBAAmB;EACnB,SAAO;EACP,uBAAuB;AACzB;AACA;EACE,kBAAkB;EAClB,cAAc;EACd,gBAAgB;EAChB,YAAY;AACd;AACA;EACE,kCAAkC;EAClC,WAAW;EACX,mBAAmB;AACrB;AACA;EACE,kBAAkB;EAClB,YAAY;EACZ,cAAc;EACd,gBAAgB;AAClB;AACA;EACE,gBAAgB;EAChB,aAAa;AACf","sourcesContent":[".aboutPage {\n  text-align: center;\n}\n.carddeck {\n  display: flex;\n  flex-direction: row;\n  flex: 1;\n  justify-content: center;\n}\n.biocard {\n  text-align: center;\n  max-width: 30%;\n  min-width: 300px;\n  border: 20px;\n}\n.headshot {\n  /* object-fit: cover !important; */\n  width: 100%;\n  /* height: 300px; */\n}\n.toolcard {\n  text-align: center;\n  border: 20px;\n  max-width: 15%;\n  min-width: 150px;\n}\n.logo {\n  max-width: 200px;\n  height: 150px;\n}\n"],"sourceRoot":""}]);
// Exports
___CSS_LOADER_EXPORT___.locals = {
	"aboutPage": "about_aboutPage__31FSf",
	"carddeck": "about_carddeck__pkQOG",
	"biocard": "about_biocard__3zZHV",
	"headshot": "about_headshot__R_wcq",
	"toolcard": "about_toolcard__1L9Rf",
	"logo": "about_logo__1vogH"
};
/* harmony default export */ __webpack_exports__["default"] = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/styles/events.module.css":
/*!*****************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--5-oneOf-5-1!./node_modules/postcss-loader/src??postcss!./src/styles/events.module.css ***!
  \*****************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
// Imports

var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(true);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".events_image__3IEay {\n  width: 100%;\n  max-height: 500px;\n  max-width: 600px;\n}\n", "",{"version":3,"sources":["webpack://src/styles/events.module.css"],"names":[],"mappings":"AAAA;EACE,WAAW;EACX,iBAAiB;EACjB,gBAAgB;AAClB","sourcesContent":[".image {\n  width: 100%;\n  max-height: 500px;\n  max-width: 600px;\n}\n"],"sourceRoot":""}]);
// Exports
___CSS_LOADER_EXPORT___.locals = {
	"image": "events_image__3IEay"
};
/* harmony default export */ __webpack_exports__["default"] = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/styles/landing.module.css":
/*!******************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--5-oneOf-5-1!./node_modules/postcss-loader/src??postcss!./src/styles/landing.module.css ***!
  \******************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
// Imports

var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(true);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".landing_landing__3qwGA {\n  display: flex;\n}\n#landing_overlay__39mm0 {\n  background-color: rgba(255, 255, 255, 0.9);\n  max-width: 850px;\n  margin: auto;\n  margin-top: 10%;\n  padding: 30px;\n  text-align: center;\n}\n.landing_row__ikMkA {\n  display: flex;\n  justify-content: center;\n}\n.landing_modelImg__3jw6J {\n  width: 100%;\n}\n.landing_button__1559b {\n  background: #b7c4bc;\n  border: #78877e;\n}\n", "",{"version":3,"sources":["webpack://src/styles/landing.module.css"],"names":[],"mappings":"AAAA;EACE,aAAa;AACf;AACA;EACE,0CAA0C;EAC1C,gBAAgB;EAChB,YAAY;EACZ,eAAe;EACf,aAAa;EACb,kBAAkB;AACpB;AACA;EACE,aAAa;EACb,uBAAuB;AACzB;AACA;EACE,WAAW;AACb;AACA;EACE,mBAAmB;EACnB,eAAe;AACjB","sourcesContent":[".landing {\n  display: flex;\n}\n#overlay {\n  background-color: rgba(255, 255, 255, 0.9);\n  max-width: 850px;\n  margin: auto;\n  margin-top: 10%;\n  padding: 30px;\n  text-align: center;\n}\n.row {\n  display: flex;\n  justify-content: center;\n}\n.modelImg {\n  width: 100%;\n}\n.button {\n  background: #b7c4bc;\n  border: #78877e;\n}\n"],"sourceRoot":""}]);
// Exports
___CSS_LOADER_EXPORT___.locals = {
	"landing": "landing_landing__3qwGA",
	"overlay": "landing_overlay__39mm0",
	"row": "landing_row__ikMkA",
	"modelImg": "landing_modelImg__3jw6J",
	"button": "landing_button__1559b"
};
/* harmony default export */ __webpack_exports__["default"] = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/styles/pages.css":
/*!*********************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--5-oneOf-4-1!./node_modules/postcss-loader/src??postcss!./src/styles/pages.css ***!
  \*********************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../node_modules/css-loader/dist/runtime/getUrl.js */ "./node_modules/css-loader/dist/runtime/getUrl.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _images_HomeBackground_jpg__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../images/HomeBackground.jpg */ "./src/images/HomeBackground.jpg");
// Imports



var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(true);
var ___CSS_LOADER_URL_REPLACEMENT_0___ = _node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_1___default()(_images_HomeBackground_jpg__WEBPACK_IMPORTED_MODULE_2__["default"]);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".page {\n  padding: 0.8in 0.2in 0.2in 0.2in;\n  max-width: 1200px;\n  margin: auto;\n}\n\n#home {\n  background: url(" + ___CSS_LOADER_URL_REPLACEMENT_0___ + ");\n  height: 100vh;\n  background-repeat: no-repeat;\n  background-position: center center;\n  background-size: cover;\n  padding: 0.8in 0.2in 0.2in 0.2in;\n}\n\n.parkImg {\n  width: 400px;\n  height: 300px;\n}\n\n.ParkCard {\n  display: inline-block;\n  width: 300px;\n  height: 400px;\n  margin-left:100px;\n  margin-bottom: 50px;\n}\n", "",{"version":3,"sources":["webpack://src/styles/pages.css"],"names":[],"mappings":"AAAA;EACE,gCAAgC;EAChC,iBAAiB;EACjB,YAAY;AACd;;AAEA;EACE,mDAA+C;EAC/C,aAAa;EACb,4BAA4B;EAC5B,kCAAkC;EAClC,sBAAsB;EACtB,gCAAgC;AAClC;;AAEA;EACE,YAAY;EACZ,aAAa;AACf;;AAEA;EACE,qBAAqB;EACrB,YAAY;EACZ,aAAa;EACb,iBAAiB;EACjB,mBAAmB;AACrB","sourcesContent":[".page {\n  padding: 0.8in 0.2in 0.2in 0.2in;\n  max-width: 1200px;\n  margin: auto;\n}\n\n#home {\n  background: url(\"../images/HomeBackground.jpg\");\n  height: 100vh;\n  background-repeat: no-repeat;\n  background-position: center center;\n  background-size: cover;\n  padding: 0.8in 0.2in 0.2in 0.2in;\n}\n\n.parkImg {\n  width: 400px;\n  height: 300px;\n}\n\n.ParkCard {\n  display: inline-block;\n  width: 300px;\n  height: 400px;\n  margin-left:100px;\n  margin-bottom: 50px;\n}\n"],"sourceRoot":""}]);
// Exports
/* harmony default export */ __webpack_exports__["default"] = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/styles/parks.module.css":
/*!****************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--5-oneOf-5-1!./node_modules/postcss-loader/src??postcss!./src/styles/parks.module.css ***!
  \****************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
// Imports

var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(true);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".parks_parkCardImg__1ihTR {\n  width: 100%;\n  height: 50%;\n}\n", "",{"version":3,"sources":["webpack://src/styles/parks.module.css"],"names":[],"mappings":"AAAA;EACE,WAAW;EACX,WAAW;AACb","sourcesContent":[".parkCardImg {\n  width: 100%;\n  height: 50%;\n}\n"],"sourceRoot":""}]);
// Exports
___CSS_LOADER_EXPORT___.locals = {
	"parkCardImg": "parks_parkCardImg__1ihTR"
};
/* harmony default export */ __webpack_exports__["default"] = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/styles/thingstodo.module.css":
/*!*********************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--5-oneOf-5-1!./node_modules/postcss-loader/src??postcss!./src/styles/thingstodo.module.css ***!
  \*********************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
// Imports

var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(true);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".thingstodo_image__2-WLd {\n  width: 100%;\n  max-height: 500px;\n  max-width: 600px;\n}\n", "",{"version":3,"sources":["webpack://src/styles/thingstodo.module.css"],"names":[],"mappings":"AAAA;EACE,WAAW;EACX,iBAAiB;EACjB,gBAAgB;AAClB","sourcesContent":[".image {\n  width: 100%;\n  max-height: 500px;\n  max-width: 600px;\n}\n"],"sourceRoot":""}]);
// Exports
___CSS_LOADER_EXPORT___.locals = {
	"image": "thingstodo_image__2-WLd"
};
/* harmony default export */ __webpack_exports__["default"] = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./src/App.tsx":
/*!*********************!*\
  !*** ./src/App.tsx ***!
  \*********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(__react_refresh_utils__, __react_refresh_error_overlay__) {/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _components_NavBar__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./components/NavBar */ "./src/components/NavBar.tsx");
/* harmony import */ var _pages_LandingPage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./pages/LandingPage */ "./src/pages/LandingPage.tsx");
/* harmony import */ var _pages_ParksModelPage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./pages/ParksModelPage */ "./src/pages/ParksModelPage.tsx");
/* harmony import */ var _pages_EventsModelPage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./pages/EventsModelPage */ "./src/pages/EventsModelPage.tsx");
/* harmony import */ var _pages_ThingstodoModelPage__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./pages/ThingstodoModelPage */ "./src/pages/ThingstodoModelPage.tsx");
/* harmony import */ var _pages_ThingstodoInstancePage__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./pages/ThingstodoInstancePage */ "./src/pages/ThingstodoInstancePage.tsx");
/* harmony import */ var _pages_AboutPage__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./pages/AboutPage */ "./src/pages/AboutPage.tsx");
/* harmony import */ var _pages_parks1__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./pages/parks1 */ "./src/pages/parks1.tsx");
/* harmony import */ var _pages_parks2__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./pages/parks2 */ "./src/pages/parks2.tsx");
/* harmony import */ var _pages_parks3__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./pages/parks3 */ "./src/pages/parks3.tsx");
/* harmony import */ var _pages_thingstodo2__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./pages/thingstodo2 */ "./src/pages/thingstodo2.tsx");
/* harmony import */ var _pages_thingstodo3__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./pages/thingstodo3 */ "./src/pages/thingstodo3.tsx");
/* harmony import */ var _pages_events1__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./pages/events1 */ "./src/pages/events1.tsx");
/* harmony import */ var _pages_events2__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./pages/events2 */ "./src/pages/events2.tsx");
/* harmony import */ var _pages_events3__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./pages/events3 */ "./src/pages/events3.tsx");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_17___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_17__);
__webpack_require__.$Refresh$.runtime = __webpack_require__(/*! ./node_modules/react-refresh/runtime.js */ "./node_modules/react-refresh/runtime.js");
__webpack_require__.$Refresh$.setup(module.i);

var _jsxFileName = "/tree-hugs/client/src/App.tsx";



















function App() {
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_17__["jsxDEV"])("div", {
    className: "App",
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_17__["jsxDEV"])(_components_NavBar__WEBPACK_IMPORTED_MODULE_2__["default"], {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 22,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_17__["jsxDEV"])(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Switch"], {
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_17__["jsxDEV"])(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
        exact: true,
        path: "/",
        component: _pages_LandingPage__WEBPACK_IMPORTED_MODULE_3__["default"]
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 24,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_17__["jsxDEV"])(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
        exact: true,
        path: "/parks",
        component: _pages_ParksModelPage__WEBPACK_IMPORTED_MODULE_4__["default"]
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 25,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_17__["jsxDEV"])(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
        exact: true,
        path: "/events",
        component: _pages_EventsModelPage__WEBPACK_IMPORTED_MODULE_5__["default"]
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 26,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_17__["jsxDEV"])(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
        exact: true,
        path: "/thingstodo",
        component: _pages_ThingstodoModelPage__WEBPACK_IMPORTED_MODULE_6__["default"]
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 27,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_17__["jsxDEV"])(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
        exact: true,
        path: "/about",
        component: _pages_AboutPage__WEBPACK_IMPORTED_MODULE_8__["default"]
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 28,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_17__["jsxDEV"])(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
        exact: true,
        path: "/parks1",
        component: _pages_parks1__WEBPACK_IMPORTED_MODULE_9__["default"]
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 29,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_17__["jsxDEV"])(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
        exact: true,
        path: "/parks2",
        component: _pages_parks2__WEBPACK_IMPORTED_MODULE_10__["default"]
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 30,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_17__["jsxDEV"])(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
        exact: true,
        path: "/parks3",
        component: _pages_parks3__WEBPACK_IMPORTED_MODULE_11__["default"]
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 31,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_17__["jsxDEV"])(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
        exact: true,
        path: "/thingstodo/:id",
        component: _pages_ThingstodoInstancePage__WEBPACK_IMPORTED_MODULE_7__["default"]
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 32,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_17__["jsxDEV"])(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
        exact: true,
        path: "/thingstodo2",
        component: _pages_thingstodo2__WEBPACK_IMPORTED_MODULE_12__["default"]
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 33,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_17__["jsxDEV"])(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
        exact: true,
        path: "/thingstodo3",
        component: _pages_thingstodo3__WEBPACK_IMPORTED_MODULE_13__["default"]
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 34,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_17__["jsxDEV"])(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
        exact: true,
        path: "/events1",
        component: _pages_events1__WEBPACK_IMPORTED_MODULE_14__["default"]
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 35,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_17__["jsxDEV"])(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
        exact: true,
        path: "/events2",
        component: _pages_events2__WEBPACK_IMPORTED_MODULE_15__["default"]
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 36,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_17__["jsxDEV"])(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
        exact: true,
        path: "/events3",
        component: _pages_events3__WEBPACK_IMPORTED_MODULE_16__["default"]
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 37,
        columnNumber: 9
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 23,
      columnNumber: 7
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 21,
    columnNumber: 5
  }, this);
}

_c = App;
/* harmony default export */ __webpack_exports__["default"] = (App);

var _c;

__webpack_require__.$Refresh$.register(_c, "App");

const currentExports = __react_refresh_utils__.getModuleExports(module.i);
__react_refresh_utils__.registerExportsForReactRefresh(currentExports, module.i);

if (true) {
  const isHotUpdate = !!module.hot.data;
  const prevExports = isHotUpdate ? module.hot.data.prevExports : null;

  if (__react_refresh_utils__.isReactRefreshBoundary(currentExports)) {
    module.hot.dispose(
      /**
       * A callback to performs a full refresh if React has unrecoverable errors,
       * and also caches the to-be-disposed module.
       * @param {*} data A hot module data object from Webpack HMR.
       * @returns {void}
       */
      function hotDisposeCallback(data) {
        // We have to mutate the data object to get data registered and cached
        data.prevExports = currentExports;
      }
    );
    module.hot.accept(
      /**
       * An error handler to allow self-recovering behaviours.
       * @param {Error} error An error occurred during evaluation of a module.
       * @returns {void}
       */
      function hotErrorHandler(error) {
        if (
          typeof __react_refresh_error_overlay__ !== 'undefined' &&
          __react_refresh_error_overlay__
        ) {
          __react_refresh_error_overlay__.handleRuntimeError(error);
        }

        if (typeof __react_refresh_test__ !== 'undefined' && __react_refresh_test__) {
          if (window.onHotAcceptError) {
            window.onHotAcceptError(error.message);
          }
        }

        __webpack_require__.c[module.i].hot.accept(hotErrorHandler);
      }
    );

    if (isHotUpdate) {
      if (
        __react_refresh_utils__.isReactRefreshBoundary(prevExports) &&
        __react_refresh_utils__.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)
      ) {
        module.hot.invalidate();
      } else {
        __react_refresh_utils__.enqueueUpdate(
          /**
           * A function to dismiss the error overlay after performing React refresh.
           * @returns {void}
           */
          function updateCallback() {
            if (
              typeof __react_refresh_error_overlay__ !== 'undefined' &&
              __react_refresh_error_overlay__
            ) {
              __react_refresh_error_overlay__.clearRuntimeErrors();
            }
          }
        );
      }
    }
  } else {
    if (isHotUpdate && __react_refresh_utils__.isReactRefreshBoundary(prevExports)) {
      module.hot.invalidate();
    }
  }
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js"), __webpack_require__(/*! ./node_modules/react-dev-utils/refreshOverlayInterop.js */ "./node_modules/react-dev-utils/refreshOverlayInterop.js")))

/***/ }),

/***/ "./src/components/BioCard.tsx":
/*!************************************!*\
  !*** ./src/components/BioCard.tsx ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(__react_refresh_utils__, __react_refresh_error_overlay__) {/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_bootstrap_Card__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-bootstrap/Card */ "./node_modules/react-bootstrap/esm/Card.js");
/* harmony import */ var _styles_about_module_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../styles/about.module.css */ "./src/styles/about.module.css");
/* harmony import */ var _styles_about_module_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_styles_about_module_css__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3__);
__webpack_require__.$Refresh$.runtime = __webpack_require__(/*! ./node_modules/react-refresh/runtime.js */ "./node_modules/react-refresh/runtime.js");
__webpack_require__.$Refresh$.setup(module.i);

var _jsxFileName = "/tree-hugs/client/src/components/BioCard.tsx";





function BioCard({
  member,
  commits,
  issues
}) {
  if (member) {
    return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3__["jsxDEV"])(react_bootstrap_Card__WEBPACK_IMPORTED_MODULE_1__["default"], {
      className: _styles_about_module_css__WEBPACK_IMPORTED_MODULE_2___default.a.biocard,
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3__["jsxDEV"])(react_bootstrap_Card__WEBPACK_IMPORTED_MODULE_1__["default"].Body, {
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3__["jsxDEV"])(react_bootstrap_Card__WEBPACK_IMPORTED_MODULE_1__["default"].Img, {
          variant: "top",
          className: _styles_about_module_css__WEBPACK_IMPORTED_MODULE_2___default.a.headshot,
          src: member.image
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 24,
          columnNumber: 11
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3__["jsxDEV"])(react_bootstrap_Card__WEBPACK_IMPORTED_MODULE_1__["default"].Title, {
          children: [" ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3__["jsxDEV"])("a", {
            href: member.linkedin,
            children: [" ", member.name, " "]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 27,
            columnNumber: 13
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 25,
          columnNumber: 11
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3__["jsxDEV"])(react_bootstrap_Card__WEBPACK_IMPORTED_MODULE_1__["default"].Subtitle, {
          children: [member.role, " ", member.leadership]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 29,
          columnNumber: 11
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3__["jsxDEV"])("hr", {}, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 32,
          columnNumber: 11
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3__["jsxDEV"])(react_bootstrap_Card__WEBPACK_IMPORTED_MODULE_1__["default"].Text, {
          children: member.bio
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 33,
          columnNumber: 11
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3__["jsxDEV"])(react_bootstrap_Card__WEBPACK_IMPORTED_MODULE_1__["default"].Text, {
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3__["jsxDEV"])("small", {
            children: ["Commits : ", commits, " Issues: ", issues, " Tests: ", member.unitTests, " "]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 35,
            columnNumber: 13
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 34,
          columnNumber: 11
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 23,
        columnNumber: 9
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 22,
      columnNumber: 7
    }, this);
  }

  return null;
}

_c = BioCard;
/* harmony default export */ __webpack_exports__["default"] = (BioCard);

var _c;

__webpack_require__.$Refresh$.register(_c, "BioCard");

const currentExports = __react_refresh_utils__.getModuleExports(module.i);
__react_refresh_utils__.registerExportsForReactRefresh(currentExports, module.i);

if (true) {
  const isHotUpdate = !!module.hot.data;
  const prevExports = isHotUpdate ? module.hot.data.prevExports : null;

  if (__react_refresh_utils__.isReactRefreshBoundary(currentExports)) {
    module.hot.dispose(
      /**
       * A callback to performs a full refresh if React has unrecoverable errors,
       * and also caches the to-be-disposed module.
       * @param {*} data A hot module data object from Webpack HMR.
       * @returns {void}
       */
      function hotDisposeCallback(data) {
        // We have to mutate the data object to get data registered and cached
        data.prevExports = currentExports;
      }
    );
    module.hot.accept(
      /**
       * An error handler to allow self-recovering behaviours.
       * @param {Error} error An error occurred during evaluation of a module.
       * @returns {void}
       */
      function hotErrorHandler(error) {
        if (
          typeof __react_refresh_error_overlay__ !== 'undefined' &&
          __react_refresh_error_overlay__
        ) {
          __react_refresh_error_overlay__.handleRuntimeError(error);
        }

        if (typeof __react_refresh_test__ !== 'undefined' && __react_refresh_test__) {
          if (window.onHotAcceptError) {
            window.onHotAcceptError(error.message);
          }
        }

        __webpack_require__.c[module.i].hot.accept(hotErrorHandler);
      }
    );

    if (isHotUpdate) {
      if (
        __react_refresh_utils__.isReactRefreshBoundary(prevExports) &&
        __react_refresh_utils__.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)
      ) {
        module.hot.invalidate();
      } else {
        __react_refresh_utils__.enqueueUpdate(
          /**
           * A function to dismiss the error overlay after performing React refresh.
           * @returns {void}
           */
          function updateCallback() {
            if (
              typeof __react_refresh_error_overlay__ !== 'undefined' &&
              __react_refresh_error_overlay__
            ) {
              __react_refresh_error_overlay__.clearRuntimeErrors();
            }
          }
        );
      }
    }
  } else {
    if (isHotUpdate && __react_refresh_utils__.isReactRefreshBoundary(prevExports)) {
      module.hot.invalidate();
    }
  }
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js"), __webpack_require__(/*! ./node_modules/react-dev-utils/refreshOverlayInterop.js */ "./node_modules/react-dev-utils/refreshOverlayInterop.js")))

/***/ }),

/***/ "./src/components/ModelCard.tsx":
/*!**************************************!*\
  !*** ./src/components/ModelCard.tsx ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(__react_refresh_utils__, __react_refresh_error_overlay__) {/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-bootstrap */ "./node_modules/react-bootstrap/esm/index.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _styles_landing_module_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../styles/landing.module.css */ "./src/styles/landing.module.css");
/* harmony import */ var _styles_landing_module_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_styles_landing_module_css__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__);
__webpack_require__.$Refresh$.runtime = __webpack_require__(/*! ./node_modules/react-refresh/runtime.js */ "./node_modules/react-refresh/runtime.js");
__webpack_require__.$Refresh$.setup(module.i);

var _jsxFileName = "/tree-hugs/client/src/components/ModelCard.tsx";






function ModelCard({
  model
}) {
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Card"], {
    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__["jsxDEV"])(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
      to: model.link,
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Card"].Img, {
        variant: "top",
        className: _styles_landing_module_css__WEBPACK_IMPORTED_MODULE_3___default.a.modelImg,
        src: model.src
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 16,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Card"].Body, {
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Card"].Title, {
          children: model.title
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 18,
          columnNumber: 11
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 17,
        columnNumber: 9
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 15,
      columnNumber: 7
    }, this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 14,
    columnNumber: 5
  }, this);
}

_c = ModelCard;
/* harmony default export */ __webpack_exports__["default"] = (ModelCard);

var _c;

__webpack_require__.$Refresh$.register(_c, "ModelCard");

const currentExports = __react_refresh_utils__.getModuleExports(module.i);
__react_refresh_utils__.registerExportsForReactRefresh(currentExports, module.i);

if (true) {
  const isHotUpdate = !!module.hot.data;
  const prevExports = isHotUpdate ? module.hot.data.prevExports : null;

  if (__react_refresh_utils__.isReactRefreshBoundary(currentExports)) {
    module.hot.dispose(
      /**
       * A callback to performs a full refresh if React has unrecoverable errors,
       * and also caches the to-be-disposed module.
       * @param {*} data A hot module data object from Webpack HMR.
       * @returns {void}
       */
      function hotDisposeCallback(data) {
        // We have to mutate the data object to get data registered and cached
        data.prevExports = currentExports;
      }
    );
    module.hot.accept(
      /**
       * An error handler to allow self-recovering behaviours.
       * @param {Error} error An error occurred during evaluation of a module.
       * @returns {void}
       */
      function hotErrorHandler(error) {
        if (
          typeof __react_refresh_error_overlay__ !== 'undefined' &&
          __react_refresh_error_overlay__
        ) {
          __react_refresh_error_overlay__.handleRuntimeError(error);
        }

        if (typeof __react_refresh_test__ !== 'undefined' && __react_refresh_test__) {
          if (window.onHotAcceptError) {
            window.onHotAcceptError(error.message);
          }
        }

        __webpack_require__.c[module.i].hot.accept(hotErrorHandler);
      }
    );

    if (isHotUpdate) {
      if (
        __react_refresh_utils__.isReactRefreshBoundary(prevExports) &&
        __react_refresh_utils__.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)
      ) {
        module.hot.invalidate();
      } else {
        __react_refresh_utils__.enqueueUpdate(
          /**
           * A function to dismiss the error overlay after performing React refresh.
           * @returns {void}
           */
          function updateCallback() {
            if (
              typeof __react_refresh_error_overlay__ !== 'undefined' &&
              __react_refresh_error_overlay__
            ) {
              __react_refresh_error_overlay__.clearRuntimeErrors();
            }
          }
        );
      }
    }
  } else {
    if (isHotUpdate && __react_refresh_utils__.isReactRefreshBoundary(prevExports)) {
      module.hot.invalidate();
    }
  }
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js"), __webpack_require__(/*! ./node_modules/react-dev-utils/refreshOverlayInterop.js */ "./node_modules/react-dev-utils/refreshOverlayInterop.js")))

/***/ }),

/***/ "./src/components/NavBar.tsx":
/*!***********************************!*\
  !*** ./src/components/NavBar.tsx ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(__react_refresh_utils__, __react_refresh_error_overlay__) {/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-bootstrap */ "./node_modules/react-bootstrap/esm/index.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _images_TreeHugLogo_svg__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../images/TreeHugLogo.svg */ "./src/images/TreeHugLogo.svg");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__);
__webpack_require__.$Refresh$.runtime = __webpack_require__(/*! ./node_modules/react-refresh/runtime.js */ "./node_modules/react-refresh/runtime.js");
__webpack_require__.$Refresh$.setup(module.i);

var _jsxFileName = "/tree-hugs/client/src/components/NavBar.tsx";






function NavBar() {
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__["jsxDEV"])("div", {
    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Navbar"], {
      expand: "lg",
      style: {
        background: "rgb(183, 196, 188)"
      },
      fixed: "top",
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Navbar"].Brand, {
        as: react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"],
        to: "/",
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__["jsxDEV"])("img", {
          src: _images_TreeHugLogo_svg__WEBPACK_IMPORTED_MODULE_3__["default"],
          alt: "logo",
          width: "30",
          height: "30"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 11,
          columnNumber: 11
        }, this), " Tree Hugs"]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 10,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Navbar"].Toggle, {
        "aria-controls": "basic-navbar-nav"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 13,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Navbar"].Collapse, {
        id: "basic-navbar-nav",
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Nav"], {
          className: "mr-auto",
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Nav"].Link, {
            as: react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"],
            to: "/parks",
            children: "Parks"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 16,
            columnNumber: 13
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Nav"].Link, {
            as: react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"],
            to: "/events",
            children: "Events"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 19,
            columnNumber: 13
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Nav"].Link, {
            as: react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"],
            to: "/thingstodo",
            children: "Things to do"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 22,
            columnNumber: 13
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Nav"].Link, {
            as: react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"],
            to: "/about",
            children: "About"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 25,
            columnNumber: 13
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 15,
          columnNumber: 11
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 14,
        columnNumber: 9
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 9,
      columnNumber: 7
    }, this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 8,
    columnNumber: 5
  }, this);
}

_c = NavBar;
/* harmony default export */ __webpack_exports__["default"] = (NavBar);

var _c;

__webpack_require__.$Refresh$.register(_c, "NavBar");

const currentExports = __react_refresh_utils__.getModuleExports(module.i);
__react_refresh_utils__.registerExportsForReactRefresh(currentExports, module.i);

if (true) {
  const isHotUpdate = !!module.hot.data;
  const prevExports = isHotUpdate ? module.hot.data.prevExports : null;

  if (__react_refresh_utils__.isReactRefreshBoundary(currentExports)) {
    module.hot.dispose(
      /**
       * A callback to performs a full refresh if React has unrecoverable errors,
       * and also caches the to-be-disposed module.
       * @param {*} data A hot module data object from Webpack HMR.
       * @returns {void}
       */
      function hotDisposeCallback(data) {
        // We have to mutate the data object to get data registered and cached
        data.prevExports = currentExports;
      }
    );
    module.hot.accept(
      /**
       * An error handler to allow self-recovering behaviours.
       * @param {Error} error An error occurred during evaluation of a module.
       * @returns {void}
       */
      function hotErrorHandler(error) {
        if (
          typeof __react_refresh_error_overlay__ !== 'undefined' &&
          __react_refresh_error_overlay__
        ) {
          __react_refresh_error_overlay__.handleRuntimeError(error);
        }

        if (typeof __react_refresh_test__ !== 'undefined' && __react_refresh_test__) {
          if (window.onHotAcceptError) {
            window.onHotAcceptError(error.message);
          }
        }

        __webpack_require__.c[module.i].hot.accept(hotErrorHandler);
      }
    );

    if (isHotUpdate) {
      if (
        __react_refresh_utils__.isReactRefreshBoundary(prevExports) &&
        __react_refresh_utils__.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)
      ) {
        module.hot.invalidate();
      } else {
        __react_refresh_utils__.enqueueUpdate(
          /**
           * A function to dismiss the error overlay after performing React refresh.
           * @returns {void}
           */
          function updateCallback() {
            if (
              typeof __react_refresh_error_overlay__ !== 'undefined' &&
              __react_refresh_error_overlay__
            ) {
              __react_refresh_error_overlay__.clearRuntimeErrors();
            }
          }
        );
      }
    }
  } else {
    if (isHotUpdate && __react_refresh_utils__.isReactRefreshBoundary(prevExports)) {
      module.hot.invalidate();
    }
  }
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js"), __webpack_require__(/*! ./node_modules/react-dev-utils/refreshOverlayInterop.js */ "./node_modules/react-dev-utils/refreshOverlayInterop.js")))

/***/ }),

/***/ "./src/components/Pagination.tsx":
/*!***************************************!*\
  !*** ./src/components/Pagination.tsx ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(__react_refresh_utils__, __react_refresh_error_overlay__) {/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__);
__webpack_require__.$Refresh$.runtime = __webpack_require__(/*! ./node_modules/react-refresh/runtime.js */ "./node_modules/react-refresh/runtime.js");
__webpack_require__.$Refresh$.setup(module.i);

var _jsxFileName = "/tree-hugs/client/src/components/Pagination.tsx";
 // import PropTypes from "prop-types";



function Pagination({
  elementsPerPage,
  totalElements,
  paginate
}) {
  const pageNumbers = [];

  for (let i = 1; i <= Math.ceil(totalElements / elementsPerPage); i++) {
    pageNumbers.push(i);
  }

  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])("nav", {
    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])("ul", {
      className: "pagination",
      children: pageNumbers.map(number => /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])("li", {
        className: "page-item",
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_1__["jsxDEV"])("a", {
          href: "#!",
          onClick: () => paginate(number),
          className: "page-link",
          children: number
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 19,
          columnNumber: 13
        }, this)
      }, number, false, {
        fileName: _jsxFileName,
        lineNumber: 18,
        columnNumber: 11
      }, this))
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 16,
      columnNumber: 7
    }, this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 15,
    columnNumber: 5
  }, this);
} // Pagination.propTypes = {
//   elementsPerPage: PropTypes.number.isRequired,
//   totalElements: PropTypes.number.isRequired,
//   paginate: PropTypes.number.isRequired,
// };


_c = Pagination;
/* harmony default export */ __webpack_exports__["default"] = (Pagination);

var _c;

__webpack_require__.$Refresh$.register(_c, "Pagination");

const currentExports = __react_refresh_utils__.getModuleExports(module.i);
__react_refresh_utils__.registerExportsForReactRefresh(currentExports, module.i);

if (true) {
  const isHotUpdate = !!module.hot.data;
  const prevExports = isHotUpdate ? module.hot.data.prevExports : null;

  if (__react_refresh_utils__.isReactRefreshBoundary(currentExports)) {
    module.hot.dispose(
      /**
       * A callback to performs a full refresh if React has unrecoverable errors,
       * and also caches the to-be-disposed module.
       * @param {*} data A hot module data object from Webpack HMR.
       * @returns {void}
       */
      function hotDisposeCallback(data) {
        // We have to mutate the data object to get data registered and cached
        data.prevExports = currentExports;
      }
    );
    module.hot.accept(
      /**
       * An error handler to allow self-recovering behaviours.
       * @param {Error} error An error occurred during evaluation of a module.
       * @returns {void}
       */
      function hotErrorHandler(error) {
        if (
          typeof __react_refresh_error_overlay__ !== 'undefined' &&
          __react_refresh_error_overlay__
        ) {
          __react_refresh_error_overlay__.handleRuntimeError(error);
        }

        if (typeof __react_refresh_test__ !== 'undefined' && __react_refresh_test__) {
          if (window.onHotAcceptError) {
            window.onHotAcceptError(error.message);
          }
        }

        __webpack_require__.c[module.i].hot.accept(hotErrorHandler);
      }
    );

    if (isHotUpdate) {
      if (
        __react_refresh_utils__.isReactRefreshBoundary(prevExports) &&
        __react_refresh_utils__.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)
      ) {
        module.hot.invalidate();
      } else {
        __react_refresh_utils__.enqueueUpdate(
          /**
           * A function to dismiss the error overlay after performing React refresh.
           * @returns {void}
           */
          function updateCallback() {
            if (
              typeof __react_refresh_error_overlay__ !== 'undefined' &&
              __react_refresh_error_overlay__
            ) {
              __react_refresh_error_overlay__.clearRuntimeErrors();
            }
          }
        );
      }
    }
  } else {
    if (isHotUpdate && __react_refresh_utils__.isReactRefreshBoundary(prevExports)) {
      module.hot.invalidate();
    }
  }
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js"), __webpack_require__(/*! ./node_modules/react-dev-utils/refreshOverlayInterop.js */ "./node_modules/react-dev-utils/refreshOverlayInterop.js")))

/***/ }),

/***/ "./src/components/TeamInfo.tsx":
/*!*************************************!*\
  !*** ./src/components/TeamInfo.tsx ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(__react_refresh_utils__, __react_refresh_error_overlay__) {/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-bootstrap */ "./node_modules/react-bootstrap/esm/index.js");
/* harmony import */ var _styles_about_module_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../styles/about.module.css */ "./src/styles/about.module.css");
/* harmony import */ var _styles_about_module_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_styles_about_module_css__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _BioCard__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./BioCard */ "./src/components/BioCard.tsx");
/* harmony import */ var _data_MemberInfo__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../data/MemberInfo */ "./src/data/MemberInfo.tsx");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__);
__webpack_require__.$Refresh$.runtime = __webpack_require__(/*! ./node_modules/react-refresh/runtime.js */ "./node_modules/react-refresh/runtime.js");
__webpack_require__.$Refresh$.setup(module.i);

var _jsxFileName = "/tree-hugs/client/src/components/TeamInfo.tsx",
    _s = __webpack_require__.$Refresh$.signature();








function TeamInfo() {
  _s();

  const [commitData, setCommits] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])([]);
  const [issueData, setIssues] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])([]);
  let totalCommitCount = 0;
  let totalIssueCount = 0; // const totalTestCount = 0;

  let eugueneCommitCount = 0;
  let eugueneIssueCount = 0;
  let reginaCommitCount = 0;
  let reginaIssueCount = 0;
  let yichenCommitCount = 0;
  let yichenIssueCount = 0;
  let stephanieCommitCount = 0;
  let stephanieIssueCount = 0;
  let sophieCommitCount = 0;
  let sophieIssueCount = 0;

  const fetchCommits = async () => {
    const res = await fetch("https://gitlab.com/api/v4/projects/cs373-group6%2Ftree-hugs/repository/commits?ref_name=develop&per_page=100");
    const data = await res.json();
    return data;
  };

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
    const getCommits = async () => {
      const commitsFromGitlab = await fetchCommits();
      setCommits(commitsFromGitlab);
    };

    getCommits();
  });

  const fetchIssues = async () => {
    const res = await fetch("https://gitlab.com/api/v4/projects/cs373-group6%2Ftree-hugs/issues?per_page=100");
    const data = await res.json();
    return data;
  };

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
    const getIssues = async () => {
      const issuesFromGitlab = await fetchIssues();
      setIssues(issuesFromGitlab);
    };

    getIssues();
  });
  totalCommitCount = commitData.length;
  totalIssueCount = issueData.length; // eslint-disable-next-line camelcase

  commitData.forEach(element => {
    if (element.author_name === "SophieZhao00" || element.author_name === "Sophie Zhao") sophieCommitCount += 1;else if (element.author_name === "eugeneccc" || element.author_name === "Eugene Chang") eugueneCommitCount += 1;else if (element.author_name === "Yichen Liu" || element.author_name === "yichen liu") yichenCommitCount += 1;else if (element.author_name === "Stephanie Tran" || element.author_name === "stephoknees") stephanieCommitCount += 1;else if (element.author_name === "Regina Chen") reginaCommitCount += 1;
  });
  issueData.forEach(element => {
    if (element.author.username === "SophieZhao00") sophieIssueCount += 1;else if (element.author.username === "eugeneccc") eugueneIssueCount += 1;else if (element.author.username === "tt0suzy") yichenIssueCount += 1;else if (element.author.username === "stephoknees") stephanieIssueCount += 1;else if (element.author.username === "reginaxchen") reginaIssueCount += 1;
  });
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("div", {
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("div", {
      className: _styles_about_module_css__WEBPACK_IMPORTED_MODULE_2___default.a.aboutPage,
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("h3", {
        children: "Git Stats"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 78,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("h5", {
        children: ["Commits: ", totalCommitCount, " Issues: ", totalIssueCount, " Unit Tests: 0"]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 79,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 82,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("h3", {
        children: "Team Members"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 83,
        columnNumber: 9
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 77,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["CardDeck"], {
      className: _styles_about_module_css__WEBPACK_IMPORTED_MODULE_2___default.a.carddeck,
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])(_BioCard__WEBPACK_IMPORTED_MODULE_3__["default"], {
        member: _data_MemberInfo__WEBPACK_IMPORTED_MODULE_4__["default"][0],
        commits: eugueneCommitCount,
        issues: eugueneIssueCount
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 86,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])(_BioCard__WEBPACK_IMPORTED_MODULE_3__["default"], {
        member: _data_MemberInfo__WEBPACK_IMPORTED_MODULE_4__["default"][1],
        commits: reginaCommitCount,
        issues: reginaIssueCount
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 87,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])(_BioCard__WEBPACK_IMPORTED_MODULE_3__["default"], {
        member: _data_MemberInfo__WEBPACK_IMPORTED_MODULE_4__["default"][2],
        commits: yichenCommitCount,
        issues: yichenIssueCount
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 88,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])(_BioCard__WEBPACK_IMPORTED_MODULE_3__["default"], {
        member: _data_MemberInfo__WEBPACK_IMPORTED_MODULE_4__["default"][3],
        commits: stephanieCommitCount,
        issues: stephanieIssueCount
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 89,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])(_BioCard__WEBPACK_IMPORTED_MODULE_3__["default"], {
        member: _data_MemberInfo__WEBPACK_IMPORTED_MODULE_4__["default"][4],
        commits: sophieCommitCount,
        issues: sophieIssueCount
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 90,
        columnNumber: 9
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 85,
      columnNumber: 7
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 76,
    columnNumber: 5
  }, this);
}

_s(TeamInfo, "8O7PWuCGzADnku3QfAD2pIGZ22s=");

_c = TeamInfo;
/* harmony default export */ __webpack_exports__["default"] = (TeamInfo);

var _c;

__webpack_require__.$Refresh$.register(_c, "TeamInfo");

const currentExports = __react_refresh_utils__.getModuleExports(module.i);
__react_refresh_utils__.registerExportsForReactRefresh(currentExports, module.i);

if (true) {
  const isHotUpdate = !!module.hot.data;
  const prevExports = isHotUpdate ? module.hot.data.prevExports : null;

  if (__react_refresh_utils__.isReactRefreshBoundary(currentExports)) {
    module.hot.dispose(
      /**
       * A callback to performs a full refresh if React has unrecoverable errors,
       * and also caches the to-be-disposed module.
       * @param {*} data A hot module data object from Webpack HMR.
       * @returns {void}
       */
      function hotDisposeCallback(data) {
        // We have to mutate the data object to get data registered and cached
        data.prevExports = currentExports;
      }
    );
    module.hot.accept(
      /**
       * An error handler to allow self-recovering behaviours.
       * @param {Error} error An error occurred during evaluation of a module.
       * @returns {void}
       */
      function hotErrorHandler(error) {
        if (
          typeof __react_refresh_error_overlay__ !== 'undefined' &&
          __react_refresh_error_overlay__
        ) {
          __react_refresh_error_overlay__.handleRuntimeError(error);
        }

        if (typeof __react_refresh_test__ !== 'undefined' && __react_refresh_test__) {
          if (window.onHotAcceptError) {
            window.onHotAcceptError(error.message);
          }
        }

        __webpack_require__.c[module.i].hot.accept(hotErrorHandler);
      }
    );

    if (isHotUpdate) {
      if (
        __react_refresh_utils__.isReactRefreshBoundary(prevExports) &&
        __react_refresh_utils__.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)
      ) {
        module.hot.invalidate();
      } else {
        __react_refresh_utils__.enqueueUpdate(
          /**
           * A function to dismiss the error overlay after performing React refresh.
           * @returns {void}
           */
          function updateCallback() {
            if (
              typeof __react_refresh_error_overlay__ !== 'undefined' &&
              __react_refresh_error_overlay__
            ) {
              __react_refresh_error_overlay__.clearRuntimeErrors();
            }
          }
        );
      }
    }
  } else {
    if (isHotUpdate && __react_refresh_utils__.isReactRefreshBoundary(prevExports)) {
      module.hot.invalidate();
    }
  }
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js"), __webpack_require__(/*! ./node_modules/react-dev-utils/refreshOverlayInterop.js */ "./node_modules/react-dev-utils/refreshOverlayInterop.js")))

/***/ }),

/***/ "./src/components/ToolList.tsx":
/*!*************************************!*\
  !*** ./src/components/ToolList.tsx ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(__react_refresh_utils__, __react_refresh_error_overlay__) {/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-bootstrap */ "./node_modules/react-bootstrap/esm/index.js");
/* harmony import */ var _styles_about_module_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../styles/about.module.css */ "./src/styles/about.module.css");
/* harmony import */ var _styles_about_module_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_styles_about_module_css__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _images_react_svg__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../images/react.svg */ "./src/images/react.svg");
/* harmony import */ var _images_docker_svg__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../images/docker.svg */ "./src/images/docker.svg");
/* harmony import */ var _images_gitlab_svg__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../images/gitlab.svg */ "./src/images/gitlab.svg");
/* harmony import */ var _images_postman_svg__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../images/postman.svg */ "./src/images/postman.svg");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__);
__webpack_require__.$Refresh$.runtime = __webpack_require__(/*! ./node_modules/react-refresh/runtime.js */ "./node_modules/react-refresh/runtime.js");
__webpack_require__.$Refresh$.setup(module.i);

var _jsxFileName = "/tree-hugs/client/src/components/ToolList.tsx";









function ToolList() {
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])("div", {
    className: _styles_about_module_css__WEBPACK_IMPORTED_MODULE_2___default.a.aboutPage,
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])("h3", {
      children: "Tools And Frameworks"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 12,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])("br", {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 13,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["CardDeck"], {
      className: _styles_about_module_css__WEBPACK_IMPORTED_MODULE_2___default.a.carddeck,
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Card"], {
        className: _styles_about_module_css__WEBPACK_IMPORTED_MODULE_2___default.a.toolcard,
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Card"].Img, {
          variant: "top",
          className: _styles_about_module_css__WEBPACK_IMPORTED_MODULE_2___default.a.logo,
          src: _images_react_svg__WEBPACK_IMPORTED_MODULE_3__["default"]
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 16,
          columnNumber: 11
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Card"].Body, {
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Card"].Title, {
            children: "React"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 18,
            columnNumber: 13
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Card"].Text, {
            children: "The fronend framework."
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 19,
            columnNumber: 13
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 17,
          columnNumber: 11
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 15,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Card"], {
        className: _styles_about_module_css__WEBPACK_IMPORTED_MODULE_2___default.a.toolcard,
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Card"].Img, {
          variant: "top",
          className: _styles_about_module_css__WEBPACK_IMPORTED_MODULE_2___default.a.logo,
          src: _images_docker_svg__WEBPACK_IMPORTED_MODULE_4__["default"]
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 23,
          columnNumber: 11
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Card"].Body, {
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Card"].Title, {
            children: "Docker"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 25,
            columnNumber: 13
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Card"].Text, {
            children: "The virtual container."
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 26,
            columnNumber: 13
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 24,
          columnNumber: 11
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 22,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Card"], {
        className: _styles_about_module_css__WEBPACK_IMPORTED_MODULE_2___default.a.toolcard,
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Card"].Img, {
          variant: "top",
          className: _styles_about_module_css__WEBPACK_IMPORTED_MODULE_2___default.a.logo,
          src: _images_gitlab_svg__WEBPACK_IMPORTED_MODULE_5__["default"]
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 30,
          columnNumber: 11
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Card"].Body, {
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Card"].Title, {
            children: "GitLab"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 32,
            columnNumber: 13
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Card"].Text, {
            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])("a", {
              href: "https://gitlab.com/cs373-group6/tree-hugs",
              children: "Our GitLab repository"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 34,
              columnNumber: 15
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 33,
            columnNumber: 13
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 31,
          columnNumber: 11
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 29,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Card"], {
        className: _styles_about_module_css__WEBPACK_IMPORTED_MODULE_2___default.a.toolcard,
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Card"].Img, {
          variant: "top",
          className: _styles_about_module_css__WEBPACK_IMPORTED_MODULE_2___default.a.logo,
          src: _images_postman_svg__WEBPACK_IMPORTED_MODULE_6__["default"]
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 39,
          columnNumber: 11
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Card"].Body, {
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Card"].Title, {
            children: "Postman"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 41,
            columnNumber: 13
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Card"].Text, {
            children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])("a", {
              href: "https://documenter.getpostman.com/view/12084061/Tz5jdKfE",
              children: "Our postman api"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 43,
              columnNumber: 15
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 42,
            columnNumber: 13
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 40,
          columnNumber: 11
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 38,
        columnNumber: 9
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 14,
      columnNumber: 7
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 11,
    columnNumber: 5
  }, this);
}

_c = ToolList;
/* harmony default export */ __webpack_exports__["default"] = (ToolList);

var _c;

__webpack_require__.$Refresh$.register(_c, "ToolList");

const currentExports = __react_refresh_utils__.getModuleExports(module.i);
__react_refresh_utils__.registerExportsForReactRefresh(currentExports, module.i);

if (true) {
  const isHotUpdate = !!module.hot.data;
  const prevExports = isHotUpdate ? module.hot.data.prevExports : null;

  if (__react_refresh_utils__.isReactRefreshBoundary(currentExports)) {
    module.hot.dispose(
      /**
       * A callback to performs a full refresh if React has unrecoverable errors,
       * and also caches the to-be-disposed module.
       * @param {*} data A hot module data object from Webpack HMR.
       * @returns {void}
       */
      function hotDisposeCallback(data) {
        // We have to mutate the data object to get data registered and cached
        data.prevExports = currentExports;
      }
    );
    module.hot.accept(
      /**
       * An error handler to allow self-recovering behaviours.
       * @param {Error} error An error occurred during evaluation of a module.
       * @returns {void}
       */
      function hotErrorHandler(error) {
        if (
          typeof __react_refresh_error_overlay__ !== 'undefined' &&
          __react_refresh_error_overlay__
        ) {
          __react_refresh_error_overlay__.handleRuntimeError(error);
        }

        if (typeof __react_refresh_test__ !== 'undefined' && __react_refresh_test__) {
          if (window.onHotAcceptError) {
            window.onHotAcceptError(error.message);
          }
        }

        __webpack_require__.c[module.i].hot.accept(hotErrorHandler);
      }
    );

    if (isHotUpdate) {
      if (
        __react_refresh_utils__.isReactRefreshBoundary(prevExports) &&
        __react_refresh_utils__.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)
      ) {
        module.hot.invalidate();
      } else {
        __react_refresh_utils__.enqueueUpdate(
          /**
           * A function to dismiss the error overlay after performing React refresh.
           * @returns {void}
           */
          function updateCallback() {
            if (
              typeof __react_refresh_error_overlay__ !== 'undefined' &&
              __react_refresh_error_overlay__
            ) {
              __react_refresh_error_overlay__.clearRuntimeErrors();
            }
          }
        );
      }
    }
  } else {
    if (isHotUpdate && __react_refresh_utils__.isReactRefreshBoundary(prevExports)) {
      module.hot.invalidate();
    }
  }
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js"), __webpack_require__(/*! ./node_modules/react-dev-utils/refreshOverlayInterop.js */ "./node_modules/react-dev-utils/refreshOverlayInterop.js")))

/***/ }),

/***/ "./src/data/MemberInfo.tsx":
/*!*********************************!*\
  !*** ./src/data/MemberInfo.tsx ***!
  \*********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(__react_refresh_utils__, __react_refresh_error_overlay__) {/* harmony import */ var _images_yichen_jpg__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../images/yichen.jpg */ "./src/images/yichen.jpg");
/* harmony import */ var _images_sophie_jpg__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../images/sophie.jpg */ "./src/images/sophie.jpg");
/* harmony import */ var _images_regina_jpeg__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../images/regina.jpeg */ "./src/images/regina.jpeg");
/* harmony import */ var _images_stephanie_jpeg__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../images/stephanie.jpeg */ "./src/images/stephanie.jpeg");
/* harmony import */ var _images_eugene_jpeg__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../images/eugene.jpeg */ "./src/images/eugene.jpeg");
__webpack_require__.$Refresh$.runtime = __webpack_require__(/*! ./node_modules/react-refresh/runtime.js */ "./node_modules/react-refresh/runtime.js");
__webpack_require__.$Refresh$.setup(module.i);






const members = [{
  name: "Eugene Chang",
  id: "eugeneccc",
  role: "Frontend Developer",
  leadership: "",
  commits: 0,
  issues: 0,
  unitTests: 0,
  image: _images_eugene_jpeg__WEBPACK_IMPORTED_MODULE_4__["default"],
  bio: "I am a fourth year CS student at UT Austin. I have interest in data science. I do guided meditation every day.",
  linkedin: "https://www.linkedin.com/in/eugene-chang-127a30175/"
}, {
  name: "Regina Chen",
  id: "reginaxchen",
  role: "Frontend Developer",
  leadership: "",
  commits: 0,
  issues: 0,
  unitTests: 0,
  image: _images_regina_jpeg__WEBPACK_IMPORTED_MODULE_2__["default"],
  bio: "Regina is a third year CS major at UT Austin from Houston, Texas. She is interested in full stack development. Her hobbies include baking and powerlifting.",
  linkedin: "https://www.linkedin.com/in/reginaxchen/"
}, {
  name: "Yichen Liu",
  id: "tt0suzy",
  role: "Frontend Developer",
  leadership: "",
  commits: 0,
  issues: 0,
  unitTests: 0,
  image: _images_yichen_jpg__WEBPACK_IMPORTED_MODULE_0__["default"],
  bio: "I'm a fourth year CS student at University of Texas at Austin. I'm very into web apps and games development, and I love playing dota2 at my free time.",
  linkedin: "https://www.linkedin.com/in/yichen-liu-44253a157/"
}, {
  name: "Stephanie Tran",
  id: "stepokness",
  role: "Backend Developer",
  leadership: "",
  commits: 0,
  issues: 0,
  unitTests: 0,
  image: _images_stephanie_jpeg__WEBPACK_IMPORTED_MODULE_3__["default"],
  bio: "I'm a third year studying CS at UT Austin, and I'm interested in back end development. Some hobbies I have are reading and drawing.",
  linkedin: "https://www.linkedin.com/in/stephctran/"
}, {
  name: "Sophie Zhao",
  id: "SophieZhao00",
  role: "Backend Developer",
  leadership: "|   Phase 1 Leader",
  commits: 0,
  issues: 0,
  unitTests: 0,
  image: _images_sophie_jpg__WEBPACK_IMPORTED_MODULE_1__["default"],
  bio: "I’m a third year CS major and business minor at UT Austin. I'm interested in full-stack development. I like watching TikTok in my free time.",
  linkedin: "https://www.linkedin.com/in/xuefei-zhao/ "
}];
/* harmony default export */ __webpack_exports__["default"] = (members);

const currentExports = __react_refresh_utils__.getModuleExports(module.i);
__react_refresh_utils__.registerExportsForReactRefresh(currentExports, module.i);

if (true) {
  const isHotUpdate = !!module.hot.data;
  const prevExports = isHotUpdate ? module.hot.data.prevExports : null;

  if (__react_refresh_utils__.isReactRefreshBoundary(currentExports)) {
    module.hot.dispose(
      /**
       * A callback to performs a full refresh if React has unrecoverable errors,
       * and also caches the to-be-disposed module.
       * @param {*} data A hot module data object from Webpack HMR.
       * @returns {void}
       */
      function hotDisposeCallback(data) {
        // We have to mutate the data object to get data registered and cached
        data.prevExports = currentExports;
      }
    );
    module.hot.accept(
      /**
       * An error handler to allow self-recovering behaviours.
       * @param {Error} error An error occurred during evaluation of a module.
       * @returns {void}
       */
      function hotErrorHandler(error) {
        if (
          typeof __react_refresh_error_overlay__ !== 'undefined' &&
          __react_refresh_error_overlay__
        ) {
          __react_refresh_error_overlay__.handleRuntimeError(error);
        }

        if (typeof __react_refresh_test__ !== 'undefined' && __react_refresh_test__) {
          if (window.onHotAcceptError) {
            window.onHotAcceptError(error.message);
          }
        }

        __webpack_require__.c[module.i].hot.accept(hotErrorHandler);
      }
    );

    if (isHotUpdate) {
      if (
        __react_refresh_utils__.isReactRefreshBoundary(prevExports) &&
        __react_refresh_utils__.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)
      ) {
        module.hot.invalidate();
      } else {
        __react_refresh_utils__.enqueueUpdate(
          /**
           * A function to dismiss the error overlay after performing React refresh.
           * @returns {void}
           */
          function updateCallback() {
            if (
              typeof __react_refresh_error_overlay__ !== 'undefined' &&
              __react_refresh_error_overlay__
            ) {
              __react_refresh_error_overlay__.clearRuntimeErrors();
            }
          }
        );
      }
    }
  } else {
    if (isHotUpdate && __react_refresh_utils__.isReactRefreshBoundary(prevExports)) {
      module.hot.invalidate();
    }
  }
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js"), __webpack_require__(/*! ./node_modules/react-dev-utils/refreshOverlayInterop.js */ "./node_modules/react-dev-utils/refreshOverlayInterop.js")))

/***/ }),

/***/ "./src/data/api.tsx":
/*!**************************!*\
  !*** ./src/data/api.tsx ***!
  \**************************/
/*! exports provided: API_ENDPOINT, PARKS, EVENTS, TTD, GOOGLE_API_KEY, YOUTUBE_API, getParks, getParkById, getEvents, getEventById, getThingsToDo, getThingsToDoById, getVideoFeedByQuery */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(__react_refresh_utils__, __react_refresh_error_overlay__) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "API_ENDPOINT", function() { return API_ENDPOINT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PARKS", function() { return PARKS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EVENTS", function() { return EVENTS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TTD", function() { return TTD; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GOOGLE_API_KEY", function() { return GOOGLE_API_KEY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "YOUTUBE_API", function() { return YOUTUBE_API; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getParks", function() { return getParks; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getParkById", function() { return getParkById; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getEvents", function() { return getEvents; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getEventById", function() { return getEventById; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getThingsToDo", function() { return getThingsToDo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getThingsToDoById", function() { return getThingsToDoById; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getVideoFeedByQuery", function() { return getVideoFeedByQuery; });
__webpack_require__.$Refresh$.runtime = __webpack_require__(/*! ./node_modules/react-refresh/runtime.js */ "./node_modules/react-refresh/runtime.js");
__webpack_require__.$Refresh$.setup(module.i);

const API_ENDPOINT = "https://treehugs.me/api/";
const PARKS = "parks";
const EVENTS = "events";
const TTD = "thingstodo";
const GOOGLE_API_KEY = "AIzaSyDZfonsQI2mQYnOfYJEYQEIyO1QOR9AODY";
const YOUTUBE_API = "https://www.googleapis.com/youtube/v3/search?part=snippet&q=";
/* eslint-disable @typescript-eslint/no-explicit-any */

async function getParks(page) {
  const res = await fetch(`${API_ENDPOINT}${PARKS}?page=${page}`);
  const data = await res.json();
  return data;
}
async function getParkById(id) {
  const res = await fetch(`${API_ENDPOINT}${PARKS}/${id}`);
  const data = await res.json();
  return data;
}
async function getEvents(page) {
  const res = await fetch(`${API_ENDPOINT}${EVENTS}?page=${page}`);
  const data = await res.json();
  return data;
}
async function getEventById(id) {
  const res = await fetch(`${API_ENDPOINT}${EVENTS}/${id}`);
  const data = await res.json();
  return data;
}
async function getThingsToDo(page) {
  const res = await fetch(`${API_ENDPOINT}${TTD}?page=${page}`);
  const data = await res.json();
  return data;
}
async function getThingsToDoById(id) {
  const res = await fetch(`${API_ENDPOINT}${TTD}/${id}`);
  const data = await res.json();
  return data;
}
async function getVideoFeedByQuery(query) {
  const res = await fetch(`${YOUTUBE_API}${query}&key=${GOOGLE_API_KEY}`);
  const data = await res.json();
  return data;
}

const currentExports = __react_refresh_utils__.getModuleExports(module.i);
__react_refresh_utils__.registerExportsForReactRefresh(currentExports, module.i);

if (true) {
  const isHotUpdate = !!module.hot.data;
  const prevExports = isHotUpdate ? module.hot.data.prevExports : null;

  if (__react_refresh_utils__.isReactRefreshBoundary(currentExports)) {
    module.hot.dispose(
      /**
       * A callback to performs a full refresh if React has unrecoverable errors,
       * and also caches the to-be-disposed module.
       * @param {*} data A hot module data object from Webpack HMR.
       * @returns {void}
       */
      function hotDisposeCallback(data) {
        // We have to mutate the data object to get data registered and cached
        data.prevExports = currentExports;
      }
    );
    module.hot.accept(
      /**
       * An error handler to allow self-recovering behaviours.
       * @param {Error} error An error occurred during evaluation of a module.
       * @returns {void}
       */
      function hotErrorHandler(error) {
        if (
          typeof __react_refresh_error_overlay__ !== 'undefined' &&
          __react_refresh_error_overlay__
        ) {
          __react_refresh_error_overlay__.handleRuntimeError(error);
        }

        if (typeof __react_refresh_test__ !== 'undefined' && __react_refresh_test__) {
          if (window.onHotAcceptError) {
            window.onHotAcceptError(error.message);
          }
        }

        __webpack_require__.c[module.i].hot.accept(hotErrorHandler);
      }
    );

    if (isHotUpdate) {
      if (
        __react_refresh_utils__.isReactRefreshBoundary(prevExports) &&
        __react_refresh_utils__.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)
      ) {
        module.hot.invalidate();
      } else {
        __react_refresh_utils__.enqueueUpdate(
          /**
           * A function to dismiss the error overlay after performing React refresh.
           * @returns {void}
           */
          function updateCallback() {
            if (
              typeof __react_refresh_error_overlay__ !== 'undefined' &&
              __react_refresh_error_overlay__
            ) {
              __react_refresh_error_overlay__.clearRuntimeErrors();
            }
          }
        );
      }
    }
  } else {
    if (isHotUpdate && __react_refresh_utils__.isReactRefreshBoundary(prevExports)) {
      module.hot.invalidate();
    }
  }
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js"), __webpack_require__(/*! ./node_modules/react-dev-utils/refreshOverlayInterop.js */ "./node_modules/react-dev-utils/refreshOverlayInterop.js")))

/***/ }),

/***/ "./src/data/events.json":
/*!******************************!*\
  !*** ./src/data/events.json ***!
  \******************************/
/*! exports provided: data, dates, errors, pagenumber, pagesize, total, default */
/***/ (function(module) {

module.exports = JSON.parse("{\"data\":[{\"category\":\"Special Event\",\"categoryid\":\"0\",\"contactemailaddress\":\"Stacy_Speas@nps.gov\",\"contactname\":\"Stacy Speas\",\"contacttelephonenumber\":\"2562347111\",\"createuser\":\"\",\"date\":\"2021-01-01\",\"dateend\":\"2021-03-21\",\"dates\":[\"2021-01-01\"],\"datestart\":\"2021-01-01\",\"datetimecreated\":\"\",\"datetimeupdated\":\"\",\"description\":\"<p>Abraham Lincoln Birthplace National Historical Park will host the 207<sup>th</sup> Anniversary of the Battle of the Horseshoe on Saturday, March 20, 2021.    This annual event recreates traditional Creek Indian life, frontier life in the year 1814 and emphasizes the importance of the battle in United States history through a variety of special demonstrations and interpretive programs.  Saturday’s activities will provide park visitors with a better understanding Southeastern American Indian life in this area more than 200 years ago, as well as provide insight into the lives of the combatants and the reasons why the battle occurred.</p>\\n<p>Experience the life of the Creek and Cherokee Indians by visiting traditional hunting camps and watch demonstrations of cultural skills such as hide tanning, cooking and finger weaving.  Children will have the opportunity to participate in an authentic Creek stickball game throughout the day.</p>\\n<p> </p>\\n<p>Experience the life of Andrew Jackson’s frontier army.  Watch Tennessee militia fire smoothbore cannon and flintlock muskets.  Learn how soldiers cooked their meals and lived while on campaign through a variety of encampments and displays in the museum.  A camp depicting American women’s lives on the frontier will provide wool dyeing demonstrations and teach visitors how to spin the wool.</p>\\n<p> </p>\\n<p>All demonstrations will be presented multiple times throughout the day and promise to be entertaining as well as educational. A refreshment stand will be available courtesy of the New Site Volunteer Fire Department.</p>\\n<p> </p>\\n<p>This Program is FREE to the public.</p>\",\"eventid\":\"0\",\"feeinfo\":\"\",\"id\":\"5A42BAEF-C3AC-01DC-FC336CC82E61E8A3\",\"imageidlist\":\"\",\"images\":[{\"altText\":\"Luminaries on the battlefield\",\"caption\":\"Luminaries representing casualties during the Battle of Horseshoe Bend\",\"credit\":\"NPS photo\",\"imageId\":\"25987\",\"ordinal\":\"0\",\"path\":\"/common/uploads/event_calendar/5884976D-B7EC-5D10-ADFB75EE3BDEEF6D.jpg\",\"title\":\"\",\"url\":\"/common/uploads/event_calendar/5884976D-B7EC-5D10-ADFB75EE3BDEEF6D.jpg\"},{\"altText\":\"Volunteers demonstrating making thread on a spinning wheel\",\"caption\":\"Park Volunteers demonstrating wool dyeing and spinning\",\"credit\":\"NPS photo\",\"imageId\":\"25989\",\"ordinal\":\"0\",\"path\":\"/common/uploads/event_calendar/5960024E-F2C5-2287-54E51D237450B27C.jpg\",\"title\":\"\",\"url\":\"/common/uploads/event_calendar/5960024E-F2C5-2287-54E51D237450B27C.jpg\"},{\"altText\":\"Costumed volunteers demonstrating flintlock muskets\",\"caption\":\"Horseshoe Bend Black Powder Volunteers demonstrating replica 1795 muskets.\",\"credit\":\"NPS photo\",\"imageId\":\"25990\",\"ordinal\":\"0\",\"path\":\"/common/uploads/event_calendar/5981C473-C1AA-3F8E-55C8662EB88B771F.jpg\",\"title\":\"\",\"url\":\"/common/uploads/event_calendar/5981C473-C1AA-3F8E-55C8662EB88B771F.jpg\"}],\"infourl\":\"https://nps.gov/hobe\",\"isallday\":\"false\",\"isfree\":\"true\",\"isrecurring\":\"false\",\"isregresrequired\":\"false\",\"latitude\":\"32.978322\",\"location\":\"\",\"longitude\":\"-85.731008\",\"organizationname\":\"\",\"parkfullname\":\"Abraham Lincoln Birthplace National Historical Park\",\"portalname\":\"\",\"recurrencedateend\":\"2021-03-21\",\"recurrencedatestart\":\"2021-01-01\",\"recurrencerule\":\"\",\"regresinfo\":\"\",\"regresurl\":\"\",\"sitecode\":\"hobe\",\"sitetype\":\"park\",\"subjectname\":\"\",\"tags\":[\"#HorseshoeBendNMP #blackpowder #NPS #BattleAnniversary #CreekWar\",\"#LivingHistory #VIPs\"],\"timeinfo\":\"\",\"times\":[{\"sunrisestart\":\"false\",\"sunsetend\":\"false\",\"timeend\":\"04:00 PM\",\"timestart\":\"09:00 AM\"}],\"title\":\"Anniversary of the Battle of Horseshoe Bend\",\"types\":[\"Children’s Program\",\"Cultural/Craft Demonstration\",\"Living History\"],\"updateuser\":\"\"},{\"category\":\"Regular Event\",\"categoryid\":\"0\",\"contactemailaddress\":\"\",\"contactname\":\"\",\"contacttelephonenumber\":\"\",\"createuser\":\"\",\"date\":\"2021-03-01\",\"dateend\":\"2021-03-05\",\"dates\":[\"2021-03-01\"],\"datestart\":\"2021-03-01\",\"datetimecreated\":\"\",\"datetimeupdated\":\"\",\"description\":\"<p>As part of the commemoration activities for the 200th anniversary, the Quivira Chapter of the Santa Fe Trail Association encourages you to participate in Read Across America, March 1 – 5, 2021 and “Read Across the Santa Fe Trail” to help us commemorate this historic 200th anniversary! As part of the Santa Fe Trail 200th anniversary, the Quivira Chapter, funded in part by the McPherson County Community Foundation, Rice County Tourism and Great Bend CVB, has donated books on the Santa Fe Trail to public libraries in our three-county area.  Visit the public libraries in McPherson, Rice and Barton counties in Kansas or your own town and read about the Santa Fe Trail!</p>\\n<p>This event is hosted by the Quivira Chapter of the Santa Fe Trail Association.  Contact them at <a href=\\\"mailto:quivira.sfta@gmail.com\\\">quivira.sfta@gmail.com</a> with questions.  </p>\",\"eventid\":\"0\",\"feeinfo\":\"\",\"id\":\"B89A95A3-DF0C-C680-D7439327233F8FBB\",\"imageidlist\":\"\",\"images\":[],\"infourl\":\"http:/www.2021santafetrailkansas.com/\",\"isallday\":\"false\",\"isfree\":\"true\",\"isrecurring\":\"false\",\"isregresrequired\":\"false\",\"latitude\":\"\",\"location\":\"Visit local public libraries in McPherson, Rice and Barton counties in Kansas to find books about the Santa Fe Trail\",\"longitude\":\"\",\"organizationname\":\"\",\"parkfullname\":\"Adams National Historical Park\",\"portalname\":\"\",\"recurrencedateend\":\"\",\"recurrencedatestart\":\"2021-03-01\",\"recurrencerule\":\"\",\"regresinfo\":\"\",\"regresurl\":\"\",\"sitecode\":\"safe\",\"sitetype\":\"park\",\"subjectname\":\"\",\"tags\":[\"Santa Fe Trail\",\" Santa Fe Trail Bicentennial\",\" Santa Fe National Historic Trail\",\" Santa Fe Trail Association\"],\"timeinfo\":\"\",\"times\":[{\"sunrisestart\":\"true\",\"sunsetend\":\"true\",\"timeend\":\"\",\"timestart\":\"\"}],\"title\":\"Read Across the Santa Fe Trail\",\"types\":[\"Partner Program\"],\"updateuser\":\"\"},{\"category\":\"Special Event\",\"categoryid\":\"0\",\"contactemailaddress\":\"\",\"contactname\":\"\",\"contacttelephonenumber\":\"\",\"createuser\":\"\",\"date\":\"2021-03-01\",\"dateend\":\"2021-03-31\",\"dates\":[\"2021-03-01\"],\"datestart\":\"2021-03-01\",\"datetimecreated\":\"\",\"datetimeupdated\":\"\",\"description\":\"<p style=\\\"text-align: center;\\\"><strong>Women’s History Month </strong></p>\\n<p style=\\\"text-align: center;\\\"><strong> </strong></p>\\n<p style=\\\"text-align: center;\\\"><strong>A Virtual Exhibit </strong></p>\\n<p style=\\\"text-align: center;\\\"><strong>  March 1, 2021—March 31, 2021</strong></p>\\n<p style=\\\"text-align: center;\\\"> </p>\\n<p style=\\\"text-align: center;\\\">“From the Suffrage to the new Millennium.” </p>\\n<p>In honor of the centennial of the passage of the 19th amendment in Congress and Women’s History Month, Federal Hall National Memorial will be presenting a virtual exhibit entitled “From the Suffrage to the new Millennium”. </p>\\n<p>On June 4th, 1919, Congress passed the 19th amendment which guaranteed women the right to vote. In the 102 years since then, women from all walks of life have made great strides in various fields, and we set out to tell some of their stories. </p>\\n<p>The virtual exhibit will feature 44 women from 10 decades, as well as a brief timeline of the Women Suffrage Movement.</p>\\n<p> </p>\\n<p> </p>\",\"eventid\":\"0\",\"feeinfo\":\"\",\"id\":\"5063470D-E2CB-2A8D-6CF59823CB83B06E\",\"imageidlist\":\"\",\"images\":[{\"altText\":\"Women pictured in exhbit\",\"caption\":\"Women pictured in exhbit\",\"credit\":\"NPS\",\"imageId\":\"29107\",\"ordinal\":\"0\",\"path\":\"/common/uploads/event_calendar/5056E64C-9850-BD81-788AA6F57153BF16.jpg\",\"title\":\"\",\"url\":\"/common/uploads/event_calendar/5056E64C-9850-BD81-788AA6F57153BF16.jpg\"}],\"infourl\":\"https://www.facebook.com/federalhallnps/\",\"isallday\":\"false\",\"isfree\":\"true\",\"isrecurring\":\"false\",\"isregresrequired\":\"false\",\"latitude\":\"\",\"location\":\"\",\"longitude\":\"\",\"organizationname\":\"\",\"parkfullname\":\"Acadia National Park\",\"portalname\":\"\",\"recurrencedateend\":\"\",\"recurrencedatestart\":\"2021-03-01\",\"recurrencerule\":\"\",\"regresinfo\":\"\",\"regresurl\":\"\",\"sitecode\":\"feha\",\"sitetype\":\"park\",\"subjectname\":\"\",\"tags\":[],\"timeinfo\":\"\",\"times\":[{\"sunrisestart\":\"true\",\"sunsetend\":\"true\",\"timeend\":\"\",\"timestart\":\"\"}],\"title\":\"Women’s History Month  A Virtual Exhibit\",\"types\":[\"Virtual/Digital\"],\"updateuser\":\"\"}],\"dates\":\"2021-03-28,2021-03-02,2021-03-18,2021-03-26,2021-03-16,2021-03-08,2021-03-25,2021-04-01,2021-03-10,2021-03-20,2021-03-13,2021-03-29,2021-03-05,2021-03-31,2021-03-15,2021-03-27,2021-03-06,2021-03-14,2021-03-24,2021-03-12,2021-03-03,2021-03-01,2021-03-17,2021-03-21,2021-03-09,2021-03-22,2021-03-30,2021-03-04,2021-03-19,2021-03-07,2021-03-23,2021-03-11,2021-01-01\",\"errors\":[],\"pagenumber\":\"1\",\"pagesize\":\"3\",\"total\":\"214\"}");

/***/ }),

/***/ "./src/data/parks.json":
/*!*****************************!*\
  !*** ./src/data/parks.json ***!
  \*****************************/
/*! exports provided: data, limit, start, total, default */
/***/ (function(module) {

module.exports = JSON.parse("{\"data\":[{\"activities\":[{\"id\":\"13A57703-BB1A-41A2-94B8-53B692EB7238\",\"name\":\"Astronomy\"},{\"id\":\"D37A0003-8317-4F04-8FB0-4CF0A272E195\",\"name\":\"Stargazing\"},{\"id\":\"1DFACD97-1B9C-4F5A-80F2-05593604799E\",\"name\":\"Food\"},{\"id\":\"C6D3230A-2CEA-4AFE-BFF3-DC1E2C2C4BB4\",\"name\":\"Picnicking\"},{\"id\":\"DF4A35E0-7983-4A3E-BC47-F37B872B0F25\",\"name\":\"Junior Ranger Program\"},{\"id\":\"0B685688-3405-4E2A-ABBA-E3069492EC50\",\"name\":\"Wildlife Watching\"},{\"id\":\"5A2C91D1-50EC-4B24-8BED-A2E11A1892DF\",\"name\":\"Birdwatching\"},{\"id\":\"24380E3F-AD9D-4E38-BF13-C8EEB21893E7\",\"name\":\"Shopping\"},{\"id\":\"467DC8B8-0B7D-436D-A026-80A22358F615\",\"name\":\"Bookstore and Park Store\"},{\"id\":\"43800AD1-D439-40F3-AAB3-9FB651FE45BB\",\"name\":\"Gift Shop and Souvenirs\"}],\"addresses\":[{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Physical\"},{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Mailing\"}],\"contacts\":{\"emailAddresses\":[{\"description\":\"\",\"emailAddress\":\"ABLI_Administration@nps.gov\"}],\"phoneNumbers\":[{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583137\",\"type\":\"Voice\"},{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583874\",\"type\":\"Fax\"}]},\"description\":\"For over a century people from around the world have come to rural Central Kentucky to honor the humble beginnings of our 16th president, Abraham Lincoln. His early life on Kentucky's frontier shaped his character and prepared him to lead the nation through Civil War. The country's first memorial to Lincoln, built with donations from young and old, enshrines the symbolic birthplace cabin.\",\"designation\":\"National Historical Park\",\"directionsInfo\":\"The Birthplace Unit of the park is located approximately 2 miles south of the town of Hodgenville on U.S. Highway 31E South. The Boyhood Home Unit at Knob Creek is located approximately 10 miles northeast of the Birthplace Unit of the park.\",\"directionsUrl\":\"http://www.nps.gov/abli/planyourvisit/directions.htm\",\"entranceFees\":[{\"cost\":\"0.00\",\"description\":\"There is no fee associated with visiting either unit of the park.\",\"title\":\"Fee Free Park\"}],\"entrancePasses\":[],\"fees\":[],\"fullName\":\"Abraham Lincoln Birthplace National Historical Park\",\"id\":\"77E0D7F0-1942-494A-ACE2-9004D2BDC59E\",\"images\":[{\"altText\":\"The Memorial Building surrounded by fall colors\",\"caption\":\"Over 200,000 people a year come to walk up the steps of the Memorial Building to visit the site where Abraham Lincoln was born\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building with fall colors\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861078-1DD8-B71B-0B774A242EF6A706.jpg\"},{\"altText\":\"The first memorial erected to honor Abraham Lincoln\",\"caption\":\"The Memorial Building constructed on the traditional site of the birth of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861263-1DD8-B71B-0B71EF9B95F9644F.jpg\"},{\"altText\":\"The symbolic birth cabin on the traditional site of the birth of Abraham Lincoln.\",\"caption\":\"The symbolic birth cabin of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Symbolic Birth Cabin of Abraham Lincoln\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C86137D-1DD8-B71B-0B978BACD7EBAEF1.jpg\"},{\"altText\":\"Statue of the Lincoln family in the park's Visitor Center\",\"caption\":\"Visitors to the park can view the statue of the Lincoln family.\",\"credit\":\"NPS Photo\",\"title\":\"Statue of the Lincoln Family in the Visitor Center\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C8614D1-1DD8-B71B-0B1AF72CA452B051.jpg\"}],\"latLong\":\"lat:37.5858662, long:-85.67330523\",\"latitude\":\"37.5858662\",\"longitude\":\"-85.67330523\",\"name\":\"Abraham Lincoln Birthplace\",\"operatingHours\":[{\"description\":\"The Memorial Building is open 9:00 am - 4:30 pm eastern time with a limited viewing area.  The Visitor Center and grounds of the Birthplace Unit are open 9:00 am - 5:00 pm eastern time with one-way traffic flow and social distancing.\\n\\n\\nThe Boyhood Home Unit at Knob Creek will be closed August 3, 2020, to July 31, 2021, due to the Lincoln Tavern Rehabilitation Project.\",\"exceptions\":[{\"endDate\":\"2022-01-01\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2022-01-01\"},{\"endDate\":\"2021-11-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-11-25\"},{\"endDate\":\"2021-12-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-12-25\"}],\"name\":\"Abraham Lincoln Birthplace National Historical Park\",\"standardHours\":{\"friday\":\"9:00AM - 5:00PM\",\"monday\":\"9:00AM - 5:00PM\",\"saturday\":\"9:00AM - 5:00PM\",\"sunday\":\"9:00AM - 5:00PM\",\"thursday\":\"9:00AM - 5:00PM\",\"tuesday\":\"9:00AM - 5:00PM\",\"wednesday\":\"9:00AM - 5:00PM\"}}],\"parkCode\":\"abli\",\"states\":\"KY\",\"topics\":[{\"id\":\"D10852A3-443C-4743-A5FA-6DD6D2A054B3\",\"name\":\"Birthplace\"},{\"id\":\"F669BC40-BDC4-41C0-9ACE-B2CD25373045\",\"name\":\"Presidents\"},{\"id\":\"0D00073E-18C3-46E5-8727-2F87B112DDC6\",\"name\":\"Animals\"},{\"id\":\"957EF2BD-AC6C-4B7B-BD9A-87593ADC6691\",\"name\":\"Birds\"},{\"id\":\"E25F3456-43ED-45DD-93BC-057F9B944F7A\",\"name\":\"Caves, Caverns and Karst\"},{\"id\":\"F0F97E32-2F29-41B4-AF98-9FBE8DAB36B1\",\"name\":\"Geology\"},{\"id\":\"4BE01DC5-52E6-4F18-8C9A-B22D65965F6D\",\"name\":\"Groundwater\"},{\"id\":\"0E1A04CC-EB51-4F18-93D4-EC0B0B4EC1E3\",\"name\":\"Freshwater Springs\"},{\"id\":\"A7359FC4-DAD8-45F5-AF15-7FF62F816ED3\",\"name\":\"Night Sky\"}],\"url\":\"https://www.nps.gov/abli/index.htm\",\"weatherInfo\":\"There are four distinct seasons in Central Kentucky. However, temperature and weather conditions can vary widely within those seasons. Spring and Fall are generally pleasant with frequent rain showers. Summer is usually hot and humid. Winter is moderately cold with mixed precipitation.\"},{\"activities\":[{\"id\":\"09DF0950-D319-4557-A57E-04CD2F63FF42\",\"name\":\"Arts and Culture\"},{\"id\":\"FAED7F97-3474-4C21-AB42-FB0768A2F826\",\"name\":\"Cultural Demonstrations\"},{\"id\":\"13A57703-BB1A-41A2-94B8-53B692EB7238\",\"name\":\"Astronomy\"},{\"id\":\"D37A0003-8317-4F04-8FB0-4CF0A272E195\",\"name\":\"Stargazing\"},{\"id\":\"7CE6E935-F839-4FEC-A63E-052B1DEF39D2\",\"name\":\"Biking\"},{\"id\":\"071BA73C-1D3C-46D4-A53C-00D5602F7F0E\",\"name\":\"Boating\"},{\"id\":\"A59947B7-3376-49B4-AD02-C0423E08C5F7\",\"name\":\"Camping\"},{\"id\":\"7CFF5F03-5ECC-4F5A-8572-75D1F0976C0C\",\"name\":\"Group Camping\"},{\"id\":\"B12FAAB9-713F-4B38-83E4-A273F5A43C77\",\"name\":\"Climbing\"},{\"id\":\"87D3B1CD-3903-4561-ABB1-2DD870C43F2F\",\"name\":\"Rock Climbing\"},{\"id\":\"C11D3746-5063-4BD0-B245-7178D1AD866C\",\"name\":\"Compass and GPS\"},{\"id\":\"CA3641A0-FADC-497F-B036-3FE426D0DDCC\",\"name\":\"Geocaching\"},{\"id\":\"AE42B46C-E4B7-4889-A122-08FE180371AE\",\"name\":\"Fishing\"},{\"id\":\"25FB188F-5AAD-459A-9092-28A9801709FF\",\"name\":\"Freshwater Fishing\"},{\"id\":\"9BC03FAF-28F1-4609-BF6F-643F58071AED\",\"name\":\"Fly Fishing\"},{\"id\":\"17411C8D-5769-4D0F-ABD1-52ED03840C18\",\"name\":\"Saltwater Fishing\"},{\"id\":\"1DFACD97-1B9C-4F5A-80F2-05593604799E\",\"name\":\"Food\"},{\"id\":\"C6D3230A-2CEA-4AFE-BFF3-DC1E2C2C4BB4\",\"name\":\"Picnicking\"},{\"id\":\"B33DC9B6-0B7D-4322-BAD7-A13A34C584A3\",\"name\":\"Guided Tours\"},{\"id\":\"3DE599E2-22ED-40BF-B383-EDA073563C1E\",\"name\":\"Bus/Shuttle Guided Tour\"},{\"id\":\"5A241412-0CFB-497A-9B5F-0AB8C03CDE72\",\"name\":\"Boat Tour\"},{\"id\":\"42FD78B9-2B90-4AA9-BC43-F10E9FEA8B5A\",\"name\":\"Hands-On\"},{\"id\":\"31F88DA6-696F-441F-89CF-D7B1415C4CB9\",\"name\":\"Citizen Science\"},{\"id\":\"BFF8C027-7C8F-480B-A5F8-CD8CE490BFBA\",\"name\":\"Hiking\"},{\"id\":\"45261C0A-00D8-4C27-A1F8-029F933A0D34\",\"name\":\"Front-Country Hiking\"},{\"id\":\"0307955A-B65C-4CE4-A780-EB36BAAADF0B\",\"name\":\"Horse Trekking\"},{\"id\":\"1886DA47-0AEC-4568-B9C2-8E9BC316AAAC\",\"name\":\"Horseback Riding\"},{\"id\":\"5FF5B286-E9C3-430E-B612-3380D8138600\",\"name\":\"Ice Skating\"},{\"id\":\"4D224BCA-C127-408B-AC75-A51563C42411\",\"name\":\"Paddling\"},{\"id\":\"21DB3AFC-8AAC-4665-BC1F-7198C0685983\",\"name\":\"Canoeing\"},{\"id\":\"F353A9ED-4A08-456E-8DEC-E61974D0FEB6\",\"name\":\"Kayaking\"},{\"id\":\"B3EF12AF-D951-419E-BD3C-6B36CBCC1E16\",\"name\":\"Stand Up Paddleboarding\"},{\"id\":\"DF4A35E0-7983-4A3E-BC47-F37B872B0F25\",\"name\":\"Junior Ranger Program\"},{\"id\":\"F9B1D433-6B86-4804-AED7-B50A519A3B7C\",\"name\":\"Skiing\"},{\"id\":\"EAB1EBDE-5B72-493F-8F8F-0EE5B1724436\",\"name\":\"Cross-Country Skiing\"},{\"id\":\"C38B3C62-1BBF-4EA1-A1A2-35DE21B74C17\",\"name\":\"Snow Play\"},{\"id\":\"7C912B83-1B1B-4807-9B66-97C12211E48E\",\"name\":\"Snowmobiling\"},{\"id\":\"01D717BC-18BB-4FE4-95BA-6B13AD702038\",\"name\":\"Snowshoeing\"},{\"id\":\"587BB2D3-EC35-41B2-B3F7-A39E2B088AEE\",\"name\":\"Swimming\"},{\"id\":\"82C3230F-6F87-452C-A01B-F8458867B26A\",\"name\":\"Freshwater Swimming\"},{\"id\":\"C2801992-F34D-4974-A0F2-80FF04309EE4\",\"name\":\"Saltwater Swimming\"},{\"id\":\"0B685688-3405-4E2A-ABBA-E3069492EC50\",\"name\":\"Wildlife Watching\"},{\"id\":\"5A2C91D1-50EC-4B24-8BED-A2E11A1892DF\",\"name\":\"Birdwatching\"},{\"id\":\"0C0D142F-06B5-4BE1-8B44-491B90F93DEB\",\"name\":\"Park Film\"},{\"id\":\"24380E3F-AD9D-4E38-BF13-C8EEB21893E7\",\"name\":\"Shopping\"},{\"id\":\"467DC8B8-0B7D-436D-A026-80A22358F615\",\"name\":\"Bookstore and Park Store\"}],\"addresses\":[{\"city\":\"Bar Harbor\",\"line1\":\"25 Visitor Center Road\",\"line2\":\"Hulls Cove Visitor Center\",\"line3\":\"\",\"postalCode\":\"04609\",\"stateCode\":\"ME\",\"type\":\"Physical\"},{\"city\":\"Bar Harbor\",\"line1\":\"PO Box 177\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"04609\",\"stateCode\":\"ME\",\"type\":\"Mailing\"}],\"contacts\":{\"emailAddresses\":[{\"description\":\"\",\"emailAddress\":\"acadia_information@nps.gov\"}],\"phoneNumbers\":[{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2072883338\",\"type\":\"Voice\"},{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2072888813\",\"type\":\"Fax\"},{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2072888800\",\"type\":\"TTY\"}]},\"description\":\"Acadia National Park protects the natural beauty of the highest rocky headlands along the Atlantic coastline of the United States, an abundance of habitats, and a rich cultural heritage. At 3.5 million visits a year, it's one of the top 10 most-visited national parks in the United States. Visitors enjoy 27 miles of historic motor roads, 158 miles of hiking trails, and 45 miles of carriage roads.\",\"designation\":\"National Park\",\"directionsInfo\":\"From Boston take I-95 north to Augusta, Maine, then Route 3 east to Ellsworth, and on to Mount Desert Island. For an alternate route, continue on I-95 north to Bangor, Maine, then take I-395 to U.S. Route 1A east to Ellsworth. In Ellsworth, take Route 3 to Mount Desert Island.\",\"directionsUrl\":\"http://www.nps.gov/acad/planyourvisit/directions.htm\",\"entranceFees\":[{\"cost\":\"30.00\",\"description\":\"Admits private, non-commercial vehicle (15 passenger capacity or less) and all occupants. Valid for 7 days. If a vehicle pass is purchased, no other pass is necessary.\",\"title\":\"Acadia Entrance Fee - Private Vehicle\"},{\"cost\":\"25.00\",\"description\":\"Admits one or two passengers on a private, non-commercial motorcycle. Valid for 7 days.\",\"title\":\"Acadia Entrance Fee - Motorcycle\"},{\"cost\":\"15.00\",\"description\":\"Admits one individual with no car (bicyclist, hiker, pedestrian). Youth 15 and under are admitted free of charge. Valid for 7 days.\",\"title\":\"Acadia Entrance Fee - Per Person\"}],\"entrancePasses\":[{\"cost\":\"55.00\",\"description\":\"Valid for 12 months from purchase date. This pass provides access to Acadia National Park only. This pass admits the pass holder and passengers in a non-commercial vehicle.\",\"title\":\"Acadia Annual Pass\"}],\"fees\":[],\"fullName\":\"Acadia National Park\",\"id\":\"6DA17C86-088E-4B4D-B862-7C1BD5CF236B\",\"images\":[{\"altText\":\"A brilliant sunset filled with hues of blue, red, orange, magenta, and purple highlight the sky.\",\"caption\":\"As the tallest point on the eastern seaboard Cadillac Mountain provides fantastic viewpoints.\",\"credit\":\"NPS / Kristi Rugg\",\"title\":\"Sunset atop Cadillac Mountain\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C7B477B-1DD8-B71B-0BCB48E009241BAA.jpg\"},{\"altText\":\"Large puffy clouds dot a brilliant blue sky as wave crash against the rocky coastline of Acadia.\",\"caption\":\"Millions of people come to Acadia for our distinctive rocky coastline.\",\"credit\":\"NPS / Kristi Rugg\",\"title\":\"Acadia's rocky coastline\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C7B45AE-1DD8-B71B-0B7EE131C7DFC2F5.jpg\"},{\"altText\":\"Two hikers ascend a sheer cliff trail by way of historic iron rung ladders.\",\"caption\":\"Whether it's a stroll along Ocean Path or a difficult ascent up The Precipice, there are hiking trails for everyone!\",\"credit\":\"NPS / Kristi Rugg\",\"title\":\"Climbing The Precipice\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C7B48F9-1DD8-B71B-0BD3B413E58978F8.jpg\"},{\"altText\":\"Hiking tracks carved through three feet of snow wind through a heavy snow-laden forest.\",\"caption\":\"During the colder months snows transform our landscape into a winter wonderland.\",\"credit\":\"NPS / Kristi Rugg\",\"title\":\"Heavy snow-laden trees\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C7B4BEC-1DD8-B71B-0B2CF833F93140FF.jpg\"},{\"altText\":\"A man peers into a telescope as the Milky Way lights up the night sky.\",\"caption\":\"With dark skies, Acadia is a fantastic place to come see the stars.\",\"credit\":\"NPS / Kristi Rugg\",\"title\":\"Starviewing\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C7B4A95-1DD8-B71B-0B8C1868A4135836.jpg\"}],\"latLong\":\"lat:44.409286, long:-68.247501\",\"latitude\":\"44.409286\",\"longitude\":\"-68.247501\",\"name\":\"Acadia\",\"operatingHours\":[{\"description\":\"In support of federal, state, and local efforts to slow the spread of the novel coronavirus (COVID-19), the park is under a phased reopening. The Park Loop Road and most hiking trails are open. Information stations are staffed outside main visitor center. Carriage Roads are closed to bicycles and horses. Campgrounds open no sooner than August 1. Gatherings of more than 50 people are prohibited.\",\"exceptions\":[],\"name\":\"COVID-19 Phased Reopening\",\"standardHours\":{\"friday\":\"All Day\",\"monday\":\"All Day\",\"saturday\":\"All Day\",\"sunday\":\"All Day\",\"thursday\":\"All Day\",\"tuesday\":\"All Day\",\"wednesday\":\"All Day\"}}],\"parkCode\":\"acad\",\"states\":\"ME\",\"topics\":[{\"id\":\"00F3C3F9-2D67-4802-81AE-CCEA5D3BA370\",\"name\":\"Arts\"},{\"id\":\"05B7868A-3F3C-433D-876E-A886C4BE7A12\",\"name\":\"Painting\"},{\"id\":\"9BD60DC0-C82B-42BA-A170-456B7423429D\",\"name\":\"Photography\"},{\"id\":\"156AD9B6-B377-418C-BC9A-F6E682D4BAF7\",\"name\":\"Poetry and Literature\"},{\"id\":\"0DE2D6B3-46DE-44B1-BB5F-E9E8874630D5\",\"name\":\"Sculpture\"},{\"id\":\"7F12224B-217A-4B07-A4A2-636B1CE7F221\",\"name\":\"Colonization and Settlement\"},{\"id\":\"4C9D4777-A9DA-47D1-A0B9-F4A3C98BC1B3\",\"name\":\"Maritime\"},{\"id\":\"263BAC6E-4DEC-47E4-909D-DA8AD351E06E\",\"name\":\"Lighthouses\"},{\"id\":\"BEB7E470-13B2-4E00-84B2-0402D98DAF69\",\"name\":\"Monuments and Memorials\"},{\"id\":\"3CDB67A9-1EAC-408D-88EC-F26FA35E90AF\",\"name\":\"Schools and Education\"},{\"id\":\"FE2C2613-B41E-4531-BC43-03EB6E45CBCF\",\"name\":\"Transportation\"},{\"id\":\"1015393C-D7B0-47F3-86FB-786F30368CA2\",\"name\":\"Bridges\"},{\"id\":\"0BBD4A42-2B3D-4E82-B5C4-1A3874C8682E\",\"name\":\"Roads, Routes and Highways\"},{\"id\":\"A160B3D9-1603-4D89-B82F-21FCAF9EEE3B\",\"name\":\"Tragic Events\"},{\"id\":\"C373F02B-7335-4F8A-A3ED-3E2E37CD4085\",\"name\":\"Catastrophic Fires\"},{\"id\":\"7DA81DAB-5045-4953-9C20-36590AD9FA95\",\"name\":\"Women's History\"},{\"id\":\"0D00073E-18C3-46E5-8727-2F87B112DDC6\",\"name\":\"Animals\"},{\"id\":\"957EF2BD-AC6C-4B7B-BD9A-87593ADC6691\",\"name\":\"Birds\"},{\"id\":\"1608649A-E7D7-4529-BD83-074C90F9FB68\",\"name\":\"Fish\"},{\"id\":\"4DC11D06-00F1-4A01-81D0-89CCCCE4FF50\",\"name\":\"Climate Change\"},{\"id\":\"04A39AB8-DD02-432F-AE5F-BA1267D41A0D\",\"name\":\"Fire\"},{\"id\":\"41B1A0A3-11FF-4F55-9CB9-034A7E28B087\",\"name\":\"Forests and Woodlands\"},{\"id\":\"F0F97E32-2F29-41B4-AF98-9FBE8DAB36B1\",\"name\":\"Geology\"},{\"id\":\"1CF1F6BB-A037-445B-8CF2-81428E50CE52\",\"name\":\"Lakes\"},{\"id\":\"101F4D51-F99D-45A6-BBB6-CD481E5FACED\",\"name\":\"Mountains\"},{\"id\":\"A7359FC4-DAD8-45F5-AF15-7FF62F816ED3\",\"name\":\"Night Sky\"},{\"id\":\"E06F3C94-AC8A-4B1C-A247-8EBA8910D5EE\",\"name\":\"Astronomy\"},{\"id\":\"0E6D8503-CB65-467F-BCD6-C6D9E28A4E0B\",\"name\":\"Oceans\"},{\"id\":\"4244F489-6813-4B7A-9D0C-20CE098C8FFF\",\"name\":\"Rock Landscapes and Features\"},{\"id\":\"5BE55D7F-BDB6-4E3D-AC35-2D8EBB974417\",\"name\":\"Trails\"},{\"id\":\"1365C347-952C-475A-B755-731DD523C195\",\"name\":\"Wetlands\"}],\"url\":\"https://www.nps.gov/acad/index.htm\",\"weatherInfo\":\"Located on Mount Desert Island in Maine, Acadia experiences all four seasons. Summer temperatures range from 45-90F (7-30C). Fall temperatures range from 30-70F (-1-21C). Typically the first frost is in mid-October and first snowfall begins in November and can continue through April with an average accumulation of 73 inches (185 cm). Winter temperatures range from 14-35F (-10 - 2C). Spring temperatures range from 30-70F (-1-21C).\"},{\"activities\":[{\"id\":\"B33DC9B6-0B7D-4322-BAD7-A13A34C584A3\",\"name\":\"Guided Tours\"},{\"id\":\"B204DE60-5A24-43DD-8902-C81625A09A74\",\"name\":\"Living History\"},{\"id\":\"28880A5B-3D29-41AC-BD8B-24743E7A070F\",\"name\":\"First Person Interpretation\"},{\"id\":\"0C0D142F-06B5-4BE1-8B44-491B90F93DEB\",\"name\":\"Park Film\"},{\"id\":\"24380E3F-AD9D-4E38-BF13-C8EEB21893E7\",\"name\":\"Shopping\"},{\"id\":\"467DC8B8-0B7D-436D-A026-80A22358F615\",\"name\":\"Bookstore and Park Store\"}],\"addresses\":[{\"city\":\"Quincy\",\"line1\":\"1250 Hancock Street\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"02169\",\"stateCode\":\"MA\",\"type\":\"Physical\"},{\"city\":\"Quincy\",\"line1\":\"135 Adams Street\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"02169\",\"stateCode\":\"MA\",\"type\":\"Mailing\"}],\"contacts\":{\"emailAddresses\":[{\"description\":\"\",\"emailAddress\":\"ADAM_Visitor_Center@nps.gov\"}],\"phoneNumbers\":[{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"617-770-1175\",\"type\":\"Voice\"}]},\"description\":\"From the sweet little farm at the foot of Penn’s Hill to the gentleman’s country estate at Peace field, Adams National Historical Park is the story of “heroes, statesman, philosophers … and learned women” whose ideas and actions helped to transform thirteen disparate colonies into one united nation.\",\"designation\":\"National Historical Park\",\"directionsInfo\":\"Traveling on U.S. Interstate 93, take exit 7 - Route 3 South to Braintree and Cape Cod. Take the first exit off Route 3 south - exit 19 - and follow signs toward Quincy Center. Continue straight on Burgin Parkway through six traffic lights. At the seventh traffic light, turn right onto Dimmock Street. Follow Dimmock Street one block and turn right onto Hancock Street. The National Park Service Visitor Center, located at 1250 Hancock Street on your left. Validated parking is in the garage to the rear.\",\"directionsUrl\":\"http://www.nps.gov/adam/planyourvisit/directions.htm\",\"entranceFees\":[{\"cost\":\"15.00\",\"description\":\"The entrance fee is for all visitors age 16 and older.  Children under 16 are free. Credit or Debit Card only.\",\"title\":\"Individual Entrance Fee\"}],\"entrancePasses\":[],\"fees\":[],\"fullName\":\"Adams National Historical Park\",\"id\":\"E4C7784E-66A0-4D44-87D0-3E072F5FEF43\",\"images\":[{\"altText\":\"The Birthplaces of Presidents John Adams (right) and John Quincy Adams (left)\",\"caption\":\"The Birthplaces of John and John Quincy Adams sit right next  to each other on Franklin Street.\",\"credit\":\"NPS Photo\",\"title\":\"The John and John Quincy Adams Birthplaces\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C7C7416-1DD8-B71B-0B1B30D0827F7C74.jpg\"},{\"altText\":\"The Birthplace of John Adams\",\"caption\":\"The house where President John Adams was born in 1735.\",\"credit\":\"NPS Photo\",\"title\":\"The Birthplace of John Adams\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C7C7565-1DD8-B71B-0BEC729A3E865792.jpg\"},{\"altText\":\"The Birthplace of John Quincy Adams\",\"caption\":\"The house where President John Quincy Adams was born in 1767.\",\"credit\":\"NPS Photo\",\"title\":\"The Birthplace of John Quincy Adams\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C7C76BE-1DD8-B71B-0B6DFACFB45AC5A4.jpg\"},{\"altText\":\"A view of Old House at Peace field\",\"caption\":\"Old House at Peace field, where four generations of the Adams family lived from 1788 to 1927.\",\"credit\":\"NPS Photo\",\"title\":\"Old House at Peace field\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C7C780E-1DD8-B71B-0B18B8DB9F39704F.jpg\"},{\"altText\":\"The Paneled Room located inside Old House at Peace field.\",\"caption\":\"The Paneled Room greets everyone who enters Old House at Peace field.\",\"credit\":\"NPS Photo / Betty Brown\",\"title\":\"The Paneled Room\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C7C7945-1DD8-B71B-0BE14B48DD78B777.jpg\"},{\"altText\":\"The Stone Library located outside Old House at Peace field.\",\"caption\":\"The Stone Library houses up to 14,000 books belonging to the Adams family.\",\"credit\":\"NPS Photo\",\"title\":\"The Stone Library\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C7C7A77-1DD8-B71B-0BDA92321AD899C5.jpg\"},{\"altText\":\"Inside the Stone Library\",\"caption\":\"A view into the Stone Library\",\"credit\":\"NPS Photo\",\"title\":\"Inside the Stone Library\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C7C7BE3-1DD8-B71B-0B864F7398605B7E.jpg\"},{\"altText\":\"The gardens located at Old House at Peace field.\",\"caption\":\"The gardens located by Old House at Peace field bloom in every color you can imagine.\",\"credit\":\"NPS Photo\",\"title\":\"The Gardens Located at Old House at Peace field\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C7C7D4E-1DD8-B71B-0B48B5CDE41703D7.jpg\"}],\"latLong\":\"lat:42.2553961, long:-71.01160356\",\"latitude\":\"42.2553961\",\"longitude\":\"-71.01160356\",\"name\":\"Adams\",\"operatingHours\":[{\"description\":\"The historic homes are open from May 1 through October 31.  The first guided tour leaves the Visitor Center at 9:15 a.m. and the last guided tour leaves the Visitor Center at 3:15 p.m.  Please allow two and a half hours for each guided tour.\",\"exceptions\":[{\"endDate\":\"2022-01-01\",\"exceptionHours\":{\"friday\":\"Closed\",\"monday\":\"Closed\",\"saturday\":\"Closed\",\"sunday\":\"Closed\",\"thursday\":\"Closed\",\"tuesday\":\"Closed\",\"wednesday\":\"Closed\"},\"name\":\"New Year's Day\",\"startDate\":\"2022-01-01\"},{\"endDate\":\"2022-01-18\",\"exceptionHours\":{\"friday\":\"Closed\",\"monday\":\"Closed\",\"saturday\":\"Closed\",\"sunday\":\"Closed\",\"thursday\":\"Closed\",\"tuesday\":\"Closed\",\"wednesday\":\"Closed\"},\"name\":\"Birthday of Martin Luther King, Jr.\",\"startDate\":\"2022-01-18\"},{\"endDate\":\"2022-02-15\",\"exceptionHours\":{\"friday\":\"Closed\",\"monday\":\"Closed\",\"saturday\":\"Closed\",\"sunday\":\"Closed\",\"thursday\":\"Closed\",\"tuesday\":\"Closed\",\"wednesday\":\"Closed\"},\"name\":\"Presidents' Day\",\"startDate\":\"2022-02-15\"},{\"endDate\":\"2021-12-31\",\"exceptionHours\":{\"friday\":\"Closed\",\"monday\":\"Closed\",\"saturday\":\"Closed\",\"sunday\":\"Closed\",\"thursday\":\"Closed\",\"tuesday\":\"Closed\",\"wednesday\":\"Closed\"},\"name\":\"Closed Due to COVID-19\",\"startDate\":\"2021-03-17\"},{\"endDate\":\"2021-11-11\",\"exceptionHours\":{\"friday\":\"Closed\",\"monday\":\"Closed\",\"saturday\":\"Closed\",\"sunday\":\"Closed\",\"thursday\":\"Closed\",\"tuesday\":\"Closed\",\"wednesday\":\"Closed\"},\"name\":\"Veterans Day\",\"startDate\":\"2021-11-11\"},{\"endDate\":\"2021-11-25\",\"exceptionHours\":{\"friday\":\"Closed\",\"monday\":\"Closed\",\"saturday\":\"Closed\",\"sunday\":\"Closed\",\"thursday\":\"Closed\",\"tuesday\":\"Closed\",\"wednesday\":\"Closed\"},\"name\":\"Thanksgiving Day\",\"startDate\":\"2021-11-25\"},{\"endDate\":\"2021-12-25\",\"exceptionHours\":{\"friday\":\"Closed\",\"monday\":\"Closed\",\"saturday\":\"Closed\",\"sunday\":\"Closed\",\"thursday\":\"Closed\",\"tuesday\":\"Closed\",\"wednesday\":\"Closed\"},\"name\":\"Christmas Day\",\"startDate\":\"2021-12-25\"}],\"name\":\"Visiting the Historic Homes\",\"standardHours\":{\"friday\":\"9:00AM - 5:00PM\",\"monday\":\"9:00AM - 5:00PM\",\"saturday\":\"9:00AM - 5:00PM\",\"sunday\":\"9:00AM - 5:00PM\",\"thursday\":\"9:00AM - 5:00PM\",\"tuesday\":\"9:00AM - 5:00PM\",\"wednesday\":\"9:00AM - 5:00PM\"}}],\"parkCode\":\"adam\",\"states\":\"MA\",\"topics\":[{\"id\":\"F3883A66-A7CB-461B-868E-1B5932224B25\",\"name\":\"American Revolution\"},{\"id\":\"7DA81DAB-5045-4953-9C20-36590AD9FA95\",\"name\":\"Women's History\"}],\"url\":\"https://www.nps.gov/adam/index.htm\",\"weatherInfo\":\"Be prepared for hot, humid weather. The historic homes are not air conditioned. While the visitor center remains open all year, the historic homes are closed from November 1 through April 30.\"},{\"activities\":[{\"id\":\"13A57703-BB1A-41A2-94B8-53B692EB7238\",\"name\":\"Astronomy\"},{\"id\":\"D37A0003-8317-4F04-8FB0-4CF0A272E195\",\"name\":\"Stargazing\"},{\"id\":\"1DFACD97-1B9C-4F5A-80F2-05593604799E\",\"name\":\"Food\"},{\"id\":\"C6D3230A-2CEA-4AFE-BFF3-DC1E2C2C4BB4\",\"name\":\"Picnicking\"},{\"id\":\"DF4A35E0-7983-4A3E-BC47-F37B872B0F25\",\"name\":\"Junior Ranger Program\"},{\"id\":\"0B685688-3405-4E2A-ABBA-E3069492EC50\",\"name\":\"Wildlife Watching\"},{\"id\":\"5A2C91D1-50EC-4B24-8BED-A2E11A1892DF\",\"name\":\"Birdwatching\"},{\"id\":\"24380E3F-AD9D-4E38-BF13-C8EEB21893E7\",\"name\":\"Shopping\"},{\"id\":\"467DC8B8-0B7D-436D-A026-80A22358F615\",\"name\":\"Bookstore and Park Store\"},{\"id\":\"43800AD1-D439-40F3-AAB3-9FB651FE45BB\",\"name\":\"Gift Shop and Souvenirs\"}],\"addresses\":[{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Physical\"},{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Mailing\"}],\"contacts\":{\"emailAddresses\":[{\"description\":\"\",\"emailAddress\":\"ABLI_Administration@nps.gov\"}],\"phoneNumbers\":[{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583137\",\"type\":\"Voice\"},{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583874\",\"type\":\"Fax\"}]},\"description\":\"For over a century people from around the world have come to rural Central Kentucky to honor the humble beginnings of our 16th president, Abraham Lincoln. His early life on Kentucky's frontier shaped his character and prepared him to lead the nation through Civil War. The country's first memorial to Lincoln, built with donations from young and old, enshrines the symbolic birthplace cabin.\",\"designation\":\"National Historical Park\",\"directionsInfo\":\"The Birthplace Unit of the park is located approximately 2 miles south of the town of Hodgenville on U.S. Highway 31E South. The Boyhood Home Unit at Knob Creek is located approximately 10 miles northeast of the Birthplace Unit of the park.\",\"directionsUrl\":\"http://www.nps.gov/abli/planyourvisit/directions.htm\",\"entranceFees\":[{\"cost\":\"0.00\",\"description\":\"There is no fee associated with visiting either unit of the park.\",\"title\":\"Fee Free Park\"}],\"entrancePasses\":[],\"fees\":[],\"fullName\":\"Abraham Lincoln Birthplace National Historical Park\",\"id\":\"77E0D7F0-1942-494A-ACE2-9004D2BDC59E\",\"images\":[{\"altText\":\"The Memorial Building surrounded by fall colors\",\"caption\":\"Over 200,000 people a year come to walk up the steps of the Memorial Building to visit the site where Abraham Lincoln was born\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building with fall colors\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861078-1DD8-B71B-0B774A242EF6A706.jpg\"},{\"altText\":\"The first memorial erected to honor Abraham Lincoln\",\"caption\":\"The Memorial Building constructed on the traditional site of the birth of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861263-1DD8-B71B-0B71EF9B95F9644F.jpg\"},{\"altText\":\"The symbolic birth cabin on the traditional site of the birth of Abraham Lincoln.\",\"caption\":\"The symbolic birth cabin of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Symbolic Birth Cabin of Abraham Lincoln\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C86137D-1DD8-B71B-0B978BACD7EBAEF1.jpg\"},{\"altText\":\"Statue of the Lincoln family in the park's Visitor Center\",\"caption\":\"Visitors to the park can view the statue of the Lincoln family.\",\"credit\":\"NPS Photo\",\"title\":\"Statue of the Lincoln Family in the Visitor Center\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C8614D1-1DD8-B71B-0B1AF72CA452B051.jpg\"}],\"latLong\":\"lat:37.5858662, long:-85.67330523\",\"latitude\":\"37.5858662\",\"longitude\":\"-85.67330523\",\"name\":\"Abraham Lincoln Birthplace\",\"operatingHours\":[{\"description\":\"The Memorial Building is open 9:00 am - 4:30 pm eastern time with a limited viewing area.  The Visitor Center and grounds of the Birthplace Unit are open 9:00 am - 5:00 pm eastern time with one-way traffic flow and social distancing.\\n\\n\\nThe Boyhood Home Unit at Knob Creek will be closed August 3, 2020, to July 31, 2021, due to the Lincoln Tavern Rehabilitation Project.\",\"exceptions\":[{\"endDate\":\"2022-01-01\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2022-01-01\"},{\"endDate\":\"2021-11-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-11-25\"},{\"endDate\":\"2021-12-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-12-25\"}],\"name\":\"Abraham Lincoln Birthplace National Historical Park\",\"standardHours\":{\"friday\":\"9:00AM - 5:00PM\",\"monday\":\"9:00AM - 5:00PM\",\"saturday\":\"9:00AM - 5:00PM\",\"sunday\":\"9:00AM - 5:00PM\",\"thursday\":\"9:00AM - 5:00PM\",\"tuesday\":\"9:00AM - 5:00PM\",\"wednesday\":\"9:00AM - 5:00PM\"}}],\"parkCode\":\"abli\",\"states\":\"KY\",\"topics\":[{\"id\":\"D10852A3-443C-4743-A5FA-6DD6D2A054B3\",\"name\":\"Birthplace\"},{\"id\":\"F669BC40-BDC4-41C0-9ACE-B2CD25373045\",\"name\":\"Presidents\"},{\"id\":\"0D00073E-18C3-46E5-8727-2F87B112DDC6\",\"name\":\"Animals\"},{\"id\":\"957EF2BD-AC6C-4B7B-BD9A-87593ADC6691\",\"name\":\"Birds\"},{\"id\":\"E25F3456-43ED-45DD-93BC-057F9B944F7A\",\"name\":\"Caves, Caverns and Karst\"},{\"id\":\"F0F97E32-2F29-41B4-AF98-9FBE8DAB36B1\",\"name\":\"Geology\"},{\"id\":\"4BE01DC5-52E6-4F18-8C9A-B22D65965F6D\",\"name\":\"Groundwater\"},{\"id\":\"0E1A04CC-EB51-4F18-93D4-EC0B0B4EC1E3\",\"name\":\"Freshwater Springs\"},{\"id\":\"A7359FC4-DAD8-45F5-AF15-7FF62F816ED3\",\"name\":\"Night Sky\"}],\"url\":\"https://www.nps.gov/abli/index.htm\",\"weatherInfo\":\"There are four distinct seasons in Central Kentucky. However, temperature and weather conditions can vary widely within those seasons. Spring and Fall are generally pleasant with frequent rain showers. Summer is usually hot and humid. Winter is moderately cold with mixed precipitation.\"},{\"activities\":[{\"id\":\"13A57703-BB1A-41A2-94B8-53B692EB7238\",\"name\":\"Astronomy\"},{\"id\":\"D37A0003-8317-4F04-8FB0-4CF0A272E195\",\"name\":\"Stargazing\"},{\"id\":\"1DFACD97-1B9C-4F5A-80F2-05593604799E\",\"name\":\"Food\"},{\"id\":\"C6D3230A-2CEA-4AFE-BFF3-DC1E2C2C4BB4\",\"name\":\"Picnicking\"},{\"id\":\"DF4A35E0-7983-4A3E-BC47-F37B872B0F25\",\"name\":\"Junior Ranger Program\"},{\"id\":\"0B685688-3405-4E2A-ABBA-E3069492EC50\",\"name\":\"Wildlife Watching\"},{\"id\":\"5A2C91D1-50EC-4B24-8BED-A2E11A1892DF\",\"name\":\"Birdwatching\"},{\"id\":\"24380E3F-AD9D-4E38-BF13-C8EEB21893E7\",\"name\":\"Shopping\"},{\"id\":\"467DC8B8-0B7D-436D-A026-80A22358F615\",\"name\":\"Bookstore and Park Store\"},{\"id\":\"43800AD1-D439-40F3-AAB3-9FB651FE45BB\",\"name\":\"Gift Shop and Souvenirs\"}],\"addresses\":[{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Physical\"},{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Mailing\"}],\"contacts\":{\"emailAddresses\":[{\"description\":\"\",\"emailAddress\":\"ABLI_Administration@nps.gov\"}],\"phoneNumbers\":[{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583137\",\"type\":\"Voice\"},{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583874\",\"type\":\"Fax\"}]},\"description\":\"For over a century people from around the world have come to rural Central Kentucky to honor the humble beginnings of our 16th president, Abraham Lincoln. His early life on Kentucky's frontier shaped his character and prepared him to lead the nation through Civil War. The country's first memorial to Lincoln, built with donations from young and old, enshrines the symbolic birthplace cabin.\",\"designation\":\"National Historical Park\",\"directionsInfo\":\"The Birthplace Unit of the park is located approximately 2 miles south of the town of Hodgenville on U.S. Highway 31E South. The Boyhood Home Unit at Knob Creek is located approximately 10 miles northeast of the Birthplace Unit of the park.\",\"directionsUrl\":\"http://www.nps.gov/abli/planyourvisit/directions.htm\",\"entranceFees\":[{\"cost\":\"0.00\",\"description\":\"There is no fee associated with visiting either unit of the park.\",\"title\":\"Fee Free Park\"}],\"entrancePasses\":[],\"fees\":[],\"fullName\":\"Abraham Lincoln Birthplace National Historical Park\",\"id\":\"77E0D7F0-1942-494A-ACE2-9004D2BDC59E\",\"images\":[{\"altText\":\"The Memorial Building surrounded by fall colors\",\"caption\":\"Over 200,000 people a year come to walk up the steps of the Memorial Building to visit the site where Abraham Lincoln was born\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building with fall colors\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861078-1DD8-B71B-0B774A242EF6A706.jpg\"},{\"altText\":\"The first memorial erected to honor Abraham Lincoln\",\"caption\":\"The Memorial Building constructed on the traditional site of the birth of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861263-1DD8-B71B-0B71EF9B95F9644F.jpg\"},{\"altText\":\"The symbolic birth cabin on the traditional site of the birth of Abraham Lincoln.\",\"caption\":\"The symbolic birth cabin of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Symbolic Birth Cabin of Abraham Lincoln\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C86137D-1DD8-B71B-0B978BACD7EBAEF1.jpg\"},{\"altText\":\"Statue of the Lincoln family in the park's Visitor Center\",\"caption\":\"Visitors to the park can view the statue of the Lincoln family.\",\"credit\":\"NPS Photo\",\"title\":\"Statue of the Lincoln Family in the Visitor Center\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C8614D1-1DD8-B71B-0B1AF72CA452B051.jpg\"}],\"latLong\":\"lat:37.5858662, long:-85.67330523\",\"latitude\":\"37.5858662\",\"longitude\":\"-85.67330523\",\"name\":\"Abraham Lincoln Birthplace\",\"operatingHours\":[{\"description\":\"The Memorial Building is open 9:00 am - 4:30 pm eastern time with a limited viewing area.  The Visitor Center and grounds of the Birthplace Unit are open 9:00 am - 5:00 pm eastern time with one-way traffic flow and social distancing.\\n\\n\\nThe Boyhood Home Unit at Knob Creek will be closed August 3, 2020, to July 31, 2021, due to the Lincoln Tavern Rehabilitation Project.\",\"exceptions\":[{\"endDate\":\"2022-01-01\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2022-01-01\"},{\"endDate\":\"2021-11-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-11-25\"},{\"endDate\":\"2021-12-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-12-25\"}],\"name\":\"Abraham Lincoln Birthplace National Historical Park\",\"standardHours\":{\"friday\":\"9:00AM - 5:00PM\",\"monday\":\"9:00AM - 5:00PM\",\"saturday\":\"9:00AM - 5:00PM\",\"sunday\":\"9:00AM - 5:00PM\",\"thursday\":\"9:00AM - 5:00PM\",\"tuesday\":\"9:00AM - 5:00PM\",\"wednesday\":\"9:00AM - 5:00PM\"}}],\"parkCode\":\"abli\",\"states\":\"KY\",\"topics\":[{\"id\":\"D10852A3-443C-4743-A5FA-6DD6D2A054B3\",\"name\":\"Birthplace\"},{\"id\":\"F669BC40-BDC4-41C0-9ACE-B2CD25373045\",\"name\":\"Presidents\"},{\"id\":\"0D00073E-18C3-46E5-8727-2F87B112DDC6\",\"name\":\"Animals\"},{\"id\":\"957EF2BD-AC6C-4B7B-BD9A-87593ADC6691\",\"name\":\"Birds\"},{\"id\":\"E25F3456-43ED-45DD-93BC-057F9B944F7A\",\"name\":\"Caves, Caverns and Karst\"},{\"id\":\"F0F97E32-2F29-41B4-AF98-9FBE8DAB36B1\",\"name\":\"Geology\"},{\"id\":\"4BE01DC5-52E6-4F18-8C9A-B22D65965F6D\",\"name\":\"Groundwater\"},{\"id\":\"0E1A04CC-EB51-4F18-93D4-EC0B0B4EC1E3\",\"name\":\"Freshwater Springs\"},{\"id\":\"A7359FC4-DAD8-45F5-AF15-7FF62F816ED3\",\"name\":\"Night Sky\"}],\"url\":\"https://www.nps.gov/abli/index.htm\",\"weatherInfo\":\"There are four distinct seasons in Central Kentucky. However, temperature and weather conditions can vary widely within those seasons. Spring and Fall are generally pleasant with frequent rain showers. Summer is usually hot and humid. Winter is moderately cold with mixed precipitation.\"},{\"activities\":[{\"id\":\"13A57703-BB1A-41A2-94B8-53B692EB7238\",\"name\":\"Astronomy\"},{\"id\":\"D37A0003-8317-4F04-8FB0-4CF0A272E195\",\"name\":\"Stargazing\"},{\"id\":\"1DFACD97-1B9C-4F5A-80F2-05593604799E\",\"name\":\"Food\"},{\"id\":\"C6D3230A-2CEA-4AFE-BFF3-DC1E2C2C4BB4\",\"name\":\"Picnicking\"},{\"id\":\"DF4A35E0-7983-4A3E-BC47-F37B872B0F25\",\"name\":\"Junior Ranger Program\"},{\"id\":\"0B685688-3405-4E2A-ABBA-E3069492EC50\",\"name\":\"Wildlife Watching\"},{\"id\":\"5A2C91D1-50EC-4B24-8BED-A2E11A1892DF\",\"name\":\"Birdwatching\"},{\"id\":\"24380E3F-AD9D-4E38-BF13-C8EEB21893E7\",\"name\":\"Shopping\"},{\"id\":\"467DC8B8-0B7D-436D-A026-80A22358F615\",\"name\":\"Bookstore and Park Store\"},{\"id\":\"43800AD1-D439-40F3-AAB3-9FB651FE45BB\",\"name\":\"Gift Shop and Souvenirs\"}],\"addresses\":[{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Physical\"},{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Mailing\"}],\"contacts\":{\"emailAddresses\":[{\"description\":\"\",\"emailAddress\":\"ABLI_Administration@nps.gov\"}],\"phoneNumbers\":[{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583137\",\"type\":\"Voice\"},{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583874\",\"type\":\"Fax\"}]},\"description\":\"For over a century people from around the world have come to rural Central Kentucky to honor the humble beginnings of our 16th president, Abraham Lincoln. His early life on Kentucky's frontier shaped his character and prepared him to lead the nation through Civil War. The country's first memorial to Lincoln, built with donations from young and old, enshrines the symbolic birthplace cabin.\",\"designation\":\"National Historical Park\",\"directionsInfo\":\"The Birthplace Unit of the park is located approximately 2 miles south of the town of Hodgenville on U.S. Highway 31E South. The Boyhood Home Unit at Knob Creek is located approximately 10 miles northeast of the Birthplace Unit of the park.\",\"directionsUrl\":\"http://www.nps.gov/abli/planyourvisit/directions.htm\",\"entranceFees\":[{\"cost\":\"0.00\",\"description\":\"There is no fee associated with visiting either unit of the park.\",\"title\":\"Fee Free Park\"}],\"entrancePasses\":[],\"fees\":[],\"fullName\":\"Abraham Lincoln Birthplace National Historical Park\",\"id\":\"77E0D7F0-1942-494A-ACE2-9004D2BDC59E\",\"images\":[{\"altText\":\"The Memorial Building surrounded by fall colors\",\"caption\":\"Over 200,000 people a year come to walk up the steps of the Memorial Building to visit the site where Abraham Lincoln was born\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building with fall colors\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861078-1DD8-B71B-0B774A242EF6A706.jpg\"},{\"altText\":\"The first memorial erected to honor Abraham Lincoln\",\"caption\":\"The Memorial Building constructed on the traditional site of the birth of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861263-1DD8-B71B-0B71EF9B95F9644F.jpg\"},{\"altText\":\"The symbolic birth cabin on the traditional site of the birth of Abraham Lincoln.\",\"caption\":\"The symbolic birth cabin of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Symbolic Birth Cabin of Abraham Lincoln\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C86137D-1DD8-B71B-0B978BACD7EBAEF1.jpg\"},{\"altText\":\"Statue of the Lincoln family in the park's Visitor Center\",\"caption\":\"Visitors to the park can view the statue of the Lincoln family.\",\"credit\":\"NPS Photo\",\"title\":\"Statue of the Lincoln Family in the Visitor Center\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C8614D1-1DD8-B71B-0B1AF72CA452B051.jpg\"}],\"latLong\":\"lat:37.5858662, long:-85.67330523\",\"latitude\":\"37.5858662\",\"longitude\":\"-85.67330523\",\"name\":\"Abraham Lincoln Birthplace\",\"operatingHours\":[{\"description\":\"The Memorial Building is open 9:00 am - 4:30 pm eastern time with a limited viewing area.  The Visitor Center and grounds of the Birthplace Unit are open 9:00 am - 5:00 pm eastern time with one-way traffic flow and social distancing.\\n\\n\\nThe Boyhood Home Unit at Knob Creek will be closed August 3, 2020, to July 31, 2021, due to the Lincoln Tavern Rehabilitation Project.\",\"exceptions\":[{\"endDate\":\"2022-01-01\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2022-01-01\"},{\"endDate\":\"2021-11-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-11-25\"},{\"endDate\":\"2021-12-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-12-25\"}],\"name\":\"Abraham Lincoln Birthplace National Historical Park\",\"standardHours\":{\"friday\":\"9:00AM - 5:00PM\",\"monday\":\"9:00AM - 5:00PM\",\"saturday\":\"9:00AM - 5:00PM\",\"sunday\":\"9:00AM - 5:00PM\",\"thursday\":\"9:00AM - 5:00PM\",\"tuesday\":\"9:00AM - 5:00PM\",\"wednesday\":\"9:00AM - 5:00PM\"}}],\"parkCode\":\"abli\",\"states\":\"KY\",\"topics\":[{\"id\":\"D10852A3-443C-4743-A5FA-6DD6D2A054B3\",\"name\":\"Birthplace\"},{\"id\":\"F669BC40-BDC4-41C0-9ACE-B2CD25373045\",\"name\":\"Presidents\"},{\"id\":\"0D00073E-18C3-46E5-8727-2F87B112DDC6\",\"name\":\"Animals\"},{\"id\":\"957EF2BD-AC6C-4B7B-BD9A-87593ADC6691\",\"name\":\"Birds\"},{\"id\":\"E25F3456-43ED-45DD-93BC-057F9B944F7A\",\"name\":\"Caves, Caverns and Karst\"},{\"id\":\"F0F97E32-2F29-41B4-AF98-9FBE8DAB36B1\",\"name\":\"Geology\"},{\"id\":\"4BE01DC5-52E6-4F18-8C9A-B22D65965F6D\",\"name\":\"Groundwater\"},{\"id\":\"0E1A04CC-EB51-4F18-93D4-EC0B0B4EC1E3\",\"name\":\"Freshwater Springs\"},{\"id\":\"A7359FC4-DAD8-45F5-AF15-7FF62F816ED3\",\"name\":\"Night Sky\"}],\"url\":\"https://www.nps.gov/abli/index.htm\",\"weatherInfo\":\"There are four distinct seasons in Central Kentucky. However, temperature and weather conditions can vary widely within those seasons. Spring and Fall are generally pleasant with frequent rain showers. Summer is usually hot and humid. Winter is moderately cold with mixed precipitation.\"},{\"activities\":[{\"id\":\"13A57703-BB1A-41A2-94B8-53B692EB7238\",\"name\":\"Astronomy\"},{\"id\":\"D37A0003-8317-4F04-8FB0-4CF0A272E195\",\"name\":\"Stargazing\"},{\"id\":\"1DFACD97-1B9C-4F5A-80F2-05593604799E\",\"name\":\"Food\"},{\"id\":\"C6D3230A-2CEA-4AFE-BFF3-DC1E2C2C4BB4\",\"name\":\"Picnicking\"},{\"id\":\"DF4A35E0-7983-4A3E-BC47-F37B872B0F25\",\"name\":\"Junior Ranger Program\"},{\"id\":\"0B685688-3405-4E2A-ABBA-E3069492EC50\",\"name\":\"Wildlife Watching\"},{\"id\":\"5A2C91D1-50EC-4B24-8BED-A2E11A1892DF\",\"name\":\"Birdwatching\"},{\"id\":\"24380E3F-AD9D-4E38-BF13-C8EEB21893E7\",\"name\":\"Shopping\"},{\"id\":\"467DC8B8-0B7D-436D-A026-80A22358F615\",\"name\":\"Bookstore and Park Store\"},{\"id\":\"43800AD1-D439-40F3-AAB3-9FB651FE45BB\",\"name\":\"Gift Shop and Souvenirs\"}],\"addresses\":[{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Physical\"},{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Mailing\"}],\"contacts\":{\"emailAddresses\":[{\"description\":\"\",\"emailAddress\":\"ABLI_Administration@nps.gov\"}],\"phoneNumbers\":[{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583137\",\"type\":\"Voice\"},{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583874\",\"type\":\"Fax\"}]},\"description\":\"For over a century people from around the world have come to rural Central Kentucky to honor the humble beginnings of our 16th president, Abraham Lincoln. His early life on Kentucky's frontier shaped his character and prepared him to lead the nation through Civil War. The country's first memorial to Lincoln, built with donations from young and old, enshrines the symbolic birthplace cabin.\",\"designation\":\"National Historical Park\",\"directionsInfo\":\"The Birthplace Unit of the park is located approximately 2 miles south of the town of Hodgenville on U.S. Highway 31E South. The Boyhood Home Unit at Knob Creek is located approximately 10 miles northeast of the Birthplace Unit of the park.\",\"directionsUrl\":\"http://www.nps.gov/abli/planyourvisit/directions.htm\",\"entranceFees\":[{\"cost\":\"0.00\",\"description\":\"There is no fee associated with visiting either unit of the park.\",\"title\":\"Fee Free Park\"}],\"entrancePasses\":[],\"fees\":[],\"fullName\":\"Abraham Lincoln Birthplace National Historical Park\",\"id\":\"77E0D7F0-1942-494A-ACE2-9004D2BDC59E\",\"images\":[{\"altText\":\"The Memorial Building surrounded by fall colors\",\"caption\":\"Over 200,000 people a year come to walk up the steps of the Memorial Building to visit the site where Abraham Lincoln was born\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building with fall colors\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861078-1DD8-B71B-0B774A242EF6A706.jpg\"},{\"altText\":\"The first memorial erected to honor Abraham Lincoln\",\"caption\":\"The Memorial Building constructed on the traditional site of the birth of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861263-1DD8-B71B-0B71EF9B95F9644F.jpg\"},{\"altText\":\"The symbolic birth cabin on the traditional site of the birth of Abraham Lincoln.\",\"caption\":\"The symbolic birth cabin of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Symbolic Birth Cabin of Abraham Lincoln\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C86137D-1DD8-B71B-0B978BACD7EBAEF1.jpg\"},{\"altText\":\"Statue of the Lincoln family in the park's Visitor Center\",\"caption\":\"Visitors to the park can view the statue of the Lincoln family.\",\"credit\":\"NPS Photo\",\"title\":\"Statue of the Lincoln Family in the Visitor Center\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C8614D1-1DD8-B71B-0B1AF72CA452B051.jpg\"}],\"latLong\":\"lat:37.5858662, long:-85.67330523\",\"latitude\":\"37.5858662\",\"longitude\":\"-85.67330523\",\"name\":\"Abraham Lincoln Birthplace\",\"operatingHours\":[{\"description\":\"The Memorial Building is open 9:00 am - 4:30 pm eastern time with a limited viewing area.  The Visitor Center and grounds of the Birthplace Unit are open 9:00 am - 5:00 pm eastern time with one-way traffic flow and social distancing.\\n\\n\\nThe Boyhood Home Unit at Knob Creek will be closed August 3, 2020, to July 31, 2021, due to the Lincoln Tavern Rehabilitation Project.\",\"exceptions\":[{\"endDate\":\"2022-01-01\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2022-01-01\"},{\"endDate\":\"2021-11-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-11-25\"},{\"endDate\":\"2021-12-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-12-25\"}],\"name\":\"Abraham Lincoln Birthplace National Historical Park\",\"standardHours\":{\"friday\":\"9:00AM - 5:00PM\",\"monday\":\"9:00AM - 5:00PM\",\"saturday\":\"9:00AM - 5:00PM\",\"sunday\":\"9:00AM - 5:00PM\",\"thursday\":\"9:00AM - 5:00PM\",\"tuesday\":\"9:00AM - 5:00PM\",\"wednesday\":\"9:00AM - 5:00PM\"}}],\"parkCode\":\"abli\",\"states\":\"KY\",\"topics\":[{\"id\":\"D10852A3-443C-4743-A5FA-6DD6D2A054B3\",\"name\":\"Birthplace\"},{\"id\":\"F669BC40-BDC4-41C0-9ACE-B2CD25373045\",\"name\":\"Presidents\"},{\"id\":\"0D00073E-18C3-46E5-8727-2F87B112DDC6\",\"name\":\"Animals\"},{\"id\":\"957EF2BD-AC6C-4B7B-BD9A-87593ADC6691\",\"name\":\"Birds\"},{\"id\":\"E25F3456-43ED-45DD-93BC-057F9B944F7A\",\"name\":\"Caves, Caverns and Karst\"},{\"id\":\"F0F97E32-2F29-41B4-AF98-9FBE8DAB36B1\",\"name\":\"Geology\"},{\"id\":\"4BE01DC5-52E6-4F18-8C9A-B22D65965F6D\",\"name\":\"Groundwater\"},{\"id\":\"0E1A04CC-EB51-4F18-93D4-EC0B0B4EC1E3\",\"name\":\"Freshwater Springs\"},{\"id\":\"A7359FC4-DAD8-45F5-AF15-7FF62F816ED3\",\"name\":\"Night Sky\"}],\"url\":\"https://www.nps.gov/abli/index.htm\",\"weatherInfo\":\"There are four distinct seasons in Central Kentucky. However, temperature and weather conditions can vary widely within those seasons. Spring and Fall are generally pleasant with frequent rain showers. Summer is usually hot and humid. Winter is moderately cold with mixed precipitation.\"},{\"activities\":[{\"id\":\"13A57703-BB1A-41A2-94B8-53B692EB7238\",\"name\":\"Astronomy\"},{\"id\":\"D37A0003-8317-4F04-8FB0-4CF0A272E195\",\"name\":\"Stargazing\"},{\"id\":\"1DFACD97-1B9C-4F5A-80F2-05593604799E\",\"name\":\"Food\"},{\"id\":\"C6D3230A-2CEA-4AFE-BFF3-DC1E2C2C4BB4\",\"name\":\"Picnicking\"},{\"id\":\"DF4A35E0-7983-4A3E-BC47-F37B872B0F25\",\"name\":\"Junior Ranger Program\"},{\"id\":\"0B685688-3405-4E2A-ABBA-E3069492EC50\",\"name\":\"Wildlife Watching\"},{\"id\":\"5A2C91D1-50EC-4B24-8BED-A2E11A1892DF\",\"name\":\"Birdwatching\"},{\"id\":\"24380E3F-AD9D-4E38-BF13-C8EEB21893E7\",\"name\":\"Shopping\"},{\"id\":\"467DC8B8-0B7D-436D-A026-80A22358F615\",\"name\":\"Bookstore and Park Store\"},{\"id\":\"43800AD1-D439-40F3-AAB3-9FB651FE45BB\",\"name\":\"Gift Shop and Souvenirs\"}],\"addresses\":[{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Physical\"},{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Mailing\"}],\"contacts\":{\"emailAddresses\":[{\"description\":\"\",\"emailAddress\":\"ABLI_Administration@nps.gov\"}],\"phoneNumbers\":[{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583137\",\"type\":\"Voice\"},{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583874\",\"type\":\"Fax\"}]},\"description\":\"For over a century people from around the world have come to rural Central Kentucky to honor the humble beginnings of our 16th president, Abraham Lincoln. His early life on Kentucky's frontier shaped his character and prepared him to lead the nation through Civil War. The country's first memorial to Lincoln, built with donations from young and old, enshrines the symbolic birthplace cabin.\",\"designation\":\"National Historical Park\",\"directionsInfo\":\"The Birthplace Unit of the park is located approximately 2 miles south of the town of Hodgenville on U.S. Highway 31E South. The Boyhood Home Unit at Knob Creek is located approximately 10 miles northeast of the Birthplace Unit of the park.\",\"directionsUrl\":\"http://www.nps.gov/abli/planyourvisit/directions.htm\",\"entranceFees\":[{\"cost\":\"0.00\",\"description\":\"There is no fee associated with visiting either unit of the park.\",\"title\":\"Fee Free Park\"}],\"entrancePasses\":[],\"fees\":[],\"fullName\":\"Abraham Lincoln Birthplace National Historical Park\",\"id\":\"77E0D7F0-1942-494A-ACE2-9004D2BDC59E\",\"images\":[{\"altText\":\"The Memorial Building surrounded by fall colors\",\"caption\":\"Over 200,000 people a year come to walk up the steps of the Memorial Building to visit the site where Abraham Lincoln was born\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building with fall colors\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861078-1DD8-B71B-0B774A242EF6A706.jpg\"},{\"altText\":\"The first memorial erected to honor Abraham Lincoln\",\"caption\":\"The Memorial Building constructed on the traditional site of the birth of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861263-1DD8-B71B-0B71EF9B95F9644F.jpg\"},{\"altText\":\"The symbolic birth cabin on the traditional site of the birth of Abraham Lincoln.\",\"caption\":\"The symbolic birth cabin of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Symbolic Birth Cabin of Abraham Lincoln\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C86137D-1DD8-B71B-0B978BACD7EBAEF1.jpg\"},{\"altText\":\"Statue of the Lincoln family in the park's Visitor Center\",\"caption\":\"Visitors to the park can view the statue of the Lincoln family.\",\"credit\":\"NPS Photo\",\"title\":\"Statue of the Lincoln Family in the Visitor Center\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C8614D1-1DD8-B71B-0B1AF72CA452B051.jpg\"}],\"latLong\":\"lat:37.5858662, long:-85.67330523\",\"latitude\":\"37.5858662\",\"longitude\":\"-85.67330523\",\"name\":\"Abraham Lincoln Birthplace\",\"operatingHours\":[{\"description\":\"The Memorial Building is open 9:00 am - 4:30 pm eastern time with a limited viewing area.  The Visitor Center and grounds of the Birthplace Unit are open 9:00 am - 5:00 pm eastern time with one-way traffic flow and social distancing.\\n\\n\\nThe Boyhood Home Unit at Knob Creek will be closed August 3, 2020, to July 31, 2021, due to the Lincoln Tavern Rehabilitation Project.\",\"exceptions\":[{\"endDate\":\"2022-01-01\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2022-01-01\"},{\"endDate\":\"2021-11-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-11-25\"},{\"endDate\":\"2021-12-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-12-25\"}],\"name\":\"Abraham Lincoln Birthplace National Historical Park\",\"standardHours\":{\"friday\":\"9:00AM - 5:00PM\",\"monday\":\"9:00AM - 5:00PM\",\"saturday\":\"9:00AM - 5:00PM\",\"sunday\":\"9:00AM - 5:00PM\",\"thursday\":\"9:00AM - 5:00PM\",\"tuesday\":\"9:00AM - 5:00PM\",\"wednesday\":\"9:00AM - 5:00PM\"}}],\"parkCode\":\"abli\",\"states\":\"KY\",\"topics\":[{\"id\":\"D10852A3-443C-4743-A5FA-6DD6D2A054B3\",\"name\":\"Birthplace\"},{\"id\":\"F669BC40-BDC4-41C0-9ACE-B2CD25373045\",\"name\":\"Presidents\"},{\"id\":\"0D00073E-18C3-46E5-8727-2F87B112DDC6\",\"name\":\"Animals\"},{\"id\":\"957EF2BD-AC6C-4B7B-BD9A-87593ADC6691\",\"name\":\"Birds\"},{\"id\":\"E25F3456-43ED-45DD-93BC-057F9B944F7A\",\"name\":\"Caves, Caverns and Karst\"},{\"id\":\"F0F97E32-2F29-41B4-AF98-9FBE8DAB36B1\",\"name\":\"Geology\"},{\"id\":\"4BE01DC5-52E6-4F18-8C9A-B22D65965F6D\",\"name\":\"Groundwater\"},{\"id\":\"0E1A04CC-EB51-4F18-93D4-EC0B0B4EC1E3\",\"name\":\"Freshwater Springs\"},{\"id\":\"A7359FC4-DAD8-45F5-AF15-7FF62F816ED3\",\"name\":\"Night Sky\"}],\"url\":\"https://www.nps.gov/abli/index.htm\",\"weatherInfo\":\"There are four distinct seasons in Central Kentucky. However, temperature and weather conditions can vary widely within those seasons. Spring and Fall are generally pleasant with frequent rain showers. Summer is usually hot and humid. Winter is moderately cold with mixed precipitation.\"},{\"activities\":[{\"id\":\"13A57703-BB1A-41A2-94B8-53B692EB7238\",\"name\":\"Astronomy\"},{\"id\":\"D37A0003-8317-4F04-8FB0-4CF0A272E195\",\"name\":\"Stargazing\"},{\"id\":\"1DFACD97-1B9C-4F5A-80F2-05593604799E\",\"name\":\"Food\"},{\"id\":\"C6D3230A-2CEA-4AFE-BFF3-DC1E2C2C4BB4\",\"name\":\"Picnicking\"},{\"id\":\"DF4A35E0-7983-4A3E-BC47-F37B872B0F25\",\"name\":\"Junior Ranger Program\"},{\"id\":\"0B685688-3405-4E2A-ABBA-E3069492EC50\",\"name\":\"Wildlife Watching\"},{\"id\":\"5A2C91D1-50EC-4B24-8BED-A2E11A1892DF\",\"name\":\"Birdwatching\"},{\"id\":\"24380E3F-AD9D-4E38-BF13-C8EEB21893E7\",\"name\":\"Shopping\"},{\"id\":\"467DC8B8-0B7D-436D-A026-80A22358F615\",\"name\":\"Bookstore and Park Store\"},{\"id\":\"43800AD1-D439-40F3-AAB3-9FB651FE45BB\",\"name\":\"Gift Shop and Souvenirs\"}],\"addresses\":[{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Physical\"},{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Mailing\"}],\"contacts\":{\"emailAddresses\":[{\"description\":\"\",\"emailAddress\":\"ABLI_Administration@nps.gov\"}],\"phoneNumbers\":[{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583137\",\"type\":\"Voice\"},{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583874\",\"type\":\"Fax\"}]},\"description\":\"For over a century people from around the world have come to rural Central Kentucky to honor the humble beginnings of our 16th president, Abraham Lincoln. His early life on Kentucky's frontier shaped his character and prepared him to lead the nation through Civil War. The country's first memorial to Lincoln, built with donations from young and old, enshrines the symbolic birthplace cabin.\",\"designation\":\"National Historical Park\",\"directionsInfo\":\"The Birthplace Unit of the park is located approximately 2 miles south of the town of Hodgenville on U.S. Highway 31E South. The Boyhood Home Unit at Knob Creek is located approximately 10 miles northeast of the Birthplace Unit of the park.\",\"directionsUrl\":\"http://www.nps.gov/abli/planyourvisit/directions.htm\",\"entranceFees\":[{\"cost\":\"0.00\",\"description\":\"There is no fee associated with visiting either unit of the park.\",\"title\":\"Fee Free Park\"}],\"entrancePasses\":[],\"fees\":[],\"fullName\":\"Abraham Lincoln Birthplace National Historical Park\",\"id\":\"77E0D7F0-1942-494A-ACE2-9004D2BDC59E\",\"images\":[{\"altText\":\"The Memorial Building surrounded by fall colors\",\"caption\":\"Over 200,000 people a year come to walk up the steps of the Memorial Building to visit the site where Abraham Lincoln was born\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building with fall colors\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861078-1DD8-B71B-0B774A242EF6A706.jpg\"},{\"altText\":\"The first memorial erected to honor Abraham Lincoln\",\"caption\":\"The Memorial Building constructed on the traditional site of the birth of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861263-1DD8-B71B-0B71EF9B95F9644F.jpg\"},{\"altText\":\"The symbolic birth cabin on the traditional site of the birth of Abraham Lincoln.\",\"caption\":\"The symbolic birth cabin of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Symbolic Birth Cabin of Abraham Lincoln\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C86137D-1DD8-B71B-0B978BACD7EBAEF1.jpg\"},{\"altText\":\"Statue of the Lincoln family in the park's Visitor Center\",\"caption\":\"Visitors to the park can view the statue of the Lincoln family.\",\"credit\":\"NPS Photo\",\"title\":\"Statue of the Lincoln Family in the Visitor Center\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C8614D1-1DD8-B71B-0B1AF72CA452B051.jpg\"}],\"latLong\":\"lat:37.5858662, long:-85.67330523\",\"latitude\":\"37.5858662\",\"longitude\":\"-85.67330523\",\"name\":\"Abraham Lincoln Birthplace\",\"operatingHours\":[{\"description\":\"The Memorial Building is open 9:00 am - 4:30 pm eastern time with a limited viewing area.  The Visitor Center and grounds of the Birthplace Unit are open 9:00 am - 5:00 pm eastern time with one-way traffic flow and social distancing.\\n\\n\\nThe Boyhood Home Unit at Knob Creek will be closed August 3, 2020, to July 31, 2021, due to the Lincoln Tavern Rehabilitation Project.\",\"exceptions\":[{\"endDate\":\"2022-01-01\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2022-01-01\"},{\"endDate\":\"2021-11-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-11-25\"},{\"endDate\":\"2021-12-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-12-25\"}],\"name\":\"Abraham Lincoln Birthplace National Historical Park\",\"standardHours\":{\"friday\":\"9:00AM - 5:00PM\",\"monday\":\"9:00AM - 5:00PM\",\"saturday\":\"9:00AM - 5:00PM\",\"sunday\":\"9:00AM - 5:00PM\",\"thursday\":\"9:00AM - 5:00PM\",\"tuesday\":\"9:00AM - 5:00PM\",\"wednesday\":\"9:00AM - 5:00PM\"}}],\"parkCode\":\"abli\",\"states\":\"KY\",\"topics\":[{\"id\":\"D10852A3-443C-4743-A5FA-6DD6D2A054B3\",\"name\":\"Birthplace\"},{\"id\":\"F669BC40-BDC4-41C0-9ACE-B2CD25373045\",\"name\":\"Presidents\"},{\"id\":\"0D00073E-18C3-46E5-8727-2F87B112DDC6\",\"name\":\"Animals\"},{\"id\":\"957EF2BD-AC6C-4B7B-BD9A-87593ADC6691\",\"name\":\"Birds\"},{\"id\":\"E25F3456-43ED-45DD-93BC-057F9B944F7A\",\"name\":\"Caves, Caverns and Karst\"},{\"id\":\"F0F97E32-2F29-41B4-AF98-9FBE8DAB36B1\",\"name\":\"Geology\"},{\"id\":\"4BE01DC5-52E6-4F18-8C9A-B22D65965F6D\",\"name\":\"Groundwater\"},{\"id\":\"0E1A04CC-EB51-4F18-93D4-EC0B0B4EC1E3\",\"name\":\"Freshwater Springs\"},{\"id\":\"A7359FC4-DAD8-45F5-AF15-7FF62F816ED3\",\"name\":\"Night Sky\"}],\"url\":\"https://www.nps.gov/abli/index.htm\",\"weatherInfo\":\"There are four distinct seasons in Central Kentucky. However, temperature and weather conditions can vary widely within those seasons. Spring and Fall are generally pleasant with frequent rain showers. Summer is usually hot and humid. Winter is moderately cold with mixed precipitation.\"},{\"activities\":[{\"id\":\"13A57703-BB1A-41A2-94B8-53B692EB7238\",\"name\":\"Astronomy\"},{\"id\":\"D37A0003-8317-4F04-8FB0-4CF0A272E195\",\"name\":\"Stargazing\"},{\"id\":\"1DFACD97-1B9C-4F5A-80F2-05593604799E\",\"name\":\"Food\"},{\"id\":\"C6D3230A-2CEA-4AFE-BFF3-DC1E2C2C4BB4\",\"name\":\"Picnicking\"},{\"id\":\"DF4A35E0-7983-4A3E-BC47-F37B872B0F25\",\"name\":\"Junior Ranger Program\"},{\"id\":\"0B685688-3405-4E2A-ABBA-E3069492EC50\",\"name\":\"Wildlife Watching\"},{\"id\":\"5A2C91D1-50EC-4B24-8BED-A2E11A1892DF\",\"name\":\"Birdwatching\"},{\"id\":\"24380E3F-AD9D-4E38-BF13-C8EEB21893E7\",\"name\":\"Shopping\"},{\"id\":\"467DC8B8-0B7D-436D-A026-80A22358F615\",\"name\":\"Bookstore and Park Store\"},{\"id\":\"43800AD1-D439-40F3-AAB3-9FB651FE45BB\",\"name\":\"Gift Shop and Souvenirs\"}],\"addresses\":[{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Physical\"},{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Mailing\"}],\"contacts\":{\"emailAddresses\":[{\"description\":\"\",\"emailAddress\":\"ABLI_Administration@nps.gov\"}],\"phoneNumbers\":[{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583137\",\"type\":\"Voice\"},{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583874\",\"type\":\"Fax\"}]},\"description\":\"For over a century people from around the world have come to rural Central Kentucky to honor the humble beginnings of our 16th president, Abraham Lincoln. His early life on Kentucky's frontier shaped his character and prepared him to lead the nation through Civil War. The country's first memorial to Lincoln, built with donations from young and old, enshrines the symbolic birthplace cabin.\",\"designation\":\"National Historical Park\",\"directionsInfo\":\"The Birthplace Unit of the park is located approximately 2 miles south of the town of Hodgenville on U.S. Highway 31E South. The Boyhood Home Unit at Knob Creek is located approximately 10 miles northeast of the Birthplace Unit of the park.\",\"directionsUrl\":\"http://www.nps.gov/abli/planyourvisit/directions.htm\",\"entranceFees\":[{\"cost\":\"0.00\",\"description\":\"There is no fee associated with visiting either unit of the park.\",\"title\":\"Fee Free Park\"}],\"entrancePasses\":[],\"fees\":[],\"fullName\":\"Abraham Lincoln Birthplace National Historical Park\",\"id\":\"77E0D7F0-1942-494A-ACE2-9004D2BDC59E\",\"images\":[{\"altText\":\"The Memorial Building surrounded by fall colors\",\"caption\":\"Over 200,000 people a year come to walk up the steps of the Memorial Building to visit the site where Abraham Lincoln was born\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building with fall colors\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861078-1DD8-B71B-0B774A242EF6A706.jpg\"},{\"altText\":\"The first memorial erected to honor Abraham Lincoln\",\"caption\":\"The Memorial Building constructed on the traditional site of the birth of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861263-1DD8-B71B-0B71EF9B95F9644F.jpg\"},{\"altText\":\"The symbolic birth cabin on the traditional site of the birth of Abraham Lincoln.\",\"caption\":\"The symbolic birth cabin of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Symbolic Birth Cabin of Abraham Lincoln\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C86137D-1DD8-B71B-0B978BACD7EBAEF1.jpg\"},{\"altText\":\"Statue of the Lincoln family in the park's Visitor Center\",\"caption\":\"Visitors to the park can view the statue of the Lincoln family.\",\"credit\":\"NPS Photo\",\"title\":\"Statue of the Lincoln Family in the Visitor Center\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C8614D1-1DD8-B71B-0B1AF72CA452B051.jpg\"}],\"latLong\":\"lat:37.5858662, long:-85.67330523\",\"latitude\":\"37.5858662\",\"longitude\":\"-85.67330523\",\"name\":\"Abraham Lincoln Birthplace\",\"operatingHours\":[{\"description\":\"The Memorial Building is open 9:00 am - 4:30 pm eastern time with a limited viewing area.  The Visitor Center and grounds of the Birthplace Unit are open 9:00 am - 5:00 pm eastern time with one-way traffic flow and social distancing.\\n\\n\\nThe Boyhood Home Unit at Knob Creek will be closed August 3, 2020, to July 31, 2021, due to the Lincoln Tavern Rehabilitation Project.\",\"exceptions\":[{\"endDate\":\"2022-01-01\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2022-01-01\"},{\"endDate\":\"2021-11-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-11-25\"},{\"endDate\":\"2021-12-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-12-25\"}],\"name\":\"Abraham Lincoln Birthplace National Historical Park\",\"standardHours\":{\"friday\":\"9:00AM - 5:00PM\",\"monday\":\"9:00AM - 5:00PM\",\"saturday\":\"9:00AM - 5:00PM\",\"sunday\":\"9:00AM - 5:00PM\",\"thursday\":\"9:00AM - 5:00PM\",\"tuesday\":\"9:00AM - 5:00PM\",\"wednesday\":\"9:00AM - 5:00PM\"}}],\"parkCode\":\"abli\",\"states\":\"KY\",\"topics\":[{\"id\":\"D10852A3-443C-4743-A5FA-6DD6D2A054B3\",\"name\":\"Birthplace\"},{\"id\":\"F669BC40-BDC4-41C0-9ACE-B2CD25373045\",\"name\":\"Presidents\"},{\"id\":\"0D00073E-18C3-46E5-8727-2F87B112DDC6\",\"name\":\"Animals\"},{\"id\":\"957EF2BD-AC6C-4B7B-BD9A-87593ADC6691\",\"name\":\"Birds\"},{\"id\":\"E25F3456-43ED-45DD-93BC-057F9B944F7A\",\"name\":\"Caves, Caverns and Karst\"},{\"id\":\"F0F97E32-2F29-41B4-AF98-9FBE8DAB36B1\",\"name\":\"Geology\"},{\"id\":\"4BE01DC5-52E6-4F18-8C9A-B22D65965F6D\",\"name\":\"Groundwater\"},{\"id\":\"0E1A04CC-EB51-4F18-93D4-EC0B0B4EC1E3\",\"name\":\"Freshwater Springs\"},{\"id\":\"A7359FC4-DAD8-45F5-AF15-7FF62F816ED3\",\"name\":\"Night Sky\"}],\"url\":\"https://www.nps.gov/abli/index.htm\",\"weatherInfo\":\"There are four distinct seasons in Central Kentucky. However, temperature and weather conditions can vary widely within those seasons. Spring and Fall are generally pleasant with frequent rain showers. Summer is usually hot and humid. Winter is moderately cold with mixed precipitation.\"},{\"activities\":[{\"id\":\"13A57703-BB1A-41A2-94B8-53B692EB7238\",\"name\":\"Astronomy\"},{\"id\":\"D37A0003-8317-4F04-8FB0-4CF0A272E195\",\"name\":\"Stargazing\"},{\"id\":\"1DFACD97-1B9C-4F5A-80F2-05593604799E\",\"name\":\"Food\"},{\"id\":\"C6D3230A-2CEA-4AFE-BFF3-DC1E2C2C4BB4\",\"name\":\"Picnicking\"},{\"id\":\"DF4A35E0-7983-4A3E-BC47-F37B872B0F25\",\"name\":\"Junior Ranger Program\"},{\"id\":\"0B685688-3405-4E2A-ABBA-E3069492EC50\",\"name\":\"Wildlife Watching\"},{\"id\":\"5A2C91D1-50EC-4B24-8BED-A2E11A1892DF\",\"name\":\"Birdwatching\"},{\"id\":\"24380E3F-AD9D-4E38-BF13-C8EEB21893E7\",\"name\":\"Shopping\"},{\"id\":\"467DC8B8-0B7D-436D-A026-80A22358F615\",\"name\":\"Bookstore and Park Store\"},{\"id\":\"43800AD1-D439-40F3-AAB3-9FB651FE45BB\",\"name\":\"Gift Shop and Souvenirs\"}],\"addresses\":[{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Physical\"},{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Mailing\"}],\"contacts\":{\"emailAddresses\":[{\"description\":\"\",\"emailAddress\":\"ABLI_Administration@nps.gov\"}],\"phoneNumbers\":[{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583137\",\"type\":\"Voice\"},{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583874\",\"type\":\"Fax\"}]},\"description\":\"For over a century people from around the world have come to rural Central Kentucky to honor the humble beginnings of our 16th president, Abraham Lincoln. His early life on Kentucky's frontier shaped his character and prepared him to lead the nation through Civil War. The country's first memorial to Lincoln, built with donations from young and old, enshrines the symbolic birthplace cabin.\",\"designation\":\"National Historical Park\",\"directionsInfo\":\"The Birthplace Unit of the park is located approximately 2 miles south of the town of Hodgenville on U.S. Highway 31E South. The Boyhood Home Unit at Knob Creek is located approximately 10 miles northeast of the Birthplace Unit of the park.\",\"directionsUrl\":\"http://www.nps.gov/abli/planyourvisit/directions.htm\",\"entranceFees\":[{\"cost\":\"0.00\",\"description\":\"There is no fee associated with visiting either unit of the park.\",\"title\":\"Fee Free Park\"}],\"entrancePasses\":[],\"fees\":[],\"fullName\":\"Abraham Lincoln Birthplace National Historical Park\",\"id\":\"77E0D7F0-1942-494A-ACE2-9004D2BDC59E\",\"images\":[{\"altText\":\"The Memorial Building surrounded by fall colors\",\"caption\":\"Over 200,000 people a year come to walk up the steps of the Memorial Building to visit the site where Abraham Lincoln was born\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building with fall colors\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861078-1DD8-B71B-0B774A242EF6A706.jpg\"},{\"altText\":\"The first memorial erected to honor Abraham Lincoln\",\"caption\":\"The Memorial Building constructed on the traditional site of the birth of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861263-1DD8-B71B-0B71EF9B95F9644F.jpg\"},{\"altText\":\"The symbolic birth cabin on the traditional site of the birth of Abraham Lincoln.\",\"caption\":\"The symbolic birth cabin of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Symbolic Birth Cabin of Abraham Lincoln\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C86137D-1DD8-B71B-0B978BACD7EBAEF1.jpg\"},{\"altText\":\"Statue of the Lincoln family in the park's Visitor Center\",\"caption\":\"Visitors to the park can view the statue of the Lincoln family.\",\"credit\":\"NPS Photo\",\"title\":\"Statue of the Lincoln Family in the Visitor Center\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C8614D1-1DD8-B71B-0B1AF72CA452B051.jpg\"}],\"latLong\":\"lat:37.5858662, long:-85.67330523\",\"latitude\":\"37.5858662\",\"longitude\":\"-85.67330523\",\"name\":\"Abraham Lincoln Birthplace\",\"operatingHours\":[{\"description\":\"The Memorial Building is open 9:00 am - 4:30 pm eastern time with a limited viewing area.  The Visitor Center and grounds of the Birthplace Unit are open 9:00 am - 5:00 pm eastern time with one-way traffic flow and social distancing.\\n\\n\\nThe Boyhood Home Unit at Knob Creek will be closed August 3, 2020, to July 31, 2021, due to the Lincoln Tavern Rehabilitation Project.\",\"exceptions\":[{\"endDate\":\"2022-01-01\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2022-01-01\"},{\"endDate\":\"2021-11-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-11-25\"},{\"endDate\":\"2021-12-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-12-25\"}],\"name\":\"Abraham Lincoln Birthplace National Historical Park\",\"standardHours\":{\"friday\":\"9:00AM - 5:00PM\",\"monday\":\"9:00AM - 5:00PM\",\"saturday\":\"9:00AM - 5:00PM\",\"sunday\":\"9:00AM - 5:00PM\",\"thursday\":\"9:00AM - 5:00PM\",\"tuesday\":\"9:00AM - 5:00PM\",\"wednesday\":\"9:00AM - 5:00PM\"}}],\"parkCode\":\"abli\",\"states\":\"KY\",\"topics\":[{\"id\":\"D10852A3-443C-4743-A5FA-6DD6D2A054B3\",\"name\":\"Birthplace\"},{\"id\":\"F669BC40-BDC4-41C0-9ACE-B2CD25373045\",\"name\":\"Presidents\"},{\"id\":\"0D00073E-18C3-46E5-8727-2F87B112DDC6\",\"name\":\"Animals\"},{\"id\":\"957EF2BD-AC6C-4B7B-BD9A-87593ADC6691\",\"name\":\"Birds\"},{\"id\":\"E25F3456-43ED-45DD-93BC-057F9B944F7A\",\"name\":\"Caves, Caverns and Karst\"},{\"id\":\"F0F97E32-2F29-41B4-AF98-9FBE8DAB36B1\",\"name\":\"Geology\"},{\"id\":\"4BE01DC5-52E6-4F18-8C9A-B22D65965F6D\",\"name\":\"Groundwater\"},{\"id\":\"0E1A04CC-EB51-4F18-93D4-EC0B0B4EC1E3\",\"name\":\"Freshwater Springs\"},{\"id\":\"A7359FC4-DAD8-45F5-AF15-7FF62F816ED3\",\"name\":\"Night Sky\"}],\"url\":\"https://www.nps.gov/abli/index.htm\",\"weatherInfo\":\"There are four distinct seasons in Central Kentucky. However, temperature and weather conditions can vary widely within those seasons. Spring and Fall are generally pleasant with frequent rain showers. Summer is usually hot and humid. Winter is moderately cold with mixed precipitation.\"},{\"activities\":[{\"id\":\"13A57703-BB1A-41A2-94B8-53B692EB7238\",\"name\":\"Astronomy\"},{\"id\":\"D37A0003-8317-4F04-8FB0-4CF0A272E195\",\"name\":\"Stargazing\"},{\"id\":\"1DFACD97-1B9C-4F5A-80F2-05593604799E\",\"name\":\"Food\"},{\"id\":\"C6D3230A-2CEA-4AFE-BFF3-DC1E2C2C4BB4\",\"name\":\"Picnicking\"},{\"id\":\"DF4A35E0-7983-4A3E-BC47-F37B872B0F25\",\"name\":\"Junior Ranger Program\"},{\"id\":\"0B685688-3405-4E2A-ABBA-E3069492EC50\",\"name\":\"Wildlife Watching\"},{\"id\":\"5A2C91D1-50EC-4B24-8BED-A2E11A1892DF\",\"name\":\"Birdwatching\"},{\"id\":\"24380E3F-AD9D-4E38-BF13-C8EEB21893E7\",\"name\":\"Shopping\"},{\"id\":\"467DC8B8-0B7D-436D-A026-80A22358F615\",\"name\":\"Bookstore and Park Store\"},{\"id\":\"43800AD1-D439-40F3-AAB3-9FB651FE45BB\",\"name\":\"Gift Shop and Souvenirs\"}],\"addresses\":[{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Physical\"},{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Mailing\"}],\"contacts\":{\"emailAddresses\":[{\"description\":\"\",\"emailAddress\":\"ABLI_Administration@nps.gov\"}],\"phoneNumbers\":[{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583137\",\"type\":\"Voice\"},{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583874\",\"type\":\"Fax\"}]},\"description\":\"For over a century people from around the world have come to rural Central Kentucky to honor the humble beginnings of our 16th president, Abraham Lincoln. His early life on Kentucky's frontier shaped his character and prepared him to lead the nation through Civil War. The country's first memorial to Lincoln, built with donations from young and old, enshrines the symbolic birthplace cabin.\",\"designation\":\"National Historical Park\",\"directionsInfo\":\"The Birthplace Unit of the park is located approximately 2 miles south of the town of Hodgenville on U.S. Highway 31E South. The Boyhood Home Unit at Knob Creek is located approximately 10 miles northeast of the Birthplace Unit of the park.\",\"directionsUrl\":\"http://www.nps.gov/abli/planyourvisit/directions.htm\",\"entranceFees\":[{\"cost\":\"0.00\",\"description\":\"There is no fee associated with visiting either unit of the park.\",\"title\":\"Fee Free Park\"}],\"entrancePasses\":[],\"fees\":[],\"fullName\":\"Abraham Lincoln Birthplace National Historical Park\",\"id\":\"77E0D7F0-1942-494A-ACE2-9004D2BDC59E\",\"images\":[{\"altText\":\"The Memorial Building surrounded by fall colors\",\"caption\":\"Over 200,000 people a year come to walk up the steps of the Memorial Building to visit the site where Abraham Lincoln was born\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building with fall colors\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861078-1DD8-B71B-0B774A242EF6A706.jpg\"},{\"altText\":\"The first memorial erected to honor Abraham Lincoln\",\"caption\":\"The Memorial Building constructed on the traditional site of the birth of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861263-1DD8-B71B-0B71EF9B95F9644F.jpg\"},{\"altText\":\"The symbolic birth cabin on the traditional site of the birth of Abraham Lincoln.\",\"caption\":\"The symbolic birth cabin of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Symbolic Birth Cabin of Abraham Lincoln\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C86137D-1DD8-B71B-0B978BACD7EBAEF1.jpg\"},{\"altText\":\"Statue of the Lincoln family in the park's Visitor Center\",\"caption\":\"Visitors to the park can view the statue of the Lincoln family.\",\"credit\":\"NPS Photo\",\"title\":\"Statue of the Lincoln Family in the Visitor Center\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C8614D1-1DD8-B71B-0B1AF72CA452B051.jpg\"}],\"latLong\":\"lat:37.5858662, long:-85.67330523\",\"latitude\":\"37.5858662\",\"longitude\":\"-85.67330523\",\"name\":\"Abraham Lincoln Birthplace\",\"operatingHours\":[{\"description\":\"The Memorial Building is open 9:00 am - 4:30 pm eastern time with a limited viewing area.  The Visitor Center and grounds of the Birthplace Unit are open 9:00 am - 5:00 pm eastern time with one-way traffic flow and social distancing.\\n\\n\\nThe Boyhood Home Unit at Knob Creek will be closed August 3, 2020, to July 31, 2021, due to the Lincoln Tavern Rehabilitation Project.\",\"exceptions\":[{\"endDate\":\"2022-01-01\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2022-01-01\"},{\"endDate\":\"2021-11-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-11-25\"},{\"endDate\":\"2021-12-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-12-25\"}],\"name\":\"Abraham Lincoln Birthplace National Historical Park\",\"standardHours\":{\"friday\":\"9:00AM - 5:00PM\",\"monday\":\"9:00AM - 5:00PM\",\"saturday\":\"9:00AM - 5:00PM\",\"sunday\":\"9:00AM - 5:00PM\",\"thursday\":\"9:00AM - 5:00PM\",\"tuesday\":\"9:00AM - 5:00PM\",\"wednesday\":\"9:00AM - 5:00PM\"}}],\"parkCode\":\"abli\",\"states\":\"KY\",\"topics\":[{\"id\":\"D10852A3-443C-4743-A5FA-6DD6D2A054B3\",\"name\":\"Birthplace\"},{\"id\":\"F669BC40-BDC4-41C0-9ACE-B2CD25373045\",\"name\":\"Presidents\"},{\"id\":\"0D00073E-18C3-46E5-8727-2F87B112DDC6\",\"name\":\"Animals\"},{\"id\":\"957EF2BD-AC6C-4B7B-BD9A-87593ADC6691\",\"name\":\"Birds\"},{\"id\":\"E25F3456-43ED-45DD-93BC-057F9B944F7A\",\"name\":\"Caves, Caverns and Karst\"},{\"id\":\"F0F97E32-2F29-41B4-AF98-9FBE8DAB36B1\",\"name\":\"Geology\"},{\"id\":\"4BE01DC5-52E6-4F18-8C9A-B22D65965F6D\",\"name\":\"Groundwater\"},{\"id\":\"0E1A04CC-EB51-4F18-93D4-EC0B0B4EC1E3\",\"name\":\"Freshwater Springs\"},{\"id\":\"A7359FC4-DAD8-45F5-AF15-7FF62F816ED3\",\"name\":\"Night Sky\"}],\"url\":\"https://www.nps.gov/abli/index.htm\",\"weatherInfo\":\"There are four distinct seasons in Central Kentucky. However, temperature and weather conditions can vary widely within those seasons. Spring and Fall are generally pleasant with frequent rain showers. Summer is usually hot and humid. Winter is moderately cold with mixed precipitation.\"},{\"activities\":[{\"id\":\"13A57703-BB1A-41A2-94B8-53B692EB7238\",\"name\":\"Astronomy\"},{\"id\":\"D37A0003-8317-4F04-8FB0-4CF0A272E195\",\"name\":\"Stargazing\"},{\"id\":\"1DFACD97-1B9C-4F5A-80F2-05593604799E\",\"name\":\"Food\"},{\"id\":\"C6D3230A-2CEA-4AFE-BFF3-DC1E2C2C4BB4\",\"name\":\"Picnicking\"},{\"id\":\"DF4A35E0-7983-4A3E-BC47-F37B872B0F25\",\"name\":\"Junior Ranger Program\"},{\"id\":\"0B685688-3405-4E2A-ABBA-E3069492EC50\",\"name\":\"Wildlife Watching\"},{\"id\":\"5A2C91D1-50EC-4B24-8BED-A2E11A1892DF\",\"name\":\"Birdwatching\"},{\"id\":\"24380E3F-AD9D-4E38-BF13-C8EEB21893E7\",\"name\":\"Shopping\"},{\"id\":\"467DC8B8-0B7D-436D-A026-80A22358F615\",\"name\":\"Bookstore and Park Store\"},{\"id\":\"43800AD1-D439-40F3-AAB3-9FB651FE45BB\",\"name\":\"Gift Shop and Souvenirs\"}],\"addresses\":[{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Physical\"},{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Mailing\"}],\"contacts\":{\"emailAddresses\":[{\"description\":\"\",\"emailAddress\":\"ABLI_Administration@nps.gov\"}],\"phoneNumbers\":[{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583137\",\"type\":\"Voice\"},{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583874\",\"type\":\"Fax\"}]},\"description\":\"For over a century people from around the world have come to rural Central Kentucky to honor the humble beginnings of our 16th president, Abraham Lincoln. His early life on Kentucky's frontier shaped his character and prepared him to lead the nation through Civil War. The country's first memorial to Lincoln, built with donations from young and old, enshrines the symbolic birthplace cabin.\",\"designation\":\"National Historical Park\",\"directionsInfo\":\"The Birthplace Unit of the park is located approximately 2 miles south of the town of Hodgenville on U.S. Highway 31E South. The Boyhood Home Unit at Knob Creek is located approximately 10 miles northeast of the Birthplace Unit of the park.\",\"directionsUrl\":\"http://www.nps.gov/abli/planyourvisit/directions.htm\",\"entranceFees\":[{\"cost\":\"0.00\",\"description\":\"There is no fee associated with visiting either unit of the park.\",\"title\":\"Fee Free Park\"}],\"entrancePasses\":[],\"fees\":[],\"fullName\":\"Abraham Lincoln Birthplace National Historical Park\",\"id\":\"77E0D7F0-1942-494A-ACE2-9004D2BDC59E\",\"images\":[{\"altText\":\"The Memorial Building surrounded by fall colors\",\"caption\":\"Over 200,000 people a year come to walk up the steps of the Memorial Building to visit the site where Abraham Lincoln was born\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building with fall colors\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861078-1DD8-B71B-0B774A242EF6A706.jpg\"},{\"altText\":\"The first memorial erected to honor Abraham Lincoln\",\"caption\":\"The Memorial Building constructed on the traditional site of the birth of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861263-1DD8-B71B-0B71EF9B95F9644F.jpg\"},{\"altText\":\"The symbolic birth cabin on the traditional site of the birth of Abraham Lincoln.\",\"caption\":\"The symbolic birth cabin of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Symbolic Birth Cabin of Abraham Lincoln\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C86137D-1DD8-B71B-0B978BACD7EBAEF1.jpg\"},{\"altText\":\"Statue of the Lincoln family in the park's Visitor Center\",\"caption\":\"Visitors to the park can view the statue of the Lincoln family.\",\"credit\":\"NPS Photo\",\"title\":\"Statue of the Lincoln Family in the Visitor Center\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C8614D1-1DD8-B71B-0B1AF72CA452B051.jpg\"}],\"latLong\":\"lat:37.5858662, long:-85.67330523\",\"latitude\":\"37.5858662\",\"longitude\":\"-85.67330523\",\"name\":\"Abraham Lincoln Birthplace\",\"operatingHours\":[{\"description\":\"The Memorial Building is open 9:00 am - 4:30 pm eastern time with a limited viewing area.  The Visitor Center and grounds of the Birthplace Unit are open 9:00 am - 5:00 pm eastern time with one-way traffic flow and social distancing.\\n\\n\\nThe Boyhood Home Unit at Knob Creek will be closed August 3, 2020, to July 31, 2021, due to the Lincoln Tavern Rehabilitation Project.\",\"exceptions\":[{\"endDate\":\"2022-01-01\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2022-01-01\"},{\"endDate\":\"2021-11-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-11-25\"},{\"endDate\":\"2021-12-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-12-25\"}],\"name\":\"Abraham Lincoln Birthplace National Historical Park\",\"standardHours\":{\"friday\":\"9:00AM - 5:00PM\",\"monday\":\"9:00AM - 5:00PM\",\"saturday\":\"9:00AM - 5:00PM\",\"sunday\":\"9:00AM - 5:00PM\",\"thursday\":\"9:00AM - 5:00PM\",\"tuesday\":\"9:00AM - 5:00PM\",\"wednesday\":\"9:00AM - 5:00PM\"}}],\"parkCode\":\"abli\",\"states\":\"KY\",\"topics\":[{\"id\":\"D10852A3-443C-4743-A5FA-6DD6D2A054B3\",\"name\":\"Birthplace\"},{\"id\":\"F669BC40-BDC4-41C0-9ACE-B2CD25373045\",\"name\":\"Presidents\"},{\"id\":\"0D00073E-18C3-46E5-8727-2F87B112DDC6\",\"name\":\"Animals\"},{\"id\":\"957EF2BD-AC6C-4B7B-BD9A-87593ADC6691\",\"name\":\"Birds\"},{\"id\":\"E25F3456-43ED-45DD-93BC-057F9B944F7A\",\"name\":\"Caves, Caverns and Karst\"},{\"id\":\"F0F97E32-2F29-41B4-AF98-9FBE8DAB36B1\",\"name\":\"Geology\"},{\"id\":\"4BE01DC5-52E6-4F18-8C9A-B22D65965F6D\",\"name\":\"Groundwater\"},{\"id\":\"0E1A04CC-EB51-4F18-93D4-EC0B0B4EC1E3\",\"name\":\"Freshwater Springs\"},{\"id\":\"A7359FC4-DAD8-45F5-AF15-7FF62F816ED3\",\"name\":\"Night Sky\"}],\"url\":\"https://www.nps.gov/abli/index.htm\",\"weatherInfo\":\"There are four distinct seasons in Central Kentucky. However, temperature and weather conditions can vary widely within those seasons. Spring and Fall are generally pleasant with frequent rain showers. Summer is usually hot and humid. Winter is moderately cold with mixed precipitation.\"},{\"activities\":[{\"id\":\"13A57703-BB1A-41A2-94B8-53B692EB7238\",\"name\":\"Astronomy\"},{\"id\":\"D37A0003-8317-4F04-8FB0-4CF0A272E195\",\"name\":\"Stargazing\"},{\"id\":\"1DFACD97-1B9C-4F5A-80F2-05593604799E\",\"name\":\"Food\"},{\"id\":\"C6D3230A-2CEA-4AFE-BFF3-DC1E2C2C4BB4\",\"name\":\"Picnicking\"},{\"id\":\"DF4A35E0-7983-4A3E-BC47-F37B872B0F25\",\"name\":\"Junior Ranger Program\"},{\"id\":\"0B685688-3405-4E2A-ABBA-E3069492EC50\",\"name\":\"Wildlife Watching\"},{\"id\":\"5A2C91D1-50EC-4B24-8BED-A2E11A1892DF\",\"name\":\"Birdwatching\"},{\"id\":\"24380E3F-AD9D-4E38-BF13-C8EEB21893E7\",\"name\":\"Shopping\"},{\"id\":\"467DC8B8-0B7D-436D-A026-80A22358F615\",\"name\":\"Bookstore and Park Store\"},{\"id\":\"43800AD1-D439-40F3-AAB3-9FB651FE45BB\",\"name\":\"Gift Shop and Souvenirs\"}],\"addresses\":[{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Physical\"},{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Mailing\"}],\"contacts\":{\"emailAddresses\":[{\"description\":\"\",\"emailAddress\":\"ABLI_Administration@nps.gov\"}],\"phoneNumbers\":[{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583137\",\"type\":\"Voice\"},{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583874\",\"type\":\"Fax\"}]},\"description\":\"For over a century people from around the world have come to rural Central Kentucky to honor the humble beginnings of our 16th president, Abraham Lincoln. His early life on Kentucky's frontier shaped his character and prepared him to lead the nation through Civil War. The country's first memorial to Lincoln, built with donations from young and old, enshrines the symbolic birthplace cabin.\",\"designation\":\"National Historical Park\",\"directionsInfo\":\"The Birthplace Unit of the park is located approximately 2 miles south of the town of Hodgenville on U.S. Highway 31E South. The Boyhood Home Unit at Knob Creek is located approximately 10 miles northeast of the Birthplace Unit of the park.\",\"directionsUrl\":\"http://www.nps.gov/abli/planyourvisit/directions.htm\",\"entranceFees\":[{\"cost\":\"0.00\",\"description\":\"There is no fee associated with visiting either unit of the park.\",\"title\":\"Fee Free Park\"}],\"entrancePasses\":[],\"fees\":[],\"fullName\":\"Abraham Lincoln Birthplace National Historical Park\",\"id\":\"77E0D7F0-1942-494A-ACE2-9004D2BDC59E\",\"images\":[{\"altText\":\"The Memorial Building surrounded by fall colors\",\"caption\":\"Over 200,000 people a year come to walk up the steps of the Memorial Building to visit the site where Abraham Lincoln was born\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building with fall colors\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861078-1DD8-B71B-0B774A242EF6A706.jpg\"},{\"altText\":\"The first memorial erected to honor Abraham Lincoln\",\"caption\":\"The Memorial Building constructed on the traditional site of the birth of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861263-1DD8-B71B-0B71EF9B95F9644F.jpg\"},{\"altText\":\"The symbolic birth cabin on the traditional site of the birth of Abraham Lincoln.\",\"caption\":\"The symbolic birth cabin of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Symbolic Birth Cabin of Abraham Lincoln\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C86137D-1DD8-B71B-0B978BACD7EBAEF1.jpg\"},{\"altText\":\"Statue of the Lincoln family in the park's Visitor Center\",\"caption\":\"Visitors to the park can view the statue of the Lincoln family.\",\"credit\":\"NPS Photo\",\"title\":\"Statue of the Lincoln Family in the Visitor Center\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C8614D1-1DD8-B71B-0B1AF72CA452B051.jpg\"}],\"latLong\":\"lat:37.5858662, long:-85.67330523\",\"latitude\":\"37.5858662\",\"longitude\":\"-85.67330523\",\"name\":\"Abraham Lincoln Birthplace\",\"operatingHours\":[{\"description\":\"The Memorial Building is open 9:00 am - 4:30 pm eastern time with a limited viewing area.  The Visitor Center and grounds of the Birthplace Unit are open 9:00 am - 5:00 pm eastern time with one-way traffic flow and social distancing.\\n\\n\\nThe Boyhood Home Unit at Knob Creek will be closed August 3, 2020, to July 31, 2021, due to the Lincoln Tavern Rehabilitation Project.\",\"exceptions\":[{\"endDate\":\"2022-01-01\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2022-01-01\"},{\"endDate\":\"2021-11-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-11-25\"},{\"endDate\":\"2021-12-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-12-25\"}],\"name\":\"Abraham Lincoln Birthplace National Historical Park\",\"standardHours\":{\"friday\":\"9:00AM - 5:00PM\",\"monday\":\"9:00AM - 5:00PM\",\"saturday\":\"9:00AM - 5:00PM\",\"sunday\":\"9:00AM - 5:00PM\",\"thursday\":\"9:00AM - 5:00PM\",\"tuesday\":\"9:00AM - 5:00PM\",\"wednesday\":\"9:00AM - 5:00PM\"}}],\"parkCode\":\"abli\",\"states\":\"KY\",\"topics\":[{\"id\":\"D10852A3-443C-4743-A5FA-6DD6D2A054B3\",\"name\":\"Birthplace\"},{\"id\":\"F669BC40-BDC4-41C0-9ACE-B2CD25373045\",\"name\":\"Presidents\"},{\"id\":\"0D00073E-18C3-46E5-8727-2F87B112DDC6\",\"name\":\"Animals\"},{\"id\":\"957EF2BD-AC6C-4B7B-BD9A-87593ADC6691\",\"name\":\"Birds\"},{\"id\":\"E25F3456-43ED-45DD-93BC-057F9B944F7A\",\"name\":\"Caves, Caverns and Karst\"},{\"id\":\"F0F97E32-2F29-41B4-AF98-9FBE8DAB36B1\",\"name\":\"Geology\"},{\"id\":\"4BE01DC5-52E6-4F18-8C9A-B22D65965F6D\",\"name\":\"Groundwater\"},{\"id\":\"0E1A04CC-EB51-4F18-93D4-EC0B0B4EC1E3\",\"name\":\"Freshwater Springs\"},{\"id\":\"A7359FC4-DAD8-45F5-AF15-7FF62F816ED3\",\"name\":\"Night Sky\"}],\"url\":\"https://www.nps.gov/abli/index.htm\",\"weatherInfo\":\"There are four distinct seasons in Central Kentucky. However, temperature and weather conditions can vary widely within those seasons. Spring and Fall are generally pleasant with frequent rain showers. Summer is usually hot and humid. Winter is moderately cold with mixed precipitation.\"},{\"activities\":[{\"id\":\"13A57703-BB1A-41A2-94B8-53B692EB7238\",\"name\":\"Astronomy\"},{\"id\":\"D37A0003-8317-4F04-8FB0-4CF0A272E195\",\"name\":\"Stargazing\"},{\"id\":\"1DFACD97-1B9C-4F5A-80F2-05593604799E\",\"name\":\"Food\"},{\"id\":\"C6D3230A-2CEA-4AFE-BFF3-DC1E2C2C4BB4\",\"name\":\"Picnicking\"},{\"id\":\"DF4A35E0-7983-4A3E-BC47-F37B872B0F25\",\"name\":\"Junior Ranger Program\"},{\"id\":\"0B685688-3405-4E2A-ABBA-E3069492EC50\",\"name\":\"Wildlife Watching\"},{\"id\":\"5A2C91D1-50EC-4B24-8BED-A2E11A1892DF\",\"name\":\"Birdwatching\"},{\"id\":\"24380E3F-AD9D-4E38-BF13-C8EEB21893E7\",\"name\":\"Shopping\"},{\"id\":\"467DC8B8-0B7D-436D-A026-80A22358F615\",\"name\":\"Bookstore and Park Store\"},{\"id\":\"43800AD1-D439-40F3-AAB3-9FB651FE45BB\",\"name\":\"Gift Shop and Souvenirs\"}],\"addresses\":[{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Physical\"},{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Mailing\"}],\"contacts\":{\"emailAddresses\":[{\"description\":\"\",\"emailAddress\":\"ABLI_Administration@nps.gov\"}],\"phoneNumbers\":[{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583137\",\"type\":\"Voice\"},{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583874\",\"type\":\"Fax\"}]},\"description\":\"For over a century people from around the world have come to rural Central Kentucky to honor the humble beginnings of our 16th president, Abraham Lincoln. His early life on Kentucky's frontier shaped his character and prepared him to lead the nation through Civil War. The country's first memorial to Lincoln, built with donations from young and old, enshrines the symbolic birthplace cabin.\",\"designation\":\"National Historical Park\",\"directionsInfo\":\"The Birthplace Unit of the park is located approximately 2 miles south of the town of Hodgenville on U.S. Highway 31E South. The Boyhood Home Unit at Knob Creek is located approximately 10 miles northeast of the Birthplace Unit of the park.\",\"directionsUrl\":\"http://www.nps.gov/abli/planyourvisit/directions.htm\",\"entranceFees\":[{\"cost\":\"0.00\",\"description\":\"There is no fee associated with visiting either unit of the park.\",\"title\":\"Fee Free Park\"}],\"entrancePasses\":[],\"fees\":[],\"fullName\":\"Abraham Lincoln Birthplace National Historical Park\",\"id\":\"77E0D7F0-1942-494A-ACE2-9004D2BDC59E\",\"images\":[{\"altText\":\"The Memorial Building surrounded by fall colors\",\"caption\":\"Over 200,000 people a year come to walk up the steps of the Memorial Building to visit the site where Abraham Lincoln was born\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building with fall colors\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861078-1DD8-B71B-0B774A242EF6A706.jpg\"},{\"altText\":\"The first memorial erected to honor Abraham Lincoln\",\"caption\":\"The Memorial Building constructed on the traditional site of the birth of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861263-1DD8-B71B-0B71EF9B95F9644F.jpg\"},{\"altText\":\"The symbolic birth cabin on the traditional site of the birth of Abraham Lincoln.\",\"caption\":\"The symbolic birth cabin of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Symbolic Birth Cabin of Abraham Lincoln\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C86137D-1DD8-B71B-0B978BACD7EBAEF1.jpg\"},{\"altText\":\"Statue of the Lincoln family in the park's Visitor Center\",\"caption\":\"Visitors to the park can view the statue of the Lincoln family.\",\"credit\":\"NPS Photo\",\"title\":\"Statue of the Lincoln Family in the Visitor Center\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C8614D1-1DD8-B71B-0B1AF72CA452B051.jpg\"}],\"latLong\":\"lat:37.5858662, long:-85.67330523\",\"latitude\":\"37.5858662\",\"longitude\":\"-85.67330523\",\"name\":\"Abraham Lincoln Birthplace\",\"operatingHours\":[{\"description\":\"The Memorial Building is open 9:00 am - 4:30 pm eastern time with a limited viewing area.  The Visitor Center and grounds of the Birthplace Unit are open 9:00 am - 5:00 pm eastern time with one-way traffic flow and social distancing.\\n\\n\\nThe Boyhood Home Unit at Knob Creek will be closed August 3, 2020, to July 31, 2021, due to the Lincoln Tavern Rehabilitation Project.\",\"exceptions\":[{\"endDate\":\"2022-01-01\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2022-01-01\"},{\"endDate\":\"2021-11-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-11-25\"},{\"endDate\":\"2021-12-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-12-25\"}],\"name\":\"Abraham Lincoln Birthplace National Historical Park\",\"standardHours\":{\"friday\":\"9:00AM - 5:00PM\",\"monday\":\"9:00AM - 5:00PM\",\"saturday\":\"9:00AM - 5:00PM\",\"sunday\":\"9:00AM - 5:00PM\",\"thursday\":\"9:00AM - 5:00PM\",\"tuesday\":\"9:00AM - 5:00PM\",\"wednesday\":\"9:00AM - 5:00PM\"}}],\"parkCode\":\"abli\",\"states\":\"KY\",\"topics\":[{\"id\":\"D10852A3-443C-4743-A5FA-6DD6D2A054B3\",\"name\":\"Birthplace\"},{\"id\":\"F669BC40-BDC4-41C0-9ACE-B2CD25373045\",\"name\":\"Presidents\"},{\"id\":\"0D00073E-18C3-46E5-8727-2F87B112DDC6\",\"name\":\"Animals\"},{\"id\":\"957EF2BD-AC6C-4B7B-BD9A-87593ADC6691\",\"name\":\"Birds\"},{\"id\":\"E25F3456-43ED-45DD-93BC-057F9B944F7A\",\"name\":\"Caves, Caverns and Karst\"},{\"id\":\"F0F97E32-2F29-41B4-AF98-9FBE8DAB36B1\",\"name\":\"Geology\"},{\"id\":\"4BE01DC5-52E6-4F18-8C9A-B22D65965F6D\",\"name\":\"Groundwater\"},{\"id\":\"0E1A04CC-EB51-4F18-93D4-EC0B0B4EC1E3\",\"name\":\"Freshwater Springs\"},{\"id\":\"A7359FC4-DAD8-45F5-AF15-7FF62F816ED3\",\"name\":\"Night Sky\"}],\"url\":\"https://www.nps.gov/abli/index.htm\",\"weatherInfo\":\"There are four distinct seasons in Central Kentucky. However, temperature and weather conditions can vary widely within those seasons. Spring and Fall are generally pleasant with frequent rain showers. Summer is usually hot and humid. Winter is moderately cold with mixed precipitation.\"},{\"activities\":[{\"id\":\"13A57703-BB1A-41A2-94B8-53B692EB7238\",\"name\":\"Astronomy\"},{\"id\":\"D37A0003-8317-4F04-8FB0-4CF0A272E195\",\"name\":\"Stargazing\"},{\"id\":\"1DFACD97-1B9C-4F5A-80F2-05593604799E\",\"name\":\"Food\"},{\"id\":\"C6D3230A-2CEA-4AFE-BFF3-DC1E2C2C4BB4\",\"name\":\"Picnicking\"},{\"id\":\"DF4A35E0-7983-4A3E-BC47-F37B872B0F25\",\"name\":\"Junior Ranger Program\"},{\"id\":\"0B685688-3405-4E2A-ABBA-E3069492EC50\",\"name\":\"Wildlife Watching\"},{\"id\":\"5A2C91D1-50EC-4B24-8BED-A2E11A1892DF\",\"name\":\"Birdwatching\"},{\"id\":\"24380E3F-AD9D-4E38-BF13-C8EEB21893E7\",\"name\":\"Shopping\"},{\"id\":\"467DC8B8-0B7D-436D-A026-80A22358F615\",\"name\":\"Bookstore and Park Store\"},{\"id\":\"43800AD1-D439-40F3-AAB3-9FB651FE45BB\",\"name\":\"Gift Shop and Souvenirs\"}],\"addresses\":[{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Physical\"},{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Mailing\"}],\"contacts\":{\"emailAddresses\":[{\"description\":\"\",\"emailAddress\":\"ABLI_Administration@nps.gov\"}],\"phoneNumbers\":[{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583137\",\"type\":\"Voice\"},{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583874\",\"type\":\"Fax\"}]},\"description\":\"For over a century people from around the world have come to rural Central Kentucky to honor the humble beginnings of our 16th president, Abraham Lincoln. His early life on Kentucky's frontier shaped his character and prepared him to lead the nation through Civil War. The country's first memorial to Lincoln, built with donations from young and old, enshrines the symbolic birthplace cabin.\",\"designation\":\"National Historical Park\",\"directionsInfo\":\"The Birthplace Unit of the park is located approximately 2 miles south of the town of Hodgenville on U.S. Highway 31E South. The Boyhood Home Unit at Knob Creek is located approximately 10 miles northeast of the Birthplace Unit of the park.\",\"directionsUrl\":\"http://www.nps.gov/abli/planyourvisit/directions.htm\",\"entranceFees\":[{\"cost\":\"0.00\",\"description\":\"There is no fee associated with visiting either unit of the park.\",\"title\":\"Fee Free Park\"}],\"entrancePasses\":[],\"fees\":[],\"fullName\":\"Abraham Lincoln Birthplace National Historical Park\",\"id\":\"77E0D7F0-1942-494A-ACE2-9004D2BDC59E\",\"images\":[{\"altText\":\"The Memorial Building surrounded by fall colors\",\"caption\":\"Over 200,000 people a year come to walk up the steps of the Memorial Building to visit the site where Abraham Lincoln was born\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building with fall colors\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861078-1DD8-B71B-0B774A242EF6A706.jpg\"},{\"altText\":\"The first memorial erected to honor Abraham Lincoln\",\"caption\":\"The Memorial Building constructed on the traditional site of the birth of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861263-1DD8-B71B-0B71EF9B95F9644F.jpg\"},{\"altText\":\"The symbolic birth cabin on the traditional site of the birth of Abraham Lincoln.\",\"caption\":\"The symbolic birth cabin of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Symbolic Birth Cabin of Abraham Lincoln\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C86137D-1DD8-B71B-0B978BACD7EBAEF1.jpg\"},{\"altText\":\"Statue of the Lincoln family in the park's Visitor Center\",\"caption\":\"Visitors to the park can view the statue of the Lincoln family.\",\"credit\":\"NPS Photo\",\"title\":\"Statue of the Lincoln Family in the Visitor Center\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C8614D1-1DD8-B71B-0B1AF72CA452B051.jpg\"}],\"latLong\":\"lat:37.5858662, long:-85.67330523\",\"latitude\":\"37.5858662\",\"longitude\":\"-85.67330523\",\"name\":\"Abraham Lincoln Birthplace\",\"operatingHours\":[{\"description\":\"The Memorial Building is open 9:00 am - 4:30 pm eastern time with a limited viewing area.  The Visitor Center and grounds of the Birthplace Unit are open 9:00 am - 5:00 pm eastern time with one-way traffic flow and social distancing.\\n\\n\\nThe Boyhood Home Unit at Knob Creek will be closed August 3, 2020, to July 31, 2021, due to the Lincoln Tavern Rehabilitation Project.\",\"exceptions\":[{\"endDate\":\"2022-01-01\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2022-01-01\"},{\"endDate\":\"2021-11-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-11-25\"},{\"endDate\":\"2021-12-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-12-25\"}],\"name\":\"Abraham Lincoln Birthplace National Historical Park\",\"standardHours\":{\"friday\":\"9:00AM - 5:00PM\",\"monday\":\"9:00AM - 5:00PM\",\"saturday\":\"9:00AM - 5:00PM\",\"sunday\":\"9:00AM - 5:00PM\",\"thursday\":\"9:00AM - 5:00PM\",\"tuesday\":\"9:00AM - 5:00PM\",\"wednesday\":\"9:00AM - 5:00PM\"}}],\"parkCode\":\"abli\",\"states\":\"KY\",\"topics\":[{\"id\":\"D10852A3-443C-4743-A5FA-6DD6D2A054B3\",\"name\":\"Birthplace\"},{\"id\":\"F669BC40-BDC4-41C0-9ACE-B2CD25373045\",\"name\":\"Presidents\"},{\"id\":\"0D00073E-18C3-46E5-8727-2F87B112DDC6\",\"name\":\"Animals\"},{\"id\":\"957EF2BD-AC6C-4B7B-BD9A-87593ADC6691\",\"name\":\"Birds\"},{\"id\":\"E25F3456-43ED-45DD-93BC-057F9B944F7A\",\"name\":\"Caves, Caverns and Karst\"},{\"id\":\"F0F97E32-2F29-41B4-AF98-9FBE8DAB36B1\",\"name\":\"Geology\"},{\"id\":\"4BE01DC5-52E6-4F18-8C9A-B22D65965F6D\",\"name\":\"Groundwater\"},{\"id\":\"0E1A04CC-EB51-4F18-93D4-EC0B0B4EC1E3\",\"name\":\"Freshwater Springs\"},{\"id\":\"A7359FC4-DAD8-45F5-AF15-7FF62F816ED3\",\"name\":\"Night Sky\"}],\"url\":\"https://www.nps.gov/abli/index.htm\",\"weatherInfo\":\"There are four distinct seasons in Central Kentucky. However, temperature and weather conditions can vary widely within those seasons. Spring and Fall are generally pleasant with frequent rain showers. Summer is usually hot and humid. Winter is moderately cold with mixed precipitation.\"},{\"activities\":[{\"id\":\"13A57703-BB1A-41A2-94B8-53B692EB7238\",\"name\":\"Astronomy\"},{\"id\":\"D37A0003-8317-4F04-8FB0-4CF0A272E195\",\"name\":\"Stargazing\"},{\"id\":\"1DFACD97-1B9C-4F5A-80F2-05593604799E\",\"name\":\"Food\"},{\"id\":\"C6D3230A-2CEA-4AFE-BFF3-DC1E2C2C4BB4\",\"name\":\"Picnicking\"},{\"id\":\"DF4A35E0-7983-4A3E-BC47-F37B872B0F25\",\"name\":\"Junior Ranger Program\"},{\"id\":\"0B685688-3405-4E2A-ABBA-E3069492EC50\",\"name\":\"Wildlife Watching\"},{\"id\":\"5A2C91D1-50EC-4B24-8BED-A2E11A1892DF\",\"name\":\"Birdwatching\"},{\"id\":\"24380E3F-AD9D-4E38-BF13-C8EEB21893E7\",\"name\":\"Shopping\"},{\"id\":\"467DC8B8-0B7D-436D-A026-80A22358F615\",\"name\":\"Bookstore and Park Store\"},{\"id\":\"43800AD1-D439-40F3-AAB3-9FB651FE45BB\",\"name\":\"Gift Shop and Souvenirs\"}],\"addresses\":[{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Physical\"},{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Mailing\"}],\"contacts\":{\"emailAddresses\":[{\"description\":\"\",\"emailAddress\":\"ABLI_Administration@nps.gov\"}],\"phoneNumbers\":[{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583137\",\"type\":\"Voice\"},{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583874\",\"type\":\"Fax\"}]},\"description\":\"For over a century people from around the world have come to rural Central Kentucky to honor the humble beginnings of our 16th president, Abraham Lincoln. His early life on Kentucky's frontier shaped his character and prepared him to lead the nation through Civil War. The country's first memorial to Lincoln, built with donations from young and old, enshrines the symbolic birthplace cabin.\",\"designation\":\"National Historical Park\",\"directionsInfo\":\"The Birthplace Unit of the park is located approximately 2 miles south of the town of Hodgenville on U.S. Highway 31E South. The Boyhood Home Unit at Knob Creek is located approximately 10 miles northeast of the Birthplace Unit of the park.\",\"directionsUrl\":\"http://www.nps.gov/abli/planyourvisit/directions.htm\",\"entranceFees\":[{\"cost\":\"0.00\",\"description\":\"There is no fee associated with visiting either unit of the park.\",\"title\":\"Fee Free Park\"}],\"entrancePasses\":[],\"fees\":[],\"fullName\":\"Abraham Lincoln Birthplace National Historical Park\",\"id\":\"77E0D7F0-1942-494A-ACE2-9004D2BDC59E\",\"images\":[{\"altText\":\"The Memorial Building surrounded by fall colors\",\"caption\":\"Over 200,000 people a year come to walk up the steps of the Memorial Building to visit the site where Abraham Lincoln was born\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building with fall colors\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861078-1DD8-B71B-0B774A242EF6A706.jpg\"},{\"altText\":\"The first memorial erected to honor Abraham Lincoln\",\"caption\":\"The Memorial Building constructed on the traditional site of the birth of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861263-1DD8-B71B-0B71EF9B95F9644F.jpg\"},{\"altText\":\"The symbolic birth cabin on the traditional site of the birth of Abraham Lincoln.\",\"caption\":\"The symbolic birth cabin of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Symbolic Birth Cabin of Abraham Lincoln\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C86137D-1DD8-B71B-0B978BACD7EBAEF1.jpg\"},{\"altText\":\"Statue of the Lincoln family in the park's Visitor Center\",\"caption\":\"Visitors to the park can view the statue of the Lincoln family.\",\"credit\":\"NPS Photo\",\"title\":\"Statue of the Lincoln Family in the Visitor Center\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C8614D1-1DD8-B71B-0B1AF72CA452B051.jpg\"}],\"latLong\":\"lat:37.5858662, long:-85.67330523\",\"latitude\":\"37.5858662\",\"longitude\":\"-85.67330523\",\"name\":\"Abraham Lincoln Birthplace\",\"operatingHours\":[{\"description\":\"The Memorial Building is open 9:00 am - 4:30 pm eastern time with a limited viewing area.  The Visitor Center and grounds of the Birthplace Unit are open 9:00 am - 5:00 pm eastern time with one-way traffic flow and social distancing.\\n\\n\\nThe Boyhood Home Unit at Knob Creek will be closed August 3, 2020, to July 31, 2021, due to the Lincoln Tavern Rehabilitation Project.\",\"exceptions\":[{\"endDate\":\"2022-01-01\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2022-01-01\"},{\"endDate\":\"2021-11-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-11-25\"},{\"endDate\":\"2021-12-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-12-25\"}],\"name\":\"Abraham Lincoln Birthplace National Historical Park\",\"standardHours\":{\"friday\":\"9:00AM - 5:00PM\",\"monday\":\"9:00AM - 5:00PM\",\"saturday\":\"9:00AM - 5:00PM\",\"sunday\":\"9:00AM - 5:00PM\",\"thursday\":\"9:00AM - 5:00PM\",\"tuesday\":\"9:00AM - 5:00PM\",\"wednesday\":\"9:00AM - 5:00PM\"}}],\"parkCode\":\"abli\",\"states\":\"KY\",\"topics\":[{\"id\":\"D10852A3-443C-4743-A5FA-6DD6D2A054B3\",\"name\":\"Birthplace\"},{\"id\":\"F669BC40-BDC4-41C0-9ACE-B2CD25373045\",\"name\":\"Presidents\"},{\"id\":\"0D00073E-18C3-46E5-8727-2F87B112DDC6\",\"name\":\"Animals\"},{\"id\":\"957EF2BD-AC6C-4B7B-BD9A-87593ADC6691\",\"name\":\"Birds\"},{\"id\":\"E25F3456-43ED-45DD-93BC-057F9B944F7A\",\"name\":\"Caves, Caverns and Karst\"},{\"id\":\"F0F97E32-2F29-41B4-AF98-9FBE8DAB36B1\",\"name\":\"Geology\"},{\"id\":\"4BE01DC5-52E6-4F18-8C9A-B22D65965F6D\",\"name\":\"Groundwater\"},{\"id\":\"0E1A04CC-EB51-4F18-93D4-EC0B0B4EC1E3\",\"name\":\"Freshwater Springs\"},{\"id\":\"A7359FC4-DAD8-45F5-AF15-7FF62F816ED3\",\"name\":\"Night Sky\"}],\"url\":\"https://www.nps.gov/abli/index.htm\",\"weatherInfo\":\"There are four distinct seasons in Central Kentucky. However, temperature and weather conditions can vary widely within those seasons. Spring and Fall are generally pleasant with frequent rain showers. Summer is usually hot and humid. Winter is moderately cold with mixed precipitation.\"},{\"activities\":[{\"id\":\"13A57703-BB1A-41A2-94B8-53B692EB7238\",\"name\":\"Astronomy\"},{\"id\":\"D37A0003-8317-4F04-8FB0-4CF0A272E195\",\"name\":\"Stargazing\"},{\"id\":\"1DFACD97-1B9C-4F5A-80F2-05593604799E\",\"name\":\"Food\"},{\"id\":\"C6D3230A-2CEA-4AFE-BFF3-DC1E2C2C4BB4\",\"name\":\"Picnicking\"},{\"id\":\"DF4A35E0-7983-4A3E-BC47-F37B872B0F25\",\"name\":\"Junior Ranger Program\"},{\"id\":\"0B685688-3405-4E2A-ABBA-E3069492EC50\",\"name\":\"Wildlife Watching\"},{\"id\":\"5A2C91D1-50EC-4B24-8BED-A2E11A1892DF\",\"name\":\"Birdwatching\"},{\"id\":\"24380E3F-AD9D-4E38-BF13-C8EEB21893E7\",\"name\":\"Shopping\"},{\"id\":\"467DC8B8-0B7D-436D-A026-80A22358F615\",\"name\":\"Bookstore and Park Store\"},{\"id\":\"43800AD1-D439-40F3-AAB3-9FB651FE45BB\",\"name\":\"Gift Shop and Souvenirs\"}],\"addresses\":[{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Physical\"},{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Mailing\"}],\"contacts\":{\"emailAddresses\":[{\"description\":\"\",\"emailAddress\":\"ABLI_Administration@nps.gov\"}],\"phoneNumbers\":[{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583137\",\"type\":\"Voice\"},{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583874\",\"type\":\"Fax\"}]},\"description\":\"For over a century people from around the world have come to rural Central Kentucky to honor the humble beginnings of our 16th president, Abraham Lincoln. His early life on Kentucky's frontier shaped his character and prepared him to lead the nation through Civil War. The country's first memorial to Lincoln, built with donations from young and old, enshrines the symbolic birthplace cabin.\",\"designation\":\"National Historical Park\",\"directionsInfo\":\"The Birthplace Unit of the park is located approximately 2 miles south of the town of Hodgenville on U.S. Highway 31E South. The Boyhood Home Unit at Knob Creek is located approximately 10 miles northeast of the Birthplace Unit of the park.\",\"directionsUrl\":\"http://www.nps.gov/abli/planyourvisit/directions.htm\",\"entranceFees\":[{\"cost\":\"0.00\",\"description\":\"There is no fee associated with visiting either unit of the park.\",\"title\":\"Fee Free Park\"}],\"entrancePasses\":[],\"fees\":[],\"fullName\":\"Abraham Lincoln Birthplace National Historical Park\",\"id\":\"77E0D7F0-1942-494A-ACE2-9004D2BDC59E\",\"images\":[{\"altText\":\"The Memorial Building surrounded by fall colors\",\"caption\":\"Over 200,000 people a year come to walk up the steps of the Memorial Building to visit the site where Abraham Lincoln was born\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building with fall colors\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861078-1DD8-B71B-0B774A242EF6A706.jpg\"},{\"altText\":\"The first memorial erected to honor Abraham Lincoln\",\"caption\":\"The Memorial Building constructed on the traditional site of the birth of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861263-1DD8-B71B-0B71EF9B95F9644F.jpg\"},{\"altText\":\"The symbolic birth cabin on the traditional site of the birth of Abraham Lincoln.\",\"caption\":\"The symbolic birth cabin of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Symbolic Birth Cabin of Abraham Lincoln\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C86137D-1DD8-B71B-0B978BACD7EBAEF1.jpg\"},{\"altText\":\"Statue of the Lincoln family in the park's Visitor Center\",\"caption\":\"Visitors to the park can view the statue of the Lincoln family.\",\"credit\":\"NPS Photo\",\"title\":\"Statue of the Lincoln Family in the Visitor Center\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C8614D1-1DD8-B71B-0B1AF72CA452B051.jpg\"}],\"latLong\":\"lat:37.5858662, long:-85.67330523\",\"latitude\":\"37.5858662\",\"longitude\":\"-85.67330523\",\"name\":\"Abraham Lincoln Birthplace\",\"operatingHours\":[{\"description\":\"The Memorial Building is open 9:00 am - 4:30 pm eastern time with a limited viewing area.  The Visitor Center and grounds of the Birthplace Unit are open 9:00 am - 5:00 pm eastern time with one-way traffic flow and social distancing.\\n\\n\\nThe Boyhood Home Unit at Knob Creek will be closed August 3, 2020, to July 31, 2021, due to the Lincoln Tavern Rehabilitation Project.\",\"exceptions\":[{\"endDate\":\"2022-01-01\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2022-01-01\"},{\"endDate\":\"2021-11-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-11-25\"},{\"endDate\":\"2021-12-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-12-25\"}],\"name\":\"Abraham Lincoln Birthplace National Historical Park\",\"standardHours\":{\"friday\":\"9:00AM - 5:00PM\",\"monday\":\"9:00AM - 5:00PM\",\"saturday\":\"9:00AM - 5:00PM\",\"sunday\":\"9:00AM - 5:00PM\",\"thursday\":\"9:00AM - 5:00PM\",\"tuesday\":\"9:00AM - 5:00PM\",\"wednesday\":\"9:00AM - 5:00PM\"}}],\"parkCode\":\"abli\",\"states\":\"KY\",\"topics\":[{\"id\":\"D10852A3-443C-4743-A5FA-6DD6D2A054B3\",\"name\":\"Birthplace\"},{\"id\":\"F669BC40-BDC4-41C0-9ACE-B2CD25373045\",\"name\":\"Presidents\"},{\"id\":\"0D00073E-18C3-46E5-8727-2F87B112DDC6\",\"name\":\"Animals\"},{\"id\":\"957EF2BD-AC6C-4B7B-BD9A-87593ADC6691\",\"name\":\"Birds\"},{\"id\":\"E25F3456-43ED-45DD-93BC-057F9B944F7A\",\"name\":\"Caves, Caverns and Karst\"},{\"id\":\"F0F97E32-2F29-41B4-AF98-9FBE8DAB36B1\",\"name\":\"Geology\"},{\"id\":\"4BE01DC5-52E6-4F18-8C9A-B22D65965F6D\",\"name\":\"Groundwater\"},{\"id\":\"0E1A04CC-EB51-4F18-93D4-EC0B0B4EC1E3\",\"name\":\"Freshwater Springs\"},{\"id\":\"A7359FC4-DAD8-45F5-AF15-7FF62F816ED3\",\"name\":\"Night Sky\"}],\"url\":\"https://www.nps.gov/abli/index.htm\",\"weatherInfo\":\"There are four distinct seasons in Central Kentucky. However, temperature and weather conditions can vary widely within those seasons. Spring and Fall are generally pleasant with frequent rain showers. Summer is usually hot and humid. Winter is moderately cold with mixed precipitation.\"},{\"activities\":[{\"id\":\"13A57703-BB1A-41A2-94B8-53B692EB7238\",\"name\":\"Astronomy\"},{\"id\":\"D37A0003-8317-4F04-8FB0-4CF0A272E195\",\"name\":\"Stargazing\"},{\"id\":\"1DFACD97-1B9C-4F5A-80F2-05593604799E\",\"name\":\"Food\"},{\"id\":\"C6D3230A-2CEA-4AFE-BFF3-DC1E2C2C4BB4\",\"name\":\"Picnicking\"},{\"id\":\"DF4A35E0-7983-4A3E-BC47-F37B872B0F25\",\"name\":\"Junior Ranger Program\"},{\"id\":\"0B685688-3405-4E2A-ABBA-E3069492EC50\",\"name\":\"Wildlife Watching\"},{\"id\":\"5A2C91D1-50EC-4B24-8BED-A2E11A1892DF\",\"name\":\"Birdwatching\"},{\"id\":\"24380E3F-AD9D-4E38-BF13-C8EEB21893E7\",\"name\":\"Shopping\"},{\"id\":\"467DC8B8-0B7D-436D-A026-80A22358F615\",\"name\":\"Bookstore and Park Store\"},{\"id\":\"43800AD1-D439-40F3-AAB3-9FB651FE45BB\",\"name\":\"Gift Shop and Souvenirs\"}],\"addresses\":[{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Physical\"},{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Mailing\"}],\"contacts\":{\"emailAddresses\":[{\"description\":\"\",\"emailAddress\":\"ABLI_Administration@nps.gov\"}],\"phoneNumbers\":[{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583137\",\"type\":\"Voice\"},{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583874\",\"type\":\"Fax\"}]},\"description\":\"For over a century people from around the world have come to rural Central Kentucky to honor the humble beginnings of our 16th president, Abraham Lincoln. His early life on Kentucky's frontier shaped his character and prepared him to lead the nation through Civil War. The country's first memorial to Lincoln, built with donations from young and old, enshrines the symbolic birthplace cabin.\",\"designation\":\"National Historical Park\",\"directionsInfo\":\"The Birthplace Unit of the park is located approximately 2 miles south of the town of Hodgenville on U.S. Highway 31E South. The Boyhood Home Unit at Knob Creek is located approximately 10 miles northeast of the Birthplace Unit of the park.\",\"directionsUrl\":\"http://www.nps.gov/abli/planyourvisit/directions.htm\",\"entranceFees\":[{\"cost\":\"0.00\",\"description\":\"There is no fee associated with visiting either unit of the park.\",\"title\":\"Fee Free Park\"}],\"entrancePasses\":[],\"fees\":[],\"fullName\":\"Abraham Lincoln Birthplace National Historical Park\",\"id\":\"77E0D7F0-1942-494A-ACE2-9004D2BDC59E\",\"images\":[{\"altText\":\"The Memorial Building surrounded by fall colors\",\"caption\":\"Over 200,000 people a year come to walk up the steps of the Memorial Building to visit the site where Abraham Lincoln was born\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building with fall colors\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861078-1DD8-B71B-0B774A242EF6A706.jpg\"},{\"altText\":\"The first memorial erected to honor Abraham Lincoln\",\"caption\":\"The Memorial Building constructed on the traditional site of the birth of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861263-1DD8-B71B-0B71EF9B95F9644F.jpg\"},{\"altText\":\"The symbolic birth cabin on the traditional site of the birth of Abraham Lincoln.\",\"caption\":\"The symbolic birth cabin of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Symbolic Birth Cabin of Abraham Lincoln\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C86137D-1DD8-B71B-0B978BACD7EBAEF1.jpg\"},{\"altText\":\"Statue of the Lincoln family in the park's Visitor Center\",\"caption\":\"Visitors to the park can view the statue of the Lincoln family.\",\"credit\":\"NPS Photo\",\"title\":\"Statue of the Lincoln Family in the Visitor Center\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C8614D1-1DD8-B71B-0B1AF72CA452B051.jpg\"}],\"latLong\":\"lat:37.5858662, long:-85.67330523\",\"latitude\":\"37.5858662\",\"longitude\":\"-85.67330523\",\"name\":\"Abraham Lincoln Birthplace\",\"operatingHours\":[{\"description\":\"The Memorial Building is open 9:00 am - 4:30 pm eastern time with a limited viewing area.  The Visitor Center and grounds of the Birthplace Unit are open 9:00 am - 5:00 pm eastern time with one-way traffic flow and social distancing.\\n\\n\\nThe Boyhood Home Unit at Knob Creek will be closed August 3, 2020, to July 31, 2021, due to the Lincoln Tavern Rehabilitation Project.\",\"exceptions\":[{\"endDate\":\"2022-01-01\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2022-01-01\"},{\"endDate\":\"2021-11-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-11-25\"},{\"endDate\":\"2021-12-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-12-25\"}],\"name\":\"Abraham Lincoln Birthplace National Historical Park\",\"standardHours\":{\"friday\":\"9:00AM - 5:00PM\",\"monday\":\"9:00AM - 5:00PM\",\"saturday\":\"9:00AM - 5:00PM\",\"sunday\":\"9:00AM - 5:00PM\",\"thursday\":\"9:00AM - 5:00PM\",\"tuesday\":\"9:00AM - 5:00PM\",\"wednesday\":\"9:00AM - 5:00PM\"}}],\"parkCode\":\"abli\",\"states\":\"KY\",\"topics\":[{\"id\":\"D10852A3-443C-4743-A5FA-6DD6D2A054B3\",\"name\":\"Birthplace\"},{\"id\":\"F669BC40-BDC4-41C0-9ACE-B2CD25373045\",\"name\":\"Presidents\"},{\"id\":\"0D00073E-18C3-46E5-8727-2F87B112DDC6\",\"name\":\"Animals\"},{\"id\":\"957EF2BD-AC6C-4B7B-BD9A-87593ADC6691\",\"name\":\"Birds\"},{\"id\":\"E25F3456-43ED-45DD-93BC-057F9B944F7A\",\"name\":\"Caves, Caverns and Karst\"},{\"id\":\"F0F97E32-2F29-41B4-AF98-9FBE8DAB36B1\",\"name\":\"Geology\"},{\"id\":\"4BE01DC5-52E6-4F18-8C9A-B22D65965F6D\",\"name\":\"Groundwater\"},{\"id\":\"0E1A04CC-EB51-4F18-93D4-EC0B0B4EC1E3\",\"name\":\"Freshwater Springs\"},{\"id\":\"A7359FC4-DAD8-45F5-AF15-7FF62F816ED3\",\"name\":\"Night Sky\"}],\"url\":\"https://www.nps.gov/abli/index.htm\",\"weatherInfo\":\"There are four distinct seasons in Central Kentucky. However, temperature and weather conditions can vary widely within those seasons. Spring and Fall are generally pleasant with frequent rain showers. Summer is usually hot and humid. Winter is moderately cold with mixed precipitation.\"},{\"activities\":[{\"id\":\"13A57703-BB1A-41A2-94B8-53B692EB7238\",\"name\":\"Astronomy\"},{\"id\":\"D37A0003-8317-4F04-8FB0-4CF0A272E195\",\"name\":\"Stargazing\"},{\"id\":\"1DFACD97-1B9C-4F5A-80F2-05593604799E\",\"name\":\"Food\"},{\"id\":\"C6D3230A-2CEA-4AFE-BFF3-DC1E2C2C4BB4\",\"name\":\"Picnicking\"},{\"id\":\"DF4A35E0-7983-4A3E-BC47-F37B872B0F25\",\"name\":\"Junior Ranger Program\"},{\"id\":\"0B685688-3405-4E2A-ABBA-E3069492EC50\",\"name\":\"Wildlife Watching\"},{\"id\":\"5A2C91D1-50EC-4B24-8BED-A2E11A1892DF\",\"name\":\"Birdwatching\"},{\"id\":\"24380E3F-AD9D-4E38-BF13-C8EEB21893E7\",\"name\":\"Shopping\"},{\"id\":\"467DC8B8-0B7D-436D-A026-80A22358F615\",\"name\":\"Bookstore and Park Store\"},{\"id\":\"43800AD1-D439-40F3-AAB3-9FB651FE45BB\",\"name\":\"Gift Shop and Souvenirs\"}],\"addresses\":[{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Physical\"},{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Mailing\"}],\"contacts\":{\"emailAddresses\":[{\"description\":\"\",\"emailAddress\":\"ABLI_Administration@nps.gov\"}],\"phoneNumbers\":[{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583137\",\"type\":\"Voice\"},{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583874\",\"type\":\"Fax\"}]},\"description\":\"For over a century people from around the world have come to rural Central Kentucky to honor the humble beginnings of our 16th president, Abraham Lincoln. His early life on Kentucky's frontier shaped his character and prepared him to lead the nation through Civil War. The country's first memorial to Lincoln, built with donations from young and old, enshrines the symbolic birthplace cabin.\",\"designation\":\"National Historical Park\",\"directionsInfo\":\"The Birthplace Unit of the park is located approximately 2 miles south of the town of Hodgenville on U.S. Highway 31E South. The Boyhood Home Unit at Knob Creek is located approximately 10 miles northeast of the Birthplace Unit of the park.\",\"directionsUrl\":\"http://www.nps.gov/abli/planyourvisit/directions.htm\",\"entranceFees\":[{\"cost\":\"0.00\",\"description\":\"There is no fee associated with visiting either unit of the park.\",\"title\":\"Fee Free Park\"}],\"entrancePasses\":[],\"fees\":[],\"fullName\":\"Abraham Lincoln Birthplace National Historical Park\",\"id\":\"77E0D7F0-1942-494A-ACE2-9004D2BDC59E\",\"images\":[{\"altText\":\"The Memorial Building surrounded by fall colors\",\"caption\":\"Over 200,000 people a year come to walk up the steps of the Memorial Building to visit the site where Abraham Lincoln was born\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building with fall colors\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861078-1DD8-B71B-0B774A242EF6A706.jpg\"},{\"altText\":\"The first memorial erected to honor Abraham Lincoln\",\"caption\":\"The Memorial Building constructed on the traditional site of the birth of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861263-1DD8-B71B-0B71EF9B95F9644F.jpg\"},{\"altText\":\"The symbolic birth cabin on the traditional site of the birth of Abraham Lincoln.\",\"caption\":\"The symbolic birth cabin of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Symbolic Birth Cabin of Abraham Lincoln\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C86137D-1DD8-B71B-0B978BACD7EBAEF1.jpg\"},{\"altText\":\"Statue of the Lincoln family in the park's Visitor Center\",\"caption\":\"Visitors to the park can view the statue of the Lincoln family.\",\"credit\":\"NPS Photo\",\"title\":\"Statue of the Lincoln Family in the Visitor Center\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C8614D1-1DD8-B71B-0B1AF72CA452B051.jpg\"}],\"latLong\":\"lat:37.5858662, long:-85.67330523\",\"latitude\":\"37.5858662\",\"longitude\":\"-85.67330523\",\"name\":\"Abraham Lincoln Birthplace\",\"operatingHours\":[{\"description\":\"The Memorial Building is open 9:00 am - 4:30 pm eastern time with a limited viewing area.  The Visitor Center and grounds of the Birthplace Unit are open 9:00 am - 5:00 pm eastern time with one-way traffic flow and social distancing.\\n\\n\\nThe Boyhood Home Unit at Knob Creek will be closed August 3, 2020, to July 31, 2021, due to the Lincoln Tavern Rehabilitation Project.\",\"exceptions\":[{\"endDate\":\"2022-01-01\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2022-01-01\"},{\"endDate\":\"2021-11-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-11-25\"},{\"endDate\":\"2021-12-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-12-25\"}],\"name\":\"Abraham Lincoln Birthplace National Historical Park\",\"standardHours\":{\"friday\":\"9:00AM - 5:00PM\",\"monday\":\"9:00AM - 5:00PM\",\"saturday\":\"9:00AM - 5:00PM\",\"sunday\":\"9:00AM - 5:00PM\",\"thursday\":\"9:00AM - 5:00PM\",\"tuesday\":\"9:00AM - 5:00PM\",\"wednesday\":\"9:00AM - 5:00PM\"}}],\"parkCode\":\"abli\",\"states\":\"KY\",\"topics\":[{\"id\":\"D10852A3-443C-4743-A5FA-6DD6D2A054B3\",\"name\":\"Birthplace\"},{\"id\":\"F669BC40-BDC4-41C0-9ACE-B2CD25373045\",\"name\":\"Presidents\"},{\"id\":\"0D00073E-18C3-46E5-8727-2F87B112DDC6\",\"name\":\"Animals\"},{\"id\":\"957EF2BD-AC6C-4B7B-BD9A-87593ADC6691\",\"name\":\"Birds\"},{\"id\":\"E25F3456-43ED-45DD-93BC-057F9B944F7A\",\"name\":\"Caves, Caverns and Karst\"},{\"id\":\"F0F97E32-2F29-41B4-AF98-9FBE8DAB36B1\",\"name\":\"Geology\"},{\"id\":\"4BE01DC5-52E6-4F18-8C9A-B22D65965F6D\",\"name\":\"Groundwater\"},{\"id\":\"0E1A04CC-EB51-4F18-93D4-EC0B0B4EC1E3\",\"name\":\"Freshwater Springs\"},{\"id\":\"A7359FC4-DAD8-45F5-AF15-7FF62F816ED3\",\"name\":\"Night Sky\"}],\"url\":\"https://www.nps.gov/abli/index.htm\",\"weatherInfo\":\"There are four distinct seasons in Central Kentucky. However, temperature and weather conditions can vary widely within those seasons. Spring and Fall are generally pleasant with frequent rain showers. Summer is usually hot and humid. Winter is moderately cold with mixed precipitation.\"},{\"activities\":[{\"id\":\"13A57703-BB1A-41A2-94B8-53B692EB7238\",\"name\":\"Astronomy\"},{\"id\":\"D37A0003-8317-4F04-8FB0-4CF0A272E195\",\"name\":\"Stargazing\"},{\"id\":\"1DFACD97-1B9C-4F5A-80F2-05593604799E\",\"name\":\"Food\"},{\"id\":\"C6D3230A-2CEA-4AFE-BFF3-DC1E2C2C4BB4\",\"name\":\"Picnicking\"},{\"id\":\"DF4A35E0-7983-4A3E-BC47-F37B872B0F25\",\"name\":\"Junior Ranger Program\"},{\"id\":\"0B685688-3405-4E2A-ABBA-E3069492EC50\",\"name\":\"Wildlife Watching\"},{\"id\":\"5A2C91D1-50EC-4B24-8BED-A2E11A1892DF\",\"name\":\"Birdwatching\"},{\"id\":\"24380E3F-AD9D-4E38-BF13-C8EEB21893E7\",\"name\":\"Shopping\"},{\"id\":\"467DC8B8-0B7D-436D-A026-80A22358F615\",\"name\":\"Bookstore and Park Store\"},{\"id\":\"43800AD1-D439-40F3-AAB3-9FB651FE45BB\",\"name\":\"Gift Shop and Souvenirs\"}],\"addresses\":[{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Physical\"},{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Mailing\"}],\"contacts\":{\"emailAddresses\":[{\"description\":\"\",\"emailAddress\":\"ABLI_Administration@nps.gov\"}],\"phoneNumbers\":[{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583137\",\"type\":\"Voice\"},{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583874\",\"type\":\"Fax\"}]},\"description\":\"For over a century people from around the world have come to rural Central Kentucky to honor the humble beginnings of our 16th president, Abraham Lincoln. His early life on Kentucky's frontier shaped his character and prepared him to lead the nation through Civil War. The country's first memorial to Lincoln, built with donations from young and old, enshrines the symbolic birthplace cabin.\",\"designation\":\"National Historical Park\",\"directionsInfo\":\"The Birthplace Unit of the park is located approximately 2 miles south of the town of Hodgenville on U.S. Highway 31E South. The Boyhood Home Unit at Knob Creek is located approximately 10 miles northeast of the Birthplace Unit of the park.\",\"directionsUrl\":\"http://www.nps.gov/abli/planyourvisit/directions.htm\",\"entranceFees\":[{\"cost\":\"0.00\",\"description\":\"There is no fee associated with visiting either unit of the park.\",\"title\":\"Fee Free Park\"}],\"entrancePasses\":[],\"fees\":[],\"fullName\":\"Abraham Lincoln Birthplace National Historical Park\",\"id\":\"77E0D7F0-1942-494A-ACE2-9004D2BDC59E\",\"images\":[{\"altText\":\"The Memorial Building surrounded by fall colors\",\"caption\":\"Over 200,000 people a year come to walk up the steps of the Memorial Building to visit the site where Abraham Lincoln was born\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building with fall colors\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861078-1DD8-B71B-0B774A242EF6A706.jpg\"},{\"altText\":\"The first memorial erected to honor Abraham Lincoln\",\"caption\":\"The Memorial Building constructed on the traditional site of the birth of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861263-1DD8-B71B-0B71EF9B95F9644F.jpg\"},{\"altText\":\"The symbolic birth cabin on the traditional site of the birth of Abraham Lincoln.\",\"caption\":\"The symbolic birth cabin of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Symbolic Birth Cabin of Abraham Lincoln\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C86137D-1DD8-B71B-0B978BACD7EBAEF1.jpg\"},{\"altText\":\"Statue of the Lincoln family in the park's Visitor Center\",\"caption\":\"Visitors to the park can view the statue of the Lincoln family.\",\"credit\":\"NPS Photo\",\"title\":\"Statue of the Lincoln Family in the Visitor Center\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C8614D1-1DD8-B71B-0B1AF72CA452B051.jpg\"}],\"latLong\":\"lat:37.5858662, long:-85.67330523\",\"latitude\":\"37.5858662\",\"longitude\":\"-85.67330523\",\"name\":\"Abraham Lincoln Birthplace\",\"operatingHours\":[{\"description\":\"The Memorial Building is open 9:00 am - 4:30 pm eastern time with a limited viewing area.  The Visitor Center and grounds of the Birthplace Unit are open 9:00 am - 5:00 pm eastern time with one-way traffic flow and social distancing.\\n\\n\\nThe Boyhood Home Unit at Knob Creek will be closed August 3, 2020, to July 31, 2021, due to the Lincoln Tavern Rehabilitation Project.\",\"exceptions\":[{\"endDate\":\"2022-01-01\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2022-01-01\"},{\"endDate\":\"2021-11-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-11-25\"},{\"endDate\":\"2021-12-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-12-25\"}],\"name\":\"Abraham Lincoln Birthplace National Historical Park\",\"standardHours\":{\"friday\":\"9:00AM - 5:00PM\",\"monday\":\"9:00AM - 5:00PM\",\"saturday\":\"9:00AM - 5:00PM\",\"sunday\":\"9:00AM - 5:00PM\",\"thursday\":\"9:00AM - 5:00PM\",\"tuesday\":\"9:00AM - 5:00PM\",\"wednesday\":\"9:00AM - 5:00PM\"}}],\"parkCode\":\"abli\",\"states\":\"KY\",\"topics\":[{\"id\":\"D10852A3-443C-4743-A5FA-6DD6D2A054B3\",\"name\":\"Birthplace\"},{\"id\":\"F669BC40-BDC4-41C0-9ACE-B2CD25373045\",\"name\":\"Presidents\"},{\"id\":\"0D00073E-18C3-46E5-8727-2F87B112DDC6\",\"name\":\"Animals\"},{\"id\":\"957EF2BD-AC6C-4B7B-BD9A-87593ADC6691\",\"name\":\"Birds\"},{\"id\":\"E25F3456-43ED-45DD-93BC-057F9B944F7A\",\"name\":\"Caves, Caverns and Karst\"},{\"id\":\"F0F97E32-2F29-41B4-AF98-9FBE8DAB36B1\",\"name\":\"Geology\"},{\"id\":\"4BE01DC5-52E6-4F18-8C9A-B22D65965F6D\",\"name\":\"Groundwater\"},{\"id\":\"0E1A04CC-EB51-4F18-93D4-EC0B0B4EC1E3\",\"name\":\"Freshwater Springs\"},{\"id\":\"A7359FC4-DAD8-45F5-AF15-7FF62F816ED3\",\"name\":\"Night Sky\"}],\"url\":\"https://www.nps.gov/abli/index.htm\",\"weatherInfo\":\"There are four distinct seasons in Central Kentucky. However, temperature and weather conditions can vary widely within those seasons. Spring and Fall are generally pleasant with frequent rain showers. Summer is usually hot and humid. Winter is moderately cold with mixed precipitation.\"},{\"activities\":[{\"id\":\"13A57703-BB1A-41A2-94B8-53B692EB7238\",\"name\":\"Astronomy\"},{\"id\":\"D37A0003-8317-4F04-8FB0-4CF0A272E195\",\"name\":\"Stargazing\"},{\"id\":\"1DFACD97-1B9C-4F5A-80F2-05593604799E\",\"name\":\"Food\"},{\"id\":\"C6D3230A-2CEA-4AFE-BFF3-DC1E2C2C4BB4\",\"name\":\"Picnicking\"},{\"id\":\"DF4A35E0-7983-4A3E-BC47-F37B872B0F25\",\"name\":\"Junior Ranger Program\"},{\"id\":\"0B685688-3405-4E2A-ABBA-E3069492EC50\",\"name\":\"Wildlife Watching\"},{\"id\":\"5A2C91D1-50EC-4B24-8BED-A2E11A1892DF\",\"name\":\"Birdwatching\"},{\"id\":\"24380E3F-AD9D-4E38-BF13-C8EEB21893E7\",\"name\":\"Shopping\"},{\"id\":\"467DC8B8-0B7D-436D-A026-80A22358F615\",\"name\":\"Bookstore and Park Store\"},{\"id\":\"43800AD1-D439-40F3-AAB3-9FB651FE45BB\",\"name\":\"Gift Shop and Souvenirs\"}],\"addresses\":[{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Physical\"},{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Mailing\"}],\"contacts\":{\"emailAddresses\":[{\"description\":\"\",\"emailAddress\":\"ABLI_Administration@nps.gov\"}],\"phoneNumbers\":[{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583137\",\"type\":\"Voice\"},{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583874\",\"type\":\"Fax\"}]},\"description\":\"For over a century people from around the world have come to rural Central Kentucky to honor the humble beginnings of our 16th president, Abraham Lincoln. His early life on Kentucky's frontier shaped his character and prepared him to lead the nation through Civil War. The country's first memorial to Lincoln, built with donations from young and old, enshrines the symbolic birthplace cabin.\",\"designation\":\"National Historical Park\",\"directionsInfo\":\"The Birthplace Unit of the park is located approximately 2 miles south of the town of Hodgenville on U.S. Highway 31E South. The Boyhood Home Unit at Knob Creek is located approximately 10 miles northeast of the Birthplace Unit of the park.\",\"directionsUrl\":\"http://www.nps.gov/abli/planyourvisit/directions.htm\",\"entranceFees\":[{\"cost\":\"0.00\",\"description\":\"There is no fee associated with visiting either unit of the park.\",\"title\":\"Fee Free Park\"}],\"entrancePasses\":[],\"fees\":[],\"fullName\":\"Abraham Lincoln Birthplace National Historical Park\",\"id\":\"77E0D7F0-1942-494A-ACE2-9004D2BDC59E\",\"images\":[{\"altText\":\"The Memorial Building surrounded by fall colors\",\"caption\":\"Over 200,000 people a year come to walk up the steps of the Memorial Building to visit the site where Abraham Lincoln was born\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building with fall colors\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861078-1DD8-B71B-0B774A242EF6A706.jpg\"},{\"altText\":\"The first memorial erected to honor Abraham Lincoln\",\"caption\":\"The Memorial Building constructed on the traditional site of the birth of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861263-1DD8-B71B-0B71EF9B95F9644F.jpg\"},{\"altText\":\"The symbolic birth cabin on the traditional site of the birth of Abraham Lincoln.\",\"caption\":\"The symbolic birth cabin of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Symbolic Birth Cabin of Abraham Lincoln\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C86137D-1DD8-B71B-0B978BACD7EBAEF1.jpg\"},{\"altText\":\"Statue of the Lincoln family in the park's Visitor Center\",\"caption\":\"Visitors to the park can view the statue of the Lincoln family.\",\"credit\":\"NPS Photo\",\"title\":\"Statue of the Lincoln Family in the Visitor Center\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C8614D1-1DD8-B71B-0B1AF72CA452B051.jpg\"}],\"latLong\":\"lat:37.5858662, long:-85.67330523\",\"latitude\":\"37.5858662\",\"longitude\":\"-85.67330523\",\"name\":\"Abraham Lincoln Birthplace\",\"operatingHours\":[{\"description\":\"The Memorial Building is open 9:00 am - 4:30 pm eastern time with a limited viewing area.  The Visitor Center and grounds of the Birthplace Unit are open 9:00 am - 5:00 pm eastern time with one-way traffic flow and social distancing.\\n\\n\\nThe Boyhood Home Unit at Knob Creek will be closed August 3, 2020, to July 31, 2021, due to the Lincoln Tavern Rehabilitation Project.\",\"exceptions\":[{\"endDate\":\"2022-01-01\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2022-01-01\"},{\"endDate\":\"2021-11-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-11-25\"},{\"endDate\":\"2021-12-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-12-25\"}],\"name\":\"Abraham Lincoln Birthplace National Historical Park\",\"standardHours\":{\"friday\":\"9:00AM - 5:00PM\",\"monday\":\"9:00AM - 5:00PM\",\"saturday\":\"9:00AM - 5:00PM\",\"sunday\":\"9:00AM - 5:00PM\",\"thursday\":\"9:00AM - 5:00PM\",\"tuesday\":\"9:00AM - 5:00PM\",\"wednesday\":\"9:00AM - 5:00PM\"}}],\"parkCode\":\"abli\",\"states\":\"KY\",\"topics\":[{\"id\":\"D10852A3-443C-4743-A5FA-6DD6D2A054B3\",\"name\":\"Birthplace\"},{\"id\":\"F669BC40-BDC4-41C0-9ACE-B2CD25373045\",\"name\":\"Presidents\"},{\"id\":\"0D00073E-18C3-46E5-8727-2F87B112DDC6\",\"name\":\"Animals\"},{\"id\":\"957EF2BD-AC6C-4B7B-BD9A-87593ADC6691\",\"name\":\"Birds\"},{\"id\":\"E25F3456-43ED-45DD-93BC-057F9B944F7A\",\"name\":\"Caves, Caverns and Karst\"},{\"id\":\"F0F97E32-2F29-41B4-AF98-9FBE8DAB36B1\",\"name\":\"Geology\"},{\"id\":\"4BE01DC5-52E6-4F18-8C9A-B22D65965F6D\",\"name\":\"Groundwater\"},{\"id\":\"0E1A04CC-EB51-4F18-93D4-EC0B0B4EC1E3\",\"name\":\"Freshwater Springs\"},{\"id\":\"A7359FC4-DAD8-45F5-AF15-7FF62F816ED3\",\"name\":\"Night Sky\"}],\"url\":\"https://www.nps.gov/abli/index.htm\",\"weatherInfo\":\"There are four distinct seasons in Central Kentucky. However, temperature and weather conditions can vary widely within those seasons. Spring and Fall are generally pleasant with frequent rain showers. Summer is usually hot and humid. Winter is moderately cold with mixed precipitation.\"},{\"activities\":[{\"id\":\"13A57703-BB1A-41A2-94B8-53B692EB7238\",\"name\":\"Astronomy\"},{\"id\":\"D37A0003-8317-4F04-8FB0-4CF0A272E195\",\"name\":\"Stargazing\"},{\"id\":\"1DFACD97-1B9C-4F5A-80F2-05593604799E\",\"name\":\"Food\"},{\"id\":\"C6D3230A-2CEA-4AFE-BFF3-DC1E2C2C4BB4\",\"name\":\"Picnicking\"},{\"id\":\"DF4A35E0-7983-4A3E-BC47-F37B872B0F25\",\"name\":\"Junior Ranger Program\"},{\"id\":\"0B685688-3405-4E2A-ABBA-E3069492EC50\",\"name\":\"Wildlife Watching\"},{\"id\":\"5A2C91D1-50EC-4B24-8BED-A2E11A1892DF\",\"name\":\"Birdwatching\"},{\"id\":\"24380E3F-AD9D-4E38-BF13-C8EEB21893E7\",\"name\":\"Shopping\"},{\"id\":\"467DC8B8-0B7D-436D-A026-80A22358F615\",\"name\":\"Bookstore and Park Store\"},{\"id\":\"43800AD1-D439-40F3-AAB3-9FB651FE45BB\",\"name\":\"Gift Shop and Souvenirs\"}],\"addresses\":[{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Physical\"},{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Mailing\"}],\"contacts\":{\"emailAddresses\":[{\"description\":\"\",\"emailAddress\":\"ABLI_Administration@nps.gov\"}],\"phoneNumbers\":[{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583137\",\"type\":\"Voice\"},{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583874\",\"type\":\"Fax\"}]},\"description\":\"For over a century people from around the world have come to rural Central Kentucky to honor the humble beginnings of our 16th president, Abraham Lincoln. His early life on Kentucky's frontier shaped his character and prepared him to lead the nation through Civil War. The country's first memorial to Lincoln, built with donations from young and old, enshrines the symbolic birthplace cabin.\",\"designation\":\"National Historical Park\",\"directionsInfo\":\"The Birthplace Unit of the park is located approximately 2 miles south of the town of Hodgenville on U.S. Highway 31E South. The Boyhood Home Unit at Knob Creek is located approximately 10 miles northeast of the Birthplace Unit of the park.\",\"directionsUrl\":\"http://www.nps.gov/abli/planyourvisit/directions.htm\",\"entranceFees\":[{\"cost\":\"0.00\",\"description\":\"There is no fee associated with visiting either unit of the park.\",\"title\":\"Fee Free Park\"}],\"entrancePasses\":[],\"fees\":[],\"fullName\":\"Abraham Lincoln Birthplace National Historical Park\",\"id\":\"77E0D7F0-1942-494A-ACE2-9004D2BDC59E\",\"images\":[{\"altText\":\"The Memorial Building surrounded by fall colors\",\"caption\":\"Over 200,000 people a year come to walk up the steps of the Memorial Building to visit the site where Abraham Lincoln was born\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building with fall colors\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861078-1DD8-B71B-0B774A242EF6A706.jpg\"},{\"altText\":\"The first memorial erected to honor Abraham Lincoln\",\"caption\":\"The Memorial Building constructed on the traditional site of the birth of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861263-1DD8-B71B-0B71EF9B95F9644F.jpg\"},{\"altText\":\"The symbolic birth cabin on the traditional site of the birth of Abraham Lincoln.\",\"caption\":\"The symbolic birth cabin of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Symbolic Birth Cabin of Abraham Lincoln\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C86137D-1DD8-B71B-0B978BACD7EBAEF1.jpg\"},{\"altText\":\"Statue of the Lincoln family in the park's Visitor Center\",\"caption\":\"Visitors to the park can view the statue of the Lincoln family.\",\"credit\":\"NPS Photo\",\"title\":\"Statue of the Lincoln Family in the Visitor Center\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C8614D1-1DD8-B71B-0B1AF72CA452B051.jpg\"}],\"latLong\":\"lat:37.5858662, long:-85.67330523\",\"latitude\":\"37.5858662\",\"longitude\":\"-85.67330523\",\"name\":\"Abraham Lincoln Birthplace\",\"operatingHours\":[{\"description\":\"The Memorial Building is open 9:00 am - 4:30 pm eastern time with a limited viewing area.  The Visitor Center and grounds of the Birthplace Unit are open 9:00 am - 5:00 pm eastern time with one-way traffic flow and social distancing.\\n\\n\\nThe Boyhood Home Unit at Knob Creek will be closed August 3, 2020, to July 31, 2021, due to the Lincoln Tavern Rehabilitation Project.\",\"exceptions\":[{\"endDate\":\"2022-01-01\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2022-01-01\"},{\"endDate\":\"2021-11-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-11-25\"},{\"endDate\":\"2021-12-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-12-25\"}],\"name\":\"Abraham Lincoln Birthplace National Historical Park\",\"standardHours\":{\"friday\":\"9:00AM - 5:00PM\",\"monday\":\"9:00AM - 5:00PM\",\"saturday\":\"9:00AM - 5:00PM\",\"sunday\":\"9:00AM - 5:00PM\",\"thursday\":\"9:00AM - 5:00PM\",\"tuesday\":\"9:00AM - 5:00PM\",\"wednesday\":\"9:00AM - 5:00PM\"}}],\"parkCode\":\"abli\",\"states\":\"KY\",\"topics\":[{\"id\":\"D10852A3-443C-4743-A5FA-6DD6D2A054B3\",\"name\":\"Birthplace\"},{\"id\":\"F669BC40-BDC4-41C0-9ACE-B2CD25373045\",\"name\":\"Presidents\"},{\"id\":\"0D00073E-18C3-46E5-8727-2F87B112DDC6\",\"name\":\"Animals\"},{\"id\":\"957EF2BD-AC6C-4B7B-BD9A-87593ADC6691\",\"name\":\"Birds\"},{\"id\":\"E25F3456-43ED-45DD-93BC-057F9B944F7A\",\"name\":\"Caves, Caverns and Karst\"},{\"id\":\"F0F97E32-2F29-41B4-AF98-9FBE8DAB36B1\",\"name\":\"Geology\"},{\"id\":\"4BE01DC5-52E6-4F18-8C9A-B22D65965F6D\",\"name\":\"Groundwater\"},{\"id\":\"0E1A04CC-EB51-4F18-93D4-EC0B0B4EC1E3\",\"name\":\"Freshwater Springs\"},{\"id\":\"A7359FC4-DAD8-45F5-AF15-7FF62F816ED3\",\"name\":\"Night Sky\"}],\"url\":\"https://www.nps.gov/abli/index.htm\",\"weatherInfo\":\"There are four distinct seasons in Central Kentucky. However, temperature and weather conditions can vary widely within those seasons. Spring and Fall are generally pleasant with frequent rain showers. Summer is usually hot and humid. Winter is moderately cold with mixed precipitation.\"},{\"activities\":[{\"id\":\"13A57703-BB1A-41A2-94B8-53B692EB7238\",\"name\":\"Astronomy\"},{\"id\":\"D37A0003-8317-4F04-8FB0-4CF0A272E195\",\"name\":\"Stargazing\"},{\"id\":\"1DFACD97-1B9C-4F5A-80F2-05593604799E\",\"name\":\"Food\"},{\"id\":\"C6D3230A-2CEA-4AFE-BFF3-DC1E2C2C4BB4\",\"name\":\"Picnicking\"},{\"id\":\"DF4A35E0-7983-4A3E-BC47-F37B872B0F25\",\"name\":\"Junior Ranger Program\"},{\"id\":\"0B685688-3405-4E2A-ABBA-E3069492EC50\",\"name\":\"Wildlife Watching\"},{\"id\":\"5A2C91D1-50EC-4B24-8BED-A2E11A1892DF\",\"name\":\"Birdwatching\"},{\"id\":\"24380E3F-AD9D-4E38-BF13-C8EEB21893E7\",\"name\":\"Shopping\"},{\"id\":\"467DC8B8-0B7D-436D-A026-80A22358F615\",\"name\":\"Bookstore and Park Store\"},{\"id\":\"43800AD1-D439-40F3-AAB3-9FB651FE45BB\",\"name\":\"Gift Shop and Souvenirs\"}],\"addresses\":[{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Physical\"},{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Mailing\"}],\"contacts\":{\"emailAddresses\":[{\"description\":\"\",\"emailAddress\":\"ABLI_Administration@nps.gov\"}],\"phoneNumbers\":[{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583137\",\"type\":\"Voice\"},{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583874\",\"type\":\"Fax\"}]},\"description\":\"For over a century people from around the world have come to rural Central Kentucky to honor the humble beginnings of our 16th president, Abraham Lincoln. His early life on Kentucky's frontier shaped his character and prepared him to lead the nation through Civil War. The country's first memorial to Lincoln, built with donations from young and old, enshrines the symbolic birthplace cabin.\",\"designation\":\"National Historical Park\",\"directionsInfo\":\"The Birthplace Unit of the park is located approximately 2 miles south of the town of Hodgenville on U.S. Highway 31E South. The Boyhood Home Unit at Knob Creek is located approximately 10 miles northeast of the Birthplace Unit of the park.\",\"directionsUrl\":\"http://www.nps.gov/abli/planyourvisit/directions.htm\",\"entranceFees\":[{\"cost\":\"0.00\",\"description\":\"There is no fee associated with visiting either unit of the park.\",\"title\":\"Fee Free Park\"}],\"entrancePasses\":[],\"fees\":[],\"fullName\":\"Abraham Lincoln Birthplace National Historical Park\",\"id\":\"77E0D7F0-1942-494A-ACE2-9004D2BDC59E\",\"images\":[{\"altText\":\"The Memorial Building surrounded by fall colors\",\"caption\":\"Over 200,000 people a year come to walk up the steps of the Memorial Building to visit the site where Abraham Lincoln was born\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building with fall colors\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861078-1DD8-B71B-0B774A242EF6A706.jpg\"},{\"altText\":\"The first memorial erected to honor Abraham Lincoln\",\"caption\":\"The Memorial Building constructed on the traditional site of the birth of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861263-1DD8-B71B-0B71EF9B95F9644F.jpg\"},{\"altText\":\"The symbolic birth cabin on the traditional site of the birth of Abraham Lincoln.\",\"caption\":\"The symbolic birth cabin of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Symbolic Birth Cabin of Abraham Lincoln\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C86137D-1DD8-B71B-0B978BACD7EBAEF1.jpg\"},{\"altText\":\"Statue of the Lincoln family in the park's Visitor Center\",\"caption\":\"Visitors to the park can view the statue of the Lincoln family.\",\"credit\":\"NPS Photo\",\"title\":\"Statue of the Lincoln Family in the Visitor Center\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C8614D1-1DD8-B71B-0B1AF72CA452B051.jpg\"}],\"latLong\":\"lat:37.5858662, long:-85.67330523\",\"latitude\":\"37.5858662\",\"longitude\":\"-85.67330523\",\"name\":\"Abraham Lincoln Birthplace\",\"operatingHours\":[{\"description\":\"The Memorial Building is open 9:00 am - 4:30 pm eastern time with a limited viewing area.  The Visitor Center and grounds of the Birthplace Unit are open 9:00 am - 5:00 pm eastern time with one-way traffic flow and social distancing.\\n\\n\\nThe Boyhood Home Unit at Knob Creek will be closed August 3, 2020, to July 31, 2021, due to the Lincoln Tavern Rehabilitation Project.\",\"exceptions\":[{\"endDate\":\"2022-01-01\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2022-01-01\"},{\"endDate\":\"2021-11-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-11-25\"},{\"endDate\":\"2021-12-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-12-25\"}],\"name\":\"Abraham Lincoln Birthplace National Historical Park\",\"standardHours\":{\"friday\":\"9:00AM - 5:00PM\",\"monday\":\"9:00AM - 5:00PM\",\"saturday\":\"9:00AM - 5:00PM\",\"sunday\":\"9:00AM - 5:00PM\",\"thursday\":\"9:00AM - 5:00PM\",\"tuesday\":\"9:00AM - 5:00PM\",\"wednesday\":\"9:00AM - 5:00PM\"}}],\"parkCode\":\"abli\",\"states\":\"KY\",\"topics\":[{\"id\":\"D10852A3-443C-4743-A5FA-6DD6D2A054B3\",\"name\":\"Birthplace\"},{\"id\":\"F669BC40-BDC4-41C0-9ACE-B2CD25373045\",\"name\":\"Presidents\"},{\"id\":\"0D00073E-18C3-46E5-8727-2F87B112DDC6\",\"name\":\"Animals\"},{\"id\":\"957EF2BD-AC6C-4B7B-BD9A-87593ADC6691\",\"name\":\"Birds\"},{\"id\":\"E25F3456-43ED-45DD-93BC-057F9B944F7A\",\"name\":\"Caves, Caverns and Karst\"},{\"id\":\"F0F97E32-2F29-41B4-AF98-9FBE8DAB36B1\",\"name\":\"Geology\"},{\"id\":\"4BE01DC5-52E6-4F18-8C9A-B22D65965F6D\",\"name\":\"Groundwater\"},{\"id\":\"0E1A04CC-EB51-4F18-93D4-EC0B0B4EC1E3\",\"name\":\"Freshwater Springs\"},{\"id\":\"A7359FC4-DAD8-45F5-AF15-7FF62F816ED3\",\"name\":\"Night Sky\"}],\"url\":\"https://www.nps.gov/abli/index.htm\",\"weatherInfo\":\"There are four distinct seasons in Central Kentucky. However, temperature and weather conditions can vary widely within those seasons. Spring and Fall are generally pleasant with frequent rain showers. Summer is usually hot and humid. Winter is moderately cold with mixed precipitation.\"},{\"activities\":[{\"id\":\"13A57703-BB1A-41A2-94B8-53B692EB7238\",\"name\":\"Astronomy\"},{\"id\":\"D37A0003-8317-4F04-8FB0-4CF0A272E195\",\"name\":\"Stargazing\"},{\"id\":\"1DFACD97-1B9C-4F5A-80F2-05593604799E\",\"name\":\"Food\"},{\"id\":\"C6D3230A-2CEA-4AFE-BFF3-DC1E2C2C4BB4\",\"name\":\"Picnicking\"},{\"id\":\"DF4A35E0-7983-4A3E-BC47-F37B872B0F25\",\"name\":\"Junior Ranger Program\"},{\"id\":\"0B685688-3405-4E2A-ABBA-E3069492EC50\",\"name\":\"Wildlife Watching\"},{\"id\":\"5A2C91D1-50EC-4B24-8BED-A2E11A1892DF\",\"name\":\"Birdwatching\"},{\"id\":\"24380E3F-AD9D-4E38-BF13-C8EEB21893E7\",\"name\":\"Shopping\"},{\"id\":\"467DC8B8-0B7D-436D-A026-80A22358F615\",\"name\":\"Bookstore and Park Store\"},{\"id\":\"43800AD1-D439-40F3-AAB3-9FB651FE45BB\",\"name\":\"Gift Shop and Souvenirs\"}],\"addresses\":[{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Physical\"},{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Mailing\"}],\"contacts\":{\"emailAddresses\":[{\"description\":\"\",\"emailAddress\":\"ABLI_Administration@nps.gov\"}],\"phoneNumbers\":[{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583137\",\"type\":\"Voice\"},{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583874\",\"type\":\"Fax\"}]},\"description\":\"For over a century people from around the world have come to rural Central Kentucky to honor the humble beginnings of our 16th president, Abraham Lincoln. His early life on Kentucky's frontier shaped his character and prepared him to lead the nation through Civil War. The country's first memorial to Lincoln, built with donations from young and old, enshrines the symbolic birthplace cabin.\",\"designation\":\"National Historical Park\",\"directionsInfo\":\"The Birthplace Unit of the park is located approximately 2 miles south of the town of Hodgenville on U.S. Highway 31E South. The Boyhood Home Unit at Knob Creek is located approximately 10 miles northeast of the Birthplace Unit of the park.\",\"directionsUrl\":\"http://www.nps.gov/abli/planyourvisit/directions.htm\",\"entranceFees\":[{\"cost\":\"0.00\",\"description\":\"There is no fee associated with visiting either unit of the park.\",\"title\":\"Fee Free Park\"}],\"entrancePasses\":[],\"fees\":[],\"fullName\":\"Abraham Lincoln Birthplace National Historical Park\",\"id\":\"77E0D7F0-1942-494A-ACE2-9004D2BDC59E\",\"images\":[{\"altText\":\"The Memorial Building surrounded by fall colors\",\"caption\":\"Over 200,000 people a year come to walk up the steps of the Memorial Building to visit the site where Abraham Lincoln was born\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building with fall colors\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861078-1DD8-B71B-0B774A242EF6A706.jpg\"},{\"altText\":\"The first memorial erected to honor Abraham Lincoln\",\"caption\":\"The Memorial Building constructed on the traditional site of the birth of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861263-1DD8-B71B-0B71EF9B95F9644F.jpg\"},{\"altText\":\"The symbolic birth cabin on the traditional site of the birth of Abraham Lincoln.\",\"caption\":\"The symbolic birth cabin of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Symbolic Birth Cabin of Abraham Lincoln\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C86137D-1DD8-B71B-0B978BACD7EBAEF1.jpg\"},{\"altText\":\"Statue of the Lincoln family in the park's Visitor Center\",\"caption\":\"Visitors to the park can view the statue of the Lincoln family.\",\"credit\":\"NPS Photo\",\"title\":\"Statue of the Lincoln Family in the Visitor Center\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C8614D1-1DD8-B71B-0B1AF72CA452B051.jpg\"}],\"latLong\":\"lat:37.5858662, long:-85.67330523\",\"latitude\":\"37.5858662\",\"longitude\":\"-85.67330523\",\"name\":\"Abraham Lincoln Birthplace\",\"operatingHours\":[{\"description\":\"The Memorial Building is open 9:00 am - 4:30 pm eastern time with a limited viewing area.  The Visitor Center and grounds of the Birthplace Unit are open 9:00 am - 5:00 pm eastern time with one-way traffic flow and social distancing.\\n\\n\\nThe Boyhood Home Unit at Knob Creek will be closed August 3, 2020, to July 31, 2021, due to the Lincoln Tavern Rehabilitation Project.\",\"exceptions\":[{\"endDate\":\"2022-01-01\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2022-01-01\"},{\"endDate\":\"2021-11-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-11-25\"},{\"endDate\":\"2021-12-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-12-25\"}],\"name\":\"Abraham Lincoln Birthplace National Historical Park\",\"standardHours\":{\"friday\":\"9:00AM - 5:00PM\",\"monday\":\"9:00AM - 5:00PM\",\"saturday\":\"9:00AM - 5:00PM\",\"sunday\":\"9:00AM - 5:00PM\",\"thursday\":\"9:00AM - 5:00PM\",\"tuesday\":\"9:00AM - 5:00PM\",\"wednesday\":\"9:00AM - 5:00PM\"}}],\"parkCode\":\"abli\",\"states\":\"KY\",\"topics\":[{\"id\":\"D10852A3-443C-4743-A5FA-6DD6D2A054B3\",\"name\":\"Birthplace\"},{\"id\":\"F669BC40-BDC4-41C0-9ACE-B2CD25373045\",\"name\":\"Presidents\"},{\"id\":\"0D00073E-18C3-46E5-8727-2F87B112DDC6\",\"name\":\"Animals\"},{\"id\":\"957EF2BD-AC6C-4B7B-BD9A-87593ADC6691\",\"name\":\"Birds\"},{\"id\":\"E25F3456-43ED-45DD-93BC-057F9B944F7A\",\"name\":\"Caves, Caverns and Karst\"},{\"id\":\"F0F97E32-2F29-41B4-AF98-9FBE8DAB36B1\",\"name\":\"Geology\"},{\"id\":\"4BE01DC5-52E6-4F18-8C9A-B22D65965F6D\",\"name\":\"Groundwater\"},{\"id\":\"0E1A04CC-EB51-4F18-93D4-EC0B0B4EC1E3\",\"name\":\"Freshwater Springs\"},{\"id\":\"A7359FC4-DAD8-45F5-AF15-7FF62F816ED3\",\"name\":\"Night Sky\"}],\"url\":\"https://www.nps.gov/abli/index.htm\",\"weatherInfo\":\"There are four distinct seasons in Central Kentucky. However, temperature and weather conditions can vary widely within those seasons. Spring and Fall are generally pleasant with frequent rain showers. Summer is usually hot and humid. Winter is moderately cold with mixed precipitation.\"},{\"activities\":[{\"id\":\"13A57703-BB1A-41A2-94B8-53B692EB7238\",\"name\":\"Astronomy\"},{\"id\":\"D37A0003-8317-4F04-8FB0-4CF0A272E195\",\"name\":\"Stargazing\"},{\"id\":\"1DFACD97-1B9C-4F5A-80F2-05593604799E\",\"name\":\"Food\"},{\"id\":\"C6D3230A-2CEA-4AFE-BFF3-DC1E2C2C4BB4\",\"name\":\"Picnicking\"},{\"id\":\"DF4A35E0-7983-4A3E-BC47-F37B872B0F25\",\"name\":\"Junior Ranger Program\"},{\"id\":\"0B685688-3405-4E2A-ABBA-E3069492EC50\",\"name\":\"Wildlife Watching\"},{\"id\":\"5A2C91D1-50EC-4B24-8BED-A2E11A1892DF\",\"name\":\"Birdwatching\"},{\"id\":\"24380E3F-AD9D-4E38-BF13-C8EEB21893E7\",\"name\":\"Shopping\"},{\"id\":\"467DC8B8-0B7D-436D-A026-80A22358F615\",\"name\":\"Bookstore and Park Store\"},{\"id\":\"43800AD1-D439-40F3-AAB3-9FB651FE45BB\",\"name\":\"Gift Shop and Souvenirs\"}],\"addresses\":[{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Physical\"},{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Mailing\"}],\"contacts\":{\"emailAddresses\":[{\"description\":\"\",\"emailAddress\":\"ABLI_Administration@nps.gov\"}],\"phoneNumbers\":[{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583137\",\"type\":\"Voice\"},{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583874\",\"type\":\"Fax\"}]},\"description\":\"For over a century people from around the world have come to rural Central Kentucky to honor the humble beginnings of our 16th president, Abraham Lincoln. His early life on Kentucky's frontier shaped his character and prepared him to lead the nation through Civil War. The country's first memorial to Lincoln, built with donations from young and old, enshrines the symbolic birthplace cabin.\",\"designation\":\"National Historical Park\",\"directionsInfo\":\"The Birthplace Unit of the park is located approximately 2 miles south of the town of Hodgenville on U.S. Highway 31E South. The Boyhood Home Unit at Knob Creek is located approximately 10 miles northeast of the Birthplace Unit of the park.\",\"directionsUrl\":\"http://www.nps.gov/abli/planyourvisit/directions.htm\",\"entranceFees\":[{\"cost\":\"0.00\",\"description\":\"There is no fee associated with visiting either unit of the park.\",\"title\":\"Fee Free Park\"}],\"entrancePasses\":[],\"fees\":[],\"fullName\":\"Abraham Lincoln Birthplace National Historical Park\",\"id\":\"77E0D7F0-1942-494A-ACE2-9004D2BDC59E\",\"images\":[{\"altText\":\"The Memorial Building surrounded by fall colors\",\"caption\":\"Over 200,000 people a year come to walk up the steps of the Memorial Building to visit the site where Abraham Lincoln was born\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building with fall colors\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861078-1DD8-B71B-0B774A242EF6A706.jpg\"},{\"altText\":\"The first memorial erected to honor Abraham Lincoln\",\"caption\":\"The Memorial Building constructed on the traditional site of the birth of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861263-1DD8-B71B-0B71EF9B95F9644F.jpg\"},{\"altText\":\"The symbolic birth cabin on the traditional site of the birth of Abraham Lincoln.\",\"caption\":\"The symbolic birth cabin of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Symbolic Birth Cabin of Abraham Lincoln\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C86137D-1DD8-B71B-0B978BACD7EBAEF1.jpg\"},{\"altText\":\"Statue of the Lincoln family in the park's Visitor Center\",\"caption\":\"Visitors to the park can view the statue of the Lincoln family.\",\"credit\":\"NPS Photo\",\"title\":\"Statue of the Lincoln Family in the Visitor Center\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C8614D1-1DD8-B71B-0B1AF72CA452B051.jpg\"}],\"latLong\":\"lat:37.5858662, long:-85.67330523\",\"latitude\":\"37.5858662\",\"longitude\":\"-85.67330523\",\"name\":\"Abraham Lincoln Birthplace\",\"operatingHours\":[{\"description\":\"The Memorial Building is open 9:00 am - 4:30 pm eastern time with a limited viewing area.  The Visitor Center and grounds of the Birthplace Unit are open 9:00 am - 5:00 pm eastern time with one-way traffic flow and social distancing.\\n\\n\\nThe Boyhood Home Unit at Knob Creek will be closed August 3, 2020, to July 31, 2021, due to the Lincoln Tavern Rehabilitation Project.\",\"exceptions\":[{\"endDate\":\"2022-01-01\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2022-01-01\"},{\"endDate\":\"2021-11-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-11-25\"},{\"endDate\":\"2021-12-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-12-25\"}],\"name\":\"Abraham Lincoln Birthplace National Historical Park\",\"standardHours\":{\"friday\":\"9:00AM - 5:00PM\",\"monday\":\"9:00AM - 5:00PM\",\"saturday\":\"9:00AM - 5:00PM\",\"sunday\":\"9:00AM - 5:00PM\",\"thursday\":\"9:00AM - 5:00PM\",\"tuesday\":\"9:00AM - 5:00PM\",\"wednesday\":\"9:00AM - 5:00PM\"}}],\"parkCode\":\"abli\",\"states\":\"KY\",\"topics\":[{\"id\":\"D10852A3-443C-4743-A5FA-6DD6D2A054B3\",\"name\":\"Birthplace\"},{\"id\":\"F669BC40-BDC4-41C0-9ACE-B2CD25373045\",\"name\":\"Presidents\"},{\"id\":\"0D00073E-18C3-46E5-8727-2F87B112DDC6\",\"name\":\"Animals\"},{\"id\":\"957EF2BD-AC6C-4B7B-BD9A-87593ADC6691\",\"name\":\"Birds\"},{\"id\":\"E25F3456-43ED-45DD-93BC-057F9B944F7A\",\"name\":\"Caves, Caverns and Karst\"},{\"id\":\"F0F97E32-2F29-41B4-AF98-9FBE8DAB36B1\",\"name\":\"Geology\"},{\"id\":\"4BE01DC5-52E6-4F18-8C9A-B22D65965F6D\",\"name\":\"Groundwater\"},{\"id\":\"0E1A04CC-EB51-4F18-93D4-EC0B0B4EC1E3\",\"name\":\"Freshwater Springs\"},{\"id\":\"A7359FC4-DAD8-45F5-AF15-7FF62F816ED3\",\"name\":\"Night Sky\"}],\"url\":\"https://www.nps.gov/abli/index.htm\",\"weatherInfo\":\"There are four distinct seasons in Central Kentucky. However, temperature and weather conditions can vary widely within those seasons. Spring and Fall are generally pleasant with frequent rain showers. Summer is usually hot and humid. Winter is moderately cold with mixed precipitation.\"},{\"activities\":[{\"id\":\"13A57703-BB1A-41A2-94B8-53B692EB7238\",\"name\":\"Astronomy\"},{\"id\":\"D37A0003-8317-4F04-8FB0-4CF0A272E195\",\"name\":\"Stargazing\"},{\"id\":\"1DFACD97-1B9C-4F5A-80F2-05593604799E\",\"name\":\"Food\"},{\"id\":\"C6D3230A-2CEA-4AFE-BFF3-DC1E2C2C4BB4\",\"name\":\"Picnicking\"},{\"id\":\"DF4A35E0-7983-4A3E-BC47-F37B872B0F25\",\"name\":\"Junior Ranger Program\"},{\"id\":\"0B685688-3405-4E2A-ABBA-E3069492EC50\",\"name\":\"Wildlife Watching\"},{\"id\":\"5A2C91D1-50EC-4B24-8BED-A2E11A1892DF\",\"name\":\"Birdwatching\"},{\"id\":\"24380E3F-AD9D-4E38-BF13-C8EEB21893E7\",\"name\":\"Shopping\"},{\"id\":\"467DC8B8-0B7D-436D-A026-80A22358F615\",\"name\":\"Bookstore and Park Store\"},{\"id\":\"43800AD1-D439-40F3-AAB3-9FB651FE45BB\",\"name\":\"Gift Shop and Souvenirs\"}],\"addresses\":[{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Physical\"},{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Mailing\"}],\"contacts\":{\"emailAddresses\":[{\"description\":\"\",\"emailAddress\":\"ABLI_Administration@nps.gov\"}],\"phoneNumbers\":[{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583137\",\"type\":\"Voice\"},{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583874\",\"type\":\"Fax\"}]},\"description\":\"For over a century people from around the world have come to rural Central Kentucky to honor the humble beginnings of our 16th president, Abraham Lincoln. His early life on Kentucky's frontier shaped his character and prepared him to lead the nation through Civil War. The country's first memorial to Lincoln, built with donations from young and old, enshrines the symbolic birthplace cabin.\",\"designation\":\"National Historical Park\",\"directionsInfo\":\"The Birthplace Unit of the park is located approximately 2 miles south of the town of Hodgenville on U.S. Highway 31E South. The Boyhood Home Unit at Knob Creek is located approximately 10 miles northeast of the Birthplace Unit of the park.\",\"directionsUrl\":\"http://www.nps.gov/abli/planyourvisit/directions.htm\",\"entranceFees\":[{\"cost\":\"0.00\",\"description\":\"There is no fee associated with visiting either unit of the park.\",\"title\":\"Fee Free Park\"}],\"entrancePasses\":[],\"fees\":[],\"fullName\":\"Abraham Lincoln Birthplace National Historical Park\",\"id\":\"77E0D7F0-1942-494A-ACE2-9004D2BDC59E\",\"images\":[{\"altText\":\"The Memorial Building surrounded by fall colors\",\"caption\":\"Over 200,000 people a year come to walk up the steps of the Memorial Building to visit the site where Abraham Lincoln was born\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building with fall colors\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861078-1DD8-B71B-0B774A242EF6A706.jpg\"},{\"altText\":\"The first memorial erected to honor Abraham Lincoln\",\"caption\":\"The Memorial Building constructed on the traditional site of the birth of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861263-1DD8-B71B-0B71EF9B95F9644F.jpg\"},{\"altText\":\"The symbolic birth cabin on the traditional site of the birth of Abraham Lincoln.\",\"caption\":\"The symbolic birth cabin of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Symbolic Birth Cabin of Abraham Lincoln\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C86137D-1DD8-B71B-0B978BACD7EBAEF1.jpg\"},{\"altText\":\"Statue of the Lincoln family in the park's Visitor Center\",\"caption\":\"Visitors to the park can view the statue of the Lincoln family.\",\"credit\":\"NPS Photo\",\"title\":\"Statue of the Lincoln Family in the Visitor Center\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C8614D1-1DD8-B71B-0B1AF72CA452B051.jpg\"}],\"latLong\":\"lat:37.5858662, long:-85.67330523\",\"latitude\":\"37.5858662\",\"longitude\":\"-85.67330523\",\"name\":\"Abraham Lincoln Birthplace\",\"operatingHours\":[{\"description\":\"The Memorial Building is open 9:00 am - 4:30 pm eastern time with a limited viewing area.  The Visitor Center and grounds of the Birthplace Unit are open 9:00 am - 5:00 pm eastern time with one-way traffic flow and social distancing.\\n\\n\\nThe Boyhood Home Unit at Knob Creek will be closed August 3, 2020, to July 31, 2021, due to the Lincoln Tavern Rehabilitation Project.\",\"exceptions\":[{\"endDate\":\"2022-01-01\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2022-01-01\"},{\"endDate\":\"2021-11-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-11-25\"},{\"endDate\":\"2021-12-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-12-25\"}],\"name\":\"Abraham Lincoln Birthplace National Historical Park\",\"standardHours\":{\"friday\":\"9:00AM - 5:00PM\",\"monday\":\"9:00AM - 5:00PM\",\"saturday\":\"9:00AM - 5:00PM\",\"sunday\":\"9:00AM - 5:00PM\",\"thursday\":\"9:00AM - 5:00PM\",\"tuesday\":\"9:00AM - 5:00PM\",\"wednesday\":\"9:00AM - 5:00PM\"}}],\"parkCode\":\"abli\",\"states\":\"KY\",\"topics\":[{\"id\":\"D10852A3-443C-4743-A5FA-6DD6D2A054B3\",\"name\":\"Birthplace\"},{\"id\":\"F669BC40-BDC4-41C0-9ACE-B2CD25373045\",\"name\":\"Presidents\"},{\"id\":\"0D00073E-18C3-46E5-8727-2F87B112DDC6\",\"name\":\"Animals\"},{\"id\":\"957EF2BD-AC6C-4B7B-BD9A-87593ADC6691\",\"name\":\"Birds\"},{\"id\":\"E25F3456-43ED-45DD-93BC-057F9B944F7A\",\"name\":\"Caves, Caverns and Karst\"},{\"id\":\"F0F97E32-2F29-41B4-AF98-9FBE8DAB36B1\",\"name\":\"Geology\"},{\"id\":\"4BE01DC5-52E6-4F18-8C9A-B22D65965F6D\",\"name\":\"Groundwater\"},{\"id\":\"0E1A04CC-EB51-4F18-93D4-EC0B0B4EC1E3\",\"name\":\"Freshwater Springs\"},{\"id\":\"A7359FC4-DAD8-45F5-AF15-7FF62F816ED3\",\"name\":\"Night Sky\"}],\"url\":\"https://www.nps.gov/abli/index.htm\",\"weatherInfo\":\"There are four distinct seasons in Central Kentucky. However, temperature and weather conditions can vary widely within those seasons. Spring and Fall are generally pleasant with frequent rain showers. Summer is usually hot and humid. Winter is moderately cold with mixed precipitation.\"},{\"activities\":[{\"id\":\"13A57703-BB1A-41A2-94B8-53B692EB7238\",\"name\":\"Astronomy\"},{\"id\":\"D37A0003-8317-4F04-8FB0-4CF0A272E195\",\"name\":\"Stargazing\"},{\"id\":\"1DFACD97-1B9C-4F5A-80F2-05593604799E\",\"name\":\"Food\"},{\"id\":\"C6D3230A-2CEA-4AFE-BFF3-DC1E2C2C4BB4\",\"name\":\"Picnicking\"},{\"id\":\"DF4A35E0-7983-4A3E-BC47-F37B872B0F25\",\"name\":\"Junior Ranger Program\"},{\"id\":\"0B685688-3405-4E2A-ABBA-E3069492EC50\",\"name\":\"Wildlife Watching\"},{\"id\":\"5A2C91D1-50EC-4B24-8BED-A2E11A1892DF\",\"name\":\"Birdwatching\"},{\"id\":\"24380E3F-AD9D-4E38-BF13-C8EEB21893E7\",\"name\":\"Shopping\"},{\"id\":\"467DC8B8-0B7D-436D-A026-80A22358F615\",\"name\":\"Bookstore and Park Store\"},{\"id\":\"43800AD1-D439-40F3-AAB3-9FB651FE45BB\",\"name\":\"Gift Shop and Souvenirs\"}],\"addresses\":[{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Physical\"},{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Mailing\"}],\"contacts\":{\"emailAddresses\":[{\"description\":\"\",\"emailAddress\":\"ABLI_Administration@nps.gov\"}],\"phoneNumbers\":[{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583137\",\"type\":\"Voice\"},{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583874\",\"type\":\"Fax\"}]},\"description\":\"For over a century people from around the world have come to rural Central Kentucky to honor the humble beginnings of our 16th president, Abraham Lincoln. His early life on Kentucky's frontier shaped his character and prepared him to lead the nation through Civil War. The country's first memorial to Lincoln, built with donations from young and old, enshrines the symbolic birthplace cabin.\",\"designation\":\"National Historical Park\",\"directionsInfo\":\"The Birthplace Unit of the park is located approximately 2 miles south of the town of Hodgenville on U.S. Highway 31E South. The Boyhood Home Unit at Knob Creek is located approximately 10 miles northeast of the Birthplace Unit of the park.\",\"directionsUrl\":\"http://www.nps.gov/abli/planyourvisit/directions.htm\",\"entranceFees\":[{\"cost\":\"0.00\",\"description\":\"There is no fee associated with visiting either unit of the park.\",\"title\":\"Fee Free Park\"}],\"entrancePasses\":[],\"fees\":[],\"fullName\":\"Abraham Lincoln Birthplace National Historical Park\",\"id\":\"77E0D7F0-1942-494A-ACE2-9004D2BDC59E\",\"images\":[{\"altText\":\"The Memorial Building surrounded by fall colors\",\"caption\":\"Over 200,000 people a year come to walk up the steps of the Memorial Building to visit the site where Abraham Lincoln was born\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building with fall colors\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861078-1DD8-B71B-0B774A242EF6A706.jpg\"},{\"altText\":\"The first memorial erected to honor Abraham Lincoln\",\"caption\":\"The Memorial Building constructed on the traditional site of the birth of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861263-1DD8-B71B-0B71EF9B95F9644F.jpg\"},{\"altText\":\"The symbolic birth cabin on the traditional site of the birth of Abraham Lincoln.\",\"caption\":\"The symbolic birth cabin of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Symbolic Birth Cabin of Abraham Lincoln\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C86137D-1DD8-B71B-0B978BACD7EBAEF1.jpg\"},{\"altText\":\"Statue of the Lincoln family in the park's Visitor Center\",\"caption\":\"Visitors to the park can view the statue of the Lincoln family.\",\"credit\":\"NPS Photo\",\"title\":\"Statue of the Lincoln Family in the Visitor Center\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C8614D1-1DD8-B71B-0B1AF72CA452B051.jpg\"}],\"latLong\":\"lat:37.5858662, long:-85.67330523\",\"latitude\":\"37.5858662\",\"longitude\":\"-85.67330523\",\"name\":\"Abraham Lincoln Birthplace\",\"operatingHours\":[{\"description\":\"The Memorial Building is open 9:00 am - 4:30 pm eastern time with a limited viewing area.  The Visitor Center and grounds of the Birthplace Unit are open 9:00 am - 5:00 pm eastern time with one-way traffic flow and social distancing.\\n\\n\\nThe Boyhood Home Unit at Knob Creek will be closed August 3, 2020, to July 31, 2021, due to the Lincoln Tavern Rehabilitation Project.\",\"exceptions\":[{\"endDate\":\"2022-01-01\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2022-01-01\"},{\"endDate\":\"2021-11-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-11-25\"},{\"endDate\":\"2021-12-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-12-25\"}],\"name\":\"Abraham Lincoln Birthplace National Historical Park\",\"standardHours\":{\"friday\":\"9:00AM - 5:00PM\",\"monday\":\"9:00AM - 5:00PM\",\"saturday\":\"9:00AM - 5:00PM\",\"sunday\":\"9:00AM - 5:00PM\",\"thursday\":\"9:00AM - 5:00PM\",\"tuesday\":\"9:00AM - 5:00PM\",\"wednesday\":\"9:00AM - 5:00PM\"}}],\"parkCode\":\"abli\",\"states\":\"KY\",\"topics\":[{\"id\":\"D10852A3-443C-4743-A5FA-6DD6D2A054B3\",\"name\":\"Birthplace\"},{\"id\":\"F669BC40-BDC4-41C0-9ACE-B2CD25373045\",\"name\":\"Presidents\"},{\"id\":\"0D00073E-18C3-46E5-8727-2F87B112DDC6\",\"name\":\"Animals\"},{\"id\":\"957EF2BD-AC6C-4B7B-BD9A-87593ADC6691\",\"name\":\"Birds\"},{\"id\":\"E25F3456-43ED-45DD-93BC-057F9B944F7A\",\"name\":\"Caves, Caverns and Karst\"},{\"id\":\"F0F97E32-2F29-41B4-AF98-9FBE8DAB36B1\",\"name\":\"Geology\"},{\"id\":\"4BE01DC5-52E6-4F18-8C9A-B22D65965F6D\",\"name\":\"Groundwater\"},{\"id\":\"0E1A04CC-EB51-4F18-93D4-EC0B0B4EC1E3\",\"name\":\"Freshwater Springs\"},{\"id\":\"A7359FC4-DAD8-45F5-AF15-7FF62F816ED3\",\"name\":\"Night Sky\"}],\"url\":\"https://www.nps.gov/abli/index.htm\",\"weatherInfo\":\"There are four distinct seasons in Central Kentucky. However, temperature and weather conditions can vary widely within those seasons. Spring and Fall are generally pleasant with frequent rain showers. Summer is usually hot and humid. Winter is moderately cold with mixed precipitation.\"},{\"activities\":[{\"id\":\"13A57703-BB1A-41A2-94B8-53B692EB7238\",\"name\":\"Astronomy\"},{\"id\":\"D37A0003-8317-4F04-8FB0-4CF0A272E195\",\"name\":\"Stargazing\"},{\"id\":\"1DFACD97-1B9C-4F5A-80F2-05593604799E\",\"name\":\"Food\"},{\"id\":\"C6D3230A-2CEA-4AFE-BFF3-DC1E2C2C4BB4\",\"name\":\"Picnicking\"},{\"id\":\"DF4A35E0-7983-4A3E-BC47-F37B872B0F25\",\"name\":\"Junior Ranger Program\"},{\"id\":\"0B685688-3405-4E2A-ABBA-E3069492EC50\",\"name\":\"Wildlife Watching\"},{\"id\":\"5A2C91D1-50EC-4B24-8BED-A2E11A1892DF\",\"name\":\"Birdwatching\"},{\"id\":\"24380E3F-AD9D-4E38-BF13-C8EEB21893E7\",\"name\":\"Shopping\"},{\"id\":\"467DC8B8-0B7D-436D-A026-80A22358F615\",\"name\":\"Bookstore and Park Store\"},{\"id\":\"43800AD1-D439-40F3-AAB3-9FB651FE45BB\",\"name\":\"Gift Shop and Souvenirs\"}],\"addresses\":[{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Physical\"},{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Mailing\"}],\"contacts\":{\"emailAddresses\":[{\"description\":\"\",\"emailAddress\":\"ABLI_Administration@nps.gov\"}],\"phoneNumbers\":[{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583137\",\"type\":\"Voice\"},{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583874\",\"type\":\"Fax\"}]},\"description\":\"For over a century people from around the world have come to rural Central Kentucky to honor the humble beginnings of our 16th president, Abraham Lincoln. His early life on Kentucky's frontier shaped his character and prepared him to lead the nation through Civil War. The country's first memorial to Lincoln, built with donations from young and old, enshrines the symbolic birthplace cabin.\",\"designation\":\"National Historical Park\",\"directionsInfo\":\"The Birthplace Unit of the park is located approximately 2 miles south of the town of Hodgenville on U.S. Highway 31E South. The Boyhood Home Unit at Knob Creek is located approximately 10 miles northeast of the Birthplace Unit of the park.\",\"directionsUrl\":\"http://www.nps.gov/abli/planyourvisit/directions.htm\",\"entranceFees\":[{\"cost\":\"0.00\",\"description\":\"There is no fee associated with visiting either unit of the park.\",\"title\":\"Fee Free Park\"}],\"entrancePasses\":[],\"fees\":[],\"fullName\":\"Abraham Lincoln Birthplace National Historical Park\",\"id\":\"77E0D7F0-1942-494A-ACE2-9004D2BDC59E\",\"images\":[{\"altText\":\"The Memorial Building surrounded by fall colors\",\"caption\":\"Over 200,000 people a year come to walk up the steps of the Memorial Building to visit the site where Abraham Lincoln was born\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building with fall colors\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861078-1DD8-B71B-0B774A242EF6A706.jpg\"},{\"altText\":\"The first memorial erected to honor Abraham Lincoln\",\"caption\":\"The Memorial Building constructed on the traditional site of the birth of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861263-1DD8-B71B-0B71EF9B95F9644F.jpg\"},{\"altText\":\"The symbolic birth cabin on the traditional site of the birth of Abraham Lincoln.\",\"caption\":\"The symbolic birth cabin of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Symbolic Birth Cabin of Abraham Lincoln\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C86137D-1DD8-B71B-0B978BACD7EBAEF1.jpg\"},{\"altText\":\"Statue of the Lincoln family in the park's Visitor Center\",\"caption\":\"Visitors to the park can view the statue of the Lincoln family.\",\"credit\":\"NPS Photo\",\"title\":\"Statue of the Lincoln Family in the Visitor Center\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C8614D1-1DD8-B71B-0B1AF72CA452B051.jpg\"}],\"latLong\":\"lat:37.5858662, long:-85.67330523\",\"latitude\":\"37.5858662\",\"longitude\":\"-85.67330523\",\"name\":\"Abraham Lincoln Birthplace\",\"operatingHours\":[{\"description\":\"The Memorial Building is open 9:00 am - 4:30 pm eastern time with a limited viewing area.  The Visitor Center and grounds of the Birthplace Unit are open 9:00 am - 5:00 pm eastern time with one-way traffic flow and social distancing.\\n\\n\\nThe Boyhood Home Unit at Knob Creek will be closed August 3, 2020, to July 31, 2021, due to the Lincoln Tavern Rehabilitation Project.\",\"exceptions\":[{\"endDate\":\"2022-01-01\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2022-01-01\"},{\"endDate\":\"2021-11-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-11-25\"},{\"endDate\":\"2021-12-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-12-25\"}],\"name\":\"Abraham Lincoln Birthplace National Historical Park\",\"standardHours\":{\"friday\":\"9:00AM - 5:00PM\",\"monday\":\"9:00AM - 5:00PM\",\"saturday\":\"9:00AM - 5:00PM\",\"sunday\":\"9:00AM - 5:00PM\",\"thursday\":\"9:00AM - 5:00PM\",\"tuesday\":\"9:00AM - 5:00PM\",\"wednesday\":\"9:00AM - 5:00PM\"}}],\"parkCode\":\"abli\",\"states\":\"KY\",\"topics\":[{\"id\":\"D10852A3-443C-4743-A5FA-6DD6D2A054B3\",\"name\":\"Birthplace\"},{\"id\":\"F669BC40-BDC4-41C0-9ACE-B2CD25373045\",\"name\":\"Presidents\"},{\"id\":\"0D00073E-18C3-46E5-8727-2F87B112DDC6\",\"name\":\"Animals\"},{\"id\":\"957EF2BD-AC6C-4B7B-BD9A-87593ADC6691\",\"name\":\"Birds\"},{\"id\":\"E25F3456-43ED-45DD-93BC-057F9B944F7A\",\"name\":\"Caves, Caverns and Karst\"},{\"id\":\"F0F97E32-2F29-41B4-AF98-9FBE8DAB36B1\",\"name\":\"Geology\"},{\"id\":\"4BE01DC5-52E6-4F18-8C9A-B22D65965F6D\",\"name\":\"Groundwater\"},{\"id\":\"0E1A04CC-EB51-4F18-93D4-EC0B0B4EC1E3\",\"name\":\"Freshwater Springs\"},{\"id\":\"A7359FC4-DAD8-45F5-AF15-7FF62F816ED3\",\"name\":\"Night Sky\"}],\"url\":\"https://www.nps.gov/abli/index.htm\",\"weatherInfo\":\"There are four distinct seasons in Central Kentucky. However, temperature and weather conditions can vary widely within those seasons. Spring and Fall are generally pleasant with frequent rain showers. Summer is usually hot and humid. Winter is moderately cold with mixed precipitation.\"},{\"activities\":[{\"id\":\"13A57703-BB1A-41A2-94B8-53B692EB7238\",\"name\":\"Astronomy\"},{\"id\":\"D37A0003-8317-4F04-8FB0-4CF0A272E195\",\"name\":\"Stargazing\"},{\"id\":\"1DFACD97-1B9C-4F5A-80F2-05593604799E\",\"name\":\"Food\"},{\"id\":\"C6D3230A-2CEA-4AFE-BFF3-DC1E2C2C4BB4\",\"name\":\"Picnicking\"},{\"id\":\"DF4A35E0-7983-4A3E-BC47-F37B872B0F25\",\"name\":\"Junior Ranger Program\"},{\"id\":\"0B685688-3405-4E2A-ABBA-E3069492EC50\",\"name\":\"Wildlife Watching\"},{\"id\":\"5A2C91D1-50EC-4B24-8BED-A2E11A1892DF\",\"name\":\"Birdwatching\"},{\"id\":\"24380E3F-AD9D-4E38-BF13-C8EEB21893E7\",\"name\":\"Shopping\"},{\"id\":\"467DC8B8-0B7D-436D-A026-80A22358F615\",\"name\":\"Bookstore and Park Store\"},{\"id\":\"43800AD1-D439-40F3-AAB3-9FB651FE45BB\",\"name\":\"Gift Shop and Souvenirs\"}],\"addresses\":[{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Physical\"},{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Mailing\"}],\"contacts\":{\"emailAddresses\":[{\"description\":\"\",\"emailAddress\":\"ABLI_Administration@nps.gov\"}],\"phoneNumbers\":[{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583137\",\"type\":\"Voice\"},{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583874\",\"type\":\"Fax\"}]},\"description\":\"For over a century people from around the world have come to rural Central Kentucky to honor the humble beginnings of our 16th president, Abraham Lincoln. His early life on Kentucky's frontier shaped his character and prepared him to lead the nation through Civil War. The country's first memorial to Lincoln, built with donations from young and old, enshrines the symbolic birthplace cabin.\",\"designation\":\"National Historical Park\",\"directionsInfo\":\"The Birthplace Unit of the park is located approximately 2 miles south of the town of Hodgenville on U.S. Highway 31E South. The Boyhood Home Unit at Knob Creek is located approximately 10 miles northeast of the Birthplace Unit of the park.\",\"directionsUrl\":\"http://www.nps.gov/abli/planyourvisit/directions.htm\",\"entranceFees\":[{\"cost\":\"0.00\",\"description\":\"There is no fee associated with visiting either unit of the park.\",\"title\":\"Fee Free Park\"}],\"entrancePasses\":[],\"fees\":[],\"fullName\":\"Abraham Lincoln Birthplace National Historical Park\",\"id\":\"77E0D7F0-1942-494A-ACE2-9004D2BDC59E\",\"images\":[{\"altText\":\"The Memorial Building surrounded by fall colors\",\"caption\":\"Over 200,000 people a year come to walk up the steps of the Memorial Building to visit the site where Abraham Lincoln was born\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building with fall colors\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861078-1DD8-B71B-0B774A242EF6A706.jpg\"},{\"altText\":\"The first memorial erected to honor Abraham Lincoln\",\"caption\":\"The Memorial Building constructed on the traditional site of the birth of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861263-1DD8-B71B-0B71EF9B95F9644F.jpg\"},{\"altText\":\"The symbolic birth cabin on the traditional site of the birth of Abraham Lincoln.\",\"caption\":\"The symbolic birth cabin of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Symbolic Birth Cabin of Abraham Lincoln\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C86137D-1DD8-B71B-0B978BACD7EBAEF1.jpg\"},{\"altText\":\"Statue of the Lincoln family in the park's Visitor Center\",\"caption\":\"Visitors to the park can view the statue of the Lincoln family.\",\"credit\":\"NPS Photo\",\"title\":\"Statue of the Lincoln Family in the Visitor Center\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C8614D1-1DD8-B71B-0B1AF72CA452B051.jpg\"}],\"latLong\":\"lat:37.5858662, long:-85.67330523\",\"latitude\":\"37.5858662\",\"longitude\":\"-85.67330523\",\"name\":\"Abraham Lincoln Birthplace\",\"operatingHours\":[{\"description\":\"The Memorial Building is open 9:00 am - 4:30 pm eastern time with a limited viewing area.  The Visitor Center and grounds of the Birthplace Unit are open 9:00 am - 5:00 pm eastern time with one-way traffic flow and social distancing.\\n\\n\\nThe Boyhood Home Unit at Knob Creek will be closed August 3, 2020, to July 31, 2021, due to the Lincoln Tavern Rehabilitation Project.\",\"exceptions\":[{\"endDate\":\"2022-01-01\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2022-01-01\"},{\"endDate\":\"2021-11-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-11-25\"},{\"endDate\":\"2021-12-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-12-25\"}],\"name\":\"Abraham Lincoln Birthplace National Historical Park\",\"standardHours\":{\"friday\":\"9:00AM - 5:00PM\",\"monday\":\"9:00AM - 5:00PM\",\"saturday\":\"9:00AM - 5:00PM\",\"sunday\":\"9:00AM - 5:00PM\",\"thursday\":\"9:00AM - 5:00PM\",\"tuesday\":\"9:00AM - 5:00PM\",\"wednesday\":\"9:00AM - 5:00PM\"}}],\"parkCode\":\"abli\",\"states\":\"KY\",\"topics\":[{\"id\":\"D10852A3-443C-4743-A5FA-6DD6D2A054B3\",\"name\":\"Birthplace\"},{\"id\":\"F669BC40-BDC4-41C0-9ACE-B2CD25373045\",\"name\":\"Presidents\"},{\"id\":\"0D00073E-18C3-46E5-8727-2F87B112DDC6\",\"name\":\"Animals\"},{\"id\":\"957EF2BD-AC6C-4B7B-BD9A-87593ADC6691\",\"name\":\"Birds\"},{\"id\":\"E25F3456-43ED-45DD-93BC-057F9B944F7A\",\"name\":\"Caves, Caverns and Karst\"},{\"id\":\"F0F97E32-2F29-41B4-AF98-9FBE8DAB36B1\",\"name\":\"Geology\"},{\"id\":\"4BE01DC5-52E6-4F18-8C9A-B22D65965F6D\",\"name\":\"Groundwater\"},{\"id\":\"0E1A04CC-EB51-4F18-93D4-EC0B0B4EC1E3\",\"name\":\"Freshwater Springs\"},{\"id\":\"A7359FC4-DAD8-45F5-AF15-7FF62F816ED3\",\"name\":\"Night Sky\"}],\"url\":\"https://www.nps.gov/abli/index.htm\",\"weatherInfo\":\"There are four distinct seasons in Central Kentucky. However, temperature and weather conditions can vary widely within those seasons. Spring and Fall are generally pleasant with frequent rain showers. Summer is usually hot and humid. Winter is moderately cold with mixed precipitation.\"},{\"activities\":[{\"id\":\"13A57703-BB1A-41A2-94B8-53B692EB7238\",\"name\":\"Astronomy\"},{\"id\":\"D37A0003-8317-4F04-8FB0-4CF0A272E195\",\"name\":\"Stargazing\"},{\"id\":\"1DFACD97-1B9C-4F5A-80F2-05593604799E\",\"name\":\"Food\"},{\"id\":\"C6D3230A-2CEA-4AFE-BFF3-DC1E2C2C4BB4\",\"name\":\"Picnicking\"},{\"id\":\"DF4A35E0-7983-4A3E-BC47-F37B872B0F25\",\"name\":\"Junior Ranger Program\"},{\"id\":\"0B685688-3405-4E2A-ABBA-E3069492EC50\",\"name\":\"Wildlife Watching\"},{\"id\":\"5A2C91D1-50EC-4B24-8BED-A2E11A1892DF\",\"name\":\"Birdwatching\"},{\"id\":\"24380E3F-AD9D-4E38-BF13-C8EEB21893E7\",\"name\":\"Shopping\"},{\"id\":\"467DC8B8-0B7D-436D-A026-80A22358F615\",\"name\":\"Bookstore and Park Store\"},{\"id\":\"43800AD1-D439-40F3-AAB3-9FB651FE45BB\",\"name\":\"Gift Shop and Souvenirs\"}],\"addresses\":[{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Physical\"},{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Mailing\"}],\"contacts\":{\"emailAddresses\":[{\"description\":\"\",\"emailAddress\":\"ABLI_Administration@nps.gov\"}],\"phoneNumbers\":[{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583137\",\"type\":\"Voice\"},{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583874\",\"type\":\"Fax\"}]},\"description\":\"For over a century people from around the world have come to rural Central Kentucky to honor the humble beginnings of our 16th president, Abraham Lincoln. His early life on Kentucky's frontier shaped his character and prepared him to lead the nation through Civil War. The country's first memorial to Lincoln, built with donations from young and old, enshrines the symbolic birthplace cabin.\",\"designation\":\"National Historical Park\",\"directionsInfo\":\"The Birthplace Unit of the park is located approximately 2 miles south of the town of Hodgenville on U.S. Highway 31E South. The Boyhood Home Unit at Knob Creek is located approximately 10 miles northeast of the Birthplace Unit of the park.\",\"directionsUrl\":\"http://www.nps.gov/abli/planyourvisit/directions.htm\",\"entranceFees\":[{\"cost\":\"0.00\",\"description\":\"There is no fee associated with visiting either unit of the park.\",\"title\":\"Fee Free Park\"}],\"entrancePasses\":[],\"fees\":[],\"fullName\":\"Abraham Lincoln Birthplace National Historical Park\",\"id\":\"77E0D7F0-1942-494A-ACE2-9004D2BDC59E\",\"images\":[{\"altText\":\"The Memorial Building surrounded by fall colors\",\"caption\":\"Over 200,000 people a year come to walk up the steps of the Memorial Building to visit the site where Abraham Lincoln was born\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building with fall colors\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861078-1DD8-B71B-0B774A242EF6A706.jpg\"},{\"altText\":\"The first memorial erected to honor Abraham Lincoln\",\"caption\":\"The Memorial Building constructed on the traditional site of the birth of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861263-1DD8-B71B-0B71EF9B95F9644F.jpg\"},{\"altText\":\"The symbolic birth cabin on the traditional site of the birth of Abraham Lincoln.\",\"caption\":\"The symbolic birth cabin of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Symbolic Birth Cabin of Abraham Lincoln\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C86137D-1DD8-B71B-0B978BACD7EBAEF1.jpg\"},{\"altText\":\"Statue of the Lincoln family in the park's Visitor Center\",\"caption\":\"Visitors to the park can view the statue of the Lincoln family.\",\"credit\":\"NPS Photo\",\"title\":\"Statue of the Lincoln Family in the Visitor Center\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C8614D1-1DD8-B71B-0B1AF72CA452B051.jpg\"}],\"latLong\":\"lat:37.5858662, long:-85.67330523\",\"latitude\":\"37.5858662\",\"longitude\":\"-85.67330523\",\"name\":\"Abraham Lincoln Birthplace\",\"operatingHours\":[{\"description\":\"The Memorial Building is open 9:00 am - 4:30 pm eastern time with a limited viewing area.  The Visitor Center and grounds of the Birthplace Unit are open 9:00 am - 5:00 pm eastern time with one-way traffic flow and social distancing.\\n\\n\\nThe Boyhood Home Unit at Knob Creek will be closed August 3, 2020, to July 31, 2021, due to the Lincoln Tavern Rehabilitation Project.\",\"exceptions\":[{\"endDate\":\"2022-01-01\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2022-01-01\"},{\"endDate\":\"2021-11-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-11-25\"},{\"endDate\":\"2021-12-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-12-25\"}],\"name\":\"Abraham Lincoln Birthplace National Historical Park\",\"standardHours\":{\"friday\":\"9:00AM - 5:00PM\",\"monday\":\"9:00AM - 5:00PM\",\"saturday\":\"9:00AM - 5:00PM\",\"sunday\":\"9:00AM - 5:00PM\",\"thursday\":\"9:00AM - 5:00PM\",\"tuesday\":\"9:00AM - 5:00PM\",\"wednesday\":\"9:00AM - 5:00PM\"}}],\"parkCode\":\"abli\",\"states\":\"KY\",\"topics\":[{\"id\":\"D10852A3-443C-4743-A5FA-6DD6D2A054B3\",\"name\":\"Birthplace\"},{\"id\":\"F669BC40-BDC4-41C0-9ACE-B2CD25373045\",\"name\":\"Presidents\"},{\"id\":\"0D00073E-18C3-46E5-8727-2F87B112DDC6\",\"name\":\"Animals\"},{\"id\":\"957EF2BD-AC6C-4B7B-BD9A-87593ADC6691\",\"name\":\"Birds\"},{\"id\":\"E25F3456-43ED-45DD-93BC-057F9B944F7A\",\"name\":\"Caves, Caverns and Karst\"},{\"id\":\"F0F97E32-2F29-41B4-AF98-9FBE8DAB36B1\",\"name\":\"Geology\"},{\"id\":\"4BE01DC5-52E6-4F18-8C9A-B22D65965F6D\",\"name\":\"Groundwater\"},{\"id\":\"0E1A04CC-EB51-4F18-93D4-EC0B0B4EC1E3\",\"name\":\"Freshwater Springs\"},{\"id\":\"A7359FC4-DAD8-45F5-AF15-7FF62F816ED3\",\"name\":\"Night Sky\"}],\"url\":\"https://www.nps.gov/abli/index.htm\",\"weatherInfo\":\"There are four distinct seasons in Central Kentucky. However, temperature and weather conditions can vary widely within those seasons. Spring and Fall are generally pleasant with frequent rain showers. Summer is usually hot and humid. Winter is moderately cold with mixed precipitation.\"},{\"activities\":[{\"id\":\"13A57703-BB1A-41A2-94B8-53B692EB7238\",\"name\":\"Astronomy\"},{\"id\":\"D37A0003-8317-4F04-8FB0-4CF0A272E195\",\"name\":\"Stargazing\"},{\"id\":\"1DFACD97-1B9C-4F5A-80F2-05593604799E\",\"name\":\"Food\"},{\"id\":\"C6D3230A-2CEA-4AFE-BFF3-DC1E2C2C4BB4\",\"name\":\"Picnicking\"},{\"id\":\"DF4A35E0-7983-4A3E-BC47-F37B872B0F25\",\"name\":\"Junior Ranger Program\"},{\"id\":\"0B685688-3405-4E2A-ABBA-E3069492EC50\",\"name\":\"Wildlife Watching\"},{\"id\":\"5A2C91D1-50EC-4B24-8BED-A2E11A1892DF\",\"name\":\"Birdwatching\"},{\"id\":\"24380E3F-AD9D-4E38-BF13-C8EEB21893E7\",\"name\":\"Shopping\"},{\"id\":\"467DC8B8-0B7D-436D-A026-80A22358F615\",\"name\":\"Bookstore and Park Store\"},{\"id\":\"43800AD1-D439-40F3-AAB3-9FB651FE45BB\",\"name\":\"Gift Shop and Souvenirs\"}],\"addresses\":[{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Physical\"},{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Mailing\"}],\"contacts\":{\"emailAddresses\":[{\"description\":\"\",\"emailAddress\":\"ABLI_Administration@nps.gov\"}],\"phoneNumbers\":[{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583137\",\"type\":\"Voice\"},{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583874\",\"type\":\"Fax\"}]},\"description\":\"For over a century people from around the world have come to rural Central Kentucky to honor the humble beginnings of our 16th president, Abraham Lincoln. His early life on Kentucky's frontier shaped his character and prepared him to lead the nation through Civil War. The country's first memorial to Lincoln, built with donations from young and old, enshrines the symbolic birthplace cabin.\",\"designation\":\"National Historical Park\",\"directionsInfo\":\"The Birthplace Unit of the park is located approximately 2 miles south of the town of Hodgenville on U.S. Highway 31E South. The Boyhood Home Unit at Knob Creek is located approximately 10 miles northeast of the Birthplace Unit of the park.\",\"directionsUrl\":\"http://www.nps.gov/abli/planyourvisit/directions.htm\",\"entranceFees\":[{\"cost\":\"0.00\",\"description\":\"There is no fee associated with visiting either unit of the park.\",\"title\":\"Fee Free Park\"}],\"entrancePasses\":[],\"fees\":[],\"fullName\":\"Abraham Lincoln Birthplace National Historical Park\",\"id\":\"77E0D7F0-1942-494A-ACE2-9004D2BDC59E\",\"images\":[{\"altText\":\"The Memorial Building surrounded by fall colors\",\"caption\":\"Over 200,000 people a year come to walk up the steps of the Memorial Building to visit the site where Abraham Lincoln was born\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building with fall colors\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861078-1DD8-B71B-0B774A242EF6A706.jpg\"},{\"altText\":\"The first memorial erected to honor Abraham Lincoln\",\"caption\":\"The Memorial Building constructed on the traditional site of the birth of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861263-1DD8-B71B-0B71EF9B95F9644F.jpg\"},{\"altText\":\"The symbolic birth cabin on the traditional site of the birth of Abraham Lincoln.\",\"caption\":\"The symbolic birth cabin of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Symbolic Birth Cabin of Abraham Lincoln\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C86137D-1DD8-B71B-0B978BACD7EBAEF1.jpg\"},{\"altText\":\"Statue of the Lincoln family in the park's Visitor Center\",\"caption\":\"Visitors to the park can view the statue of the Lincoln family.\",\"credit\":\"NPS Photo\",\"title\":\"Statue of the Lincoln Family in the Visitor Center\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C8614D1-1DD8-B71B-0B1AF72CA452B051.jpg\"}],\"latLong\":\"lat:37.5858662, long:-85.67330523\",\"latitude\":\"37.5858662\",\"longitude\":\"-85.67330523\",\"name\":\"Abraham Lincoln Birthplace\",\"operatingHours\":[{\"description\":\"The Memorial Building is open 9:00 am - 4:30 pm eastern time with a limited viewing area.  The Visitor Center and grounds of the Birthplace Unit are open 9:00 am - 5:00 pm eastern time with one-way traffic flow and social distancing.\\n\\n\\nThe Boyhood Home Unit at Knob Creek will be closed August 3, 2020, to July 31, 2021, due to the Lincoln Tavern Rehabilitation Project.\",\"exceptions\":[{\"endDate\":\"2022-01-01\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2022-01-01\"},{\"endDate\":\"2021-11-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-11-25\"},{\"endDate\":\"2021-12-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-12-25\"}],\"name\":\"Abraham Lincoln Birthplace National Historical Park\",\"standardHours\":{\"friday\":\"9:00AM - 5:00PM\",\"monday\":\"9:00AM - 5:00PM\",\"saturday\":\"9:00AM - 5:00PM\",\"sunday\":\"9:00AM - 5:00PM\",\"thursday\":\"9:00AM - 5:00PM\",\"tuesday\":\"9:00AM - 5:00PM\",\"wednesday\":\"9:00AM - 5:00PM\"}}],\"parkCode\":\"abli\",\"states\":\"KY\",\"topics\":[{\"id\":\"D10852A3-443C-4743-A5FA-6DD6D2A054B3\",\"name\":\"Birthplace\"},{\"id\":\"F669BC40-BDC4-41C0-9ACE-B2CD25373045\",\"name\":\"Presidents\"},{\"id\":\"0D00073E-18C3-46E5-8727-2F87B112DDC6\",\"name\":\"Animals\"},{\"id\":\"957EF2BD-AC6C-4B7B-BD9A-87593ADC6691\",\"name\":\"Birds\"},{\"id\":\"E25F3456-43ED-45DD-93BC-057F9B944F7A\",\"name\":\"Caves, Caverns and Karst\"},{\"id\":\"F0F97E32-2F29-41B4-AF98-9FBE8DAB36B1\",\"name\":\"Geology\"},{\"id\":\"4BE01DC5-52E6-4F18-8C9A-B22D65965F6D\",\"name\":\"Groundwater\"},{\"id\":\"0E1A04CC-EB51-4F18-93D4-EC0B0B4EC1E3\",\"name\":\"Freshwater Springs\"},{\"id\":\"A7359FC4-DAD8-45F5-AF15-7FF62F816ED3\",\"name\":\"Night Sky\"}],\"url\":\"https://www.nps.gov/abli/index.htm\",\"weatherInfo\":\"There are four distinct seasons in Central Kentucky. However, temperature and weather conditions can vary widely within those seasons. Spring and Fall are generally pleasant with frequent rain showers. Summer is usually hot and humid. Winter is moderately cold with mixed precipitation.\"},{\"activities\":[{\"id\":\"13A57703-BB1A-41A2-94B8-53B692EB7238\",\"name\":\"Astronomy\"},{\"id\":\"D37A0003-8317-4F04-8FB0-4CF0A272E195\",\"name\":\"Stargazing\"},{\"id\":\"1DFACD97-1B9C-4F5A-80F2-05593604799E\",\"name\":\"Food\"},{\"id\":\"C6D3230A-2CEA-4AFE-BFF3-DC1E2C2C4BB4\",\"name\":\"Picnicking\"},{\"id\":\"DF4A35E0-7983-4A3E-BC47-F37B872B0F25\",\"name\":\"Junior Ranger Program\"},{\"id\":\"0B685688-3405-4E2A-ABBA-E3069492EC50\",\"name\":\"Wildlife Watching\"},{\"id\":\"5A2C91D1-50EC-4B24-8BED-A2E11A1892DF\",\"name\":\"Birdwatching\"},{\"id\":\"24380E3F-AD9D-4E38-BF13-C8EEB21893E7\",\"name\":\"Shopping\"},{\"id\":\"467DC8B8-0B7D-436D-A026-80A22358F615\",\"name\":\"Bookstore and Park Store\"},{\"id\":\"43800AD1-D439-40F3-AAB3-9FB651FE45BB\",\"name\":\"Gift Shop and Souvenirs\"}],\"addresses\":[{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Physical\"},{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Mailing\"}],\"contacts\":{\"emailAddresses\":[{\"description\":\"\",\"emailAddress\":\"ABLI_Administration@nps.gov\"}],\"phoneNumbers\":[{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583137\",\"type\":\"Voice\"},{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583874\",\"type\":\"Fax\"}]},\"description\":\"For over a century people from around the world have come to rural Central Kentucky to honor the humble beginnings of our 16th president, Abraham Lincoln. His early life on Kentucky's frontier shaped his character and prepared him to lead the nation through Civil War. The country's first memorial to Lincoln, built with donations from young and old, enshrines the symbolic birthplace cabin.\",\"designation\":\"National Historical Park\",\"directionsInfo\":\"The Birthplace Unit of the park is located approximately 2 miles south of the town of Hodgenville on U.S. Highway 31E South. The Boyhood Home Unit at Knob Creek is located approximately 10 miles northeast of the Birthplace Unit of the park.\",\"directionsUrl\":\"http://www.nps.gov/abli/planyourvisit/directions.htm\",\"entranceFees\":[{\"cost\":\"0.00\",\"description\":\"There is no fee associated with visiting either unit of the park.\",\"title\":\"Fee Free Park\"}],\"entrancePasses\":[],\"fees\":[],\"fullName\":\"Abraham Lincoln Birthplace National Historical Park\",\"id\":\"77E0D7F0-1942-494A-ACE2-9004D2BDC59E\",\"images\":[{\"altText\":\"The Memorial Building surrounded by fall colors\",\"caption\":\"Over 200,000 people a year come to walk up the steps of the Memorial Building to visit the site where Abraham Lincoln was born\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building with fall colors\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861078-1DD8-B71B-0B774A242EF6A706.jpg\"},{\"altText\":\"The first memorial erected to honor Abraham Lincoln\",\"caption\":\"The Memorial Building constructed on the traditional site of the birth of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861263-1DD8-B71B-0B71EF9B95F9644F.jpg\"},{\"altText\":\"The symbolic birth cabin on the traditional site of the birth of Abraham Lincoln.\",\"caption\":\"The symbolic birth cabin of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Symbolic Birth Cabin of Abraham Lincoln\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C86137D-1DD8-B71B-0B978BACD7EBAEF1.jpg\"},{\"altText\":\"Statue of the Lincoln family in the park's Visitor Center\",\"caption\":\"Visitors to the park can view the statue of the Lincoln family.\",\"credit\":\"NPS Photo\",\"title\":\"Statue of the Lincoln Family in the Visitor Center\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C8614D1-1DD8-B71B-0B1AF72CA452B051.jpg\"}],\"latLong\":\"lat:37.5858662, long:-85.67330523\",\"latitude\":\"37.5858662\",\"longitude\":\"-85.67330523\",\"name\":\"Abraham Lincoln Birthplace\",\"operatingHours\":[{\"description\":\"The Memorial Building is open 9:00 am - 4:30 pm eastern time with a limited viewing area.  The Visitor Center and grounds of the Birthplace Unit are open 9:00 am - 5:00 pm eastern time with one-way traffic flow and social distancing.\\n\\n\\nThe Boyhood Home Unit at Knob Creek will be closed August 3, 2020, to July 31, 2021, due to the Lincoln Tavern Rehabilitation Project.\",\"exceptions\":[{\"endDate\":\"2022-01-01\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2022-01-01\"},{\"endDate\":\"2021-11-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-11-25\"},{\"endDate\":\"2021-12-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-12-25\"}],\"name\":\"Abraham Lincoln Birthplace National Historical Park\",\"standardHours\":{\"friday\":\"9:00AM - 5:00PM\",\"monday\":\"9:00AM - 5:00PM\",\"saturday\":\"9:00AM - 5:00PM\",\"sunday\":\"9:00AM - 5:00PM\",\"thursday\":\"9:00AM - 5:00PM\",\"tuesday\":\"9:00AM - 5:00PM\",\"wednesday\":\"9:00AM - 5:00PM\"}}],\"parkCode\":\"abli\",\"states\":\"KY\",\"topics\":[{\"id\":\"D10852A3-443C-4743-A5FA-6DD6D2A054B3\",\"name\":\"Birthplace\"},{\"id\":\"F669BC40-BDC4-41C0-9ACE-B2CD25373045\",\"name\":\"Presidents\"},{\"id\":\"0D00073E-18C3-46E5-8727-2F87B112DDC6\",\"name\":\"Animals\"},{\"id\":\"957EF2BD-AC6C-4B7B-BD9A-87593ADC6691\",\"name\":\"Birds\"},{\"id\":\"E25F3456-43ED-45DD-93BC-057F9B944F7A\",\"name\":\"Caves, Caverns and Karst\"},{\"id\":\"F0F97E32-2F29-41B4-AF98-9FBE8DAB36B1\",\"name\":\"Geology\"},{\"id\":\"4BE01DC5-52E6-4F18-8C9A-B22D65965F6D\",\"name\":\"Groundwater\"},{\"id\":\"0E1A04CC-EB51-4F18-93D4-EC0B0B4EC1E3\",\"name\":\"Freshwater Springs\"},{\"id\":\"A7359FC4-DAD8-45F5-AF15-7FF62F816ED3\",\"name\":\"Night Sky\"}],\"url\":\"https://www.nps.gov/abli/index.htm\",\"weatherInfo\":\"There are four distinct seasons in Central Kentucky. However, temperature and weather conditions can vary widely within those seasons. Spring and Fall are generally pleasant with frequent rain showers. Summer is usually hot and humid. Winter is moderately cold with mixed precipitation.\"},{\"activities\":[{\"id\":\"13A57703-BB1A-41A2-94B8-53B692EB7238\",\"name\":\"Astronomy\"},{\"id\":\"D37A0003-8317-4F04-8FB0-4CF0A272E195\",\"name\":\"Stargazing\"},{\"id\":\"1DFACD97-1B9C-4F5A-80F2-05593604799E\",\"name\":\"Food\"},{\"id\":\"C6D3230A-2CEA-4AFE-BFF3-DC1E2C2C4BB4\",\"name\":\"Picnicking\"},{\"id\":\"DF4A35E0-7983-4A3E-BC47-F37B872B0F25\",\"name\":\"Junior Ranger Program\"},{\"id\":\"0B685688-3405-4E2A-ABBA-E3069492EC50\",\"name\":\"Wildlife Watching\"},{\"id\":\"5A2C91D1-50EC-4B24-8BED-A2E11A1892DF\",\"name\":\"Birdwatching\"},{\"id\":\"24380E3F-AD9D-4E38-BF13-C8EEB21893E7\",\"name\":\"Shopping\"},{\"id\":\"467DC8B8-0B7D-436D-A026-80A22358F615\",\"name\":\"Bookstore and Park Store\"},{\"id\":\"43800AD1-D439-40F3-AAB3-9FB651FE45BB\",\"name\":\"Gift Shop and Souvenirs\"}],\"addresses\":[{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Physical\"},{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Mailing\"}],\"contacts\":{\"emailAddresses\":[{\"description\":\"\",\"emailAddress\":\"ABLI_Administration@nps.gov\"}],\"phoneNumbers\":[{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583137\",\"type\":\"Voice\"},{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583874\",\"type\":\"Fax\"}]},\"description\":\"For over a century people from around the world have come to rural Central Kentucky to honor the humble beginnings of our 16th president, Abraham Lincoln. His early life on Kentucky's frontier shaped his character and prepared him to lead the nation through Civil War. The country's first memorial to Lincoln, built with donations from young and old, enshrines the symbolic birthplace cabin.\",\"designation\":\"National Historical Park\",\"directionsInfo\":\"The Birthplace Unit of the park is located approximately 2 miles south of the town of Hodgenville on U.S. Highway 31E South. The Boyhood Home Unit at Knob Creek is located approximately 10 miles northeast of the Birthplace Unit of the park.\",\"directionsUrl\":\"http://www.nps.gov/abli/planyourvisit/directions.htm\",\"entranceFees\":[{\"cost\":\"0.00\",\"description\":\"There is no fee associated with visiting either unit of the park.\",\"title\":\"Fee Free Park\"}],\"entrancePasses\":[],\"fees\":[],\"fullName\":\"Abraham Lincoln Birthplace National Historical Park\",\"id\":\"77E0D7F0-1942-494A-ACE2-9004D2BDC59E\",\"images\":[{\"altText\":\"The Memorial Building surrounded by fall colors\",\"caption\":\"Over 200,000 people a year come to walk up the steps of the Memorial Building to visit the site where Abraham Lincoln was born\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building with fall colors\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861078-1DD8-B71B-0B774A242EF6A706.jpg\"},{\"altText\":\"The first memorial erected to honor Abraham Lincoln\",\"caption\":\"The Memorial Building constructed on the traditional site of the birth of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861263-1DD8-B71B-0B71EF9B95F9644F.jpg\"},{\"altText\":\"The symbolic birth cabin on the traditional site of the birth of Abraham Lincoln.\",\"caption\":\"The symbolic birth cabin of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Symbolic Birth Cabin of Abraham Lincoln\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C86137D-1DD8-B71B-0B978BACD7EBAEF1.jpg\"},{\"altText\":\"Statue of the Lincoln family in the park's Visitor Center\",\"caption\":\"Visitors to the park can view the statue of the Lincoln family.\",\"credit\":\"NPS Photo\",\"title\":\"Statue of the Lincoln Family in the Visitor Center\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C8614D1-1DD8-B71B-0B1AF72CA452B051.jpg\"}],\"latLong\":\"lat:37.5858662, long:-85.67330523\",\"latitude\":\"37.5858662\",\"longitude\":\"-85.67330523\",\"name\":\"Abraham Lincoln Birthplace\",\"operatingHours\":[{\"description\":\"The Memorial Building is open 9:00 am - 4:30 pm eastern time with a limited viewing area.  The Visitor Center and grounds of the Birthplace Unit are open 9:00 am - 5:00 pm eastern time with one-way traffic flow and social distancing.\\n\\n\\nThe Boyhood Home Unit at Knob Creek will be closed August 3, 2020, to July 31, 2021, due to the Lincoln Tavern Rehabilitation Project.\",\"exceptions\":[{\"endDate\":\"2022-01-01\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2022-01-01\"},{\"endDate\":\"2021-11-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-11-25\"},{\"endDate\":\"2021-12-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-12-25\"}],\"name\":\"Abraham Lincoln Birthplace National Historical Park\",\"standardHours\":{\"friday\":\"9:00AM - 5:00PM\",\"monday\":\"9:00AM - 5:00PM\",\"saturday\":\"9:00AM - 5:00PM\",\"sunday\":\"9:00AM - 5:00PM\",\"thursday\":\"9:00AM - 5:00PM\",\"tuesday\":\"9:00AM - 5:00PM\",\"wednesday\":\"9:00AM - 5:00PM\"}}],\"parkCode\":\"abli\",\"states\":\"KY\",\"topics\":[{\"id\":\"D10852A3-443C-4743-A5FA-6DD6D2A054B3\",\"name\":\"Birthplace\"},{\"id\":\"F669BC40-BDC4-41C0-9ACE-B2CD25373045\",\"name\":\"Presidents\"},{\"id\":\"0D00073E-18C3-46E5-8727-2F87B112DDC6\",\"name\":\"Animals\"},{\"id\":\"957EF2BD-AC6C-4B7B-BD9A-87593ADC6691\",\"name\":\"Birds\"},{\"id\":\"E25F3456-43ED-45DD-93BC-057F9B944F7A\",\"name\":\"Caves, Caverns and Karst\"},{\"id\":\"F0F97E32-2F29-41B4-AF98-9FBE8DAB36B1\",\"name\":\"Geology\"},{\"id\":\"4BE01DC5-52E6-4F18-8C9A-B22D65965F6D\",\"name\":\"Groundwater\"},{\"id\":\"0E1A04CC-EB51-4F18-93D4-EC0B0B4EC1E3\",\"name\":\"Freshwater Springs\"},{\"id\":\"A7359FC4-DAD8-45F5-AF15-7FF62F816ED3\",\"name\":\"Night Sky\"}],\"url\":\"https://www.nps.gov/abli/index.htm\",\"weatherInfo\":\"There are four distinct seasons in Central Kentucky. However, temperature and weather conditions can vary widely within those seasons. Spring and Fall are generally pleasant with frequent rain showers. Summer is usually hot and humid. Winter is moderately cold with mixed precipitation.\"},{\"activities\":[{\"id\":\"13A57703-BB1A-41A2-94B8-53B692EB7238\",\"name\":\"Astronomy\"},{\"id\":\"D37A0003-8317-4F04-8FB0-4CF0A272E195\",\"name\":\"Stargazing\"},{\"id\":\"1DFACD97-1B9C-4F5A-80F2-05593604799E\",\"name\":\"Food\"},{\"id\":\"C6D3230A-2CEA-4AFE-BFF3-DC1E2C2C4BB4\",\"name\":\"Picnicking\"},{\"id\":\"DF4A35E0-7983-4A3E-BC47-F37B872B0F25\",\"name\":\"Junior Ranger Program\"},{\"id\":\"0B685688-3405-4E2A-ABBA-E3069492EC50\",\"name\":\"Wildlife Watching\"},{\"id\":\"5A2C91D1-50EC-4B24-8BED-A2E11A1892DF\",\"name\":\"Birdwatching\"},{\"id\":\"24380E3F-AD9D-4E38-BF13-C8EEB21893E7\",\"name\":\"Shopping\"},{\"id\":\"467DC8B8-0B7D-436D-A026-80A22358F615\",\"name\":\"Bookstore and Park Store\"},{\"id\":\"43800AD1-D439-40F3-AAB3-9FB651FE45BB\",\"name\":\"Gift Shop and Souvenirs\"}],\"addresses\":[{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Physical\"},{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Mailing\"}],\"contacts\":{\"emailAddresses\":[{\"description\":\"\",\"emailAddress\":\"ABLI_Administration@nps.gov\"}],\"phoneNumbers\":[{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583137\",\"type\":\"Voice\"},{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583874\",\"type\":\"Fax\"}]},\"description\":\"For over a century people from around the world have come to rural Central Kentucky to honor the humble beginnings of our 16th president, Abraham Lincoln. His early life on Kentucky's frontier shaped his character and prepared him to lead the nation through Civil War. The country's first memorial to Lincoln, built with donations from young and old, enshrines the symbolic birthplace cabin.\",\"designation\":\"National Historical Park\",\"directionsInfo\":\"The Birthplace Unit of the park is located approximately 2 miles south of the town of Hodgenville on U.S. Highway 31E South. The Boyhood Home Unit at Knob Creek is located approximately 10 miles northeast of the Birthplace Unit of the park.\",\"directionsUrl\":\"http://www.nps.gov/abli/planyourvisit/directions.htm\",\"entranceFees\":[{\"cost\":\"0.00\",\"description\":\"There is no fee associated with visiting either unit of the park.\",\"title\":\"Fee Free Park\"}],\"entrancePasses\":[],\"fees\":[],\"fullName\":\"Abraham Lincoln Birthplace National Historical Park\",\"id\":\"77E0D7F0-1942-494A-ACE2-9004D2BDC59E\",\"images\":[{\"altText\":\"The Memorial Building surrounded by fall colors\",\"caption\":\"Over 200,000 people a year come to walk up the steps of the Memorial Building to visit the site where Abraham Lincoln was born\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building with fall colors\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861078-1DD8-B71B-0B774A242EF6A706.jpg\"},{\"altText\":\"The first memorial erected to honor Abraham Lincoln\",\"caption\":\"The Memorial Building constructed on the traditional site of the birth of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861263-1DD8-B71B-0B71EF9B95F9644F.jpg\"},{\"altText\":\"The symbolic birth cabin on the traditional site of the birth of Abraham Lincoln.\",\"caption\":\"The symbolic birth cabin of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Symbolic Birth Cabin of Abraham Lincoln\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C86137D-1DD8-B71B-0B978BACD7EBAEF1.jpg\"},{\"altText\":\"Statue of the Lincoln family in the park's Visitor Center\",\"caption\":\"Visitors to the park can view the statue of the Lincoln family.\",\"credit\":\"NPS Photo\",\"title\":\"Statue of the Lincoln Family in the Visitor Center\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C8614D1-1DD8-B71B-0B1AF72CA452B051.jpg\"}],\"latLong\":\"lat:37.5858662, long:-85.67330523\",\"latitude\":\"37.5858662\",\"longitude\":\"-85.67330523\",\"name\":\"Abraham Lincoln Birthplace\",\"operatingHours\":[{\"description\":\"The Memorial Building is open 9:00 am - 4:30 pm eastern time with a limited viewing area.  The Visitor Center and grounds of the Birthplace Unit are open 9:00 am - 5:00 pm eastern time with one-way traffic flow and social distancing.\\n\\n\\nThe Boyhood Home Unit at Knob Creek will be closed August 3, 2020, to July 31, 2021, due to the Lincoln Tavern Rehabilitation Project.\",\"exceptions\":[{\"endDate\":\"2022-01-01\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2022-01-01\"},{\"endDate\":\"2021-11-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-11-25\"},{\"endDate\":\"2021-12-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-12-25\"}],\"name\":\"Abraham Lincoln Birthplace National Historical Park\",\"standardHours\":{\"friday\":\"9:00AM - 5:00PM\",\"monday\":\"9:00AM - 5:00PM\",\"saturday\":\"9:00AM - 5:00PM\",\"sunday\":\"9:00AM - 5:00PM\",\"thursday\":\"9:00AM - 5:00PM\",\"tuesday\":\"9:00AM - 5:00PM\",\"wednesday\":\"9:00AM - 5:00PM\"}}],\"parkCode\":\"abli\",\"states\":\"KY\",\"topics\":[{\"id\":\"D10852A3-443C-4743-A5FA-6DD6D2A054B3\",\"name\":\"Birthplace\"},{\"id\":\"F669BC40-BDC4-41C0-9ACE-B2CD25373045\",\"name\":\"Presidents\"},{\"id\":\"0D00073E-18C3-46E5-8727-2F87B112DDC6\",\"name\":\"Animals\"},{\"id\":\"957EF2BD-AC6C-4B7B-BD9A-87593ADC6691\",\"name\":\"Birds\"},{\"id\":\"E25F3456-43ED-45DD-93BC-057F9B944F7A\",\"name\":\"Caves, Caverns and Karst\"},{\"id\":\"F0F97E32-2F29-41B4-AF98-9FBE8DAB36B1\",\"name\":\"Geology\"},{\"id\":\"4BE01DC5-52E6-4F18-8C9A-B22D65965F6D\",\"name\":\"Groundwater\"},{\"id\":\"0E1A04CC-EB51-4F18-93D4-EC0B0B4EC1E3\",\"name\":\"Freshwater Springs\"},{\"id\":\"A7359FC4-DAD8-45F5-AF15-7FF62F816ED3\",\"name\":\"Night Sky\"}],\"url\":\"https://www.nps.gov/abli/index.htm\",\"weatherInfo\":\"There are four distinct seasons in Central Kentucky. However, temperature and weather conditions can vary widely within those seasons. Spring and Fall are generally pleasant with frequent rain showers. Summer is usually hot and humid. Winter is moderately cold with mixed precipitation.\"},{\"activities\":[{\"id\":\"13A57703-BB1A-41A2-94B8-53B692EB7238\",\"name\":\"Astronomy\"},{\"id\":\"D37A0003-8317-4F04-8FB0-4CF0A272E195\",\"name\":\"Stargazing\"},{\"id\":\"1DFACD97-1B9C-4F5A-80F2-05593604799E\",\"name\":\"Food\"},{\"id\":\"C6D3230A-2CEA-4AFE-BFF3-DC1E2C2C4BB4\",\"name\":\"Picnicking\"},{\"id\":\"DF4A35E0-7983-4A3E-BC47-F37B872B0F25\",\"name\":\"Junior Ranger Program\"},{\"id\":\"0B685688-3405-4E2A-ABBA-E3069492EC50\",\"name\":\"Wildlife Watching\"},{\"id\":\"5A2C91D1-50EC-4B24-8BED-A2E11A1892DF\",\"name\":\"Birdwatching\"},{\"id\":\"24380E3F-AD9D-4E38-BF13-C8EEB21893E7\",\"name\":\"Shopping\"},{\"id\":\"467DC8B8-0B7D-436D-A026-80A22358F615\",\"name\":\"Bookstore and Park Store\"},{\"id\":\"43800AD1-D439-40F3-AAB3-9FB651FE45BB\",\"name\":\"Gift Shop and Souvenirs\"}],\"addresses\":[{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Physical\"},{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Mailing\"}],\"contacts\":{\"emailAddresses\":[{\"description\":\"\",\"emailAddress\":\"ABLI_Administration@nps.gov\"}],\"phoneNumbers\":[{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583137\",\"type\":\"Voice\"},{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583874\",\"type\":\"Fax\"}]},\"description\":\"For over a century people from around the world have come to rural Central Kentucky to honor the humble beginnings of our 16th president, Abraham Lincoln. His early life on Kentucky's frontier shaped his character and prepared him to lead the nation through Civil War. The country's first memorial to Lincoln, built with donations from young and old, enshrines the symbolic birthplace cabin.\",\"designation\":\"National Historical Park\",\"directionsInfo\":\"The Birthplace Unit of the park is located approximately 2 miles south of the town of Hodgenville on U.S. Highway 31E South. The Boyhood Home Unit at Knob Creek is located approximately 10 miles northeast of the Birthplace Unit of the park.\",\"directionsUrl\":\"http://www.nps.gov/abli/planyourvisit/directions.htm\",\"entranceFees\":[{\"cost\":\"0.00\",\"description\":\"There is no fee associated with visiting either unit of the park.\",\"title\":\"Fee Free Park\"}],\"entrancePasses\":[],\"fees\":[],\"fullName\":\"Abraham Lincoln Birthplace National Historical Park\",\"id\":\"77E0D7F0-1942-494A-ACE2-9004D2BDC59E\",\"images\":[{\"altText\":\"The Memorial Building surrounded by fall colors\",\"caption\":\"Over 200,000 people a year come to walk up the steps of the Memorial Building to visit the site where Abraham Lincoln was born\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building with fall colors\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861078-1DD8-B71B-0B774A242EF6A706.jpg\"},{\"altText\":\"The first memorial erected to honor Abraham Lincoln\",\"caption\":\"The Memorial Building constructed on the traditional site of the birth of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861263-1DD8-B71B-0B71EF9B95F9644F.jpg\"},{\"altText\":\"The symbolic birth cabin on the traditional site of the birth of Abraham Lincoln.\",\"caption\":\"The symbolic birth cabin of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Symbolic Birth Cabin of Abraham Lincoln\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C86137D-1DD8-B71B-0B978BACD7EBAEF1.jpg\"},{\"altText\":\"Statue of the Lincoln family in the park's Visitor Center\",\"caption\":\"Visitors to the park can view the statue of the Lincoln family.\",\"credit\":\"NPS Photo\",\"title\":\"Statue of the Lincoln Family in the Visitor Center\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C8614D1-1DD8-B71B-0B1AF72CA452B051.jpg\"}],\"latLong\":\"lat:37.5858662, long:-85.67330523\",\"latitude\":\"37.5858662\",\"longitude\":\"-85.67330523\",\"name\":\"Abraham Lincoln Birthplace\",\"operatingHours\":[{\"description\":\"The Memorial Building is open 9:00 am - 4:30 pm eastern time with a limited viewing area.  The Visitor Center and grounds of the Birthplace Unit are open 9:00 am - 5:00 pm eastern time with one-way traffic flow and social distancing.\\n\\n\\nThe Boyhood Home Unit at Knob Creek will be closed August 3, 2020, to July 31, 2021, due to the Lincoln Tavern Rehabilitation Project.\",\"exceptions\":[{\"endDate\":\"2022-01-01\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2022-01-01\"},{\"endDate\":\"2021-11-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-11-25\"},{\"endDate\":\"2021-12-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-12-25\"}],\"name\":\"Abraham Lincoln Birthplace National Historical Park\",\"standardHours\":{\"friday\":\"9:00AM - 5:00PM\",\"monday\":\"9:00AM - 5:00PM\",\"saturday\":\"9:00AM - 5:00PM\",\"sunday\":\"9:00AM - 5:00PM\",\"thursday\":\"9:00AM - 5:00PM\",\"tuesday\":\"9:00AM - 5:00PM\",\"wednesday\":\"9:00AM - 5:00PM\"}}],\"parkCode\":\"abli\",\"states\":\"KY\",\"topics\":[{\"id\":\"D10852A3-443C-4743-A5FA-6DD6D2A054B3\",\"name\":\"Birthplace\"},{\"id\":\"F669BC40-BDC4-41C0-9ACE-B2CD25373045\",\"name\":\"Presidents\"},{\"id\":\"0D00073E-18C3-46E5-8727-2F87B112DDC6\",\"name\":\"Animals\"},{\"id\":\"957EF2BD-AC6C-4B7B-BD9A-87593ADC6691\",\"name\":\"Birds\"},{\"id\":\"E25F3456-43ED-45DD-93BC-057F9B944F7A\",\"name\":\"Caves, Caverns and Karst\"},{\"id\":\"F0F97E32-2F29-41B4-AF98-9FBE8DAB36B1\",\"name\":\"Geology\"},{\"id\":\"4BE01DC5-52E6-4F18-8C9A-B22D65965F6D\",\"name\":\"Groundwater\"},{\"id\":\"0E1A04CC-EB51-4F18-93D4-EC0B0B4EC1E3\",\"name\":\"Freshwater Springs\"},{\"id\":\"A7359FC4-DAD8-45F5-AF15-7FF62F816ED3\",\"name\":\"Night Sky\"}],\"url\":\"https://www.nps.gov/abli/index.htm\",\"weatherInfo\":\"There are four distinct seasons in Central Kentucky. However, temperature and weather conditions can vary widely within those seasons. Spring and Fall are generally pleasant with frequent rain showers. Summer is usually hot and humid. Winter is moderately cold with mixed precipitation.\"},{\"activities\":[{\"id\":\"13A57703-BB1A-41A2-94B8-53B692EB7238\",\"name\":\"Astronomy\"},{\"id\":\"D37A0003-8317-4F04-8FB0-4CF0A272E195\",\"name\":\"Stargazing\"},{\"id\":\"1DFACD97-1B9C-4F5A-80F2-05593604799E\",\"name\":\"Food\"},{\"id\":\"C6D3230A-2CEA-4AFE-BFF3-DC1E2C2C4BB4\",\"name\":\"Picnicking\"},{\"id\":\"DF4A35E0-7983-4A3E-BC47-F37B872B0F25\",\"name\":\"Junior Ranger Program\"},{\"id\":\"0B685688-3405-4E2A-ABBA-E3069492EC50\",\"name\":\"Wildlife Watching\"},{\"id\":\"5A2C91D1-50EC-4B24-8BED-A2E11A1892DF\",\"name\":\"Birdwatching\"},{\"id\":\"24380E3F-AD9D-4E38-BF13-C8EEB21893E7\",\"name\":\"Shopping\"},{\"id\":\"467DC8B8-0B7D-436D-A026-80A22358F615\",\"name\":\"Bookstore and Park Store\"},{\"id\":\"43800AD1-D439-40F3-AAB3-9FB651FE45BB\",\"name\":\"Gift Shop and Souvenirs\"}],\"addresses\":[{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Physical\"},{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Mailing\"}],\"contacts\":{\"emailAddresses\":[{\"description\":\"\",\"emailAddress\":\"ABLI_Administration@nps.gov\"}],\"phoneNumbers\":[{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583137\",\"type\":\"Voice\"},{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583874\",\"type\":\"Fax\"}]},\"description\":\"For over a century people from around the world have come to rural Central Kentucky to honor the humble beginnings of our 16th president, Abraham Lincoln. His early life on Kentucky's frontier shaped his character and prepared him to lead the nation through Civil War. The country's first memorial to Lincoln, built with donations from young and old, enshrines the symbolic birthplace cabin.\",\"designation\":\"National Historical Park\",\"directionsInfo\":\"The Birthplace Unit of the park is located approximately 2 miles south of the town of Hodgenville on U.S. Highway 31E South. The Boyhood Home Unit at Knob Creek is located approximately 10 miles northeast of the Birthplace Unit of the park.\",\"directionsUrl\":\"http://www.nps.gov/abli/planyourvisit/directions.htm\",\"entranceFees\":[{\"cost\":\"0.00\",\"description\":\"There is no fee associated with visiting either unit of the park.\",\"title\":\"Fee Free Park\"}],\"entrancePasses\":[],\"fees\":[],\"fullName\":\"Abraham Lincoln Birthplace National Historical Park\",\"id\":\"77E0D7F0-1942-494A-ACE2-9004D2BDC59E\",\"images\":[{\"altText\":\"The Memorial Building surrounded by fall colors\",\"caption\":\"Over 200,000 people a year come to walk up the steps of the Memorial Building to visit the site where Abraham Lincoln was born\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building with fall colors\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861078-1DD8-B71B-0B774A242EF6A706.jpg\"},{\"altText\":\"The first memorial erected to honor Abraham Lincoln\",\"caption\":\"The Memorial Building constructed on the traditional site of the birth of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861263-1DD8-B71B-0B71EF9B95F9644F.jpg\"},{\"altText\":\"The symbolic birth cabin on the traditional site of the birth of Abraham Lincoln.\",\"caption\":\"The symbolic birth cabin of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Symbolic Birth Cabin of Abraham Lincoln\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C86137D-1DD8-B71B-0B978BACD7EBAEF1.jpg\"},{\"altText\":\"Statue of the Lincoln family in the park's Visitor Center\",\"caption\":\"Visitors to the park can view the statue of the Lincoln family.\",\"credit\":\"NPS Photo\",\"title\":\"Statue of the Lincoln Family in the Visitor Center\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C8614D1-1DD8-B71B-0B1AF72CA452B051.jpg\"}],\"latLong\":\"lat:37.5858662, long:-85.67330523\",\"latitude\":\"37.5858662\",\"longitude\":\"-85.67330523\",\"name\":\"Abraham Lincoln Birthplace\",\"operatingHours\":[{\"description\":\"The Memorial Building is open 9:00 am - 4:30 pm eastern time with a limited viewing area.  The Visitor Center and grounds of the Birthplace Unit are open 9:00 am - 5:00 pm eastern time with one-way traffic flow and social distancing.\\n\\n\\nThe Boyhood Home Unit at Knob Creek will be closed August 3, 2020, to July 31, 2021, due to the Lincoln Tavern Rehabilitation Project.\",\"exceptions\":[{\"endDate\":\"2022-01-01\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2022-01-01\"},{\"endDate\":\"2021-11-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-11-25\"},{\"endDate\":\"2021-12-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-12-25\"}],\"name\":\"Abraham Lincoln Birthplace National Historical Park\",\"standardHours\":{\"friday\":\"9:00AM - 5:00PM\",\"monday\":\"9:00AM - 5:00PM\",\"saturday\":\"9:00AM - 5:00PM\",\"sunday\":\"9:00AM - 5:00PM\",\"thursday\":\"9:00AM - 5:00PM\",\"tuesday\":\"9:00AM - 5:00PM\",\"wednesday\":\"9:00AM - 5:00PM\"}}],\"parkCode\":\"abli\",\"states\":\"KY\",\"topics\":[{\"id\":\"D10852A3-443C-4743-A5FA-6DD6D2A054B3\",\"name\":\"Birthplace\"},{\"id\":\"F669BC40-BDC4-41C0-9ACE-B2CD25373045\",\"name\":\"Presidents\"},{\"id\":\"0D00073E-18C3-46E5-8727-2F87B112DDC6\",\"name\":\"Animals\"},{\"id\":\"957EF2BD-AC6C-4B7B-BD9A-87593ADC6691\",\"name\":\"Birds\"},{\"id\":\"E25F3456-43ED-45DD-93BC-057F9B944F7A\",\"name\":\"Caves, Caverns and Karst\"},{\"id\":\"F0F97E32-2F29-41B4-AF98-9FBE8DAB36B1\",\"name\":\"Geology\"},{\"id\":\"4BE01DC5-52E6-4F18-8C9A-B22D65965F6D\",\"name\":\"Groundwater\"},{\"id\":\"0E1A04CC-EB51-4F18-93D4-EC0B0B4EC1E3\",\"name\":\"Freshwater Springs\"},{\"id\":\"A7359FC4-DAD8-45F5-AF15-7FF62F816ED3\",\"name\":\"Night Sky\"}],\"url\":\"https://www.nps.gov/abli/index.htm\",\"weatherInfo\":\"There are four distinct seasons in Central Kentucky. However, temperature and weather conditions can vary widely within those seasons. Spring and Fall are generally pleasant with frequent rain showers. Summer is usually hot and humid. Winter is moderately cold with mixed precipitation.\"},{\"activities\":[{\"id\":\"13A57703-BB1A-41A2-94B8-53B692EB7238\",\"name\":\"Astronomy\"},{\"id\":\"D37A0003-8317-4F04-8FB0-4CF0A272E195\",\"name\":\"Stargazing\"},{\"id\":\"1DFACD97-1B9C-4F5A-80F2-05593604799E\",\"name\":\"Food\"},{\"id\":\"C6D3230A-2CEA-4AFE-BFF3-DC1E2C2C4BB4\",\"name\":\"Picnicking\"},{\"id\":\"DF4A35E0-7983-4A3E-BC47-F37B872B0F25\",\"name\":\"Junior Ranger Program\"},{\"id\":\"0B685688-3405-4E2A-ABBA-E3069492EC50\",\"name\":\"Wildlife Watching\"},{\"id\":\"5A2C91D1-50EC-4B24-8BED-A2E11A1892DF\",\"name\":\"Birdwatching\"},{\"id\":\"24380E3F-AD9D-4E38-BF13-C8EEB21893E7\",\"name\":\"Shopping\"},{\"id\":\"467DC8B8-0B7D-436D-A026-80A22358F615\",\"name\":\"Bookstore and Park Store\"},{\"id\":\"43800AD1-D439-40F3-AAB3-9FB651FE45BB\",\"name\":\"Gift Shop and Souvenirs\"}],\"addresses\":[{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Physical\"},{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Mailing\"}],\"contacts\":{\"emailAddresses\":[{\"description\":\"\",\"emailAddress\":\"ABLI_Administration@nps.gov\"}],\"phoneNumbers\":[{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583137\",\"type\":\"Voice\"},{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583874\",\"type\":\"Fax\"}]},\"description\":\"For over a century people from around the world have come to rural Central Kentucky to honor the humble beginnings of our 16th president, Abraham Lincoln. His early life on Kentucky's frontier shaped his character and prepared him to lead the nation through Civil War. The country's first memorial to Lincoln, built with donations from young and old, enshrines the symbolic birthplace cabin.\",\"designation\":\"National Historical Park\",\"directionsInfo\":\"The Birthplace Unit of the park is located approximately 2 miles south of the town of Hodgenville on U.S. Highway 31E South. The Boyhood Home Unit at Knob Creek is located approximately 10 miles northeast of the Birthplace Unit of the park.\",\"directionsUrl\":\"http://www.nps.gov/abli/planyourvisit/directions.htm\",\"entranceFees\":[{\"cost\":\"0.00\",\"description\":\"There is no fee associated with visiting either unit of the park.\",\"title\":\"Fee Free Park\"}],\"entrancePasses\":[],\"fees\":[],\"fullName\":\"Abraham Lincoln Birthplace National Historical Park\",\"id\":\"77E0D7F0-1942-494A-ACE2-9004D2BDC59E\",\"images\":[{\"altText\":\"The Memorial Building surrounded by fall colors\",\"caption\":\"Over 200,000 people a year come to walk up the steps of the Memorial Building to visit the site where Abraham Lincoln was born\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building with fall colors\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861078-1DD8-B71B-0B774A242EF6A706.jpg\"},{\"altText\":\"The first memorial erected to honor Abraham Lincoln\",\"caption\":\"The Memorial Building constructed on the traditional site of the birth of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861263-1DD8-B71B-0B71EF9B95F9644F.jpg\"},{\"altText\":\"The symbolic birth cabin on the traditional site of the birth of Abraham Lincoln.\",\"caption\":\"The symbolic birth cabin of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Symbolic Birth Cabin of Abraham Lincoln\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C86137D-1DD8-B71B-0B978BACD7EBAEF1.jpg\"},{\"altText\":\"Statue of the Lincoln family in the park's Visitor Center\",\"caption\":\"Visitors to the park can view the statue of the Lincoln family.\",\"credit\":\"NPS Photo\",\"title\":\"Statue of the Lincoln Family in the Visitor Center\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C8614D1-1DD8-B71B-0B1AF72CA452B051.jpg\"}],\"latLong\":\"lat:37.5858662, long:-85.67330523\",\"latitude\":\"37.5858662\",\"longitude\":\"-85.67330523\",\"name\":\"Abraham Lincoln Birthplace\",\"operatingHours\":[{\"description\":\"The Memorial Building is open 9:00 am - 4:30 pm eastern time with a limited viewing area.  The Visitor Center and grounds of the Birthplace Unit are open 9:00 am - 5:00 pm eastern time with one-way traffic flow and social distancing.\\n\\n\\nThe Boyhood Home Unit at Knob Creek will be closed August 3, 2020, to July 31, 2021, due to the Lincoln Tavern Rehabilitation Project.\",\"exceptions\":[{\"endDate\":\"2022-01-01\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2022-01-01\"},{\"endDate\":\"2021-11-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-11-25\"},{\"endDate\":\"2021-12-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-12-25\"}],\"name\":\"Abraham Lincoln Birthplace National Historical Park\",\"standardHours\":{\"friday\":\"9:00AM - 5:00PM\",\"monday\":\"9:00AM - 5:00PM\",\"saturday\":\"9:00AM - 5:00PM\",\"sunday\":\"9:00AM - 5:00PM\",\"thursday\":\"9:00AM - 5:00PM\",\"tuesday\":\"9:00AM - 5:00PM\",\"wednesday\":\"9:00AM - 5:00PM\"}}],\"parkCode\":\"abli\",\"states\":\"KY\",\"topics\":[{\"id\":\"D10852A3-443C-4743-A5FA-6DD6D2A054B3\",\"name\":\"Birthplace\"},{\"id\":\"F669BC40-BDC4-41C0-9ACE-B2CD25373045\",\"name\":\"Presidents\"},{\"id\":\"0D00073E-18C3-46E5-8727-2F87B112DDC6\",\"name\":\"Animals\"},{\"id\":\"957EF2BD-AC6C-4B7B-BD9A-87593ADC6691\",\"name\":\"Birds\"},{\"id\":\"E25F3456-43ED-45DD-93BC-057F9B944F7A\",\"name\":\"Caves, Caverns and Karst\"},{\"id\":\"F0F97E32-2F29-41B4-AF98-9FBE8DAB36B1\",\"name\":\"Geology\"},{\"id\":\"4BE01DC5-52E6-4F18-8C9A-B22D65965F6D\",\"name\":\"Groundwater\"},{\"id\":\"0E1A04CC-EB51-4F18-93D4-EC0B0B4EC1E3\",\"name\":\"Freshwater Springs\"},{\"id\":\"A7359FC4-DAD8-45F5-AF15-7FF62F816ED3\",\"name\":\"Night Sky\"}],\"url\":\"https://www.nps.gov/abli/index.htm\",\"weatherInfo\":\"There are four distinct seasons in Central Kentucky. However, temperature and weather conditions can vary widely within those seasons. Spring and Fall are generally pleasant with frequent rain showers. Summer is usually hot and humid. Winter is moderately cold with mixed precipitation.\"},{\"activities\":[{\"id\":\"13A57703-BB1A-41A2-94B8-53B692EB7238\",\"name\":\"Astronomy\"},{\"id\":\"D37A0003-8317-4F04-8FB0-4CF0A272E195\",\"name\":\"Stargazing\"},{\"id\":\"1DFACD97-1B9C-4F5A-80F2-05593604799E\",\"name\":\"Food\"},{\"id\":\"C6D3230A-2CEA-4AFE-BFF3-DC1E2C2C4BB4\",\"name\":\"Picnicking\"},{\"id\":\"DF4A35E0-7983-4A3E-BC47-F37B872B0F25\",\"name\":\"Junior Ranger Program\"},{\"id\":\"0B685688-3405-4E2A-ABBA-E3069492EC50\",\"name\":\"Wildlife Watching\"},{\"id\":\"5A2C91D1-50EC-4B24-8BED-A2E11A1892DF\",\"name\":\"Birdwatching\"},{\"id\":\"24380E3F-AD9D-4E38-BF13-C8EEB21893E7\",\"name\":\"Shopping\"},{\"id\":\"467DC8B8-0B7D-436D-A026-80A22358F615\",\"name\":\"Bookstore and Park Store\"},{\"id\":\"43800AD1-D439-40F3-AAB3-9FB651FE45BB\",\"name\":\"Gift Shop and Souvenirs\"}],\"addresses\":[{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Physical\"},{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Mailing\"}],\"contacts\":{\"emailAddresses\":[{\"description\":\"\",\"emailAddress\":\"ABLI_Administration@nps.gov\"}],\"phoneNumbers\":[{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583137\",\"type\":\"Voice\"},{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583874\",\"type\":\"Fax\"}]},\"description\":\"For over a century people from around the world have come to rural Central Kentucky to honor the humble beginnings of our 16th president, Abraham Lincoln. His early life on Kentucky's frontier shaped his character and prepared him to lead the nation through Civil War. The country's first memorial to Lincoln, built with donations from young and old, enshrines the symbolic birthplace cabin.\",\"designation\":\"National Historical Park\",\"directionsInfo\":\"The Birthplace Unit of the park is located approximately 2 miles south of the town of Hodgenville on U.S. Highway 31E South. The Boyhood Home Unit at Knob Creek is located approximately 10 miles northeast of the Birthplace Unit of the park.\",\"directionsUrl\":\"http://www.nps.gov/abli/planyourvisit/directions.htm\",\"entranceFees\":[{\"cost\":\"0.00\",\"description\":\"There is no fee associated with visiting either unit of the park.\",\"title\":\"Fee Free Park\"}],\"entrancePasses\":[],\"fees\":[],\"fullName\":\"Abraham Lincoln Birthplace National Historical Park\",\"id\":\"77E0D7F0-1942-494A-ACE2-9004D2BDC59E\",\"images\":[{\"altText\":\"The Memorial Building surrounded by fall colors\",\"caption\":\"Over 200,000 people a year come to walk up the steps of the Memorial Building to visit the site where Abraham Lincoln was born\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building with fall colors\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861078-1DD8-B71B-0B774A242EF6A706.jpg\"},{\"altText\":\"The first memorial erected to honor Abraham Lincoln\",\"caption\":\"The Memorial Building constructed on the traditional site of the birth of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861263-1DD8-B71B-0B71EF9B95F9644F.jpg\"},{\"altText\":\"The symbolic birth cabin on the traditional site of the birth of Abraham Lincoln.\",\"caption\":\"The symbolic birth cabin of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Symbolic Birth Cabin of Abraham Lincoln\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C86137D-1DD8-B71B-0B978BACD7EBAEF1.jpg\"},{\"altText\":\"Statue of the Lincoln family in the park's Visitor Center\",\"caption\":\"Visitors to the park can view the statue of the Lincoln family.\",\"credit\":\"NPS Photo\",\"title\":\"Statue of the Lincoln Family in the Visitor Center\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C8614D1-1DD8-B71B-0B1AF72CA452B051.jpg\"}],\"latLong\":\"lat:37.5858662, long:-85.67330523\",\"latitude\":\"37.5858662\",\"longitude\":\"-85.67330523\",\"name\":\"Abraham Lincoln Birthplace\",\"operatingHours\":[{\"description\":\"The Memorial Building is open 9:00 am - 4:30 pm eastern time with a limited viewing area.  The Visitor Center and grounds of the Birthplace Unit are open 9:00 am - 5:00 pm eastern time with one-way traffic flow and social distancing.\\n\\n\\nThe Boyhood Home Unit at Knob Creek will be closed August 3, 2020, to July 31, 2021, due to the Lincoln Tavern Rehabilitation Project.\",\"exceptions\":[{\"endDate\":\"2022-01-01\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2022-01-01\"},{\"endDate\":\"2021-11-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-11-25\"},{\"endDate\":\"2021-12-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-12-25\"}],\"name\":\"Abraham Lincoln Birthplace National Historical Park\",\"standardHours\":{\"friday\":\"9:00AM - 5:00PM\",\"monday\":\"9:00AM - 5:00PM\",\"saturday\":\"9:00AM - 5:00PM\",\"sunday\":\"9:00AM - 5:00PM\",\"thursday\":\"9:00AM - 5:00PM\",\"tuesday\":\"9:00AM - 5:00PM\",\"wednesday\":\"9:00AM - 5:00PM\"}}],\"parkCode\":\"abli\",\"states\":\"KY\",\"topics\":[{\"id\":\"D10852A3-443C-4743-A5FA-6DD6D2A054B3\",\"name\":\"Birthplace\"},{\"id\":\"F669BC40-BDC4-41C0-9ACE-B2CD25373045\",\"name\":\"Presidents\"},{\"id\":\"0D00073E-18C3-46E5-8727-2F87B112DDC6\",\"name\":\"Animals\"},{\"id\":\"957EF2BD-AC6C-4B7B-BD9A-87593ADC6691\",\"name\":\"Birds\"},{\"id\":\"E25F3456-43ED-45DD-93BC-057F9B944F7A\",\"name\":\"Caves, Caverns and Karst\"},{\"id\":\"F0F97E32-2F29-41B4-AF98-9FBE8DAB36B1\",\"name\":\"Geology\"},{\"id\":\"4BE01DC5-52E6-4F18-8C9A-B22D65965F6D\",\"name\":\"Groundwater\"},{\"id\":\"0E1A04CC-EB51-4F18-93D4-EC0B0B4EC1E3\",\"name\":\"Freshwater Springs\"},{\"id\":\"A7359FC4-DAD8-45F5-AF15-7FF62F816ED3\",\"name\":\"Night Sky\"}],\"url\":\"https://www.nps.gov/abli/index.htm\",\"weatherInfo\":\"There are four distinct seasons in Central Kentucky. However, temperature and weather conditions can vary widely within those seasons. Spring and Fall are generally pleasant with frequent rain showers. Summer is usually hot and humid. Winter is moderately cold with mixed precipitation.\"},{\"activities\":[{\"id\":\"13A57703-BB1A-41A2-94B8-53B692EB7238\",\"name\":\"Astronomy\"},{\"id\":\"D37A0003-8317-4F04-8FB0-4CF0A272E195\",\"name\":\"Stargazing\"},{\"id\":\"1DFACD97-1B9C-4F5A-80F2-05593604799E\",\"name\":\"Food\"},{\"id\":\"C6D3230A-2CEA-4AFE-BFF3-DC1E2C2C4BB4\",\"name\":\"Picnicking\"},{\"id\":\"DF4A35E0-7983-4A3E-BC47-F37B872B0F25\",\"name\":\"Junior Ranger Program\"},{\"id\":\"0B685688-3405-4E2A-ABBA-E3069492EC50\",\"name\":\"Wildlife Watching\"},{\"id\":\"5A2C91D1-50EC-4B24-8BED-A2E11A1892DF\",\"name\":\"Birdwatching\"},{\"id\":\"24380E3F-AD9D-4E38-BF13-C8EEB21893E7\",\"name\":\"Shopping\"},{\"id\":\"467DC8B8-0B7D-436D-A026-80A22358F615\",\"name\":\"Bookstore and Park Store\"},{\"id\":\"43800AD1-D439-40F3-AAB3-9FB651FE45BB\",\"name\":\"Gift Shop and Souvenirs\"}],\"addresses\":[{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Physical\"},{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Mailing\"}],\"contacts\":{\"emailAddresses\":[{\"description\":\"\",\"emailAddress\":\"ABLI_Administration@nps.gov\"}],\"phoneNumbers\":[{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583137\",\"type\":\"Voice\"},{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583874\",\"type\":\"Fax\"}]},\"description\":\"For over a century people from around the world have come to rural Central Kentucky to honor the humble beginnings of our 16th president, Abraham Lincoln. His early life on Kentucky's frontier shaped his character and prepared him to lead the nation through Civil War. The country's first memorial to Lincoln, built with donations from young and old, enshrines the symbolic birthplace cabin.\",\"designation\":\"National Historical Park\",\"directionsInfo\":\"The Birthplace Unit of the park is located approximately 2 miles south of the town of Hodgenville on U.S. Highway 31E South. The Boyhood Home Unit at Knob Creek is located approximately 10 miles northeast of the Birthplace Unit of the park.\",\"directionsUrl\":\"http://www.nps.gov/abli/planyourvisit/directions.htm\",\"entranceFees\":[{\"cost\":\"0.00\",\"description\":\"There is no fee associated with visiting either unit of the park.\",\"title\":\"Fee Free Park\"}],\"entrancePasses\":[],\"fees\":[],\"fullName\":\"Abraham Lincoln Birthplace National Historical Park\",\"id\":\"77E0D7F0-1942-494A-ACE2-9004D2BDC59E\",\"images\":[{\"altText\":\"The Memorial Building surrounded by fall colors\",\"caption\":\"Over 200,000 people a year come to walk up the steps of the Memorial Building to visit the site where Abraham Lincoln was born\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building with fall colors\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861078-1DD8-B71B-0B774A242EF6A706.jpg\"},{\"altText\":\"The first memorial erected to honor Abraham Lincoln\",\"caption\":\"The Memorial Building constructed on the traditional site of the birth of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861263-1DD8-B71B-0B71EF9B95F9644F.jpg\"},{\"altText\":\"The symbolic birth cabin on the traditional site of the birth of Abraham Lincoln.\",\"caption\":\"The symbolic birth cabin of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Symbolic Birth Cabin of Abraham Lincoln\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C86137D-1DD8-B71B-0B978BACD7EBAEF1.jpg\"},{\"altText\":\"Statue of the Lincoln family in the park's Visitor Center\",\"caption\":\"Visitors to the park can view the statue of the Lincoln family.\",\"credit\":\"NPS Photo\",\"title\":\"Statue of the Lincoln Family in the Visitor Center\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C8614D1-1DD8-B71B-0B1AF72CA452B051.jpg\"}],\"latLong\":\"lat:37.5858662, long:-85.67330523\",\"latitude\":\"37.5858662\",\"longitude\":\"-85.67330523\",\"name\":\"Abraham Lincoln Birthplace\",\"operatingHours\":[{\"description\":\"The Memorial Building is open 9:00 am - 4:30 pm eastern time with a limited viewing area.  The Visitor Center and grounds of the Birthplace Unit are open 9:00 am - 5:00 pm eastern time with one-way traffic flow and social distancing.\\n\\n\\nThe Boyhood Home Unit at Knob Creek will be closed August 3, 2020, to July 31, 2021, due to the Lincoln Tavern Rehabilitation Project.\",\"exceptions\":[{\"endDate\":\"2022-01-01\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2022-01-01\"},{\"endDate\":\"2021-11-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-11-25\"},{\"endDate\":\"2021-12-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-12-25\"}],\"name\":\"Abraham Lincoln Birthplace National Historical Park\",\"standardHours\":{\"friday\":\"9:00AM - 5:00PM\",\"monday\":\"9:00AM - 5:00PM\",\"saturday\":\"9:00AM - 5:00PM\",\"sunday\":\"9:00AM - 5:00PM\",\"thursday\":\"9:00AM - 5:00PM\",\"tuesday\":\"9:00AM - 5:00PM\",\"wednesday\":\"9:00AM - 5:00PM\"}}],\"parkCode\":\"abli\",\"states\":\"KY\",\"topics\":[{\"id\":\"D10852A3-443C-4743-A5FA-6DD6D2A054B3\",\"name\":\"Birthplace\"},{\"id\":\"F669BC40-BDC4-41C0-9ACE-B2CD25373045\",\"name\":\"Presidents\"},{\"id\":\"0D00073E-18C3-46E5-8727-2F87B112DDC6\",\"name\":\"Animals\"},{\"id\":\"957EF2BD-AC6C-4B7B-BD9A-87593ADC6691\",\"name\":\"Birds\"},{\"id\":\"E25F3456-43ED-45DD-93BC-057F9B944F7A\",\"name\":\"Caves, Caverns and Karst\"},{\"id\":\"F0F97E32-2F29-41B4-AF98-9FBE8DAB36B1\",\"name\":\"Geology\"},{\"id\":\"4BE01DC5-52E6-4F18-8C9A-B22D65965F6D\",\"name\":\"Groundwater\"},{\"id\":\"0E1A04CC-EB51-4F18-93D4-EC0B0B4EC1E3\",\"name\":\"Freshwater Springs\"},{\"id\":\"A7359FC4-DAD8-45F5-AF15-7FF62F816ED3\",\"name\":\"Night Sky\"}],\"url\":\"https://www.nps.gov/abli/index.htm\",\"weatherInfo\":\"There are four distinct seasons in Central Kentucky. However, temperature and weather conditions can vary widely within those seasons. Spring and Fall are generally pleasant with frequent rain showers. Summer is usually hot and humid. Winter is moderately cold with mixed precipitation.\"},{\"activities\":[{\"id\":\"13A57703-BB1A-41A2-94B8-53B692EB7238\",\"name\":\"Astronomy\"},{\"id\":\"D37A0003-8317-4F04-8FB0-4CF0A272E195\",\"name\":\"Stargazing\"},{\"id\":\"1DFACD97-1B9C-4F5A-80F2-05593604799E\",\"name\":\"Food\"},{\"id\":\"C6D3230A-2CEA-4AFE-BFF3-DC1E2C2C4BB4\",\"name\":\"Picnicking\"},{\"id\":\"DF4A35E0-7983-4A3E-BC47-F37B872B0F25\",\"name\":\"Junior Ranger Program\"},{\"id\":\"0B685688-3405-4E2A-ABBA-E3069492EC50\",\"name\":\"Wildlife Watching\"},{\"id\":\"5A2C91D1-50EC-4B24-8BED-A2E11A1892DF\",\"name\":\"Birdwatching\"},{\"id\":\"24380E3F-AD9D-4E38-BF13-C8EEB21893E7\",\"name\":\"Shopping\"},{\"id\":\"467DC8B8-0B7D-436D-A026-80A22358F615\",\"name\":\"Bookstore and Park Store\"},{\"id\":\"43800AD1-D439-40F3-AAB3-9FB651FE45BB\",\"name\":\"Gift Shop and Souvenirs\"}],\"addresses\":[{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Physical\"},{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Mailing\"}],\"contacts\":{\"emailAddresses\":[{\"description\":\"\",\"emailAddress\":\"ABLI_Administration@nps.gov\"}],\"phoneNumbers\":[{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583137\",\"type\":\"Voice\"},{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583874\",\"type\":\"Fax\"}]},\"description\":\"For over a century people from around the world have come to rural Central Kentucky to honor the humble beginnings of our 16th president, Abraham Lincoln. His early life on Kentucky's frontier shaped his character and prepared him to lead the nation through Civil War. The country's first memorial to Lincoln, built with donations from young and old, enshrines the symbolic birthplace cabin.\",\"designation\":\"National Historical Park\",\"directionsInfo\":\"The Birthplace Unit of the park is located approximately 2 miles south of the town of Hodgenville on U.S. Highway 31E South. The Boyhood Home Unit at Knob Creek is located approximately 10 miles northeast of the Birthplace Unit of the park.\",\"directionsUrl\":\"http://www.nps.gov/abli/planyourvisit/directions.htm\",\"entranceFees\":[{\"cost\":\"0.00\",\"description\":\"There is no fee associated with visiting either unit of the park.\",\"title\":\"Fee Free Park\"}],\"entrancePasses\":[],\"fees\":[],\"fullName\":\"Abraham Lincoln Birthplace National Historical Park\",\"id\":\"77E0D7F0-1942-494A-ACE2-9004D2BDC59E\",\"images\":[{\"altText\":\"The Memorial Building surrounded by fall colors\",\"caption\":\"Over 200,000 people a year come to walk up the steps of the Memorial Building to visit the site where Abraham Lincoln was born\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building with fall colors\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861078-1DD8-B71B-0B774A242EF6A706.jpg\"},{\"altText\":\"The first memorial erected to honor Abraham Lincoln\",\"caption\":\"The Memorial Building constructed on the traditional site of the birth of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861263-1DD8-B71B-0B71EF9B95F9644F.jpg\"},{\"altText\":\"The symbolic birth cabin on the traditional site of the birth of Abraham Lincoln.\",\"caption\":\"The symbolic birth cabin of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Symbolic Birth Cabin of Abraham Lincoln\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C86137D-1DD8-B71B-0B978BACD7EBAEF1.jpg\"},{\"altText\":\"Statue of the Lincoln family in the park's Visitor Center\",\"caption\":\"Visitors to the park can view the statue of the Lincoln family.\",\"credit\":\"NPS Photo\",\"title\":\"Statue of the Lincoln Family in the Visitor Center\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C8614D1-1DD8-B71B-0B1AF72CA452B051.jpg\"}],\"latLong\":\"lat:37.5858662, long:-85.67330523\",\"latitude\":\"37.5858662\",\"longitude\":\"-85.67330523\",\"name\":\"Abraham Lincoln Birthplace\",\"operatingHours\":[{\"description\":\"The Memorial Building is open 9:00 am - 4:30 pm eastern time with a limited viewing area.  The Visitor Center and grounds of the Birthplace Unit are open 9:00 am - 5:00 pm eastern time with one-way traffic flow and social distancing.\\n\\n\\nThe Boyhood Home Unit at Knob Creek will be closed August 3, 2020, to July 31, 2021, due to the Lincoln Tavern Rehabilitation Project.\",\"exceptions\":[{\"endDate\":\"2022-01-01\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2022-01-01\"},{\"endDate\":\"2021-11-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-11-25\"},{\"endDate\":\"2021-12-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-12-25\"}],\"name\":\"Abraham Lincoln Birthplace National Historical Park\",\"standardHours\":{\"friday\":\"9:00AM - 5:00PM\",\"monday\":\"9:00AM - 5:00PM\",\"saturday\":\"9:00AM - 5:00PM\",\"sunday\":\"9:00AM - 5:00PM\",\"thursday\":\"9:00AM - 5:00PM\",\"tuesday\":\"9:00AM - 5:00PM\",\"wednesday\":\"9:00AM - 5:00PM\"}}],\"parkCode\":\"abli\",\"states\":\"KY\",\"topics\":[{\"id\":\"D10852A3-443C-4743-A5FA-6DD6D2A054B3\",\"name\":\"Birthplace\"},{\"id\":\"F669BC40-BDC4-41C0-9ACE-B2CD25373045\",\"name\":\"Presidents\"},{\"id\":\"0D00073E-18C3-46E5-8727-2F87B112DDC6\",\"name\":\"Animals\"},{\"id\":\"957EF2BD-AC6C-4B7B-BD9A-87593ADC6691\",\"name\":\"Birds\"},{\"id\":\"E25F3456-43ED-45DD-93BC-057F9B944F7A\",\"name\":\"Caves, Caverns and Karst\"},{\"id\":\"F0F97E32-2F29-41B4-AF98-9FBE8DAB36B1\",\"name\":\"Geology\"},{\"id\":\"4BE01DC5-52E6-4F18-8C9A-B22D65965F6D\",\"name\":\"Groundwater\"},{\"id\":\"0E1A04CC-EB51-4F18-93D4-EC0B0B4EC1E3\",\"name\":\"Freshwater Springs\"},{\"id\":\"A7359FC4-DAD8-45F5-AF15-7FF62F816ED3\",\"name\":\"Night Sky\"}],\"url\":\"https://www.nps.gov/abli/index.htm\",\"weatherInfo\":\"There are four distinct seasons in Central Kentucky. However, temperature and weather conditions can vary widely within those seasons. Spring and Fall are generally pleasant with frequent rain showers. Summer is usually hot and humid. Winter is moderately cold with mixed precipitation.\"},{\"activities\":[{\"id\":\"13A57703-BB1A-41A2-94B8-53B692EB7238\",\"name\":\"Astronomy\"},{\"id\":\"D37A0003-8317-4F04-8FB0-4CF0A272E195\",\"name\":\"Stargazing\"},{\"id\":\"1DFACD97-1B9C-4F5A-80F2-05593604799E\",\"name\":\"Food\"},{\"id\":\"C6D3230A-2CEA-4AFE-BFF3-DC1E2C2C4BB4\",\"name\":\"Picnicking\"},{\"id\":\"DF4A35E0-7983-4A3E-BC47-F37B872B0F25\",\"name\":\"Junior Ranger Program\"},{\"id\":\"0B685688-3405-4E2A-ABBA-E3069492EC50\",\"name\":\"Wildlife Watching\"},{\"id\":\"5A2C91D1-50EC-4B24-8BED-A2E11A1892DF\",\"name\":\"Birdwatching\"},{\"id\":\"24380E3F-AD9D-4E38-BF13-C8EEB21893E7\",\"name\":\"Shopping\"},{\"id\":\"467DC8B8-0B7D-436D-A026-80A22358F615\",\"name\":\"Bookstore and Park Store\"},{\"id\":\"43800AD1-D439-40F3-AAB3-9FB651FE45BB\",\"name\":\"Gift Shop and Souvenirs\"}],\"addresses\":[{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Physical\"},{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Mailing\"}],\"contacts\":{\"emailAddresses\":[{\"description\":\"\",\"emailAddress\":\"ABLI_Administration@nps.gov\"}],\"phoneNumbers\":[{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583137\",\"type\":\"Voice\"},{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583874\",\"type\":\"Fax\"}]},\"description\":\"For over a century people from around the world have come to rural Central Kentucky to honor the humble beginnings of our 16th president, Abraham Lincoln. His early life on Kentucky's frontier shaped his character and prepared him to lead the nation through Civil War. The country's first memorial to Lincoln, built with donations from young and old, enshrines the symbolic birthplace cabin.\",\"designation\":\"National Historical Park\",\"directionsInfo\":\"The Birthplace Unit of the park is located approximately 2 miles south of the town of Hodgenville on U.S. Highway 31E South. The Boyhood Home Unit at Knob Creek is located approximately 10 miles northeast of the Birthplace Unit of the park.\",\"directionsUrl\":\"http://www.nps.gov/abli/planyourvisit/directions.htm\",\"entranceFees\":[{\"cost\":\"0.00\",\"description\":\"There is no fee associated with visiting either unit of the park.\",\"title\":\"Fee Free Park\"}],\"entrancePasses\":[],\"fees\":[],\"fullName\":\"Abraham Lincoln Birthplace National Historical Park\",\"id\":\"77E0D7F0-1942-494A-ACE2-9004D2BDC59E\",\"images\":[{\"altText\":\"The Memorial Building surrounded by fall colors\",\"caption\":\"Over 200,000 people a year come to walk up the steps of the Memorial Building to visit the site where Abraham Lincoln was born\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building with fall colors\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861078-1DD8-B71B-0B774A242EF6A706.jpg\"},{\"altText\":\"The first memorial erected to honor Abraham Lincoln\",\"caption\":\"The Memorial Building constructed on the traditional site of the birth of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861263-1DD8-B71B-0B71EF9B95F9644F.jpg\"},{\"altText\":\"The symbolic birth cabin on the traditional site of the birth of Abraham Lincoln.\",\"caption\":\"The symbolic birth cabin of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Symbolic Birth Cabin of Abraham Lincoln\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C86137D-1DD8-B71B-0B978BACD7EBAEF1.jpg\"},{\"altText\":\"Statue of the Lincoln family in the park's Visitor Center\",\"caption\":\"Visitors to the park can view the statue of the Lincoln family.\",\"credit\":\"NPS Photo\",\"title\":\"Statue of the Lincoln Family in the Visitor Center\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C8614D1-1DD8-B71B-0B1AF72CA452B051.jpg\"}],\"latLong\":\"lat:37.5858662, long:-85.67330523\",\"latitude\":\"37.5858662\",\"longitude\":\"-85.67330523\",\"name\":\"Abraham Lincoln Birthplace\",\"operatingHours\":[{\"description\":\"The Memorial Building is open 9:00 am - 4:30 pm eastern time with a limited viewing area.  The Visitor Center and grounds of the Birthplace Unit are open 9:00 am - 5:00 pm eastern time with one-way traffic flow and social distancing.\\n\\n\\nThe Boyhood Home Unit at Knob Creek will be closed August 3, 2020, to July 31, 2021, due to the Lincoln Tavern Rehabilitation Project.\",\"exceptions\":[{\"endDate\":\"2022-01-01\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2022-01-01\"},{\"endDate\":\"2021-11-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-11-25\"},{\"endDate\":\"2021-12-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-12-25\"}],\"name\":\"Abraham Lincoln Birthplace National Historical Park\",\"standardHours\":{\"friday\":\"9:00AM - 5:00PM\",\"monday\":\"9:00AM - 5:00PM\",\"saturday\":\"9:00AM - 5:00PM\",\"sunday\":\"9:00AM - 5:00PM\",\"thursday\":\"9:00AM - 5:00PM\",\"tuesday\":\"9:00AM - 5:00PM\",\"wednesday\":\"9:00AM - 5:00PM\"}}],\"parkCode\":\"abli\",\"states\":\"KY\",\"topics\":[{\"id\":\"D10852A3-443C-4743-A5FA-6DD6D2A054B3\",\"name\":\"Birthplace\"},{\"id\":\"F669BC40-BDC4-41C0-9ACE-B2CD25373045\",\"name\":\"Presidents\"},{\"id\":\"0D00073E-18C3-46E5-8727-2F87B112DDC6\",\"name\":\"Animals\"},{\"id\":\"957EF2BD-AC6C-4B7B-BD9A-87593ADC6691\",\"name\":\"Birds\"},{\"id\":\"E25F3456-43ED-45DD-93BC-057F9B944F7A\",\"name\":\"Caves, Caverns and Karst\"},{\"id\":\"F0F97E32-2F29-41B4-AF98-9FBE8DAB36B1\",\"name\":\"Geology\"},{\"id\":\"4BE01DC5-52E6-4F18-8C9A-B22D65965F6D\",\"name\":\"Groundwater\"},{\"id\":\"0E1A04CC-EB51-4F18-93D4-EC0B0B4EC1E3\",\"name\":\"Freshwater Springs\"},{\"id\":\"A7359FC4-DAD8-45F5-AF15-7FF62F816ED3\",\"name\":\"Night Sky\"}],\"url\":\"https://www.nps.gov/abli/index.htm\",\"weatherInfo\":\"There are four distinct seasons in Central Kentucky. However, temperature and weather conditions can vary widely within those seasons. Spring and Fall are generally pleasant with frequent rain showers. Summer is usually hot and humid. Winter is moderately cold with mixed precipitation.\"},{\"activities\":[{\"id\":\"13A57703-BB1A-41A2-94B8-53B692EB7238\",\"name\":\"Astronomy\"},{\"id\":\"D37A0003-8317-4F04-8FB0-4CF0A272E195\",\"name\":\"Stargazing\"},{\"id\":\"1DFACD97-1B9C-4F5A-80F2-05593604799E\",\"name\":\"Food\"},{\"id\":\"C6D3230A-2CEA-4AFE-BFF3-DC1E2C2C4BB4\",\"name\":\"Picnicking\"},{\"id\":\"DF4A35E0-7983-4A3E-BC47-F37B872B0F25\",\"name\":\"Junior Ranger Program\"},{\"id\":\"0B685688-3405-4E2A-ABBA-E3069492EC50\",\"name\":\"Wildlife Watching\"},{\"id\":\"5A2C91D1-50EC-4B24-8BED-A2E11A1892DF\",\"name\":\"Birdwatching\"},{\"id\":\"24380E3F-AD9D-4E38-BF13-C8EEB21893E7\",\"name\":\"Shopping\"},{\"id\":\"467DC8B8-0B7D-436D-A026-80A22358F615\",\"name\":\"Bookstore and Park Store\"},{\"id\":\"43800AD1-D439-40F3-AAB3-9FB651FE45BB\",\"name\":\"Gift Shop and Souvenirs\"}],\"addresses\":[{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Physical\"},{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Mailing\"}],\"contacts\":{\"emailAddresses\":[{\"description\":\"\",\"emailAddress\":\"ABLI_Administration@nps.gov\"}],\"phoneNumbers\":[{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583137\",\"type\":\"Voice\"},{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583874\",\"type\":\"Fax\"}]},\"description\":\"For over a century people from around the world have come to rural Central Kentucky to honor the humble beginnings of our 16th president, Abraham Lincoln. His early life on Kentucky's frontier shaped his character and prepared him to lead the nation through Civil War. The country's first memorial to Lincoln, built with donations from young and old, enshrines the symbolic birthplace cabin.\",\"designation\":\"National Historical Park\",\"directionsInfo\":\"The Birthplace Unit of the park is located approximately 2 miles south of the town of Hodgenville on U.S. Highway 31E South. The Boyhood Home Unit at Knob Creek is located approximately 10 miles northeast of the Birthplace Unit of the park.\",\"directionsUrl\":\"http://www.nps.gov/abli/planyourvisit/directions.htm\",\"entranceFees\":[{\"cost\":\"0.00\",\"description\":\"There is no fee associated with visiting either unit of the park.\",\"title\":\"Fee Free Park\"}],\"entrancePasses\":[],\"fees\":[],\"fullName\":\"Abraham Lincoln Birthplace National Historical Park\",\"id\":\"77E0D7F0-1942-494A-ACE2-9004D2BDC59E\",\"images\":[{\"altText\":\"The Memorial Building surrounded by fall colors\",\"caption\":\"Over 200,000 people a year come to walk up the steps of the Memorial Building to visit the site where Abraham Lincoln was born\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building with fall colors\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861078-1DD8-B71B-0B774A242EF6A706.jpg\"},{\"altText\":\"The first memorial erected to honor Abraham Lincoln\",\"caption\":\"The Memorial Building constructed on the traditional site of the birth of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861263-1DD8-B71B-0B71EF9B95F9644F.jpg\"},{\"altText\":\"The symbolic birth cabin on the traditional site of the birth of Abraham Lincoln.\",\"caption\":\"The symbolic birth cabin of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Symbolic Birth Cabin of Abraham Lincoln\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C86137D-1DD8-B71B-0B978BACD7EBAEF1.jpg\"},{\"altText\":\"Statue of the Lincoln family in the park's Visitor Center\",\"caption\":\"Visitors to the park can view the statue of the Lincoln family.\",\"credit\":\"NPS Photo\",\"title\":\"Statue of the Lincoln Family in the Visitor Center\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C8614D1-1DD8-B71B-0B1AF72CA452B051.jpg\"}],\"latLong\":\"lat:37.5858662, long:-85.67330523\",\"latitude\":\"37.5858662\",\"longitude\":\"-85.67330523\",\"name\":\"Abraham Lincoln Birthplace\",\"operatingHours\":[{\"description\":\"The Memorial Building is open 9:00 am - 4:30 pm eastern time with a limited viewing area.  The Visitor Center and grounds of the Birthplace Unit are open 9:00 am - 5:00 pm eastern time with one-way traffic flow and social distancing.\\n\\n\\nThe Boyhood Home Unit at Knob Creek will be closed August 3, 2020, to July 31, 2021, due to the Lincoln Tavern Rehabilitation Project.\",\"exceptions\":[{\"endDate\":\"2022-01-01\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2022-01-01\"},{\"endDate\":\"2021-11-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-11-25\"},{\"endDate\":\"2021-12-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-12-25\"}],\"name\":\"Abraham Lincoln Birthplace National Historical Park\",\"standardHours\":{\"friday\":\"9:00AM - 5:00PM\",\"monday\":\"9:00AM - 5:00PM\",\"saturday\":\"9:00AM - 5:00PM\",\"sunday\":\"9:00AM - 5:00PM\",\"thursday\":\"9:00AM - 5:00PM\",\"tuesday\":\"9:00AM - 5:00PM\",\"wednesday\":\"9:00AM - 5:00PM\"}}],\"parkCode\":\"abli\",\"states\":\"KY\",\"topics\":[{\"id\":\"D10852A3-443C-4743-A5FA-6DD6D2A054B3\",\"name\":\"Birthplace\"},{\"id\":\"F669BC40-BDC4-41C0-9ACE-B2CD25373045\",\"name\":\"Presidents\"},{\"id\":\"0D00073E-18C3-46E5-8727-2F87B112DDC6\",\"name\":\"Animals\"},{\"id\":\"957EF2BD-AC6C-4B7B-BD9A-87593ADC6691\",\"name\":\"Birds\"},{\"id\":\"E25F3456-43ED-45DD-93BC-057F9B944F7A\",\"name\":\"Caves, Caverns and Karst\"},{\"id\":\"F0F97E32-2F29-41B4-AF98-9FBE8DAB36B1\",\"name\":\"Geology\"},{\"id\":\"4BE01DC5-52E6-4F18-8C9A-B22D65965F6D\",\"name\":\"Groundwater\"},{\"id\":\"0E1A04CC-EB51-4F18-93D4-EC0B0B4EC1E3\",\"name\":\"Freshwater Springs\"},{\"id\":\"A7359FC4-DAD8-45F5-AF15-7FF62F816ED3\",\"name\":\"Night Sky\"}],\"url\":\"https://www.nps.gov/abli/index.htm\",\"weatherInfo\":\"There are four distinct seasons in Central Kentucky. However, temperature and weather conditions can vary widely within those seasons. Spring and Fall are generally pleasant with frequent rain showers. Summer is usually hot and humid. Winter is moderately cold with mixed precipitation.\"},{\"activities\":[{\"id\":\"13A57703-BB1A-41A2-94B8-53B692EB7238\",\"name\":\"Astronomy\"},{\"id\":\"D37A0003-8317-4F04-8FB0-4CF0A272E195\",\"name\":\"Stargazing\"},{\"id\":\"1DFACD97-1B9C-4F5A-80F2-05593604799E\",\"name\":\"Food\"},{\"id\":\"C6D3230A-2CEA-4AFE-BFF3-DC1E2C2C4BB4\",\"name\":\"Picnicking\"},{\"id\":\"DF4A35E0-7983-4A3E-BC47-F37B872B0F25\",\"name\":\"Junior Ranger Program\"},{\"id\":\"0B685688-3405-4E2A-ABBA-E3069492EC50\",\"name\":\"Wildlife Watching\"},{\"id\":\"5A2C91D1-50EC-4B24-8BED-A2E11A1892DF\",\"name\":\"Birdwatching\"},{\"id\":\"24380E3F-AD9D-4E38-BF13-C8EEB21893E7\",\"name\":\"Shopping\"},{\"id\":\"467DC8B8-0B7D-436D-A026-80A22358F615\",\"name\":\"Bookstore and Park Store\"},{\"id\":\"43800AD1-D439-40F3-AAB3-9FB651FE45BB\",\"name\":\"Gift Shop and Souvenirs\"}],\"addresses\":[{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Physical\"},{\"city\":\"Hodgenville\",\"line1\":\"2995 Lincoln Farm Road\",\"line2\":\"\",\"line3\":\"\",\"postalCode\":\"42748\",\"stateCode\":\"KY\",\"type\":\"Mailing\"}],\"contacts\":{\"emailAddresses\":[{\"description\":\"\",\"emailAddress\":\"ABLI_Administration@nps.gov\"}],\"phoneNumbers\":[{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583137\",\"type\":\"Voice\"},{\"description\":\"\",\"extension\":\"\",\"phoneNumber\":\"2703583874\",\"type\":\"Fax\"}]},\"description\":\"For over a century people from around the world have come to rural Central Kentucky to honor the humble beginnings of our 16th president, Abraham Lincoln. His early life on Kentucky's frontier shaped his character and prepared him to lead the nation through Civil War. The country's first memorial to Lincoln, built with donations from young and old, enshrines the symbolic birthplace cabin.\",\"designation\":\"National Historical Park\",\"directionsInfo\":\"The Birthplace Unit of the park is located approximately 2 miles south of the town of Hodgenville on U.S. Highway 31E South. The Boyhood Home Unit at Knob Creek is located approximately 10 miles northeast of the Birthplace Unit of the park.\",\"directionsUrl\":\"http://www.nps.gov/abli/planyourvisit/directions.htm\",\"entranceFees\":[{\"cost\":\"0.00\",\"description\":\"There is no fee associated with visiting either unit of the park.\",\"title\":\"Fee Free Park\"}],\"entrancePasses\":[],\"fees\":[],\"fullName\":\"Abraham Lincoln Birthplace National Historical Park\",\"id\":\"77E0D7F0-1942-494A-ACE2-9004D2BDC59E\",\"images\":[{\"altText\":\"The Memorial Building surrounded by fall colors\",\"caption\":\"Over 200,000 people a year come to walk up the steps of the Memorial Building to visit the site where Abraham Lincoln was born\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building with fall colors\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861078-1DD8-B71B-0B774A242EF6A706.jpg\"},{\"altText\":\"The first memorial erected to honor Abraham Lincoln\",\"caption\":\"The Memorial Building constructed on the traditional site of the birth of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Memorial Building\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C861263-1DD8-B71B-0B71EF9B95F9644F.jpg\"},{\"altText\":\"The symbolic birth cabin on the traditional site of the birth of Abraham Lincoln.\",\"caption\":\"The symbolic birth cabin of Abraham Lincoln.\",\"credit\":\"NPS Photo\",\"title\":\"The Symbolic Birth Cabin of Abraham Lincoln\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C86137D-1DD8-B71B-0B978BACD7EBAEF1.jpg\"},{\"altText\":\"Statue of the Lincoln family in the park's Visitor Center\",\"caption\":\"Visitors to the park can view the statue of the Lincoln family.\",\"credit\":\"NPS Photo\",\"title\":\"Statue of the Lincoln Family in the Visitor Center\",\"url\":\"https://www.nps.gov/common/uploads/structured_data/3C8614D1-1DD8-B71B-0B1AF72CA452B051.jpg\"}],\"latLong\":\"lat:37.5858662, long:-85.67330523\",\"latitude\":\"37.5858662\",\"longitude\":\"-85.67330523\",\"name\":\"Abraham Lincoln Birthplace\",\"operatingHours\":[{\"description\":\"The Memorial Building is open 9:00 am - 4:30 pm eastern time with a limited viewing area.  The Visitor Center and grounds of the Birthplace Unit are open 9:00 am - 5:00 pm eastern time with one-way traffic flow and social distancing.\\n\\n\\nThe Boyhood Home Unit at Knob Creek will be closed August 3, 2020, to July 31, 2021, due to the Lincoln Tavern Rehabilitation Project.\",\"exceptions\":[{\"endDate\":\"2022-01-01\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2022-01-01\"},{\"endDate\":\"2021-11-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-11-25\"},{\"endDate\":\"2021-12-25\",\"exceptionHours\":{},\"name\":\"Park is Closed\",\"startDate\":\"2021-12-25\"}],\"name\":\"Abraham Lincoln Birthplace National Historical Park\",\"standardHours\":{\"friday\":\"9:00AM - 5:00PM\",\"monday\":\"9:00AM - 5:00PM\",\"saturday\":\"9:00AM - 5:00PM\",\"sunday\":\"9:00AM - 5:00PM\",\"thursday\":\"9:00AM - 5:00PM\",\"tuesday\":\"9:00AM - 5:00PM\",\"wednesday\":\"9:00AM - 5:00PM\"}}],\"parkCode\":\"abli\",\"states\":\"KY\",\"topics\":[{\"id\":\"D10852A3-443C-4743-A5FA-6DD6D2A054B3\",\"name\":\"Birthplace\"},{\"id\":\"F669BC40-BDC4-41C0-9ACE-B2CD25373045\",\"name\":\"Presidents\"},{\"id\":\"0D00073E-18C3-46E5-8727-2F87B112DDC6\",\"name\":\"Animals\"},{\"id\":\"957EF2BD-AC6C-4B7B-BD9A-87593ADC6691\",\"name\":\"Birds\"},{\"id\":\"E25F3456-43ED-45DD-93BC-057F9B944F7A\",\"name\":\"Caves, Caverns and Karst\"},{\"id\":\"F0F97E32-2F29-41B4-AF98-9FBE8DAB36B1\",\"name\":\"Geology\"},{\"id\":\"4BE01DC5-52E6-4F18-8C9A-B22D65965F6D\",\"name\":\"Groundwater\"},{\"id\":\"0E1A04CC-EB51-4F18-93D4-EC0B0B4EC1E3\",\"name\":\"Freshwater Springs\"},{\"id\":\"A7359FC4-DAD8-45F5-AF15-7FF62F816ED3\",\"name\":\"Night Sky\"}],\"url\":\"https://www.nps.gov/abli/index.htm\",\"weatherInfo\":\"There are four distinct seasons in Central Kentucky. However, temperature and weather conditions can vary widely within those seasons. Spring and Fall are generally pleasant with frequent rain showers. Summer is usually hot and humid. Winter is moderately cold with mixed precipitation.\"}],\"limit\":\"3\",\"start\":\"0\",\"total\":\"468\"}");

/***/ }),

/***/ "./src/data/thingstodo.json":
/*!**********************************!*\
  !*** ./src/data/thingstodo.json ***!
  \**********************************/
/*! exports provided: data, limit, start, total, default */
/***/ (function(module) {

module.exports = JSON.parse("{\"data\":[{\"accessibilityInformation\":\"<p>Most of the historic buildings are wheelchair accessible. Please see our <a href=\\\"/fols/planyourvisit/accessibility.htm\\\" id=\\\"CP___PAGEID=3020534,accessibility.htm,22292|\\\">Accessibility</a> page for more information.</p>\",\"activities\":[{\"id\":\"A0631906-9672-4583-91DE-113B93DB6B6E\",\"name\":\"Self-Guided Tours - Walking\"}],\"activityDescription\":\"\",\"age\":\"\",\"ageDescription\":\"\",\"amenities\":[],\"arePetsPermitted\":\"true\",\"arePetsPermittedWithRestrictions\":\"false\",\"doFeesApply\":\"false\",\"duration\":\"45-60 Minutes\",\"durationDescription\":\"\",\"feeDescription\":\"\",\"id\":\"F7B724F1-73D2-4266-B9B5-ED69270FAFF1\",\"images\":[{\"altText\":\"Room with shelves of reproduction food containers on shelves.\",\"caption\":\"All the historic buildings are furnished with reproduction items to look as authentic as possible.\",\"credit\":\"NPS Photo\",\"crops\":[{\"aspectRatio\":\"1.78\",\"url\":\"https://www.nps.gov/common/uploads/cropped_image/primary/928D157F-BB27-6614-8C0833EE8C35672E.jpg\"},{\"aspectRatio\":\"1\",\"url\":\"https://www.nps.gov/common/uploads/cropped_image/secondary/928D157F-BB27-6614-8C0833EE8C35672E.jpg\"}],\"description\":\"\",\"title\":\"Fort Larned Cell Phone Tour\",\"url\":\"https://www.nps.gov/common/uploads/cropped_image/928D157F-BB27-6614-8C0833EE8C35672E.jpg\"}],\"isReservationRequired\":\"false\",\"latitude\":\"38.18372\",\"location\":\"Start at the Fort Larned Visitor Center\",\"locationDescription\":\"\",\"longDescription\":\"<p>The Oncell Cell Phone tour is a great way to explore the historic buildings and the History & Nature Trail at Fort Larned.</p>\",\"longitude\":\"-99.218562\",\"petsDescription\":\"Pets need to be on a leash and accompanies at all times. Pets are not allowed in any of the historic buildings.\",\"relatedOrganizations\":[],\"relatedParks\":[{\"designation\":\"National Historic Site\",\"fullName\":\"Fort Larned National Historic Site\",\"name\":\"Fort Larned\",\"parkCode\":\"fols\",\"states\":\"KS\",\"url\":\"https://www.nps.gov/fols/index.htm\"}],\"reservationDescription\":\"\",\"season\":[\"Winter\",\"Spring\",\"Summer\",\"Fall\"],\"seasonDescription\":\"<p>Keep in mind that Kansas winters can be very cold and the summers very hot. See our <a href=\\\"/fols/planyourvisit/weather.htm\\\" id=\\\"CP___PAGEID=3414815,weather.htm,22292|\\\">Weather </a>page for more information about the Kansas climate.</p>\",\"shortDescription\":\"The Oncell Cell Phone tour is a great way to explore the historic buildings and the History & Nature Trail at Fort Larned.\",\"tags\":[\"Outdoor activities\",\"fort larned\",\"indian wars\",\"Santa Fe Trail\",\"Kansas history\",\"U.S. Army\"],\"timeOfDay\":[\"Day\"],\"timeOfDayDescription\":\"The park grounds are only open from 8:30 am to 4:30 pm daily.\",\"title\":\"Explore With the Cell Phone Tour\",\"topics\":[{\"id\":\"28AEAE85-9DDA-45B6-981B-1CFCDCC61E14\",\"name\":\"African American Heritage\"},{\"id\":\"0D00073E-18C3-46E5-8727-2F87B112DDC6\",\"name\":\"Animals\"},{\"id\":\"957EF2BD-AC6C-4B7B-BD9A-87593ADC6691\",\"name\":\"Birds\"},{\"id\":\"324C31EC-7D75-41C7-AA28-EF8ACB5B6BF5\",\"name\":\"Bison\"},{\"id\":\"69693007-2DF2-4EDE-BB3B-A25EBA72BDF5\",\"name\":\"Architecture and Building\"},{\"id\":\"1170EEB6-5070-4760-8E7D-FF1A98272FAD\",\"name\":\"Commerce\"},{\"id\":\"A010EADF-78B8-4526-B0A4-70B0E0B3470E\",\"name\":\"Trade\"},{\"id\":\"988B4AFC-F478-4673-B66D-E6BDB0CCFF35\",\"name\":\"Forts\"},{\"id\":\"94262026-92F5-48E9-90EF-01CEAEFBA4FF\",\"name\":\"Grasslands\"},{\"id\":\"1AEDC86F-5792-487F-8CDF-9E92CAB97ACE\",\"name\":\"Prairies\"},{\"id\":\"78078CA8-DCBC-4320-A7BF-259A56D55CA2\",\"name\":\"Hispanic American Heritage\"},{\"id\":\"3B0D607D-9933-425A-BFA0-21529AC4734C\",\"name\":\"Military\"},{\"id\":\"69790851-A62A-4920-AA3E-E61406FFC27D\",\"name\":\"Cavalry\"},{\"id\":\"6766B838-9493-4EF8-830E-2D1EFB917461\",\"name\":\"Indigenous and Native Warrior\"},{\"id\":\"E411F474-A530-4804-9D23-1D94C78B41E4\",\"name\":\"Infantry and Militia\"},{\"id\":\"97CCB419-196C-4B95-BB3A-621458F78415\",\"name\":\"US Army\"},{\"id\":\"A1BAF33E-EA84-4608-A888-4CEE9541F027\",\"name\":\"Native American Heritage\"},{\"id\":\"53798A16-7CDB-4F17-9C28-095F92D2ED8D\",\"name\":\"Indian and Frontier Wars\"},{\"id\":\"7DA81DAB-5045-4953-9C20-36590AD9FA95\",\"name\":\"Women's History\"}],\"url\":\"https://www.nps.gov/thingstodo/explore-with-the-cell-phone-tour.htm\"},{\"accessibilityInformation\":\"<p>All viewpoints along the Southern Scenic Drive are paved and provide relatively flat access to the views. The exception to this is Piracy Point, which is reached via a flat 1,000 foot (39m) unpaved path that provides good access when dry. Visit our <a href=\\\"https://www.nps.gov/brca/planyourvisit/accessibility.htm\\\">Accessibility page</a> for more information.</p>\",\"activities\":[{\"id\":\"0B4A5320-216D-451A-9990-626E1D5ACE28\",\"name\":\"Scenic Driving\"}],\"activityDescription\":\"<p>This trip provides opportunities to see 9 unique viewpoints along the southern, higher elevation stretches of the main park road. These viewpoints not only overlook unique geologic features, but the grand landscape of the Paria Valley and the Grand Staircase beyond.</p>\",\"age\":\"\",\"ageDescription\":\"\",\"amenities\":[],\"arePetsPermitted\":\"true\",\"arePetsPermittedWithRestrictions\":\"false\",\"doFeesApply\":\"true\",\"duration\":\"2-12 Hours\",\"durationDescription\":\"<p>Depending on whether you just enjoy the drive and the viewpoints or choose to take a hike along the way, the Southern Scenic Drive can be a relatively short excursion or your whole day.</p>\",\"feeDescription\":\"<p>Park admission provides access to this activity.</p>\",\"id\":\"0847C602-A853-48F0-A394-67A12607DEDA\",\"images\":[{\"altText\":\"A large red rock limestone arch stands along a slope above forested cliffs\",\"caption\":\"Natural Bridge is one of nine viewpoints along the Southern Scenic Drive\",\"credit\":\"NPS Photo / Peter Densmore\",\"crops\":[{\"aspectRatio\":\"1.78\",\"url\":\"https://www.nps.gov/common/uploads/cropped_image/primary/8644FCF8-E1C9-4C45-6CDE52E153F53894.jpg\"},{\"aspectRatio\":\"1\",\"url\":\"https://www.nps.gov/common/uploads/cropped_image/secondary/8644FCF8-E1C9-4C45-6CDE52E153F53894.jpg\"}],\"description\":\"\",\"title\":\"Natural Bridge\",\"url\":\"https://www.nps.gov/common/uploads/cropped_image/8644FCF8-E1C9-4C45-6CDE52E153F53894.jpg\"}],\"isReservationRequired\":\"false\",\"latitude\":\"\",\"location\":\"\",\"locationDescription\":\"\",\"longDescription\":\"<p><b>Check out the Park App's \\\"Tours\\\" section for a guided walkthrough of how to take a driving tour of this area.</b><br /> <br /> The main park road is 18 miles (29 km) from the park entrance in the north to the end of the road at Rainbow at Yovimpa Points. While the Bryce Amphitheater area is found along its first 3 miles (4.8 km), the next 15 miles (24 km) to the road's end are known as the Southern Scenic Drive. <br /> <br /> Along this section of the main road are 9 scenic overlooks that display a lesser-seen beauty of Bryce Canyon. One aspect that makes the Southern Scenic Drive unique is that the park's elevations increase the further south you travel. While the Visitor Center is situated at 7,894 ft (2,406 m), Rainbow and Yovimpa Points exceed 9,000 ft (2,743 m). This change in elevation is the result uneven tectonic forces that uplifted this region over the last 20 million years, and creates a dramatic contrast--with exceptionally long views and unique features--between the north and south ends of the park.</p> <h3>Winter Closures</h3> <p>The high elevations along this section of the main road also lead to temporary winter closures at the Mile 3 gate, so be sure to visit the <a href=\\\"https://www.nps.gov/brca/planyourvisit/conditions.htm\\\">Conditions page</a> for the latest road status.</p> <h3>Planning Your Drive</h3> <p>It takes about 40 minutes to drive to the end of the Southern Scenic Drive from the Visitor Center. It is recommended that you begin by first doing this all the way to Rainbow and Yovimpa Points. After enjoying these viewpoints, those you will have passed along the road can be visited on the way back. This way, everything will be on the right-hand side of the road; much safer than pulling across traffic everytime you want to see a viewpoint. Typically a trip of only driving and viewpoints will take around 2-3 hours.</p> <h3>Guided Tours</h3> <p>During shuttle season (mid-April through mid-October), a 3-hour Rainbow Point Tour is offered by the park's shuttle operator, taking visitors to viewpoints along this section of the main road. The tour is free, but requires reservations. Reservations can be made by calling 435-834-5290.</p> <h3>Highlights</h3> <p>It is easy to enjoy all the views along the road, as none of them require much walking to reach. That said, most visitors would likely agree that Rainbow Point, Yovimpa Point, Agua Canyon and Natural Bridge are not to be missed.</p> <h3>Walks and Hikes</h3> <p>All the viewpoints along the Southern Scenic Drive are located a very short walk from their parking lots, so no hiking is required to enjoy this trip. That said, very little can be seen from the car, so you will want to walk the short distances to the railings to enjoy the view. There are two popular day hikes located along this drive: the easy Bristlecone Loop at Rainbow and Yovimpa Points (usually takes about an hour to hike) and the Swamp Canyon Loop at Swamp Canyon overlook (often takes 3 to 4 hours).<br /> <br /> Backcountry hiking options also exist, with the Under-the-Rim Trail running approximately 22.9 miles (36.9 km) from Bryce Point to Rainbow Point. Connecting trails offer access from along the Southern Scenic Drive and intersect the Under-the-Rim Trail at Swamp Canyon, and Whiteman Bench. The Riggs Spring Loop is an 8.6 mile (13.9 km) hike beginning and ending at the Rainbow and Yovimpa Point parking lot at the south end of the Scenic Drive. Either the Yovimpa or Rainbow Point trailhead may be used to access this loop. A backcountry hiking map with mileage is available at the Visitor Center. <a href=\\\"https://www.nps.gov/brca/planyourvisit/backcountryinfo.htm\\\">More information</a></p>\",\"longitude\":\"\",\"petsDescription\":\"<ul> <li><strong>Pets are permitted</strong> <strong>on all paved surfaces in the park:</strong> campgrounds, parking lots, paved roads, paved viewpoint areas, on the paved trail between Sunset Point and Sunrise Point, and on the paved Shared Use Path between the park entrance and Inspiration Point.</li> <li><strong>Pets are not permitted</strong> on unpaved trails or viewpoints, in public buildings or on public transportation vehicles. These regulations also apply to pets that are carried.</li> <li><strong>Pets must be on a leash at all times</strong>; the leash must be no longer than 6 feet.</li> <li><strong>Pet owners may not leave pet(s) unattended</strong> or tied to an object. Pets may not be left in vehicles while their owners hike. Be aware that idling and generator use is not permitted in park parking lots.</li> <li><strong>Pets may not make unreasonable noise.</strong></li> <li><strong>Pet owners are required to pick-up after their pets</strong> (excrement and other solid waste).</li> </ul>\",\"relatedOrganizations\":[],\"relatedParks\":[{\"designation\":\"National Park\",\"fullName\":\"Bryce Canyon National Park\",\"name\":\"Bryce Canyon\",\"parkCode\":\"brca\",\"states\":\"UT\",\"url\":\"https://www.nps.gov/brca/index.htm\"}],\"reservationDescription\":\"\",\"season\":[\"Winter\",\"Spring\",\"Summer\",\"Fall\"],\"seasonDescription\":\"\",\"shortDescription\":\"The main park road is 18 miles (29 km) from the park entrance in the north to the end of the road at Rainbow at Yovimpa Points. While the Bryce Amphitheater area is found along its first 3 miles (4.8 km), the next 15 miles (24 km) to the road's end are known as the Southern Scenic Drive. Along this section of the main road are 9 scenic overlooks that display a lesser-seen beauty of Bryce Canyon.\",\"tags\":[\"Southern Scenic Drive\",\"Bryce Canyon National Park\",\"driving\",\"Scenic Drive\"],\"timeOfDay\":[\"Day\",\"Night\",\"Dawn\",\"Dusk\"],\"timeOfDayDescription\":\"\",\"title\":\"Drive the Southern Scenic Drive\",\"topics\":[{\"id\":\"9C9FCBB6-360B-4743-8155-6F9341CBE01B\",\"name\":\"Scenic Views\"}],\"url\":\"https://www.nps.gov/thingstodo/drive-the-southern-scenic-drive.htm\"},{\"accessibilityInformation\":\"A.K. Bissell Park’s paved, flat trails are ideal for persons of all abilities to enjoy. Parking is plentiful and located within the park boundaries. \",\"activities\":[{\"id\":\"C6D3230A-2CEA-4AFE-BFF3-DC1E2C2C4BB4\",\"name\":\"Picnicking\"}],\"activityDescription\":\"\",\"age\":\"\",\"ageDescription\":\"\",\"amenities\":[],\"arePetsPermitted\":\"true\",\"arePetsPermittedWithRestrictions\":\"false\",\"doFeesApply\":\"false\",\"duration\":\"\",\"durationDescription\":\"\",\"feeDescription\":\"\",\"id\":\"82F4F90F-5FE5-4883-9BE6-204663D92BFA\",\"images\":[{\"altText\":\"People gathered around a large bronze bell with grass, trees, and trails throughout\",\"caption\":\"The International Friendship Bell is located within A.K. Bissell Park.\",\"credit\":\"NPS/KLEIN\",\"crops\":[{\"aspectRatio\":\"1.78\",\"url\":\"https://www.nps.gov/common/uploads/cropped_image/primary/0FB17572-DD4C-AE04-DEBF79EA38606BD1.jpg\"},{\"aspectRatio\":\"1\",\"url\":\"https://www.nps.gov/common/uploads/cropped_image/secondary/0FB17572-DD4C-AE04-DEBF79EA38606BD1.jpg\"}],\"description\":\"\",\"title\":\"A.K. Bissell Park\",\"url\":\"https://www.nps.gov/common/uploads/cropped_image/0FB17572-DD4C-AE04-DEBF79EA38606BD1.jpg\"}],\"isReservationRequired\":\"false\",\"latitude\":\"\",\"location\":\"\",\"locationDescription\":\"\",\"longDescription\":\"Located in the heart of Oak Ridge, A.K. Bissell Park offers a paved 1.25 mile walking loop on 38 acres of land. The park includes a picnic shelter and performance pavilion where visitors can enjoy various special events throughout the year. In addition, the park is home to the <a href=\\\"http://www.oakridgetn.gov/department/library/home\\\">Oak Ridge Public Library </a>and two sites related to the Manhattan Project. The International Friendship Bell, an 8,000 pound bronze bell that commemorates peace and unity between the United States and Japan and the Secret City Commemorative Walk, a series of plaques and memorials dedicated to the founding of Oak Ridge during the Manhattan Project, are both found <a href=\\\"http://exploreoakridge.com/attractions/a-k-bissell-park/\\\">here</a>.  \",\"longitude\":\"\",\"petsDescription\":\"\",\"relatedOrganizations\":[],\"relatedParks\":[{\"designation\":\"National Historical Park\",\"fullName\":\"Manhattan Project National Historical Park\",\"name\":\"Manhattan Project\",\"parkCode\":\"mapr\",\"states\":\"NM,WA,TN\",\"url\":\"https://www.nps.gov/mapr/index.htm\"}],\"reservationDescription\":\"\",\"season\":[\"Winter\",\"Spring\",\"Summer\",\"Fall\"],\"seasonDescription\":\"\",\"shortDescription\":\"Acadia National Park is a favorite location for Oak Ridgers to exercise, walk a pet, or just relax and enjoy the various attractions the park offers.\",\"tags\":[],\"timeOfDay\":[\"Day\",\"Dawn\",\"Dusk\"],\"timeOfDayDescription\":\"\",\"title\":\"Oak Ridge: Explore Acadia National Park\",\"topics\":[{\"id\":\"69693007-2DF2-4EDE-BB3B-A25EBA72BDF5\",\"name\":\"Architecture and Building\"},{\"id\":\"156AD9B6-B377-418C-BC9A-F6E682D4BAF7\",\"name\":\"Poetry and Literature\"},{\"id\":\"0DE2D6B3-46DE-44B1-BB5F-E9E8874630D5\",\"name\":\"Sculpture\"},{\"id\":\"3B0D607D-9933-425A-BFA0-21529AC4734C\",\"name\":\"Military\"},{\"id\":\"BEB7E470-13B2-4E00-84B2-0402D98DAF69\",\"name\":\"Monuments and Memorials\"},{\"id\":\"D1CF31DE-AFED-412B-9425-DD1FD4CBB5C7\",\"name\":\"Science, Technology and Innovation\"},{\"id\":\"5BE55D7F-BDB6-4E3D-AC35-2D8EBB974417\",\"name\":\"Trails\"},{\"id\":\"846E5C56-E5AC-489C-B3BF-11F4C73F12C2\",\"name\":\"Urban America\"},{\"id\":\"74869FAE-8DFD-4454-847F-11FDDB679F94\",\"name\":\"World War II\"}],\"url\":\"https://www.nps.gov/thingstodo/explore-a-k-bissell-park.htm\"}],\"limit\":\"3\",\"start\":\"0\",\"total\":\"1683\"}");

/***/ }),

/***/ "./src/images/HomeBackground.jpg":
/*!***************************************!*\
  !*** ./src/images/HomeBackground.jpg ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "static/media/HomeBackground.6f7b536d.jpg");

/***/ }),

/***/ "./src/images/TreeHugLogo.svg":
/*!************************************!*\
  !*** ./src/images/TreeHugLogo.svg ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "static/media/TreeHugLogo.512693fd.svg");

/***/ }),

/***/ "./src/images/docker.svg":
/*!*******************************!*\
  !*** ./src/images/docker.svg ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "static/media/docker.3253a6fe.svg");

/***/ }),

/***/ "./src/images/eugene.jpeg":
/*!********************************!*\
  !*** ./src/images/eugene.jpeg ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "static/media/eugene.3b8d6b46.jpeg");

/***/ }),

/***/ "./src/images/gitlab.svg":
/*!*******************************!*\
  !*** ./src/images/gitlab.svg ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "static/media/gitlab.55c62a16.svg");

/***/ }),

/***/ "./src/images/postman.svg":
/*!********************************!*\
  !*** ./src/images/postman.svg ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "static/media/postman.eda685d0.svg");

/***/ }),

/***/ "./src/images/react.svg":
/*!******************************!*\
  !*** ./src/images/react.svg ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "static/media/react.903d77ad.svg");

/***/ }),

/***/ "./src/images/regina.jpeg":
/*!********************************!*\
  !*** ./src/images/regina.jpeg ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "static/media/regina.226fd193.jpeg");

/***/ }),

/***/ "./src/images/sophie.jpg":
/*!*******************************!*\
  !*** ./src/images/sophie.jpg ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "static/media/sophie.fddfbda5.jpg");

/***/ }),

/***/ "./src/images/stephanie.jpeg":
/*!***********************************!*\
  !*** ./src/images/stephanie.jpeg ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "static/media/stephanie.ca3b08c0.jpeg");

/***/ }),

/***/ "./src/images/yichen.jpg":
/*!*******************************!*\
  !*** ./src/images/yichen.jpg ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "static/media/yichen.d799daec.jpg");

/***/ }),

/***/ "./src/index.tsx":
/*!***********************!*\
  !*** ./src/index.tsx ***!
  \***********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(__react_refresh_utils__, __react_refresh_error_overlay__) {/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var bootstrap_dist_css_bootstrap_min_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! bootstrap/dist/css/bootstrap.min.css */ "./node_modules/bootstrap/dist/css/bootstrap.min.css");
/* harmony import */ var bootstrap_dist_css_bootstrap_min_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(bootstrap_dist_css_bootstrap_min_css__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _App__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./App */ "./src/App.tsx");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__);
__webpack_require__.$Refresh$.runtime = __webpack_require__(/*! ./node_modules/react-refresh/runtime.js */ "./node_modules/react-refresh/runtime.js");
__webpack_require__.$Refresh$.setup(module.i);

var _jsxFileName = "/tree-hugs/client/src/index.tsx";






react_dom__WEBPACK_IMPORTED_MODULE_1___default.a.render( /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["BrowserRouter"], {
  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])(_App__WEBPACK_IMPORTED_MODULE_4__["default"], {}, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 9,
    columnNumber: 5
  }, undefined)
}, void 0, false, {
  fileName: _jsxFileName,
  lineNumber: 8,
  columnNumber: 3
}, undefined), document.getElementById("root"));

const currentExports = __react_refresh_utils__.getModuleExports(module.i);
__react_refresh_utils__.registerExportsForReactRefresh(currentExports, module.i);

if (true) {
  const isHotUpdate = !!module.hot.data;
  const prevExports = isHotUpdate ? module.hot.data.prevExports : null;

  if (__react_refresh_utils__.isReactRefreshBoundary(currentExports)) {
    module.hot.dispose(
      /**
       * A callback to performs a full refresh if React has unrecoverable errors,
       * and also caches the to-be-disposed module.
       * @param {*} data A hot module data object from Webpack HMR.
       * @returns {void}
       */
      function hotDisposeCallback(data) {
        // We have to mutate the data object to get data registered and cached
        data.prevExports = currentExports;
      }
    );
    module.hot.accept(
      /**
       * An error handler to allow self-recovering behaviours.
       * @param {Error} error An error occurred during evaluation of a module.
       * @returns {void}
       */
      function hotErrorHandler(error) {
        if (
          typeof __react_refresh_error_overlay__ !== 'undefined' &&
          __react_refresh_error_overlay__
        ) {
          __react_refresh_error_overlay__.handleRuntimeError(error);
        }

        if (typeof __react_refresh_test__ !== 'undefined' && __react_refresh_test__) {
          if (window.onHotAcceptError) {
            window.onHotAcceptError(error.message);
          }
        }

        __webpack_require__.c[module.i].hot.accept(hotErrorHandler);
      }
    );

    if (isHotUpdate) {
      if (
        __react_refresh_utils__.isReactRefreshBoundary(prevExports) &&
        __react_refresh_utils__.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)
      ) {
        module.hot.invalidate();
      } else {
        __react_refresh_utils__.enqueueUpdate(
          /**
           * A function to dismiss the error overlay after performing React refresh.
           * @returns {void}
           */
          function updateCallback() {
            if (
              typeof __react_refresh_error_overlay__ !== 'undefined' &&
              __react_refresh_error_overlay__
            ) {
              __react_refresh_error_overlay__.clearRuntimeErrors();
            }
          }
        );
      }
    }
  } else {
    if (isHotUpdate && __react_refresh_utils__.isReactRefreshBoundary(prevExports)) {
      module.hot.invalidate();
    }
  }
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js"), __webpack_require__(/*! ./node_modules/react-dev-utils/refreshOverlayInterop.js */ "./node_modules/react-dev-utils/refreshOverlayInterop.js")))

/***/ }),

/***/ "./src/pages/AboutPage.tsx":
/*!*********************************!*\
  !*** ./src/pages/AboutPage.tsx ***!
  \*********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(__react_refresh_utils__, __react_refresh_error_overlay__) {/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _styles_pages_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../styles/pages.css */ "./src/styles/pages.css");
/* harmony import */ var _styles_pages_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_styles_pages_css__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _styles_about_module_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../styles/about.module.css */ "./src/styles/about.module.css");
/* harmony import */ var _styles_about_module_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_styles_about_module_css__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _components_TeamInfo__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/TeamInfo */ "./src/components/TeamInfo.tsx");
/* harmony import */ var _components_ToolList__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components/ToolList */ "./src/components/ToolList.tsx");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__);
__webpack_require__.$Refresh$.runtime = __webpack_require__(/*! ./node_modules/react-refresh/runtime.js */ "./node_modules/react-refresh/runtime.js");
__webpack_require__.$Refresh$.setup(module.i);

var _jsxFileName = "/tree-hugs/client/src/pages/AboutPage.tsx";







function AboutPage() {
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("div", {
    className: "page",
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("div", {
      className: _styles_about_module_css__WEBPACK_IMPORTED_MODULE_2___default.a.aboutPage,
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("h1", {
        children: "About Us"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 11,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("p", {
        children: "Treehugs is a website for travelers to find information about national parks in United States, and the related events and things to do in the park."
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 12,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("h3", {
        children: ["Our Data Source:", " ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("a", {
          href: "https://www.nps.gov/subjects/developer/api-documentation.htm",
          children: "National Park Service"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 18,
          columnNumber: 11
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 16,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("p", {
        children: "This API is designed to provide authoritative National Park Service (NPS) data and content about parks and their facilities, events, news, alerts, and more."
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 21,
        columnNumber: 9
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 10,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])(_components_TeamInfo__WEBPACK_IMPORTED_MODULE_3__["default"], {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 26,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 27,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 28,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 29,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])(_components_ToolList__WEBPACK_IMPORTED_MODULE_4__["default"], {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 30,
      columnNumber: 7
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 9,
    columnNumber: 5
  }, this);
}

_c = AboutPage;
/* harmony default export */ __webpack_exports__["default"] = (AboutPage);

var _c;

__webpack_require__.$Refresh$.register(_c, "AboutPage");

const currentExports = __react_refresh_utils__.getModuleExports(module.i);
__react_refresh_utils__.registerExportsForReactRefresh(currentExports, module.i);

if (true) {
  const isHotUpdate = !!module.hot.data;
  const prevExports = isHotUpdate ? module.hot.data.prevExports : null;

  if (__react_refresh_utils__.isReactRefreshBoundary(currentExports)) {
    module.hot.dispose(
      /**
       * A callback to performs a full refresh if React has unrecoverable errors,
       * and also caches the to-be-disposed module.
       * @param {*} data A hot module data object from Webpack HMR.
       * @returns {void}
       */
      function hotDisposeCallback(data) {
        // We have to mutate the data object to get data registered and cached
        data.prevExports = currentExports;
      }
    );
    module.hot.accept(
      /**
       * An error handler to allow self-recovering behaviours.
       * @param {Error} error An error occurred during evaluation of a module.
       * @returns {void}
       */
      function hotErrorHandler(error) {
        if (
          typeof __react_refresh_error_overlay__ !== 'undefined' &&
          __react_refresh_error_overlay__
        ) {
          __react_refresh_error_overlay__.handleRuntimeError(error);
        }

        if (typeof __react_refresh_test__ !== 'undefined' && __react_refresh_test__) {
          if (window.onHotAcceptError) {
            window.onHotAcceptError(error.message);
          }
        }

        __webpack_require__.c[module.i].hot.accept(hotErrorHandler);
      }
    );

    if (isHotUpdate) {
      if (
        __react_refresh_utils__.isReactRefreshBoundary(prevExports) &&
        __react_refresh_utils__.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)
      ) {
        module.hot.invalidate();
      } else {
        __react_refresh_utils__.enqueueUpdate(
          /**
           * A function to dismiss the error overlay after performing React refresh.
           * @returns {void}
           */
          function updateCallback() {
            if (
              typeof __react_refresh_error_overlay__ !== 'undefined' &&
              __react_refresh_error_overlay__
            ) {
              __react_refresh_error_overlay__.clearRuntimeErrors();
            }
          }
        );
      }
    }
  } else {
    if (isHotUpdate && __react_refresh_utils__.isReactRefreshBoundary(prevExports)) {
      module.hot.invalidate();
    }
  }
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js"), __webpack_require__(/*! ./node_modules/react-dev-utils/refreshOverlayInterop.js */ "./node_modules/react-dev-utils/refreshOverlayInterop.js")))

/***/ }),

/***/ "./src/pages/EventsModelPage.tsx":
/*!***************************************!*\
  !*** ./src/pages/EventsModelPage.tsx ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(__react_refresh_utils__, __react_refresh_error_overlay__) {/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_bootstrap_Table__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-bootstrap/Table */ "./node_modules/react-bootstrap/esm/Table.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _components_Pagination__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/Pagination */ "./src/components/Pagination.tsx");
/* harmony import */ var _styles_pages_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../styles/pages.css */ "./src/styles/pages.css");
/* harmony import */ var _styles_pages_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_styles_pages_css__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _data_events_json__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../data/events.json */ "./src/data/events.json");
var _data_events_json__WEBPACK_IMPORTED_MODULE_5___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../data/events.json */ "./src/data/events.json", 1);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__);
__webpack_require__.$Refresh$.runtime = __webpack_require__(/*! ./node_modules/react-refresh/runtime.js */ "./node_modules/react-refresh/runtime.js");
__webpack_require__.$Refresh$.setup(module.i);

var _jsxFileName = "/tree-hugs/client/src/pages/EventsModelPage.tsx",
    _s = __webpack_require__.$Refresh$.signature();









function Event(index) {
  const event = _data_events_json__WEBPACK_IMPORTED_MODULE_5__.data[index];
  const link = `events${index + 1}`;
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])("tr", {
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])("td", {
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
        to: link,
        children: event.title
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 14,
        columnNumber: 9
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 13,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])("td", {
      children: event.parkfullname
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 16,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])("td", {
      children: event.category
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 17,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])("td", {
      children: event.datestart
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 18,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])("td", {
      children: event.dateend
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 19,
      columnNumber: 7
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 12,
    columnNumber: 5
  }, this);
}

_c = Event;

function Events() {
  _s();

  const indices = [];
  let i;
  const eventsData = _data_events_json__WEBPACK_IMPORTED_MODULE_5__.data;
  const dataLength = eventsData.length;
  const [currentPage, setCurrentPage] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(1);
  const [eventsPerPage] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(10);
  const indexOfLastEvent = currentPage * eventsPerPage;
  const indexOfFirstEvent = indexOfLastEvent - eventsPerPage;

  for (i = indexOfFirstEvent; i < indexOfLastEvent; ++i) {
    if (i < dataLength) indices.push(Event(i));
  }

  const paginate = pageNumber => setCurrentPage(pageNumber);

  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])("div", {
    className: "page",
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])("h1", {
      children: "Events"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 41,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])(react_bootstrap_Table__WEBPACK_IMPORTED_MODULE_1__["default"], {
      striped: true,
      bordered: true,
      hover: true,
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])("thead", {
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])("tr", {
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])("th", {
            children: "Title of Event"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 45,
            columnNumber: 13
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])("th", {
            children: "Park Name"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 46,
            columnNumber: 13
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])("th", {
            children: "Category"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 47,
            columnNumber: 13
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])("th", {
            children: "Date Begin"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 48,
            columnNumber: 13
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])("th", {
            children: "Date End"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 49,
            columnNumber: 13
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 44,
          columnNumber: 11
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 43,
        columnNumber: 9
      }, this), indices.map(index => /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])("tbody", {
        children: index
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 53,
        columnNumber: 11
      }, this))]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 42,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])(_components_Pagination__WEBPACK_IMPORTED_MODULE_3__["default"], {
      elementsPerPage: eventsPerPage,
      totalElements: _data_events_json__WEBPACK_IMPORTED_MODULE_5__.data.length,
      paginate: paginate
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 56,
      columnNumber: 7
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 40,
    columnNumber: 5
  }, this);
}

_s(Events, "aJAhbwI2gA3Qe2r5Yx3wnKJXkOs=");

_c2 = Events;
/* harmony default export */ __webpack_exports__["default"] = (Events);

var _c, _c2;

__webpack_require__.$Refresh$.register(_c, "Event");
__webpack_require__.$Refresh$.register(_c2, "Events");

const currentExports = __react_refresh_utils__.getModuleExports(module.i);
__react_refresh_utils__.registerExportsForReactRefresh(currentExports, module.i);

if (true) {
  const isHotUpdate = !!module.hot.data;
  const prevExports = isHotUpdate ? module.hot.data.prevExports : null;

  if (__react_refresh_utils__.isReactRefreshBoundary(currentExports)) {
    module.hot.dispose(
      /**
       * A callback to performs a full refresh if React has unrecoverable errors,
       * and also caches the to-be-disposed module.
       * @param {*} data A hot module data object from Webpack HMR.
       * @returns {void}
       */
      function hotDisposeCallback(data) {
        // We have to mutate the data object to get data registered and cached
        data.prevExports = currentExports;
      }
    );
    module.hot.accept(
      /**
       * An error handler to allow self-recovering behaviours.
       * @param {Error} error An error occurred during evaluation of a module.
       * @returns {void}
       */
      function hotErrorHandler(error) {
        if (
          typeof __react_refresh_error_overlay__ !== 'undefined' &&
          __react_refresh_error_overlay__
        ) {
          __react_refresh_error_overlay__.handleRuntimeError(error);
        }

        if (typeof __react_refresh_test__ !== 'undefined' && __react_refresh_test__) {
          if (window.onHotAcceptError) {
            window.onHotAcceptError(error.message);
          }
        }

        __webpack_require__.c[module.i].hot.accept(hotErrorHandler);
      }
    );

    if (isHotUpdate) {
      if (
        __react_refresh_utils__.isReactRefreshBoundary(prevExports) &&
        __react_refresh_utils__.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)
      ) {
        module.hot.invalidate();
      } else {
        __react_refresh_utils__.enqueueUpdate(
          /**
           * A function to dismiss the error overlay after performing React refresh.
           * @returns {void}
           */
          function updateCallback() {
            if (
              typeof __react_refresh_error_overlay__ !== 'undefined' &&
              __react_refresh_error_overlay__
            ) {
              __react_refresh_error_overlay__.clearRuntimeErrors();
            }
          }
        );
      }
    }
  } else {
    if (isHotUpdate && __react_refresh_utils__.isReactRefreshBoundary(prevExports)) {
      module.hot.invalidate();
    }
  }
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js"), __webpack_require__(/*! ./node_modules/react-dev-utils/refreshOverlayInterop.js */ "./node_modules/react-dev-utils/refreshOverlayInterop.js")))

/***/ }),

/***/ "./src/pages/LandingPage.tsx":
/*!***********************************!*\
  !*** ./src/pages/LandingPage.tsx ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(__react_refresh_utils__, __react_refresh_error_overlay__) {/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-bootstrap */ "./node_modules/react-bootstrap/esm/index.js");
/* harmony import */ var _styles_pages_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../styles/pages.css */ "./src/styles/pages.css");
/* harmony import */ var _styles_pages_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_styles_pages_css__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _styles_landing_module_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../styles/landing.module.css */ "./src/styles/landing.module.css");
/* harmony import */ var _styles_landing_module_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_styles_landing_module_css__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _components_ModelCard__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components/ModelCard */ "./src/components/ModelCard.tsx");
/* harmony import */ var _images_TreeHugLogo_svg__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../images/TreeHugLogo.svg */ "./src/images/TreeHugLogo.svg");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__);
__webpack_require__.$Refresh$.runtime = __webpack_require__(/*! ./node_modules/react-refresh/runtime.js */ "./node_modules/react-refresh/runtime.js");
__webpack_require__.$Refresh$.setup(module.i);

var _jsxFileName = "/tree-hugs/client/src/pages/LandingPage.tsx";








function LandingPage() {
  const models = [{
    link: "/parks",
    src: "https://www.nps.gov/common/uploads/structured_data/3C861078-1DD8-B71B-0B774A242EF6A706.jpg",
    title: "National Parks"
  }, {
    link: "/events",
    src: "https://www.nps.gov/common/uploads/event_calendar/5884976D-B7EC-5D10-ADFB75EE3BDEEF6D.jpg",
    title: "Events"
  }, {
    link: "/thingstodo",
    src: "https://www.nps.gov/common/uploads/cropped_image/928D157F-BB27-6614-8C0833EE8C35672E.jpg",
    title: "Things to do"
  }];
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])("div", {
    id: "home",
    className: _styles_landing_module_css__WEBPACK_IMPORTED_MODULE_3___default.a.landing,
    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])("div", {
      id: _styles_landing_module_css__WEBPACK_IMPORTED_MODULE_3___default.a.overlay,
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])("div", {
        className: _styles_landing_module_css__WEBPACK_IMPORTED_MODULE_3___default.a.row,
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])("img", {
          src: _images_TreeHugLogo_svg__WEBPACK_IMPORTED_MODULE_5__["default"],
          alt: "logo",
          width: "50",
          height: "50"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 31,
          columnNumber: 11
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])("h1", {
          children: "Welcome to Tree Hugs"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 32,
          columnNumber: 11
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])("img", {
          src: _images_TreeHugLogo_svg__WEBPACK_IMPORTED_MODULE_5__["default"],
          alt: "logo",
          width: "50",
          height: "50"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 33,
          columnNumber: 11
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 30,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])("p", {
        children: "Find a tree to hug"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 35,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])("div", {
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["CardDeck"], {
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])(_components_ModelCard__WEBPACK_IMPORTED_MODULE_4__["default"], {
            model: models[0]
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 38,
            columnNumber: 13
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])(_components_ModelCard__WEBPACK_IMPORTED_MODULE_4__["default"], {
            model: models[1]
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 39,
            columnNumber: 13
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])(_components_ModelCard__WEBPACK_IMPORTED_MODULE_4__["default"], {
            model: models[2]
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 40,
            columnNumber: 13
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 37,
          columnNumber: 11
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 36,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 43,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])("div", {
        className: _styles_landing_module_css__WEBPACK_IMPORTED_MODULE_3___default.a.row,
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Button"], {
          href: "/about",
          size: "lg",
          className: _styles_landing_module_css__WEBPACK_IMPORTED_MODULE_3___default.a.button,
          children: "About"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 45,
          columnNumber: 11
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 44,
        columnNumber: 9
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 29,
      columnNumber: 7
    }, this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 28,
    columnNumber: 5
  }, this);
}

_c = LandingPage;
/* harmony default export */ __webpack_exports__["default"] = (LandingPage);

var _c;

__webpack_require__.$Refresh$.register(_c, "LandingPage");

const currentExports = __react_refresh_utils__.getModuleExports(module.i);
__react_refresh_utils__.registerExportsForReactRefresh(currentExports, module.i);

if (true) {
  const isHotUpdate = !!module.hot.data;
  const prevExports = isHotUpdate ? module.hot.data.prevExports : null;

  if (__react_refresh_utils__.isReactRefreshBoundary(currentExports)) {
    module.hot.dispose(
      /**
       * A callback to performs a full refresh if React has unrecoverable errors,
       * and also caches the to-be-disposed module.
       * @param {*} data A hot module data object from Webpack HMR.
       * @returns {void}
       */
      function hotDisposeCallback(data) {
        // We have to mutate the data object to get data registered and cached
        data.prevExports = currentExports;
      }
    );
    module.hot.accept(
      /**
       * An error handler to allow self-recovering behaviours.
       * @param {Error} error An error occurred during evaluation of a module.
       * @returns {void}
       */
      function hotErrorHandler(error) {
        if (
          typeof __react_refresh_error_overlay__ !== 'undefined' &&
          __react_refresh_error_overlay__
        ) {
          __react_refresh_error_overlay__.handleRuntimeError(error);
        }

        if (typeof __react_refresh_test__ !== 'undefined' && __react_refresh_test__) {
          if (window.onHotAcceptError) {
            window.onHotAcceptError(error.message);
          }
        }

        __webpack_require__.c[module.i].hot.accept(hotErrorHandler);
      }
    );

    if (isHotUpdate) {
      if (
        __react_refresh_utils__.isReactRefreshBoundary(prevExports) &&
        __react_refresh_utils__.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)
      ) {
        module.hot.invalidate();
      } else {
        __react_refresh_utils__.enqueueUpdate(
          /**
           * A function to dismiss the error overlay after performing React refresh.
           * @returns {void}
           */
          function updateCallback() {
            if (
              typeof __react_refresh_error_overlay__ !== 'undefined' &&
              __react_refresh_error_overlay__
            ) {
              __react_refresh_error_overlay__.clearRuntimeErrors();
            }
          }
        );
      }
    }
  } else {
    if (isHotUpdate && __react_refresh_utils__.isReactRefreshBoundary(prevExports)) {
      module.hot.invalidate();
    }
  }
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js"), __webpack_require__(/*! ./node_modules/react-dev-utils/refreshOverlayInterop.js */ "./node_modules/react-dev-utils/refreshOverlayInterop.js")))

/***/ }),

/***/ "./src/pages/ParksModelPage.tsx":
/*!**************************************!*\
  !*** ./src/pages/ParksModelPage.tsx ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(__react_refresh_utils__, __react_refresh_error_overlay__) {/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-bootstrap */ "./node_modules/react-bootstrap/esm/index.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _styles_pages_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../styles/pages.css */ "./src/styles/pages.css");
/* harmony import */ var _styles_pages_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_styles_pages_css__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _styles_parks_module_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../styles/parks.module.css */ "./src/styles/parks.module.css");
/* harmony import */ var _styles_parks_module_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_styles_parks_module_css__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _data_parks_json__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../data/parks.json */ "./src/data/parks.json");
var _data_parks_json__WEBPACK_IMPORTED_MODULE_5___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../data/parks.json */ "./src/data/parks.json", 1);
/* harmony import */ var _components_Pagination__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../components/Pagination */ "./src/components/Pagination.tsx");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__);
__webpack_require__.$Refresh$.runtime = __webpack_require__(/*! ./node_modules/react-refresh/runtime.js */ "./node_modules/react-refresh/runtime.js");
__webpack_require__.$Refresh$.setup(module.i);

var _jsxFileName = "/tree-hugs/client/src/pages/ParksModelPage.tsx",
    _s = __webpack_require__.$Refresh$.signature();










function Park(index) {
  const park = _data_parks_json__WEBPACK_IMPORTED_MODULE_5__.data[index];
  const link = `parks${index + 1}`;
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Card"], {
    className: "ParkCard",
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Card"].Img, {
      variant: "top",
      className: _styles_parks_module_css__WEBPACK_IMPORTED_MODULE_4___default.a.parkCardImg,
      src: park.images[0].url
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 14,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Card"].Body, {
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Card"].Title, {
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
          to: link,
          children: park.fullName
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 17,
          columnNumber: 11
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 16,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Card"].Text, {
        children: [park.states, /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])("br", {}, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 21,
          columnNumber: 11
        }, this), park.addresses[0].city, /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])("br", {}, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 23,
          columnNumber: 11
        }, this), "Cost: $", park.entranceFees[0].cost, /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])("br", {}, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 25,
          columnNumber: 11
        }, this), "Regular Hours: ", park.operatingHours[0].standardHours.friday]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 19,
        columnNumber: 9
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 15,
      columnNumber: 7
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 13,
    columnNumber: 5
  }, this);
}

_c = Park;

function Parks() {
  _s();

  const indices = [];
  const cardDecks = [];
  let i;
  const parksData = _data_parks_json__WEBPACK_IMPORTED_MODULE_5__.data;
  const dataLength = parksData.length;
  const [currentPage, setCurrentPage] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(1);
  const [parksPerPage] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(12);
  const indexOfLastEvent = currentPage * parksPerPage;
  const indexOfFirstEvent = indexOfLastEvent - parksPerPage;
  const cardDecksPerPage = parksPerPage / 3;

  for (i = indexOfFirstEvent; i < indexOfLastEvent; ++i) {
    if (i < dataLength) indices.push(Park(i));
  }

  for (let j = 0; i < cardDecksPerPage; ++j) {
    cardDecks.push();
  }

  const paginate = pageNumber => setCurrentPage(pageNumber);

  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])("div", {
    className: "page",
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])("header", {
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])("h1", {
        children: "Parks"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 56,
        columnNumber: 9
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 55,
      columnNumber: 7
    }, this), indices.map(index => index), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxDEV"])(_components_Pagination__WEBPACK_IMPORTED_MODULE_6__["default"], {
      elementsPerPage: parksPerPage,
      totalElements: _data_parks_json__WEBPACK_IMPORTED_MODULE_5__.data.length,
      paginate: paginate
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 59,
      columnNumber: 7
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 54,
    columnNumber: 5
  }, this);
}

_s(Parks, "cfA4sr4cNyYNIW71NUhtB1RXHao=");

_c2 = Parks;
/* harmony default export */ __webpack_exports__["default"] = (Parks);

var _c, _c2;

__webpack_require__.$Refresh$.register(_c, "Park");
__webpack_require__.$Refresh$.register(_c2, "Parks");

const currentExports = __react_refresh_utils__.getModuleExports(module.i);
__react_refresh_utils__.registerExportsForReactRefresh(currentExports, module.i);

if (true) {
  const isHotUpdate = !!module.hot.data;
  const prevExports = isHotUpdate ? module.hot.data.prevExports : null;

  if (__react_refresh_utils__.isReactRefreshBoundary(currentExports)) {
    module.hot.dispose(
      /**
       * A callback to performs a full refresh if React has unrecoverable errors,
       * and also caches the to-be-disposed module.
       * @param {*} data A hot module data object from Webpack HMR.
       * @returns {void}
       */
      function hotDisposeCallback(data) {
        // We have to mutate the data object to get data registered and cached
        data.prevExports = currentExports;
      }
    );
    module.hot.accept(
      /**
       * An error handler to allow self-recovering behaviours.
       * @param {Error} error An error occurred during evaluation of a module.
       * @returns {void}
       */
      function hotErrorHandler(error) {
        if (
          typeof __react_refresh_error_overlay__ !== 'undefined' &&
          __react_refresh_error_overlay__
        ) {
          __react_refresh_error_overlay__.handleRuntimeError(error);
        }

        if (typeof __react_refresh_test__ !== 'undefined' && __react_refresh_test__) {
          if (window.onHotAcceptError) {
            window.onHotAcceptError(error.message);
          }
        }

        __webpack_require__.c[module.i].hot.accept(hotErrorHandler);
      }
    );

    if (isHotUpdate) {
      if (
        __react_refresh_utils__.isReactRefreshBoundary(prevExports) &&
        __react_refresh_utils__.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)
      ) {
        module.hot.invalidate();
      } else {
        __react_refresh_utils__.enqueueUpdate(
          /**
           * A function to dismiss the error overlay after performing React refresh.
           * @returns {void}
           */
          function updateCallback() {
            if (
              typeof __react_refresh_error_overlay__ !== 'undefined' &&
              __react_refresh_error_overlay__
            ) {
              __react_refresh_error_overlay__.clearRuntimeErrors();
            }
          }
        );
      }
    }
  } else {
    if (isHotUpdate && __react_refresh_utils__.isReactRefreshBoundary(prevExports)) {
      module.hot.invalidate();
    }
  }
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js"), __webpack_require__(/*! ./node_modules/react-dev-utils/refreshOverlayInterop.js */ "./node_modules/react-dev-utils/refreshOverlayInterop.js")))

/***/ }),

/***/ "./src/pages/ThingstodoInstancePage.tsx":
/*!**********************************************!*\
  !*** ./src/pages/ThingstodoInstancePage.tsx ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(__react_refresh_utils__, __react_refresh_error_overlay__) {/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-bootstrap */ "./node_modules/react-bootstrap/esm/index.js");
/* harmony import */ var react_player__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-player */ "./node_modules/react-player/lib/index.js");
/* harmony import */ var react_player__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_player__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _data_api__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../data/api */ "./src/data/api.tsx");
/* harmony import */ var _styles_pages_css__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../styles/pages.css */ "./src/styles/pages.css");
/* harmony import */ var _styles_pages_css__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_styles_pages_css__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _styles_thingstodo_module_css__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../styles/thingstodo.module.css */ "./src/styles/thingstodo.module.css");
/* harmony import */ var _styles_thingstodo_module_css__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_styles_thingstodo_module_css__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _images_yichen_jpg__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../images/yichen.jpg */ "./src/images/yichen.jpg");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_8__);
__webpack_require__.$Refresh$.runtime = __webpack_require__(/*! ./node_modules/react-refresh/runtime.js */ "./node_modules/react-refresh/runtime.js");
__webpack_require__.$Refresh$.setup(module.i);

var _jsxFileName = "/tree-hugs/client/src/pages/ThingstodoInstancePage.tsx",
    _s = __webpack_require__.$Refresh$.signature();



 // import { GoogleMap, useLoadScript } from "@react-google-maps/api";
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore






/* eslint-disable camelcase */



function ThingsToDoInstance() {
  _s();

  var _thingsToDo$media, _videoFeed$items, _videoFeed$items$, _videoFeed$items$$id, _thingsToDo$parks, _thingsToDo$park, _thingsToDo$descripti, _thingsToDo$accessibi, _thingsToDo$media2, _thingsToDo$media2$im, _thingsToDo$media3, _thingsToDo$media3$, _thingsToDo$tags;

  const [thingsToDo, setThingsToDo] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])({});
  const [loading, setLoading] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false);
  const [videoFeed, setVideoFeed] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])({});
  const [searchQuery, setSearchQuery] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])("");
  const thingsToDoId = parseInt(window.location.pathname.split("/").pop(), 10);
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
    const getThingsToDoData = async () => {
      var _apiThingsToDoData$pa;

      const apiThingsToDoData = await Object(_data_api__WEBPACK_IMPORTED_MODULE_4__["getThingsToDoById"])(thingsToDoId);
      setThingsToDo(apiThingsToDoData);
      setSearchQuery(`${apiThingsToDoData.title} ${(_apiThingsToDoData$pa = apiThingsToDoData.parks) === null || _apiThingsToDoData$pa === void 0 ? void 0 : _apiThingsToDoData$pa[0].name}`);
      console.log("search query is ");
      console.log(searchQuery);
    };

    setLoading(true);
    getThingsToDoData();
  }, []);
  const imgLength = (_thingsToDo$media = thingsToDo.media) === null || _thingsToDo$media === void 0 ? void 0 : _thingsToDo$media.length;
  const hasImg = imgLength > 0;
  const imgId = Math.floor(Math.random() * imgLength); // const searchQuery = `${thingsToDo.title} ${thingsToDo.parks?.[0].name}`;

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
    const getVideoData = async () => {
      const videoData = await Object(_data_api__WEBPACK_IMPORTED_MODULE_4__["getVideoFeedByQuery"])(searchQuery);
      setVideoFeed(videoData);
    };

    getVideoData();
  }, []);
  const videoId = videoFeed === null || videoFeed === void 0 ? void 0 : (_videoFeed$items = videoFeed.items) === null || _videoFeed$items === void 0 ? void 0 : (_videoFeed$items$ = _videoFeed$items[0]) === null || _videoFeed$items$ === void 0 ? void 0 : (_videoFeed$items$$id = _videoFeed$items$.id) === null || _videoFeed$items$$id === void 0 ? void 0 : _videoFeed$items$$id.videoId;
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_8__["jsxDEV"])("div", {
    className: "page",
    children: loading ? /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_8__["jsxDEV"])("div", {
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_8__["jsxDEV"])("h1", {
        children: thingsToDo.title
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 65,
        columnNumber: 11
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_8__["jsxDEV"])("p", {
        children: ["Park: ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_8__["jsxDEV"])(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
          to: `/parks/${(_thingsToDo$parks = thingsToDo.parks) === null || _thingsToDo$parks === void 0 ? void 0 : _thingsToDo$parks[0].id}`,
          children: (_thingsToDo$park = thingsToDo.park) === null || _thingsToDo$park === void 0 ? void 0 : _thingsToDo$park[0].name
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 67,
          columnNumber: 19
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_8__["jsxDEV"])("br", {}, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 68,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_8__["jsxDEV"])("br", {}, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 69,
          columnNumber: 13
        }, this), "Description: ", (_thingsToDo$descripti = thingsToDo.description) === null || _thingsToDo$descripti === void 0 ? void 0 : _thingsToDo$descripti.replace(/<\/?[^>]+>/gi, ""), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_8__["jsxDEV"])("br", {}, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 71,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_8__["jsxDEV"])("br", {}, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 72,
          columnNumber: 13
        }, this), "Duration: ", thingsToDo.duration, /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_8__["jsxDEV"])("br", {}, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 74,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_8__["jsxDEV"])("br", {}, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 75,
          columnNumber: 13
        }, this), "Fee Description: ", thingsToDo.fee_description, /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_8__["jsxDEV"])("br", {}, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 77,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_8__["jsxDEV"])("br", {}, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 78,
          columnNumber: 13
        }, this), "Accessibility Information: ", (_thingsToDo$accessibi = thingsToDo.accessibility_info) === null || _thingsToDo$accessibi === void 0 ? void 0 : _thingsToDo$accessibi.replace(/<\/?[^>]+>/gi, ""), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_8__["jsxDEV"])("br", {}, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 80,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_8__["jsxDEV"])("img", {
          src: hasImg ? (_thingsToDo$media2 = thingsToDo.media) === null || _thingsToDo$media2 === void 0 ? void 0 : (_thingsToDo$media2$im = _thingsToDo$media2[imgId]) === null || _thingsToDo$media2$im === void 0 ? void 0 : _thingsToDo$media2$im.url : _images_yichen_jpg__WEBPACK_IMPORTED_MODULE_7__["default"],
          alt: (_thingsToDo$media3 = thingsToDo.media) === null || _thingsToDo$media3 === void 0 ? void 0 : (_thingsToDo$media3$ = _thingsToDo$media3[0]) === null || _thingsToDo$media3$ === void 0 ? void 0 : _thingsToDo$media3$.altText,
          className: _styles_thingstodo_module_css__WEBPACK_IMPORTED_MODULE_6___default.a.image
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 81,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_8__["jsxDEV"])("br", {}, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 86,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_8__["jsxDEV"])("br", {}, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 87,
          columnNumber: 13
        }, this), "Tags:", " ", (_thingsToDo$tags = thingsToDo.tags) === null || _thingsToDo$tags === void 0 ? void 0 : _thingsToDo$tags.map(tag => /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_8__["jsxDEV"])("h4", {
          children: tag === null || tag === void 0 ? void 0 : tag.name
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 90,
          columnNumber: 15
        }, this)), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_8__["jsxDEV"])("hr", {}, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 92,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_8__["jsxDEV"])("br", {}, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 93,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_8__["jsxDEV"])(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
          to: "/thingstodo1",
          children: "Click here to see realted things to do"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 94,
          columnNumber: 13
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 66,
        columnNumber: 11
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_8__["jsxDEV"])(react_player__WEBPACK_IMPORTED_MODULE_3___default.a, {
        url: `https://www.youtube.com/watch?v=${videoId}`
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 96,
        columnNumber: 11
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 64,
      columnNumber: 9
    }, this) : /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_8__["jsxDEV"])(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Spinner"], {
      animation: "border"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 99,
      columnNumber: 9
    }, this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 62,
    columnNumber: 5
  }, this);
}

_s(ThingsToDoInstance, "cfYNk4VEc7W/CUdLPFT+iknX2as=");

_c = ThingsToDoInstance;
/* harmony default export */ __webpack_exports__["default"] = (ThingsToDoInstance);

var _c;

__webpack_require__.$Refresh$.register(_c, "ThingsToDoInstance");

const currentExports = __react_refresh_utils__.getModuleExports(module.i);
__react_refresh_utils__.registerExportsForReactRefresh(currentExports, module.i);

if (true) {
  const isHotUpdate = !!module.hot.data;
  const prevExports = isHotUpdate ? module.hot.data.prevExports : null;

  if (__react_refresh_utils__.isReactRefreshBoundary(currentExports)) {
    module.hot.dispose(
      /**
       * A callback to performs a full refresh if React has unrecoverable errors,
       * and also caches the to-be-disposed module.
       * @param {*} data A hot module data object from Webpack HMR.
       * @returns {void}
       */
      function hotDisposeCallback(data) {
        // We have to mutate the data object to get data registered and cached
        data.prevExports = currentExports;
      }
    );
    module.hot.accept(
      /**
       * An error handler to allow self-recovering behaviours.
       * @param {Error} error An error occurred during evaluation of a module.
       * @returns {void}
       */
      function hotErrorHandler(error) {
        if (
          typeof __react_refresh_error_overlay__ !== 'undefined' &&
          __react_refresh_error_overlay__
        ) {
          __react_refresh_error_overlay__.handleRuntimeError(error);
        }

        if (typeof __react_refresh_test__ !== 'undefined' && __react_refresh_test__) {
          if (window.onHotAcceptError) {
            window.onHotAcceptError(error.message);
          }
        }

        __webpack_require__.c[module.i].hot.accept(hotErrorHandler);
      }
    );

    if (isHotUpdate) {
      if (
        __react_refresh_utils__.isReactRefreshBoundary(prevExports) &&
        __react_refresh_utils__.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)
      ) {
        module.hot.invalidate();
      } else {
        __react_refresh_utils__.enqueueUpdate(
          /**
           * A function to dismiss the error overlay after performing React refresh.
           * @returns {void}
           */
          function updateCallback() {
            if (
              typeof __react_refresh_error_overlay__ !== 'undefined' &&
              __react_refresh_error_overlay__
            ) {
              __react_refresh_error_overlay__.clearRuntimeErrors();
            }
          }
        );
      }
    }
  } else {
    if (isHotUpdate && __react_refresh_utils__.isReactRefreshBoundary(prevExports)) {
      module.hot.invalidate();
    }
  }
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js"), __webpack_require__(/*! ./node_modules/react-dev-utils/refreshOverlayInterop.js */ "./node_modules/react-dev-utils/refreshOverlayInterop.js")))

/***/ }),

/***/ "./src/pages/ThingstodoModelPage.tsx":
/*!*******************************************!*\
  !*** ./src/pages/ThingstodoModelPage.tsx ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(__react_refresh_utils__, __react_refresh_error_overlay__) {/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_bootstrap_Table__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-bootstrap/Table */ "./node_modules/react-bootstrap/esm/Table.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _components_Pagination__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/Pagination */ "./src/components/Pagination.tsx");
/* harmony import */ var _styles_pages_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../styles/pages.css */ "./src/styles/pages.css");
/* harmony import */ var _styles_pages_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_styles_pages_css__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _data_api__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../data/api */ "./src/data/api.tsx");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__);
__webpack_require__.$Refresh$.runtime = __webpack_require__(/*! ./node_modules/react-refresh/runtime.js */ "./node_modules/react-refresh/runtime.js");
__webpack_require__.$Refresh$.setup(module.i);

var _jsxFileName = "/tree-hugs/client/src/pages/ThingstodoModelPage.tsx",
    _s = __webpack_require__.$Refresh$.signature();







/* eslint-disable camelcase */



function getSeasons(season) {
  let seasons = "";
  const binaryString = season.toString(2);

  for (let i = 0; i < binaryString.length; i++) {
    if (i === 0 && binaryString[i] === "1") seasons = `${seasons}Winter `;else if (i === 1 && binaryString[i] === "1") seasons = `${seasons}Spring `;else if (i === 2 && binaryString[i] === "1") seasons = `${seasons}Summer `;else if (i === 3 && binaryString[i] === "1") seasons = `${seasons}Fall`;
  }

  return seasons;
}

function getTimes(time) {
  let times = "";
  const binaryString = time.toString(2);

  for (let i = 0; i < binaryString.length; i++) {
    if (i === 0 && binaryString[i] === "1") times = `${times}Dawn `;else if (i === 1 && binaryString[i] === "1") times = `${times}Day `;else if (i === 2 && binaryString[i] === "1") times = `${times}Dusk `;else if (i === 3 && binaryString[i] === "1") times = `${times}Night`;
  }

  return times;
}

function Event(thingsToDo) {
  const link = `/thingstodo/${thingsToDo.id}`;
  getSeasons(1);
  const hasFee = thingsToDo.has_fees ? "Yes" : "No";
  const reservation = thingsToDo.require_reservation ? "Yes" : "No";
  const pets = thingsToDo.pets_allowed ? "Yes" : "No";
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])("tr", {
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])("td", {
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
        to: link,
        children: thingsToDo.title
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 53,
        columnNumber: 9
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 52,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])("td", {
      children: getSeasons(thingsToDo.seasons)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 55,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])("td", {
      children: getTimes(thingsToDo.time_of_day)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 56,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])("td", {
      children: reservation
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 57,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])("td", {
      children: hasFee
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 58,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])("td", {
      children: pets
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 59,
      columnNumber: 7
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 51,
    columnNumber: 5
  }, this);
}

_c = Event;

function ThingsToDo() {
  _s();

  const thingsToDosPerPage = 25;
  const [thingsToDosData, setThingsToDoData] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])({});
  const [dataLength, setDataLength] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(0);
  const [currentPage, setCurrentPage] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(1);
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
    const getData = async () => {
      const apiThingsToDoData = await Object(_data_api__WEBPACK_IMPORTED_MODULE_5__["getThingsToDo"])(currentPage);
      setThingsToDoData(apiThingsToDoData);
      setDataLength(apiThingsToDoData.num_results);
    };

    getData();
  }, [currentPage]);
  console.log(thingsToDosData);
  const indices = [];

  for (let i = 0; i < thingsToDosPerPage; ++i) {
    if (currentPage < 66 && i < dataLength || currentPage === 66 && i < 25) indices.push(Event(thingsToDosData.objects[i]));
  }

  const paginate = pageNumber => setCurrentPage(pageNumber);

  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])("div", {
    className: "page",
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])("h1", {
      children: "Things To Do"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 87,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])(react_bootstrap_Table__WEBPACK_IMPORTED_MODULE_1__["default"], {
      striped: true,
      bordered: true,
      hover: true,
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])("thead", {
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])("tr", {
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])("th", {
            children: "Title"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 91,
            columnNumber: 13
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])("th", {
            children: "Season"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 92,
            columnNumber: 13
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])("th", {
            children: "Time of Day"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 93,
            columnNumber: 13
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])("th", {
            children: "Reservation Required"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 94,
            columnNumber: 13
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])("th", {
            children: "Fees Required"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 95,
            columnNumber: 13
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])("th", {
            children: "Pets allowed"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 96,
            columnNumber: 13
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 90,
          columnNumber: 11
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 89,
        columnNumber: 9
      }, this), indices.map(index => /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])("tbody", {
        children: index
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 100,
        columnNumber: 11
      }, this))]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 88,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_6__["jsxDEV"])(_components_Pagination__WEBPACK_IMPORTED_MODULE_3__["default"], {
      elementsPerPage: thingsToDosPerPage,
      totalElements: dataLength,
      paginate: paginate
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 103,
      columnNumber: 7
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 86,
    columnNumber: 5
  }, this);
}

_s(ThingsToDo, "YdLrHUYFAHA7+Cx8XK2XVluX67Y=");

_c2 = ThingsToDo;
/* harmony default export */ __webpack_exports__["default"] = (ThingsToDo);

var _c, _c2;

__webpack_require__.$Refresh$.register(_c, "Event");
__webpack_require__.$Refresh$.register(_c2, "ThingsToDo");

const currentExports = __react_refresh_utils__.getModuleExports(module.i);
__react_refresh_utils__.registerExportsForReactRefresh(currentExports, module.i);

if (true) {
  const isHotUpdate = !!module.hot.data;
  const prevExports = isHotUpdate ? module.hot.data.prevExports : null;

  if (__react_refresh_utils__.isReactRefreshBoundary(currentExports)) {
    module.hot.dispose(
      /**
       * A callback to performs a full refresh if React has unrecoverable errors,
       * and also caches the to-be-disposed module.
       * @param {*} data A hot module data object from Webpack HMR.
       * @returns {void}
       */
      function hotDisposeCallback(data) {
        // We have to mutate the data object to get data registered and cached
        data.prevExports = currentExports;
      }
    );
    module.hot.accept(
      /**
       * An error handler to allow self-recovering behaviours.
       * @param {Error} error An error occurred during evaluation of a module.
       * @returns {void}
       */
      function hotErrorHandler(error) {
        if (
          typeof __react_refresh_error_overlay__ !== 'undefined' &&
          __react_refresh_error_overlay__
        ) {
          __react_refresh_error_overlay__.handleRuntimeError(error);
        }

        if (typeof __react_refresh_test__ !== 'undefined' && __react_refresh_test__) {
          if (window.onHotAcceptError) {
            window.onHotAcceptError(error.message);
          }
        }

        __webpack_require__.c[module.i].hot.accept(hotErrorHandler);
      }
    );

    if (isHotUpdate) {
      if (
        __react_refresh_utils__.isReactRefreshBoundary(prevExports) &&
        __react_refresh_utils__.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)
      ) {
        module.hot.invalidate();
      } else {
        __react_refresh_utils__.enqueueUpdate(
          /**
           * A function to dismiss the error overlay after performing React refresh.
           * @returns {void}
           */
          function updateCallback() {
            if (
              typeof __react_refresh_error_overlay__ !== 'undefined' &&
              __react_refresh_error_overlay__
            ) {
              __react_refresh_error_overlay__.clearRuntimeErrors();
            }
          }
        );
      }
    }
  } else {
    if (isHotUpdate && __react_refresh_utils__.isReactRefreshBoundary(prevExports)) {
      module.hot.invalidate();
    }
  }
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js"), __webpack_require__(/*! ./node_modules/react-dev-utils/refreshOverlayInterop.js */ "./node_modules/react-dev-utils/refreshOverlayInterop.js")))

/***/ }),

/***/ "./src/pages/events1.tsx":
/*!*******************************!*\
  !*** ./src/pages/events1.tsx ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(__react_refresh_utils__, __react_refresh_error_overlay__) {/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _styles_pages_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../styles/pages.css */ "./src/styles/pages.css");
/* harmony import */ var _styles_pages_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_styles_pages_css__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _styles_events_module_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../styles/events.module.css */ "./src/styles/events.module.css");
/* harmony import */ var _styles_events_module_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_styles_events_module_css__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _data_events_json__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../data/events.json */ "./src/data/events.json");
var _data_events_json__WEBPACK_IMPORTED_MODULE_4___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../data/events.json */ "./src/data/events.json", 1);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__);
__webpack_require__.$Refresh$.runtime = __webpack_require__(/*! ./node_modules/react-refresh/runtime.js */ "./node_modules/react-refresh/runtime.js");
__webpack_require__.$Refresh$.setup(module.i);

var _jsxFileName = "/tree-hugs/client/src/pages/events1.tsx";







function Events1() {
  const event = _data_events_json__WEBPACK_IMPORTED_MODULE_4__.data[0];
  const image = event.images[0];
  const imageurl = `https://www.nps.gov${image.url}`;
  const isfree = event.isfree ? "This event is free." : "This event is not free.";
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("div", {
    className: "page",
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("h1", {
      children: event.title
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 14,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("p", {
      children: ["Park: Abraham Lincoln Birthplace National Historical Park", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 18,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 19,
        columnNumber: 9
      }, this), "Category: ", event.category, /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 21,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 22,
        columnNumber: 9
      }, this), "Date Start: ", event.datestart, /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 24,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 25,
        columnNumber: 9
      }, this), "Date End: ", event.dateend, /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 27,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 28,
        columnNumber: 9
      }, this), "Free: ", isfree, /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 30,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 31,
        columnNumber: 9
      }, this), "Recurring event: ", event.isrecurring, /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 33,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 34,
        columnNumber: 9
      }, this), "All day: ", event.isallday, /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 36,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 37,
        columnNumber: 9
      }, this), "Site type: ", event.sitetype, /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 39,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 40,
        columnNumber: 9
      }, this), "Get more info here: ", event.infourl, /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 42,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 43,
        columnNumber: 9
      }, this), "Description:", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("p", {
        children: ["Abaraham Lincoln Birthplace National Historical Park will host the 207", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("sup", {
          children: "th"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 46,
          columnNumber: 81
        }, this), " Anniversary of the Battle of th Horseshoe on Saturday, March 20, 2021. This annual event recreates traditional Creek Indian life, frontier, life in the year 1814 and emphasizes the importance of the battle in United States history through a variety of special demonstrations and interpretive programs. Saturday's activities will provide park visitors with a better understanding Southeastern American Indian life in this area more than 200 years ago, as well as provide insight into the lives of the combatants and the reasons why the battle occurred."]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 45,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("p", {
        children: "Experience the life of the Creek and Cherokee Indians by visiting traditional hunting camps and watch demonstrations of cultural skills such as hide tanning, cooking and finger weaving. Children will have the opportunity to participate in an authentic Creek stickball game throughout the day."
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 53,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("p", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 58,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("p", {
        children: "Experience the life of Andrew Jackson's frontier army. Watch Tennessee militia fire smoothbore cannon and flintlock muskets. Learn how soldiers cooked their meals and lived while on campaign through a variety of encampments and displays in the museum. A camp depicting American women's lives on the frontier will provide wool dyeing demonstrations and teach visitors how to spin the wool."
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 59,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("p", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 65,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("p", {
        children: "All demonstrations will be presented multiple times throughout the day and promise to be entertaining as well as educational. A refreshment stand will be available courtesy of the New Site Volunteer Fire Department."
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 66,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("p", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 70,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("p", {
        children: "This Program is FREE to the public."
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 71,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 72,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("img", {
        src: imageurl,
        alt: image.altText,
        className: _styles_events_module_css__WEBPACK_IMPORTED_MODULE_3___default.a.image
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 73,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 74,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 75,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("hr", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 76,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 77,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
        to: "/parks1",
        children: "Click here to see related park"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 78,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 79,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
        to: "/thingstodo1",
        children: "Click here to see realted things to do"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 80,
        columnNumber: 9
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 15,
      columnNumber: 7
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 13,
    columnNumber: 5
  }, this);
}

_c = Events1;
/* harmony default export */ __webpack_exports__["default"] = (Events1);

var _c;

__webpack_require__.$Refresh$.register(_c, "Events1");

const currentExports = __react_refresh_utils__.getModuleExports(module.i);
__react_refresh_utils__.registerExportsForReactRefresh(currentExports, module.i);

if (true) {
  const isHotUpdate = !!module.hot.data;
  const prevExports = isHotUpdate ? module.hot.data.prevExports : null;

  if (__react_refresh_utils__.isReactRefreshBoundary(currentExports)) {
    module.hot.dispose(
      /**
       * A callback to performs a full refresh if React has unrecoverable errors,
       * and also caches the to-be-disposed module.
       * @param {*} data A hot module data object from Webpack HMR.
       * @returns {void}
       */
      function hotDisposeCallback(data) {
        // We have to mutate the data object to get data registered and cached
        data.prevExports = currentExports;
      }
    );
    module.hot.accept(
      /**
       * An error handler to allow self-recovering behaviours.
       * @param {Error} error An error occurred during evaluation of a module.
       * @returns {void}
       */
      function hotErrorHandler(error) {
        if (
          typeof __react_refresh_error_overlay__ !== 'undefined' &&
          __react_refresh_error_overlay__
        ) {
          __react_refresh_error_overlay__.handleRuntimeError(error);
        }

        if (typeof __react_refresh_test__ !== 'undefined' && __react_refresh_test__) {
          if (window.onHotAcceptError) {
            window.onHotAcceptError(error.message);
          }
        }

        __webpack_require__.c[module.i].hot.accept(hotErrorHandler);
      }
    );

    if (isHotUpdate) {
      if (
        __react_refresh_utils__.isReactRefreshBoundary(prevExports) &&
        __react_refresh_utils__.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)
      ) {
        module.hot.invalidate();
      } else {
        __react_refresh_utils__.enqueueUpdate(
          /**
           * A function to dismiss the error overlay after performing React refresh.
           * @returns {void}
           */
          function updateCallback() {
            if (
              typeof __react_refresh_error_overlay__ !== 'undefined' &&
              __react_refresh_error_overlay__
            ) {
              __react_refresh_error_overlay__.clearRuntimeErrors();
            }
          }
        );
      }
    }
  } else {
    if (isHotUpdate && __react_refresh_utils__.isReactRefreshBoundary(prevExports)) {
      module.hot.invalidate();
    }
  }
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js"), __webpack_require__(/*! ./node_modules/react-dev-utils/refreshOverlayInterop.js */ "./node_modules/react-dev-utils/refreshOverlayInterop.js")))

/***/ }),

/***/ "./src/pages/events2.tsx":
/*!*******************************!*\
  !*** ./src/pages/events2.tsx ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(__react_refresh_utils__, __react_refresh_error_overlay__) {/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _styles_pages_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../styles/pages.css */ "./src/styles/pages.css");
/* harmony import */ var _styles_pages_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_styles_pages_css__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _styles_events_module_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../styles/events.module.css */ "./src/styles/events.module.css");
/* harmony import */ var _styles_events_module_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_styles_events_module_css__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _data_events_json__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../data/events.json */ "./src/data/events.json");
var _data_events_json__WEBPACK_IMPORTED_MODULE_4___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../data/events.json */ "./src/data/events.json", 1);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__);
__webpack_require__.$Refresh$.runtime = __webpack_require__(/*! ./node_modules/react-refresh/runtime.js */ "./node_modules/react-refresh/runtime.js");
__webpack_require__.$Refresh$.setup(module.i);

var _jsxFileName = "/tree-hugs/client/src/pages/events2.tsx";







function Events2() {
  const event = _data_events_json__WEBPACK_IMPORTED_MODULE_4__.data[1];
  const imageurl = "https://www.nps.gov/common/uploads/grid_builder/safe/crop16_9/91948716-D973-6DCF-1E5CDC7DD9EF1DDB.jpg?width=465&quality=90&mode=crop";
  const isfree = event.isfree ? "This event is free." : "This event is not free.";
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("div", {
    className: "page",
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("h1", {
      children: event.title
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 14,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("p", {
      children: ["Park: Adams National Historical Park", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 17,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 18,
        columnNumber: 9
      }, this), "Category: ", event.category, /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 20,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 21,
        columnNumber: 9
      }, this), "Date Start: ", event.datestart, /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 23,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 24,
        columnNumber: 9
      }, this), "Date End: ", event.dateend, /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 26,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 27,
        columnNumber: 9
      }, this), "Free: ", isfree, /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 29,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 30,
        columnNumber: 9
      }, this), "Recurring event: ", event.isrecurring, /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 32,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 33,
        columnNumber: 9
      }, this), "All day: ", event.isallday, /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 35,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 36,
        columnNumber: 9
      }, this), "Site type: ", event.sitetype, /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 38,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 39,
        columnNumber: 9
      }, this), "Get more info here: ", event.infourl, /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 41,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 42,
        columnNumber: 9
      }, this), "Description:", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("p", {
        children: "As part of the commemoration activities for the 200th anniversary, the Quivira Chapter of the Santa Fe Trail Association encourages you to participate in Read Across America, March 1 - 5, 2021 and \"Read Across the Santa Fe Trail\" to help us commemorate this historic 200th anniversary! As part of the Santa Fe Trail 200th anniversary, the Quivira Chapter, funded in part by the McPherson County Community Foundation, Rice County Tourism and Great Bend CVB, has donated books on the Santa Fe Trail to public libraries in our three-county area. Visit the public libraries in McPherson, Rice and Barton counties in Kansas or your own town and read about the Santa Fe Trail!"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 44,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("p", {
        children: "This event is hosted by the Quivira Chapter of the Santa Fe Trail Association."
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 53,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 54,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("img", {
        src: imageurl,
        alt: "The Great Prairie Highway",
        className: _styles_events_module_css__WEBPACK_IMPORTED_MODULE_3___default.a.image
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 55,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 56,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 57,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("hr", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 58,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 59,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
        to: "/parks2",
        children: "Click here to see related park"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 60,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 61,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
        to: "/thingstodo2",
        children: "Click here to see related things to do"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 62,
        columnNumber: 9
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 15,
      columnNumber: 7
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 13,
    columnNumber: 5
  }, this);
}

_c = Events2;
/* harmony default export */ __webpack_exports__["default"] = (Events2);

var _c;

__webpack_require__.$Refresh$.register(_c, "Events2");

const currentExports = __react_refresh_utils__.getModuleExports(module.i);
__react_refresh_utils__.registerExportsForReactRefresh(currentExports, module.i);

if (true) {
  const isHotUpdate = !!module.hot.data;
  const prevExports = isHotUpdate ? module.hot.data.prevExports : null;

  if (__react_refresh_utils__.isReactRefreshBoundary(currentExports)) {
    module.hot.dispose(
      /**
       * A callback to performs a full refresh if React has unrecoverable errors,
       * and also caches the to-be-disposed module.
       * @param {*} data A hot module data object from Webpack HMR.
       * @returns {void}
       */
      function hotDisposeCallback(data) {
        // We have to mutate the data object to get data registered and cached
        data.prevExports = currentExports;
      }
    );
    module.hot.accept(
      /**
       * An error handler to allow self-recovering behaviours.
       * @param {Error} error An error occurred during evaluation of a module.
       * @returns {void}
       */
      function hotErrorHandler(error) {
        if (
          typeof __react_refresh_error_overlay__ !== 'undefined' &&
          __react_refresh_error_overlay__
        ) {
          __react_refresh_error_overlay__.handleRuntimeError(error);
        }

        if (typeof __react_refresh_test__ !== 'undefined' && __react_refresh_test__) {
          if (window.onHotAcceptError) {
            window.onHotAcceptError(error.message);
          }
        }

        __webpack_require__.c[module.i].hot.accept(hotErrorHandler);
      }
    );

    if (isHotUpdate) {
      if (
        __react_refresh_utils__.isReactRefreshBoundary(prevExports) &&
        __react_refresh_utils__.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)
      ) {
        module.hot.invalidate();
      } else {
        __react_refresh_utils__.enqueueUpdate(
          /**
           * A function to dismiss the error overlay after performing React refresh.
           * @returns {void}
           */
          function updateCallback() {
            if (
              typeof __react_refresh_error_overlay__ !== 'undefined' &&
              __react_refresh_error_overlay__
            ) {
              __react_refresh_error_overlay__.clearRuntimeErrors();
            }
          }
        );
      }
    }
  } else {
    if (isHotUpdate && __react_refresh_utils__.isReactRefreshBoundary(prevExports)) {
      module.hot.invalidate();
    }
  }
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js"), __webpack_require__(/*! ./node_modules/react-dev-utils/refreshOverlayInterop.js */ "./node_modules/react-dev-utils/refreshOverlayInterop.js")))

/***/ }),

/***/ "./src/pages/events3.tsx":
/*!*******************************!*\
  !*** ./src/pages/events3.tsx ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(__react_refresh_utils__, __react_refresh_error_overlay__) {/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _styles_pages_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../styles/pages.css */ "./src/styles/pages.css");
/* harmony import */ var _styles_pages_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_styles_pages_css__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _styles_events_module_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../styles/events.module.css */ "./src/styles/events.module.css");
/* harmony import */ var _styles_events_module_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_styles_events_module_css__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _data_events_json__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../data/events.json */ "./src/data/events.json");
var _data_events_json__WEBPACK_IMPORTED_MODULE_4___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../data/events.json */ "./src/data/events.json", 1);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__);
__webpack_require__.$Refresh$.runtime = __webpack_require__(/*! ./node_modules/react-refresh/runtime.js */ "./node_modules/react-refresh/runtime.js");
__webpack_require__.$Refresh$.setup(module.i);

var _jsxFileName = "/tree-hugs/client/src/pages/events3.tsx";







function Events3() {
  const event = _data_events_json__WEBPACK_IMPORTED_MODULE_4__.data[2];
  const image = event.images[0];
  const imageurl = `https://www.nps.gov${image.url}`;
  const isfree = event.isfree ? "This event is free." : "This event is not free.";
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("div", {
    className: "page",
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("h1", {
      children: event.title
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 14,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("p", {
      children: ["Park: Acadia National Park", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 17,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 18,
        columnNumber: 9
      }, this), "Category: ", event.category, /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 20,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 21,
        columnNumber: 9
      }, this), "Date Start: ", event.datestart, /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 23,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 24,
        columnNumber: 9
      }, this), "Date End: ", event.dateend, /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 26,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 27,
        columnNumber: 9
      }, this), "Free: ", isfree, /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 29,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 30,
        columnNumber: 9
      }, this), "Recurring event: ", event.isrecurring, /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 32,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 33,
        columnNumber: 9
      }, this), "All day: ", event.isallday, /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 35,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 36,
        columnNumber: 9
      }, this), "Site type: ", event.sitetype, /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 38,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 39,
        columnNumber: 9
      }, this), "Get more info here: ", event.infourl, /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 41,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 42,
        columnNumber: 9
      }, this), "Description:", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("p", {
        children: "\"From the Suffrage to the new Millennium.\""
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 44,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("p", {
        children: "In honor of the centennial of the passage of the 19th amendment in Congress and Women's History Month, Federal Hall National Memorial will be presenting a virtual exhibit entitled \"From the Suffrage to the new Millennium\"."
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 45,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("p", {
        children: "On June 4th, 1919, Congress passed the 19th amendment which guaranteed women the right to vote. In the 102 years since then, women from all walks of life have made great strides in various fields, and we set out to tell some of their stories."
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 50,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("p", {
        children: "The virtual exhibit will feature 44 women from 10 decades, as well as a brief timeline of the Women Suffrage Movement."
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 55,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("p", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 59,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("p", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 60,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 61,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("img", {
        src: imageurl,
        alt: image.altText,
        className: _styles_events_module_css__WEBPACK_IMPORTED_MODULE_3___default.a.image
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 62,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 63,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 64,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("hr", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 65,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 66,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
        to: "/parks3",
        children: "Click here to see related park"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 67,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 68,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
        to: "/thingstodo3",
        children: "Click here to see related things to do"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 69,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_5__["jsxDEV"])("br", {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 70,
        columnNumber: 9
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 15,
      columnNumber: 7
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 13,
    columnNumber: 5
  }, this);
}

_c = Events3;
/* harmony default export */ __webpack_exports__["default"] = (Events3);

var _c;

__webpack_require__.$Refresh$.register(_c, "Events3");

const currentExports = __react_refresh_utils__.getModuleExports(module.i);
__react_refresh_utils__.registerExportsForReactRefresh(currentExports, module.i);

if (true) {
  const isHotUpdate = !!module.hot.data;
  const prevExports = isHotUpdate ? module.hot.data.prevExports : null;

  if (__react_refresh_utils__.isReactRefreshBoundary(currentExports)) {
    module.hot.dispose(
      /**
       * A callback to performs a full refresh if React has unrecoverable errors,
       * and also caches the to-be-disposed module.
       * @param {*} data A hot module data object from Webpack HMR.
       * @returns {void}
       */
      function hotDisposeCallback(data) {
        // We have to mutate the data object to get data registered and cached
        data.prevExports = currentExports;
      }
    );
    module.hot.accept(
      /**
       * An error handler to allow self-recovering behaviours.
       * @param {Error} error An error occurred during evaluation of a module.
       * @returns {void}
       */
      function hotErrorHandler(error) {
        if (
          typeof __react_refresh_error_overlay__ !== 'undefined' &&
          __react_refresh_error_overlay__
        ) {
          __react_refresh_error_overlay__.handleRuntimeError(error);
        }

        if (typeof __react_refresh_test__ !== 'undefined' && __react_refresh_test__) {
          if (window.onHotAcceptError) {
            window.onHotAcceptError(error.message);
          }
        }

        __webpack_require__.c[module.i].hot.accept(hotErrorHandler);
      }
    );

    if (isHotUpdate) {
      if (
        __react_refresh_utils__.isReactRefreshBoundary(prevExports) &&
        __react_refresh_utils__.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)
      ) {
        module.hot.invalidate();
      } else {
        __react_refresh_utils__.enqueueUpdate(
          /**
           * A function to dismiss the error overlay after performing React refresh.
           * @returns {void}
           */
          function updateCallback() {
            if (
              typeof __react_refresh_error_overlay__ !== 'undefined' &&
              __react_refresh_error_overlay__
            ) {
              __react_refresh_error_overlay__.clearRuntimeErrors();
            }
          }
        );
      }
    }
  } else {
    if (isHotUpdate && __react_refresh_utils__.isReactRefreshBoundary(prevExports)) {
      module.hot.invalidate();
    }
  }
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js"), __webpack_require__(/*! ./node_modules/react-dev-utils/refreshOverlayInterop.js */ "./node_modules/react-dev-utils/refreshOverlayInterop.js")))

/***/ }),

/***/ "./src/pages/parks1.tsx":
/*!******************************!*\
  !*** ./src/pages/parks1.tsx ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(__react_refresh_utils__, __react_refresh_error_overlay__) {/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _styles_pages_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../styles/pages.css */ "./src/styles/pages.css");
/* harmony import */ var _styles_pages_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_styles_pages_css__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3__);
__webpack_require__.$Refresh$.runtime = __webpack_require__(/*! ./node_modules/react-refresh/runtime.js */ "./node_modules/react-refresh/runtime.js");
__webpack_require__.$Refresh$.setup(module.i);

var _jsxFileName = "/tree-hugs/client/src/pages/parks1.tsx";





function parks1() {
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3__["jsxDEV"])("div", {
    className: "page",
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3__["jsxDEV"])("h1", {
      children: "Abraham Lincoln Birthplace National Historical Park"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 8,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3__["jsxDEV"])("img", {
      className: "parkImg",
      src: "https://www.nps.gov/common/uploads/structured_data/3C861078-1DD8-B71B-0B774A242EF6A706.jpg",
      alt: "Park"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 9,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3__["jsxDEV"])("br", {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 14,
      columnNumber: 7
    }, this), "State: KY", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3__["jsxDEV"])("br", {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 16,
      columnNumber: 7
    }, this), "City: Hodgenville", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3__["jsxDEV"])("br", {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 18,
      columnNumber: 7
    }, this), "Fee: No cost", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3__["jsxDEV"])("br", {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 20,
      columnNumber: 7
    }, this), "Regular Hours: 9:00AM - 5:00PM", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3__["jsxDEV"])("p", {
      children: "For over a century people from around the world have come to rural Central Kentucky to honor the humble beginnings of our 16th president, Abraham Lincoln. His early life on Kentucky's frontier shaped his character and prepared him to lead the nation through Civil War. The country's first memorial to Lincoln, built with donations from young and old, enshrines the symbolic birthplace cabin."
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 22,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3__["jsxDEV"])(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
      to: "/events1",
      children: "Click here to see related events"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 28,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3__["jsxDEV"])("br", {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 29,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3__["jsxDEV"])(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
      to: "/thingstodo1",
      children: "Click here to see related things to do"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 30,
      columnNumber: 7
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 7,
    columnNumber: 5
  }, this);
}

/* harmony default export */ __webpack_exports__["default"] = (parks1);

const currentExports = __react_refresh_utils__.getModuleExports(module.i);
__react_refresh_utils__.registerExportsForReactRefresh(currentExports, module.i);

if (true) {
  const isHotUpdate = !!module.hot.data;
  const prevExports = isHotUpdate ? module.hot.data.prevExports : null;

  if (__react_refresh_utils__.isReactRefreshBoundary(currentExports)) {
    module.hot.dispose(
      /**
       * A callback to performs a full refresh if React has unrecoverable errors,
       * and also caches the to-be-disposed module.
       * @param {*} data A hot module data object from Webpack HMR.
       * @returns {void}
       */
      function hotDisposeCallback(data) {
        // We have to mutate the data object to get data registered and cached
        data.prevExports = currentExports;
      }
    );
    module.hot.accept(
      /**
       * An error handler to allow self-recovering behaviours.
       * @param {Error} error An error occurred during evaluation of a module.
       * @returns {void}
       */
      function hotErrorHandler(error) {
        if (
          typeof __react_refresh_error_overlay__ !== 'undefined' &&
          __react_refresh_error_overlay__
        ) {
          __react_refresh_error_overlay__.handleRuntimeError(error);
        }

        if (typeof __react_refresh_test__ !== 'undefined' && __react_refresh_test__) {
          if (window.onHotAcceptError) {
            window.onHotAcceptError(error.message);
          }
        }

        __webpack_require__.c[module.i].hot.accept(hotErrorHandler);
      }
    );

    if (isHotUpdate) {
      if (
        __react_refresh_utils__.isReactRefreshBoundary(prevExports) &&
        __react_refresh_utils__.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)
      ) {
        module.hot.invalidate();
      } else {
        __react_refresh_utils__.enqueueUpdate(
          /**
           * A function to dismiss the error overlay after performing React refresh.
           * @returns {void}
           */
          function updateCallback() {
            if (
              typeof __react_refresh_error_overlay__ !== 'undefined' &&
              __react_refresh_error_overlay__
            ) {
              __react_refresh_error_overlay__.clearRuntimeErrors();
            }
          }
        );
      }
    }
  } else {
    if (isHotUpdate && __react_refresh_utils__.isReactRefreshBoundary(prevExports)) {
      module.hot.invalidate();
    }
  }
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js"), __webpack_require__(/*! ./node_modules/react-dev-utils/refreshOverlayInterop.js */ "./node_modules/react-dev-utils/refreshOverlayInterop.js")))

/***/ }),

/***/ "./src/pages/parks2.tsx":
/*!******************************!*\
  !*** ./src/pages/parks2.tsx ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(__react_refresh_utils__, __react_refresh_error_overlay__) {/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _styles_pages_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../styles/pages.css */ "./src/styles/pages.css");
/* harmony import */ var _styles_pages_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_styles_pages_css__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3__);
__webpack_require__.$Refresh$.runtime = __webpack_require__(/*! ./node_modules/react-refresh/runtime.js */ "./node_modules/react-refresh/runtime.js");
__webpack_require__.$Refresh$.setup(module.i);

var _jsxFileName = "/tree-hugs/client/src/pages/parks2.tsx";





function parks2() {
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3__["jsxDEV"])("div", {
    className: "page",
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3__["jsxDEV"])("h1", {
      children: "Adams National Historical Park"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 8,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3__["jsxDEV"])("img", {
      className: "parkImg",
      src: "https://www.nps.gov/common/uploads/structured_data/3C7C7416-1DD8-B71B-0B1B30D0827F7C74.jpg",
      alt: "Park"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 9,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3__["jsxDEV"])("br", {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 14,
      columnNumber: 7
    }, this), "State: MA", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3__["jsxDEV"])("br", {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 16,
      columnNumber: 7
    }, this), "City: Quincy", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3__["jsxDEV"])("br", {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 18,
      columnNumber: 7
    }, this), "Fee: 15.00", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3__["jsxDEV"])("br", {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 20,
      columnNumber: 7
    }, this), "Regular Hours: 9:00AM - 5:00PM", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3__["jsxDEV"])("p", {
      children: "Acadia National Park protects the natural beauty of the highest rocky headlands along the Atlantic coastline of the United States, an abundance of habitats, and a rich cultural heritage. At 3.5 million visits a year, it's one of the top 10 most-visited national parks in the United States. Visitors enjoy 27 miles of historic motor roads, 158 miles of hiking trails, and 45 miles of carriage roads."
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 22,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3__["jsxDEV"])(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
      to: "/events2",
      children: "Click here to see related events"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 28,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3__["jsxDEV"])("br", {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 29,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3__["jsxDEV"])(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
      to: "/thingstodo2",
      children: "Click here to see related things to do"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 30,
      columnNumber: 7
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 7,
    columnNumber: 5
  }, this);
}

/* harmony default export */ __webpack_exports__["default"] = (parks2);

const currentExports = __react_refresh_utils__.getModuleExports(module.i);
__react_refresh_utils__.registerExportsForReactRefresh(currentExports, module.i);

if (true) {
  const isHotUpdate = !!module.hot.data;
  const prevExports = isHotUpdate ? module.hot.data.prevExports : null;

  if (__react_refresh_utils__.isReactRefreshBoundary(currentExports)) {
    module.hot.dispose(
      /**
       * A callback to performs a full refresh if React has unrecoverable errors,
       * and also caches the to-be-disposed module.
       * @param {*} data A hot module data object from Webpack HMR.
       * @returns {void}
       */
      function hotDisposeCallback(data) {
        // We have to mutate the data object to get data registered and cached
        data.prevExports = currentExports;
      }
    );
    module.hot.accept(
      /**
       * An error handler to allow self-recovering behaviours.
       * @param {Error} error An error occurred during evaluation of a module.
       * @returns {void}
       */
      function hotErrorHandler(error) {
        if (
          typeof __react_refresh_error_overlay__ !== 'undefined' &&
          __react_refresh_error_overlay__
        ) {
          __react_refresh_error_overlay__.handleRuntimeError(error);
        }

        if (typeof __react_refresh_test__ !== 'undefined' && __react_refresh_test__) {
          if (window.onHotAcceptError) {
            window.onHotAcceptError(error.message);
          }
        }

        __webpack_require__.c[module.i].hot.accept(hotErrorHandler);
      }
    );

    if (isHotUpdate) {
      if (
        __react_refresh_utils__.isReactRefreshBoundary(prevExports) &&
        __react_refresh_utils__.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)
      ) {
        module.hot.invalidate();
      } else {
        __react_refresh_utils__.enqueueUpdate(
          /**
           * A function to dismiss the error overlay after performing React refresh.
           * @returns {void}
           */
          function updateCallback() {
            if (
              typeof __react_refresh_error_overlay__ !== 'undefined' &&
              __react_refresh_error_overlay__
            ) {
              __react_refresh_error_overlay__.clearRuntimeErrors();
            }
          }
        );
      }
    }
  } else {
    if (isHotUpdate && __react_refresh_utils__.isReactRefreshBoundary(prevExports)) {
      module.hot.invalidate();
    }
  }
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js"), __webpack_require__(/*! ./node_modules/react-dev-utils/refreshOverlayInterop.js */ "./node_modules/react-dev-utils/refreshOverlayInterop.js")))

/***/ }),

/***/ "./src/pages/parks3.tsx":
/*!******************************!*\
  !*** ./src/pages/parks3.tsx ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(__react_refresh_utils__, __react_refresh_error_overlay__) {/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _styles_pages_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../styles/pages.css */ "./src/styles/pages.css");
/* harmony import */ var _styles_pages_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_styles_pages_css__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3__);
__webpack_require__.$Refresh$.runtime = __webpack_require__(/*! ./node_modules/react-refresh/runtime.js */ "./node_modules/react-refresh/runtime.js");
__webpack_require__.$Refresh$.setup(module.i);

var _jsxFileName = "/tree-hugs/client/src/pages/parks3.tsx";





function parks3() {
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3__["jsxDEV"])("div", {
    className: "page",
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3__["jsxDEV"])("h1", {
      children: "Acadia National Park"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 8,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3__["jsxDEV"])("img", {
      className: "parkImg",
      src: "https://www.nps.gov/common/uploads/structured_data/3C7B477B-1DD8-B71B-0BCB48E009241BAA.jpg",
      alt: "Park"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 9,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3__["jsxDEV"])("br", {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 14,
      columnNumber: 7
    }, this), "State: ME", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3__["jsxDEV"])("br", {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 16,
      columnNumber: 7
    }, this), "City: Bar Harbor", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3__["jsxDEV"])("br", {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 18,
      columnNumber: 7
    }, this), "Fee: 30.00", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3__["jsxDEV"])("br", {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 20,
      columnNumber: 7
    }, this), "Regular Hours: All Day", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3__["jsxDEV"])("p", {
      children: "Acadia National Park protects the natural beauty of the highest rocky headlands along the Atlantic coastline of the United States, an abundance of habitats, and a rich cultural heritage. At 3.5 million visits a year, it's one of the top 10 most-visited national parks in the United States. Visitors enjoy 27 miles of historic motor roads, 158 miles of hiking trails, and 45 miles of carriage roads."
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 22,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3__["jsxDEV"])(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
      to: "/events3",
      children: "Click here to see related events"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 28,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3__["jsxDEV"])("br", {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 29,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_3__["jsxDEV"])(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
      to: "/thingstodo3",
      children: "Click here to see related things to do"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 30,
      columnNumber: 7
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 7,
    columnNumber: 5
  }, this);
}

/* harmony default export */ __webpack_exports__["default"] = (parks3);

const currentExports = __react_refresh_utils__.getModuleExports(module.i);
__react_refresh_utils__.registerExportsForReactRefresh(currentExports, module.i);

if (true) {
  const isHotUpdate = !!module.hot.data;
  const prevExports = isHotUpdate ? module.hot.data.prevExports : null;

  if (__react_refresh_utils__.isReactRefreshBoundary(currentExports)) {
    module.hot.dispose(
      /**
       * A callback to performs a full refresh if React has unrecoverable errors,
       * and also caches the to-be-disposed module.
       * @param {*} data A hot module data object from Webpack HMR.
       * @returns {void}
       */
      function hotDisposeCallback(data) {
        // We have to mutate the data object to get data registered and cached
        data.prevExports = currentExports;
      }
    );
    module.hot.accept(
      /**
       * An error handler to allow self-recovering behaviours.
       * @param {Error} error An error occurred during evaluation of a module.
       * @returns {void}
       */
      function hotErrorHandler(error) {
        if (
          typeof __react_refresh_error_overlay__ !== 'undefined' &&
          __react_refresh_error_overlay__
        ) {
          __react_refresh_error_overlay__.handleRuntimeError(error);
        }

        if (typeof __react_refresh_test__ !== 'undefined' && __react_refresh_test__) {
          if (window.onHotAcceptError) {
            window.onHotAcceptError(error.message);
          }
        }

        __webpack_require__.c[module.i].hot.accept(hotErrorHandler);
      }
    );

    if (isHotUpdate) {
      if (
        __react_refresh_utils__.isReactRefreshBoundary(prevExports) &&
        __react_refresh_utils__.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)
      ) {
        module.hot.invalidate();
      } else {
        __react_refresh_utils__.enqueueUpdate(
          /**
           * A function to dismiss the error overlay after performing React refresh.
           * @returns {void}
           */
          function updateCallback() {
            if (
              typeof __react_refresh_error_overlay__ !== 'undefined' &&
              __react_refresh_error_overlay__
            ) {
              __react_refresh_error_overlay__.clearRuntimeErrors();
            }
          }
        );
      }
    }
  } else {
    if (isHotUpdate && __react_refresh_utils__.isReactRefreshBoundary(prevExports)) {
      module.hot.invalidate();
    }
  }
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js"), __webpack_require__(/*! ./node_modules/react-dev-utils/refreshOverlayInterop.js */ "./node_modules/react-dev-utils/refreshOverlayInterop.js")))

/***/ }),

/***/ "./src/pages/thingstodo2.tsx":
/*!***********************************!*\
  !*** ./src/pages/thingstodo2.tsx ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(__react_refresh_utils__, __react_refresh_error_overlay__) {/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _data_thingstodo_json__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../data/thingstodo.json */ "./src/data/thingstodo.json");
var _data_thingstodo_json__WEBPACK_IMPORTED_MODULE_2___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../data/thingstodo.json */ "./src/data/thingstodo.json", 1);
/* harmony import */ var _styles_thingstodo_module_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../styles/thingstodo.module.css */ "./src/styles/thingstodo.module.css");
/* harmony import */ var _styles_thingstodo_module_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_styles_thingstodo_module_css__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__);
__webpack_require__.$Refresh$.runtime = __webpack_require__(/*! ./node_modules/react-refresh/runtime.js */ "./node_modules/react-refresh/runtime.js");
__webpack_require__.$Refresh$.setup(module.i);

var _jsxFileName = "/tree-hugs/client/src/pages/thingstodo2.tsx";






function thingstodo2() {
  const instance = _data_thingstodo_json__WEBPACK_IMPORTED_MODULE_2__.data[1];

  function listList(a) {
    let seasons = "";

    for (let i = 0; i < a.length - 1; i++) {
      seasons = `${seasons + a[i]}, `;
    }

    seasons += a[a.length - 1];
    return seasons;
  }

  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__["jsxDEV"])("div", {
    className: "page",
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__["jsxDEV"])("h1", {
      children: instance.title
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 18,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__["jsxDEV"])("img", {
      className: _styles_thingstodo_module_css__WEBPACK_IMPORTED_MODULE_3___default.a.image,
      src: instance.images[0].crops[1].url,
      alt: "Things to do"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 19,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__["jsxDEV"])("p", {
      children: ["Seasons: ", listList(instance.season)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 20,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__["jsxDEV"])("p", {
      children: ["Time of Day: ", listList(instance.timeOfDay)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 21,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__["jsxDEV"])("p", {
      children: ["Reservation Required: ", instance.isReservationRequired]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 22,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__["jsxDEV"])("p", {
      children: ["Fees Required: ", instance.doFeesApply]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 23,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__["jsxDEV"])("p", {
      children: ["Pets Allowed: ", instance.arePetsPermitted]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 24,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__["jsxDEV"])("div", {
      dangerouslySetInnerHTML: {
        __html: instance.shortDescription
      }
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 25,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__["jsxDEV"])("br", {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 26,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__["jsxDEV"])(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
      to: "/parks2",
      children: "Click here to see related park"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 27,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__["jsxDEV"])("br", {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 28,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__["jsxDEV"])(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
      to: "/events2",
      children: "Click here to see related events"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 29,
      columnNumber: 7
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 17,
    columnNumber: 5
  }, this);
}

/* harmony default export */ __webpack_exports__["default"] = (thingstodo2);

const currentExports = __react_refresh_utils__.getModuleExports(module.i);
__react_refresh_utils__.registerExportsForReactRefresh(currentExports, module.i);

if (true) {
  const isHotUpdate = !!module.hot.data;
  const prevExports = isHotUpdate ? module.hot.data.prevExports : null;

  if (__react_refresh_utils__.isReactRefreshBoundary(currentExports)) {
    module.hot.dispose(
      /**
       * A callback to performs a full refresh if React has unrecoverable errors,
       * and also caches the to-be-disposed module.
       * @param {*} data A hot module data object from Webpack HMR.
       * @returns {void}
       */
      function hotDisposeCallback(data) {
        // We have to mutate the data object to get data registered and cached
        data.prevExports = currentExports;
      }
    );
    module.hot.accept(
      /**
       * An error handler to allow self-recovering behaviours.
       * @param {Error} error An error occurred during evaluation of a module.
       * @returns {void}
       */
      function hotErrorHandler(error) {
        if (
          typeof __react_refresh_error_overlay__ !== 'undefined' &&
          __react_refresh_error_overlay__
        ) {
          __react_refresh_error_overlay__.handleRuntimeError(error);
        }

        if (typeof __react_refresh_test__ !== 'undefined' && __react_refresh_test__) {
          if (window.onHotAcceptError) {
            window.onHotAcceptError(error.message);
          }
        }

        __webpack_require__.c[module.i].hot.accept(hotErrorHandler);
      }
    );

    if (isHotUpdate) {
      if (
        __react_refresh_utils__.isReactRefreshBoundary(prevExports) &&
        __react_refresh_utils__.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)
      ) {
        module.hot.invalidate();
      } else {
        __react_refresh_utils__.enqueueUpdate(
          /**
           * A function to dismiss the error overlay after performing React refresh.
           * @returns {void}
           */
          function updateCallback() {
            if (
              typeof __react_refresh_error_overlay__ !== 'undefined' &&
              __react_refresh_error_overlay__
            ) {
              __react_refresh_error_overlay__.clearRuntimeErrors();
            }
          }
        );
      }
    }
  } else {
    if (isHotUpdate && __react_refresh_utils__.isReactRefreshBoundary(prevExports)) {
      module.hot.invalidate();
    }
  }
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js"), __webpack_require__(/*! ./node_modules/react-dev-utils/refreshOverlayInterop.js */ "./node_modules/react-dev-utils/refreshOverlayInterop.js")))

/***/ }),

/***/ "./src/pages/thingstodo3.tsx":
/*!***********************************!*\
  !*** ./src/pages/thingstodo3.tsx ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(__react_refresh_utils__, __react_refresh_error_overlay__) {/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _data_thingstodo_json__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../data/thingstodo.json */ "./src/data/thingstodo.json");
var _data_thingstodo_json__WEBPACK_IMPORTED_MODULE_2___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../data/thingstodo.json */ "./src/data/thingstodo.json", 1);
/* harmony import */ var _styles_thingstodo_module_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../styles/thingstodo.module.css */ "./src/styles/thingstodo.module.css");
/* harmony import */ var _styles_thingstodo_module_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_styles_thingstodo_module_css__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__);
__webpack_require__.$Refresh$.runtime = __webpack_require__(/*! ./node_modules/react-refresh/runtime.js */ "./node_modules/react-refresh/runtime.js");
__webpack_require__.$Refresh$.setup(module.i);

var _jsxFileName = "/tree-hugs/client/src/pages/thingstodo3.tsx";






function thingstodo3() {
  const instance = _data_thingstodo_json__WEBPACK_IMPORTED_MODULE_2__.data[2];

  function listList(a) {
    let seasons = "";

    for (let i = 0; i < a.length - 1; i++) {
      seasons = `${seasons + a[i]}, `;
    }

    seasons += a[a.length - 1];
    return seasons;
  }

  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__["jsxDEV"])("div", {
    className: "page",
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__["jsxDEV"])("h1", {
      children: instance.title
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 18,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__["jsxDEV"])("img", {
      className: _styles_thingstodo_module_css__WEBPACK_IMPORTED_MODULE_3___default.a.image,
      src: instance.images[0].url,
      alt: "Things to do"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 19,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__["jsxDEV"])("p", {
      children: ["Seasons: ", listList(instance.season)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 20,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__["jsxDEV"])("p", {
      children: ["Time of Day: ", listList(instance.timeOfDay)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 21,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__["jsxDEV"])("p", {
      children: ["Reservation Required: ", instance.isReservationRequired]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 22,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__["jsxDEV"])("p", {
      children: ["Fees Required: ", instance.doFeesApply]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 23,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__["jsxDEV"])("p", {
      children: ["Pets Allowed: ", instance.arePetsPermitted]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 24,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__["jsxDEV"])("div", {
      dangerouslySetInnerHTML: {
        __html: instance.shortDescription
      }
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 25,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__["jsxDEV"])("br", {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 26,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__["jsxDEV"])(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
      to: "/parks3",
      children: "Click here to see related park"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 27,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__["jsxDEV"])("br", {}, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 28,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__["jsxDEV"])(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
      to: "/events3",
      children: "Click here to see related events"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 29,
      columnNumber: 7
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 17,
    columnNumber: 5
  }, this);
}

/* harmony default export */ __webpack_exports__["default"] = (thingstodo3);

const currentExports = __react_refresh_utils__.getModuleExports(module.i);
__react_refresh_utils__.registerExportsForReactRefresh(currentExports, module.i);

if (true) {
  const isHotUpdate = !!module.hot.data;
  const prevExports = isHotUpdate ? module.hot.data.prevExports : null;

  if (__react_refresh_utils__.isReactRefreshBoundary(currentExports)) {
    module.hot.dispose(
      /**
       * A callback to performs a full refresh if React has unrecoverable errors,
       * and also caches the to-be-disposed module.
       * @param {*} data A hot module data object from Webpack HMR.
       * @returns {void}
       */
      function hotDisposeCallback(data) {
        // We have to mutate the data object to get data registered and cached
        data.prevExports = currentExports;
      }
    );
    module.hot.accept(
      /**
       * An error handler to allow self-recovering behaviours.
       * @param {Error} error An error occurred during evaluation of a module.
       * @returns {void}
       */
      function hotErrorHandler(error) {
        if (
          typeof __react_refresh_error_overlay__ !== 'undefined' &&
          __react_refresh_error_overlay__
        ) {
          __react_refresh_error_overlay__.handleRuntimeError(error);
        }

        if (typeof __react_refresh_test__ !== 'undefined' && __react_refresh_test__) {
          if (window.onHotAcceptError) {
            window.onHotAcceptError(error.message);
          }
        }

        __webpack_require__.c[module.i].hot.accept(hotErrorHandler);
      }
    );

    if (isHotUpdate) {
      if (
        __react_refresh_utils__.isReactRefreshBoundary(prevExports) &&
        __react_refresh_utils__.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)
      ) {
        module.hot.invalidate();
      } else {
        __react_refresh_utils__.enqueueUpdate(
          /**
           * A function to dismiss the error overlay after performing React refresh.
           * @returns {void}
           */
          function updateCallback() {
            if (
              typeof __react_refresh_error_overlay__ !== 'undefined' &&
              __react_refresh_error_overlay__
            ) {
              __react_refresh_error_overlay__.clearRuntimeErrors();
            }
          }
        );
      }
    }
  } else {
    if (isHotUpdate && __react_refresh_utils__.isReactRefreshBoundary(prevExports)) {
      module.hot.invalidate();
    }
  }
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js"), __webpack_require__(/*! ./node_modules/react-dev-utils/refreshOverlayInterop.js */ "./node_modules/react-dev-utils/refreshOverlayInterop.js")))

/***/ }),

/***/ "./src/styles/about.module.css":
/*!*************************************!*\
  !*** ./src/styles/about.module.css ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var api = __webpack_require__(/*! ../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
            var content = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js??ref--5-oneOf-5-1!../../node_modules/postcss-loader/src??postcss!./about.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/styles/about.module.css");

            content = content.__esModule ? content.default : content;

            if (typeof content === 'string') {
              content = [[module.i, content, '']];
            }

var options = {};

options.insert = "head";
options.singleton = false;

var update = api(content, options);


if (true) {
  if (!content.locals || module.hot.invalidate) {
    var isEqualLocals = function isEqualLocals(a, b, isNamedExport) {
  if (!a && b || a && !b) {
    return false;
  }

  var p;

  for (p in a) {
    if (isNamedExport && p === 'default') {
      // eslint-disable-next-line no-continue
      continue;
    }

    if (a[p] !== b[p]) {
      return false;
    }
  }

  for (p in b) {
    if (isNamedExport && p === 'default') {
      // eslint-disable-next-line no-continue
      continue;
    }

    if (!a[p]) {
      return false;
    }
  }

  return true;
};
    var oldLocals = content.locals;

    module.hot.accept(
      /*! !../../node_modules/css-loader/dist/cjs.js??ref--5-oneOf-5-1!../../node_modules/postcss-loader/src??postcss!./about.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/styles/about.module.css",
      function () {
        content = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js??ref--5-oneOf-5-1!../../node_modules/postcss-loader/src??postcss!./about.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/styles/about.module.css");

              content = content.__esModule ? content.default : content;

              if (typeof content === 'string') {
                content = [[module.i, content, '']];
              }

              if (!isEqualLocals(oldLocals, content.locals)) {
                module.hot.invalidate();

                return;
              }

              oldLocals = content.locals;

              update(content);
      }
    )
  }

  module.hot.dispose(function() {
    update();
  });
}

module.exports = content.locals || {};

/***/ }),

/***/ "./src/styles/events.module.css":
/*!**************************************!*\
  !*** ./src/styles/events.module.css ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var api = __webpack_require__(/*! ../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
            var content = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js??ref--5-oneOf-5-1!../../node_modules/postcss-loader/src??postcss!./events.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/styles/events.module.css");

            content = content.__esModule ? content.default : content;

            if (typeof content === 'string') {
              content = [[module.i, content, '']];
            }

var options = {};

options.insert = "head";
options.singleton = false;

var update = api(content, options);


if (true) {
  if (!content.locals || module.hot.invalidate) {
    var isEqualLocals = function isEqualLocals(a, b, isNamedExport) {
  if (!a && b || a && !b) {
    return false;
  }

  var p;

  for (p in a) {
    if (isNamedExport && p === 'default') {
      // eslint-disable-next-line no-continue
      continue;
    }

    if (a[p] !== b[p]) {
      return false;
    }
  }

  for (p in b) {
    if (isNamedExport && p === 'default') {
      // eslint-disable-next-line no-continue
      continue;
    }

    if (!a[p]) {
      return false;
    }
  }

  return true;
};
    var oldLocals = content.locals;

    module.hot.accept(
      /*! !../../node_modules/css-loader/dist/cjs.js??ref--5-oneOf-5-1!../../node_modules/postcss-loader/src??postcss!./events.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/styles/events.module.css",
      function () {
        content = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js??ref--5-oneOf-5-1!../../node_modules/postcss-loader/src??postcss!./events.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/styles/events.module.css");

              content = content.__esModule ? content.default : content;

              if (typeof content === 'string') {
                content = [[module.i, content, '']];
              }

              if (!isEqualLocals(oldLocals, content.locals)) {
                module.hot.invalidate();

                return;
              }

              oldLocals = content.locals;

              update(content);
      }
    )
  }

  module.hot.dispose(function() {
    update();
  });
}

module.exports = content.locals || {};

/***/ }),

/***/ "./src/styles/landing.module.css":
/*!***************************************!*\
  !*** ./src/styles/landing.module.css ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var api = __webpack_require__(/*! ../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
            var content = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js??ref--5-oneOf-5-1!../../node_modules/postcss-loader/src??postcss!./landing.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/styles/landing.module.css");

            content = content.__esModule ? content.default : content;

            if (typeof content === 'string') {
              content = [[module.i, content, '']];
            }

var options = {};

options.insert = "head";
options.singleton = false;

var update = api(content, options);


if (true) {
  if (!content.locals || module.hot.invalidate) {
    var isEqualLocals = function isEqualLocals(a, b, isNamedExport) {
  if (!a && b || a && !b) {
    return false;
  }

  var p;

  for (p in a) {
    if (isNamedExport && p === 'default') {
      // eslint-disable-next-line no-continue
      continue;
    }

    if (a[p] !== b[p]) {
      return false;
    }
  }

  for (p in b) {
    if (isNamedExport && p === 'default') {
      // eslint-disable-next-line no-continue
      continue;
    }

    if (!a[p]) {
      return false;
    }
  }

  return true;
};
    var oldLocals = content.locals;

    module.hot.accept(
      /*! !../../node_modules/css-loader/dist/cjs.js??ref--5-oneOf-5-1!../../node_modules/postcss-loader/src??postcss!./landing.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/styles/landing.module.css",
      function () {
        content = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js??ref--5-oneOf-5-1!../../node_modules/postcss-loader/src??postcss!./landing.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/styles/landing.module.css");

              content = content.__esModule ? content.default : content;

              if (typeof content === 'string') {
                content = [[module.i, content, '']];
              }

              if (!isEqualLocals(oldLocals, content.locals)) {
                module.hot.invalidate();

                return;
              }

              oldLocals = content.locals;

              update(content);
      }
    )
  }

  module.hot.dispose(function() {
    update();
  });
}

module.exports = content.locals || {};

/***/ }),

/***/ "./src/styles/pages.css":
/*!******************************!*\
  !*** ./src/styles/pages.css ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var api = __webpack_require__(/*! ../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
            var content = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js??ref--5-oneOf-4-1!../../node_modules/postcss-loader/src??postcss!./pages.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/styles/pages.css");

            content = content.__esModule ? content.default : content;

            if (typeof content === 'string') {
              content = [[module.i, content, '']];
            }

var options = {};

options.insert = "head";
options.singleton = false;

var update = api(content, options);


if (true) {
  if (!content.locals || module.hot.invalidate) {
    var isEqualLocals = function isEqualLocals(a, b, isNamedExport) {
  if (!a && b || a && !b) {
    return false;
  }

  var p;

  for (p in a) {
    if (isNamedExport && p === 'default') {
      // eslint-disable-next-line no-continue
      continue;
    }

    if (a[p] !== b[p]) {
      return false;
    }
  }

  for (p in b) {
    if (isNamedExport && p === 'default') {
      // eslint-disable-next-line no-continue
      continue;
    }

    if (!a[p]) {
      return false;
    }
  }

  return true;
};
    var oldLocals = content.locals;

    module.hot.accept(
      /*! !../../node_modules/css-loader/dist/cjs.js??ref--5-oneOf-4-1!../../node_modules/postcss-loader/src??postcss!./pages.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/styles/pages.css",
      function () {
        content = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js??ref--5-oneOf-4-1!../../node_modules/postcss-loader/src??postcss!./pages.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/styles/pages.css");

              content = content.__esModule ? content.default : content;

              if (typeof content === 'string') {
                content = [[module.i, content, '']];
              }

              if (!isEqualLocals(oldLocals, content.locals)) {
                module.hot.invalidate();

                return;
              }

              oldLocals = content.locals;

              update(content);
      }
    )
  }

  module.hot.dispose(function() {
    update();
  });
}

module.exports = content.locals || {};

/***/ }),

/***/ "./src/styles/parks.module.css":
/*!*************************************!*\
  !*** ./src/styles/parks.module.css ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var api = __webpack_require__(/*! ../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
            var content = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js??ref--5-oneOf-5-1!../../node_modules/postcss-loader/src??postcss!./parks.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/styles/parks.module.css");

            content = content.__esModule ? content.default : content;

            if (typeof content === 'string') {
              content = [[module.i, content, '']];
            }

var options = {};

options.insert = "head";
options.singleton = false;

var update = api(content, options);


if (true) {
  if (!content.locals || module.hot.invalidate) {
    var isEqualLocals = function isEqualLocals(a, b, isNamedExport) {
  if (!a && b || a && !b) {
    return false;
  }

  var p;

  for (p in a) {
    if (isNamedExport && p === 'default') {
      // eslint-disable-next-line no-continue
      continue;
    }

    if (a[p] !== b[p]) {
      return false;
    }
  }

  for (p in b) {
    if (isNamedExport && p === 'default') {
      // eslint-disable-next-line no-continue
      continue;
    }

    if (!a[p]) {
      return false;
    }
  }

  return true;
};
    var oldLocals = content.locals;

    module.hot.accept(
      /*! !../../node_modules/css-loader/dist/cjs.js??ref--5-oneOf-5-1!../../node_modules/postcss-loader/src??postcss!./parks.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/styles/parks.module.css",
      function () {
        content = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js??ref--5-oneOf-5-1!../../node_modules/postcss-loader/src??postcss!./parks.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/styles/parks.module.css");

              content = content.__esModule ? content.default : content;

              if (typeof content === 'string') {
                content = [[module.i, content, '']];
              }

              if (!isEqualLocals(oldLocals, content.locals)) {
                module.hot.invalidate();

                return;
              }

              oldLocals = content.locals;

              update(content);
      }
    )
  }

  module.hot.dispose(function() {
    update();
  });
}

module.exports = content.locals || {};

/***/ }),

/***/ "./src/styles/thingstodo.module.css":
/*!******************************************!*\
  !*** ./src/styles/thingstodo.module.css ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var api = __webpack_require__(/*! ../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
            var content = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js??ref--5-oneOf-5-1!../../node_modules/postcss-loader/src??postcss!./thingstodo.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/styles/thingstodo.module.css");

            content = content.__esModule ? content.default : content;

            if (typeof content === 'string') {
              content = [[module.i, content, '']];
            }

var options = {};

options.insert = "head";
options.singleton = false;

var update = api(content, options);


if (true) {
  if (!content.locals || module.hot.invalidate) {
    var isEqualLocals = function isEqualLocals(a, b, isNamedExport) {
  if (!a && b || a && !b) {
    return false;
  }

  var p;

  for (p in a) {
    if (isNamedExport && p === 'default') {
      // eslint-disable-next-line no-continue
      continue;
    }

    if (a[p] !== b[p]) {
      return false;
    }
  }

  for (p in b) {
    if (isNamedExport && p === 'default') {
      // eslint-disable-next-line no-continue
      continue;
    }

    if (!a[p]) {
      return false;
    }
  }

  return true;
};
    var oldLocals = content.locals;

    module.hot.accept(
      /*! !../../node_modules/css-loader/dist/cjs.js??ref--5-oneOf-5-1!../../node_modules/postcss-loader/src??postcss!./thingstodo.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/styles/thingstodo.module.css",
      function () {
        content = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js??ref--5-oneOf-5-1!../../node_modules/postcss-loader/src??postcss!./thingstodo.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/styles/thingstodo.module.css");

              content = content.__esModule ? content.default : content;

              if (typeof content === 'string') {
                content = [[module.i, content, '']];
              }

              if (!isEqualLocals(oldLocals, content.locals)) {
                module.hot.invalidate();

                return;
              }

              oldLocals = content.locals;

              update(content);
      }
    )
  }

  module.hot.dispose(function() {
    update();
  });
}

module.exports = content.locals || {};

/***/ }),

/***/ 1:
/*!***********************************************************************************************************************************************************************************************!*\
  !*** multi (webpack)/hot/dev-server.js ./node_modules/@pmmmwh/react-refresh-webpack-plugin/client/ReactRefreshEntry.js ./node_modules/react-dev-utils/webpackHotDevClient.js ./src/index.tsx ***!
  \***********************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! /tree-hugs/client/node_modules/webpack/hot/dev-server.js */"./node_modules/webpack/hot/dev-server.js");
__webpack_require__(/*! /tree-hugs/client/node_modules/@pmmmwh/react-refresh-webpack-plugin/client/ReactRefreshEntry.js */"./node_modules/@pmmmwh/react-refresh-webpack-plugin/client/ReactRefreshEntry.js");
__webpack_require__(/*! /tree-hugs/client/node_modules/react-dev-utils/webpackHotDevClient.js */"./node_modules/react-dev-utils/webpackHotDevClient.js");
module.exports = __webpack_require__(/*! /tree-hugs/client/src/index.tsx */"./src/index.tsx");


/***/ })

},[[1,"runtime-main","vendors~main"]]]);
//# sourceMappingURL=main.chunk.js.map