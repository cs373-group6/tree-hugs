import React from "react";
import ReactDOM from "react-dom";
import { MemoryRouter, Route } from "react-router-dom";
import ThingstodoInstancePage from "../pages/ThingstodoInstancePage";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(
    <MemoryRouter initialEntries={["/thingstodo/1"]}>
      <Route exact path="/thingstodo/:id" component={ThingstodoInstancePage} />
    </MemoryRouter>,
    div,
  );
  expect(div.textContent).not.toBe(null);
});
