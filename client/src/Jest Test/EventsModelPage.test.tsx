import React from "react";
import ReactDOM from "react-dom";
import EventModelPage from "../pages/EventsModelPage";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<EventModelPage />, div);
  expect(div.textContent).not.toBe(null);
});
