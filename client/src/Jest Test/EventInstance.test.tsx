import React from "react";
import ReactDOM from "react-dom";
import { MemoryRouter, Route } from "react-router-dom";
import EventInstancePage from "../pages/EventInstance";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(
    <MemoryRouter initialEntries={["/events/1"]}>
      <Route exact path="/events/:id" component={EventInstancePage} />
    </MemoryRouter>,
    div,
  );
  expect(div.textContent).not.toBe(null);
});
