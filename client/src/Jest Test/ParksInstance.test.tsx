import React from "react";
import ReactDOM from "react-dom";
import { MemoryRouter, Route } from "react-router-dom";
import ParksInstance from "../pages/ParksInstance";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(
    <MemoryRouter initialEntries={["/parks/1"]}>
      <Route exact path="/parks/:id" component={ParksInstance} />
    </MemoryRouter>,
    div,
  );
  expect(div.textContent).not.toBe(null);
});
