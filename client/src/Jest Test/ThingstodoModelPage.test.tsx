import React from "react";
import ReactDOM from "react-dom";
import ThingsToDo from "../pages/ThingstodoModelPage";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<ThingsToDo />, div);
  expect(div).not.toBe(null);
});
