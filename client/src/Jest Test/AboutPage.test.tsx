import React from "react";
import ReactDOM from "react-dom";
import AboutPage from "../pages/AboutPage";
import BioCard from "../components/BioCard";
import ToolList from "../components/ToolList";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<AboutPage />, div);
  expect(div.textContent).not.toBe(null);
});

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<BioCard />, div);
  expect(div.textContent).not.toBe(null);
});

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<ToolList />, div);
  expect(div.textContent).not.toBe(null);
});
