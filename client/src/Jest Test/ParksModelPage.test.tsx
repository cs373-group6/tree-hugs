import React from "react";
import ReactDOM from "react-dom";
import ParksModelPage from "../pages/ParksModelPage";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<ParksModelPage />, div);
  expect(div.textContent).not.toBe(null);
});
