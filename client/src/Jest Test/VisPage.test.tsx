import React from "react";
import ReactDOM from "react-dom";
import Visualization from "../pages/VisualizationsPage";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<Visualization />, div);
  expect(div).not.toBe(null);
});
