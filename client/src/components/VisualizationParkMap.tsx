import React, { useState, useEffect, ReactElement } from "react";
import { ComposableMap, Geographies, Geography } from "react-simple-maps";
import { scaleQuantile } from "d3-scale";
import { csv } from "d3-fetch";
import { CSVType } from "./Types";

function ParkMap(): ReactElement {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const [data, setData] = useState<any>([]);
  const geoUrl = "https://cdn.jsdelivr.net/npm/us-atlas@3/states-10m.json";

  useEffect(() => {
    csv("/parks.csv").then((csvData) => {
      setData(csvData);
    });
  }, []);

  const colorScale = scaleQuantile<string>()
    .domain(data.map((d: CSVType) => d.rate))
    .range(["#CDDFDF", "#9BC0C0", "#69A1A1", "#378282", "#056363", "#034545"]);

  return (
    <ComposableMap projection="geoAlbersUsa">
      <Geographies geography={geoUrl}>
        {({ geographies }) =>
          geographies.map((geo) => {
            const cur = data.find((s: CSVType) => s.id === geo.id);
            return <Geography key={geo.rsmKey} geography={geo} fill={cur ? colorScale(cur.rate) : "#EEE"} />;
          })
        }
      </Geographies>
    </ComposableMap>
  );
}

export default ParkMap;
