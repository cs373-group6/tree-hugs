import React, { useState } from "react";
import { Row, Col } from "react-bootstrap";
import Select from "react-select";
import "../styles/pages.css";
import Slider from "./Slider";
import { ParkParamType } from "./Types";

/* eslint-disable  @typescript-eslint/no-explicit-any */
/* eslint-disable  @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable  no-unused-vars */

function ParksFilter(props: { parkFilters: ParkParamType; setFilter: (newFilters: ParkParamType) => void }) {
  const [value, setValue] = useState<number[]>([0, 300]);

  const nameOptions = [
    { value: "asc", label: "Name, A-Z" },
    { value: "desc", label: "Name, Z-A" },
  ];
  const stateOptions = [
    { value: "AZ", label: "AZ" },
    { value: "AR", label: "AR" },
    { value: "CA", label: "CA" },
    { value: "CO", label: "CO" },
    { value: "DC", label: "DC" },
    { value: "DE", label: "DE" },
    { value: "FL", label: "FL" },
    { value: "GA", label: "GA" },
    { value: "ID", label: "ID" },
    { value: "IL", label: "IL" },
    { value: "IN", label: "IN" },
    { value: "IA", label: "IA" },
    { value: "KS", label: "KS" },
    { value: "KY", label: "KY" },
    { value: "LA", label: "LA" },
    { value: "ME", label: "ME" },
    { value: "MD", label: "MD" },
    { value: "MA", label: "MA" },
    { value: "MN", label: "MN" },
    { value: "MS", label: "MS" },
    { value: "MO", label: "MO" },
    { value: "MT", label: "MT" },
    { value: "NE", label: "NE" },
    { value: "NV", label: "NV" },
    { value: "NJ", label: "NJ" },
    { value: "NM", label: "NM" },
    { value: "NY", label: "NY" },
    { value: "NC", label: "NC" },
    { value: "ND", label: "ND" },
    { value: "OH", label: "OH" },
    { value: "OK", label: "OK" },
    { value: "OR", label: "OR" },
    { value: "PA", label: "PA" },
    { value: "RI", label: "RI" },
    { value: "SC", label: "SC" },
    { value: "SD", label: "SD" },
    { value: "TN", label: "TN" },
    { value: "TX", label: "TX" },
    { value: "UT", label: "UT" },
    { value: "VA", label: "VA" },
    { value: "WA", label: "WA" },
    { value: "WV", label: "WV" },
    { value: "WI", label: "WI" },
  ];
  const dayOptions = [
    { value: null, label: "None" },
    { value: "sunday", label: "Sunday" },
    { value: "monday", label: "Monday" },
    { value: "tuesday", label: "Tuesday" },
    { value: "wednesday", label: "Wednesday" },
    { value: "thursday", label: "Thursday" },
    { value: "friday", label: "Friday" },
    { value: "saturday", label: "Saturday" },
  ];
  const worldHeritageOptions = [
    { value: null, label: "All" },
    { value: "True", label: "World Heritage Sites" },
    { value: "False", label: "Not World Heritage" },
  ];

  const parseFilterValues = (selected: any) => {
    let val = "";
    if (selected.length !== 0) {
      for (let i = 0; i < selected.length - 1; i++) {
        val += selected[i].value;
        val += ",";
      }
      val += selected[selected.length - 1].value;
    }
    return val;
  };

  const handleNameChange = (selected: any) => {
    const newFilters: ParkParamType = props.parkFilters;
    newFilters.name = selected.value;
    props.setFilter(newFilters);
  };

  const handleStateChange = (selected: any) => {
    const newFilters: ParkParamType = props.parkFilters;
    newFilters.state = parseFilterValues(selected);
    props.setFilter(newFilters);
  };

  const handleDayChange = (selected: any) => {
    const newFilters: ParkParamType = props.parkFilters;
    newFilters.day = selected.value;
    props.setFilter(newFilters);
  };

  const handleWorldHeritageChange = (selected: any) => {
    const newFilters: ParkParamType = props.parkFilters;
    newFilters.world_heritage = selected.value;
    props.setFilter(newFilters);
  };

  const handleMinMaxChange = (event: any, newValue: number | number[]) => {
    const newFilters: ParkParamType = props.parkFilters;
    setValue(newValue as number[]);
    const [min, max] = value;
    newFilters.min_budget = Math.min(min, max);
    newFilters.max_budget = Math.max(min, max);
    props.setFilter(newFilters);
  };

  return (
    <div>
      <b>Sort by...</b>
      <Select className="sortSize" options={nameOptions} onChange={handleNameChange} />
      <br />
      <b>Filter by...</b>
      <br />
      <Row>
        <Col>
          State:
          <Select options={stateOptions} isMulti onChange={handleStateChange} />
        </Col>
        <Col>
          Day open:
          <Select options={dayOptions} onChange={handleDayChange} />
        </Col>
        <Col>
          World Heritage sites:
          <Select options={worldHeritageOptions} onChange={handleWorldHeritageChange} />
        </Col>
        <Col>
          Cost:
          <Slider
            value={value}
            min={0}
            max={300}
            onChange={handleMinMaxChange}
            valueLabelDisplay="auto"
            aria-labelledby="range-slider"
          />
        </Col>
        <br />
      </Row>
    </div>
  );
}

export default ParksFilter;
