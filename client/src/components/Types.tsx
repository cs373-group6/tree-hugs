/* eslint-disable camelcase */

export type ParkType = {
  id: number;
  name: string;
  state: string;
  park_code: string;
  min_fee: number;
  max_fee: number;
  directions_url: string;
  longitude: number;
  latitude: number;
  description: string;
  email: string;
  url: string;
  weather: string;
  stdhour_description: string;
  world_heritage: boolean;
  standard_hours: Array<StandardHoursType>;
  tags: Array<TagsType>;
  media: Array<ImageType>;
  events: Array<EventType>;
  thingstodo: Array<ThingsToDoType>;
};

export type EventType = {
  id: number;
  title: string;
  category: boolean;
  date: string;
  is_free: boolean;
  fee_info: string;
  longitude: number;
  latitude: number;
  description: string;
  url: string;
  contact_name: string;
  contact_cell: string;
  contact_email: string;
  require_registration: boolean;
  tags: Array<TagsType>;
  media: Array<ImageType>;
  parks: Array<ParkType>;
  thingstodo: Array<ThingsToDoType>;
};

export type ThingsToDoType = {
  id: number;
  title: string;
  seasons: string;
  require_reservation: boolean;
  has_fees: boolean;
  fee_description: string;
  pets_allowed: boolean;
  duration: string;
  description: string;
  accessibility_info: string;
  time_of_day: number;
  video_url: string;
  tags: Array<TagsType>;
  media: Array<ImageType>;
  parks: Array<ParkType>;
  events: Array<EventType>;
};

export type ImageType = {
  title: string;
  url: string;
  alt_text: string;
  caption: string;
  credit: string;
};

export type StandardHoursType = {
  day: string;
  time: string;
};

export type TagsType = {
  name: string;
};

export type ResponseType = {
  num_results: number;
  objects: Array<any>;
};

export type ParkParamType = {
  day: string | null;
  // True or False
  world_heritage: string | null;
  min_budget: number | null;
  max_budget: number | null;
  // asc or desc
  name: string | null;
  state: string | null;
  page_size: number | null;
};

export type EventParamType = {
  // True or False
  category: string | null;
  // True or False
  free: string | null;
  // True or False
  registration: string | null;
  // asc or desc
  date: string | null;
  // asc or desc
  title: string | null;
};

export type ThingsToDoParamType = {
  seasons: string | null;
  // True or False
  reservation: string | null;
  // True or False
  fees: string | null;
  // True or False
  pets: string | null;
  // asc or desc
  title: string | null;
};

export type ParkCSVType = {
  id: number;
  name: string;
  rate: number;
};

export type SearchAttributeType = {
  name: string;
  attribute: string;
  attribute_id: number;
};

export type CSVType = {
  id: string;
  name: string;
  rate: string;
};
