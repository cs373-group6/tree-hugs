import React, { ReactElement } from "react";
import { Nav, Navbar, Form, Button, FormControl, InputGroup } from "react-bootstrap";
import { Link } from "react-router-dom";
import TreeHugLogo from "../images/TreeHugLogo.svg";

function NavBar(): ReactElement {
  /* queries whole site with user input on search bar */
  // eslint-disable-next-line  @typescript-eslint/no-explicit-any
  const textInput: any = React.useRef();
  function searchOnClick() {
    if (textInput.current.value === "") window.location.assign("/search/q=Yellowstone");
    else window.location.assign(`/search/q=${textInput.current.value}`);
  }

  return (
    <div>
      <Navbar expand="lg" style={{ background: "rgb(183, 196, 188)" }} fixed="top">
        <Navbar.Brand as={Link} to="/">
          <img src={TreeHugLogo} alt="logo" width="30" height="30" /> Tree Hugs
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link as={Link} to="/parks">
              Parks
            </Nav.Link>
            <Nav.Link as={Link} to="/events">
              Events
            </Nav.Link>
            <Nav.Link as={Link} to="/thingstodo">
              Things to do
            </Nav.Link>
            <Nav.Link as={Link} to="/about">
              About
            </Nav.Link>
            <Nav.Link as={Link} to="/visualizations">
              Visualizations
            </Nav.Link>
          </Nav>
          <Form
            inline
            onSubmit={(e) => {
              e.preventDefault();
            }}
          >
            <InputGroup>
              <FormControl
                type="text"
                placeholder="Yellowstone"
                ref={textInput}
                // eslint-disable-next-line  @typescript-eslint/no-explicit-any
                onKeyPress={(event: any) => {
                  if (event.key === "Enter") {
                    searchOnClick();
                  }
                }}
              />
              <InputGroup.Append>
                <Button style={{ background: "#2f4f4f", border: 0 }} onClick={() => searchOnClick()}>
                  Search
                </Button>
              </InputGroup.Append>
            </InputGroup>
          </Form>
        </Navbar.Collapse>
      </Navbar>
    </div>
  );
}

export default NavBar;
