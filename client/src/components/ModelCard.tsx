import React, { ReactElement } from "react";
import { Card } from "react-bootstrap";
import { Link } from "react-router-dom";
import landingStylesheet from "../styles/landing.module.css";

export type Model = {
  link: string;
  src: string;
  title: string;
};

function ModelCard({ model }: { model: Model }): ReactElement {
  return (
    <Card>
      <Link to={model.link}>
        <Card.Img variant="top" className={landingStylesheet.modelImg} src={model.src} />
        <Card.Body>
          <Card.Title>{model.title}</Card.Title>
        </Card.Body>
      </Link>
    </Card>
  );
}

export default ModelCard;
