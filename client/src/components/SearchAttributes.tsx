export const parkAttributes = [
  {
    name: "Name: ",
    attribute: "name",
    attribute_id: 0,
  },
  {
    name: "State: ",
    attribute: "state",
    attribute_id: 1,
  },
  {
    name: "latitude: ",
    attribute: "latitude",
    attribute_id: 2,
  },
  {
    name: "longitude: ",
    attribute: "longitude",
    attribute_id: 3,
  },
  {
    name: "weather:",
    attribute: "weather",
    attribute_id: 4,
  },
];

export const eventAttributes = [
  {
    name: "Title: ",
    attribute: "title",
    attribute_id: 0,
  },
  {
    name: "Date: ",
    attribute: "date",
    attribute_id: 1,
  },
  {
    name: "Contact Name: ",
    attribute: "contact_name",
    attribute_id: 2,
  },
  {
    name: "Fee Information: ",
    attribute: "fee_info",
    attribute_id: 3,
  },
  {
    name: "Latitude: ",
    attribute: "latitude",
    attribute_id: 4,
  },
  {
    name: "Longitude: ",
    attribute: "longitude",
    attribute_id: 5,
  },
];

export const thingstodoAttributes = [
  {
    name: "Title: ",
    attribute: "title",
    attribute_id: 0,
  },
  {
    name: "Duration: ",
    attribute: "duration",
    attribute_id: 1,
  },
];
