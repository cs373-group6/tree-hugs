import React, { useState, useEffect, ReactElement } from "react";
/* eslint-disable-next-line import/no-unresolved */
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from "recharts";
import visualizationStylesheet from "../styles/visualization.module.css";
import { getEventsPerDay } from "../data/api";

function EventsLineChart(): ReactElement {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const [eventsData, setEventsData] = useState<any>({});

  useEffect(() => {
    const getData = async () => {
      const apiParkData = await getEventsPerDay();
      setEventsData(apiParkData);
    };
    getData();
  }, []);

  return (
    <div className={visualizationStylesheet.center}>
      <LineChart
        width={900}
        height={500}
        data={eventsData.objects}
        margin={{
          top: 5,
          right: 30,
          left: 20,
          bottom: 5,
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="date" />
        <YAxis dataKey="num_events" />
        <Tooltip />
        <Legend />
        <Line type="monotone" dataKey="num_events" stroke="#8884d8" activeDot={{ r: 8 }} />
      </LineChart>
    </div>
  );
}

export default EventsLineChart;
