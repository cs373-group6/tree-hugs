import React, { ReactElement } from "react";
import Card from "react-bootstrap/Card";
import aboutStylesheet from "../styles/about.module.css";

type BioCardProps = {
  member: {
    image: string;
    linkedin: string;
    name: string;
    role: string;
    leadership: string;
    bio: string;
  };
  commits: number;
  issues: number;
  tests: number;
};

function BioCard({ member, commits, issues, tests }: BioCardProps): ReactElement | null {
  if (member) {
    return (
      <Card id={aboutStylesheet.card} className={aboutStylesheet.biocard}>
        <Card.Body>
          <Card.Img variant="top" className={aboutStylesheet.headshot} src={member.image} />
          <br />
          <br />
          <Card.Title>
            {" "}
            <a href={member.linkedin}> {member.name} </a>
          </Card.Title>
          <Card.Subtitle>
            {member.role} {member.leadership}
          </Card.Subtitle>
          <hr />
          <Card.Text>{member.bio}</Card.Text>
          <Card.Text>
            <small>
              Commits : {commits} Issues: {issues} Tests: {tests}{" "}
            </small>
          </Card.Text>
        </Card.Body>
      </Card>
    );
  }
  return null;
}

export default BioCard;
