import React, { ReactElement } from "react";
import { Carousel } from "react-bootstrap";
import "../styles/pages.css";
import { ImageType } from "./Types";

function Content(images: Array<ImageType>, defaultImg: string): ReactElement {
  if (images.length) {
    return (
      <Carousel>
        {images.map((image: ImageType) => (
          <Carousel.Item className="image-container" key={image.url}>
            <img className="image" src={image.url} alt={image.alt_text} />
            {image.title || image.caption || image.credit ? (
              <Carousel.Caption>
                <div className="image-caption">
                  {image.title ? (
                    <h5 style={{ opacity: 1 }}>
                      {image.title}: {image.caption}
                    </h5>
                  ) : (
                    <h5 style={{ opacity: 1 }}>{image.caption}</h5>
                  )}
                  {image.credit ? <p>credit: {image.credit}</p> : null}
                </div>
              </Carousel.Caption>
            ) : null}
          </Carousel.Item>
        ))}
      </Carousel>
    );
  }
  return <img className="image" src={defaultImg} alt="default" />;
}

function ImageCarousel(images: Array<ImageType>, defaultImg: string): ReactElement {
  return <div className="image-carousel">{Content(images, defaultImg)}</div>;
}

export default ImageCarousel;
