import React, { useEffect, ReactElement } from "react";
import * as d3 from "d3";

function createBubbleChart() {
  /* eslint-disable @typescript-eslint/no-explicit-any */
  const json = {
    children: [
      { name: "Winter (1196)", value: 1196 },
      { name: "Spring (1306)", value: 1306 },
      { name: "Summer (1391)", value: 1391 },
      { name: "Fall (1374)", value: 1374 },
      { name: "No Reservation (878)", value: 878 },
      { name: "Free of Charge (1398)", value: 1398 },
      { name: "Allow Pets (916)", value: 916 },
    ],
  };

  const diameter = 800;

  const color = d3.scaleOrdinal(d3.schemeCategory10);

  const bubble = d3.pack().size([diameter, diameter]).padding(5);

  const margin = {
    left: 0,
    right: 100,
    top: 0,
    bottom: 0,
  };

  const svg = d3
    .select("#stateCompanyBubbleChart")
    .append("svg")
    .attr("viewBox", `0 0 ${diameter + margin.right} ${diameter}`)
    .attr("preserveAspectRatio", "xMinYMin meet")
    .attr("class", "chart-svg");

  const root: any = d3
    .hierarchy(json)
    .sum((d: any) => d.value)
    .sort((a: any, b: any) => b.value - a.value);

  bubble(root);

  const node = svg
    .selectAll(".node")
    .data(root.children)
    .enter()
    .append("g")
    .attr("class", "node")
    .attr("transform", (d: any) => `translate(${d.x} ${d.y})`)
    .append("g")
    .attr("class", "graph");

  // Create a bubble
  node
    .append("circle")
    .attr("r", (d: any) => d.r)
    .style("fill", (d: any) => color(d.data.name));

  // Add names to bubbles
  node
    .append("text")
    .attr("dy", ".3em")
    .style("text-anchor", "middle")
    .text((d: any) => d.data.name)
    .style("fill", "#FFF");

  /* eslint-enable @typescript-eslint/no-explicit-any */
}

function PollutionInWaterBubbleChart(): ReactElement {
  useEffect(() => {
    createBubbleChart();
  }, []);

  return <div id="stateCompanyBubbleChart" />;
}

export default PollutionInWaterBubbleChart;
