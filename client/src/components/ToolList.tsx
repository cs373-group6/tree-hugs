import React, { ReactElement } from "react";
import { Card, CardDeck } from "react-bootstrap";
import aboutStylesheet from "../styles/about.module.css";
import awsImage from "../images/aws.svg";
import bootstrapImage from "../images/bootstrap.svg";
import dockerImage from "../images/docker.svg";
import eslintImage from "../images/eslint.svg";
import flaskImage from "../images/flask.svg";
import gitImage from "../images/git.svg";
import gitlabImage from "../images/gitlab.svg";
import jestImage from "../images/jest.svg";
import namecheapImage from "../images/namecheap.svg";
import postmanImage from "../images/postman.svg";
import prettierImage from "../images/prettier.svg";
import reactImage from "../images/react.svg";
import algoliaImage from "../images/algolia.svg";

function ToolCard(src: string): ReactElement {
  return (
    <Card id={aboutStylesheet.card} className={aboutStylesheet.toolcard}>
      <Card.Img variant="top" className={aboutStylesheet.logo} src={src} />
    </Card>
  );
}

function ToolList(): ReactElement {
  return (
    <div className={aboutStylesheet.aboutPage}>
      <h3 className="title">Development Tools</h3>
      <br />
      <CardDeck className={aboutStylesheet.carddeck}>
        {ToolCard(awsImage)}
        {ToolCard(bootstrapImage)}
        {ToolCard(dockerImage)}
        {ToolCard(eslintImage)}
        {ToolCard(flaskImage)}
        {ToolCard(gitImage)}
        {ToolCard(gitlabImage)}
        {ToolCard(jestImage)}
        {ToolCard(namecheapImage)}
        {ToolCard(postmanImage)}
        {ToolCard(prettierImage)}
        {ToolCard(reactImage)}
        {ToolCard(algoliaImage)}
      </CardDeck>
    </div>
  );
}

export default ToolList;
