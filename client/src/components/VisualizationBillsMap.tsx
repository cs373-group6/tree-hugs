import React, { useState, useEffect, ReactElement } from "react";
import { ComposableMap, Geographies, Geography } from "react-simple-maps";
import { scaleQuantile } from "d3-scale";
import { csv } from "d3-fetch";
import { CSVType } from "./Types";

function BillsMap(): ReactElement {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const [data, setData] = useState<any>([]);
  const geoUrl = "https://cdn.jsdelivr.net/npm/us-atlas@3/states-10m.json";

  useEffect(() => {
    csv("/bills.csv").then((csvData) => {
      setData(csvData);
    });
  }, []);

  const colorScale = scaleQuantile<string>()
    .domain(data.map((d: CSVType) => d.rate))
    .range(["#E4D3D0", "#C9A8A2", "#AE7C74", "#935146", "#782618", "#541A10"]);

  return (
    <ComposableMap projection="geoAlbersUsa">
      <Geographies geography={geoUrl}>
        {({ geographies }) =>
          geographies.map((geo) => {
            const cur = data.find((s: CSVType) => s.id === geo.id);
            return <Geography key={geo.rsmKey} geography={geo} fill={cur ? colorScale(cur.rate) : "#EEE"} />;
          })
        }
      </Geographies>
    </ComposableMap>
  );
}

export default BillsMap;
