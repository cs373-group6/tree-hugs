import React, { ReactElement } from "react";
import { Card, CardDeck } from "react-bootstrap";
import aboutStylesheet from "../styles/about.module.css";
import NationalParkImage from "../images/nationalParkService.svg";
import GoogleMapsImage from "../images/googleMaps.svg";
import YoutubeImage from "../images/youtube.svg";

function ApiList(): ReactElement {
  return (
    <div className={aboutStylesheet.aboutPage}>
      <h3 className="title">Acknowledgement</h3>
      <p>Tree Hugs used APIs from the following sources</p>
      <CardDeck className={aboutStylesheet.carddeck}>
        <Card id={aboutStylesheet.card} className={aboutStylesheet.toolcard}>
          <Card.Img variant="top" className={aboutStylesheet.logo} src={NationalParkImage} />
          <Card.Body>
            <Card.Title>
              <a href="https://www.nps.gov/subjects/developer/api-documentation.htm"> National Park Serivice </a>
            </Card.Title>
          </Card.Body>
        </Card>
        <Card id={aboutStylesheet.card} className={aboutStylesheet.toolcard}>
          <Card.Img variant="top" className={aboutStylesheet.logo} src={GoogleMapsImage} />
          <Card.Body>
            <Card.Title>
              <a href="https://developers.google.com/maps/documentation"> Google Maps </a>
            </Card.Title>
          </Card.Body>
        </Card>
        <Card id={aboutStylesheet.card} className={aboutStylesheet.toolcard}>
          <Card.Img variant="top" className={aboutStylesheet.logo} src={YoutubeImage} />
          <Card.Body>
            <Card.Title>
              <a href="https://developers.google.com/youtube/v3"> YouTube </a>
            </Card.Title>
          </Card.Body>
        </Card>
      </CardDeck>
    </div>
  );
}

export default ApiList;
