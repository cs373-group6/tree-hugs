import React, { ReactElement } from "react";
import { Index, connectHits, connectStateResults } from "react-instantsearch-dom";
import "../styles/pages.css";
import SearchCard from "./SearchCard";
import { SearchAttributeType } from "./Types";

function SearchContent(props: { title: string; path: string; attributes: Array<SearchAttributeType> }): ReactElement {
  const { title, path, attributes } = props;

  // eslint-disable-next-line  @typescript-eslint/no-explicit-any
  const Hits = ({ hits }: any) => (
    <div>
      {/* eslint-disable-next-line @typescript-eslint/no-explicit-any */}
      {hits.map((hit: any) => (
        <div key={hit.id}>
          <SearchCard hit={hit} path={path} attributes={attributes} />
        </div>
      ))}
    </div>
  );

  const CustomHits = connectHits(Hits);

  // eslint-disable-next-line  @typescript-eslint/no-explicit-any
  const Content = ({ searchState }: any) =>
    searchState && searchState.query ? (
      <div>
        <CustomHits />
      </div>
    ) : null;

  const CustomStateResults = connectStateResults(Content);

  return (
    <Index indexName={title}>
      <div>
        <h2 className="title">Search Results For {title}</h2>
        <hr />
        <CustomStateResults />
      </div>
    </Index>
  );
}

export default SearchContent;
