import React, { ReactElement } from "react";
import { Pagination } from "react-bootstrap";
import "../styles/pages.css";

type PaginationProps = {
  currentPage: number;
  totalPages: number;
  range: number;
  // eslint-disable-next-line no-unused-vars
  onChange: (args: number) => void;
};

function PaginationBar(props: PaginationProps): ReactElement {
  const leftRange = Math.floor(props.range / 2);
  const rightRange = props.range - leftRange - 1;
  const start =
    // eslint-disable-next-line no-nested-ternary
    props.currentPage <= leftRange + 1
      ? 2
      : props.currentPage >= props.totalPages - rightRange - 1
      ? props.totalPages - props.range - 1
      : props.currentPage - leftRange;
  const range = Math.min(
    // eslint-disable-next-line no-nested-ternary
    start > 2
      ? props.currentPage < props.totalPages - rightRange - 1
        ? props.range
        : props.range + 1
      : props.currentPage < props.totalPages - rightRange - 1
      ? props.range + 1
      : props.range + 2,
    props.totalPages - 2,
  );
  const showLeftEllipsis: boolean = start > 2;
  const showRightEllipsis: boolean = start + range < props.totalPages;

  const handleClick = (activePage: number) => {
    if (activePage >= 1 && activePage <= props.totalPages) {
      props.onChange(activePage);
    }
  };

  const paginationItems = () => {
    const items = [];
    for (let i = 0; i < range; i++) {
      const page = start + i;
      items.push(
        <Pagination.Item
          style={{ width: "50px", textAlign: "center" }}
          active={props.currentPage === page}
          onClick={() => handleClick(page)}
          key={page}
        >
          {page}
        </Pagination.Item>,
      );
    }
    return items;
  };

  return (
    <Pagination style={{ textAlign: "center", justifyContent: "center" }}>
      <Pagination.Prev style={{ width: "50px" }} onClick={() => handleClick(props.currentPage - 1)} />
      <Pagination.Item style={{ width: "50px" }} active={props.currentPage === 1} onClick={() => handleClick(1)}>
        {1}
      </Pagination.Item>
      {showLeftEllipsis ? <Pagination.Ellipsis style={{ width: "50px" }} /> : null}
      {paginationItems()}
      {showRightEllipsis ? <Pagination.Ellipsis style={{ width: "50px" }} /> : null}
      {props.totalPages > 1 ? (
        <Pagination.Item
          style={{ width: "50px" }}
          active={props.currentPage === props.totalPages}
          onClick={() => handleClick(props.totalPages)}
        >
          {props.totalPages}
        </Pagination.Item>
      ) : null}
      <Pagination.Next style={{ width: "50px" }} onClick={() => handleClick(props.currentPage + 1)} />
    </Pagination>
  );
}
export default PaginationBar;
