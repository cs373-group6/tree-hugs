import React, { ReactElement } from "react";
import { CardDeck } from "react-bootstrap";
import aboutStylesheet from "../styles/about.module.css";
import BioCard from "./BioCard";
import members from "../data/MemberInfo";

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function TeamInfo(props: { commitData: Array<any>; issueData: Array<any> }): ReactElement {
  let totalCommitCount = 0;
  let totalIssueCount = 0;
  let eugueneCommitCount = 0;
  let eugueneIssueCount = 0;
  let reginaCommitCount = 0;
  let reginaIssueCount = 0;
  let yichenCommitCount = 0;
  let yichenIssueCount = 0;
  let stephanieCommitCount = 0;
  let stephanieIssueCount = 0;
  let sophieCommitCount = 0;
  let sophieIssueCount = 0;

  totalCommitCount = props.commitData.length;
  totalIssueCount = props.issueData.length;

  // eslint-disable-next-line camelcase
  props.commitData.forEach((element: { author_name: string }) => {
    if (element.author_name === "SophieZhao00" || element.author_name === "Sophie Zhao") sophieCommitCount += 1;
    else if (element.author_name === "eugeneccc" || element.author_name === "Eugene Chang") eugueneCommitCount += 1;
    else if (element.author_name === "Yichen Liu" || element.author_name === "yichen liu") yichenCommitCount += 1;
    else if (element.author_name === "Stephanie Tran" || element.author_name === "stephoknees")
      stephanieCommitCount += 1;
    else if (element.author_name === "Regina Chen") reginaCommitCount += 1;
  });

  props.issueData.forEach((element: { assignees: Array<{ username: string }> }) => {
    for (let i = 0; i < element.assignees.length; i++) {
      if (element.assignees[i].username === "SophieZhao00") sophieIssueCount += 1;
      else if (element.assignees[i].username === "eugeneccc") eugueneIssueCount += 1;
      else if (element.assignees[i].username === "tt0suzy") yichenIssueCount += 1;
      else if (element.assignees[i].username === "stephoknees") stephanieIssueCount += 1;
      else if (element.assignees[i].username === "reginaxchen") reginaIssueCount += 1;
    }
  });

  return (
    <div className={aboutStylesheet.aboutPage}>
      <h3 className="title">Developers</h3>
      <br />
      <CardDeck className={aboutStylesheet.carddeck}>
        <BioCard member={members[0]} commits={eugueneCommitCount} issues={eugueneIssueCount} tests={0} />
        <BioCard member={members[1]} commits={reginaCommitCount} issues={reginaIssueCount} tests={12} />
        <BioCard member={members[2]} commits={yichenCommitCount} issues={yichenIssueCount} tests={11} />
        <BioCard member={members[3]} commits={stephanieCommitCount} issues={stephanieIssueCount} tests={52} />
        <BioCard member={members[4]} commits={sophieCommitCount} issues={sophieIssueCount} tests={64} />
      </CardDeck>
      <h5>
        Git Stats: {totalCommitCount} Commits, {totalIssueCount} Issues, 125 Tests
      </h5>
    </div>
  );
}

export default TeamInfo;
