import React from "react";
import { Row, Col } from "react-bootstrap";
import Select from "react-select";
import "../styles/pages.css";
import { ThingsToDoParamType } from "./Types";

/* eslint-disable  @typescript-eslint/no-explicit-any */
/* eslint-disable  @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable  no-unused-vars */

function ThingstodoFilter(props: {
  thingsToDoFilters: ThingsToDoParamType;
  setFilter: (newFilters: ThingsToDoParamType) => void;
}) {
  const titleOptions = [
    { value: "asc", label: "Title, A-Z" },
    { value: "desc", label: "Title, Z-A" },
  ];
  const seasonsOptions = [
    { value: null, label: "All" },
    { value: "winter", label: "Winter" },
    { value: "spring", label: "Spring" },
    { value: "summer", label: "Summer" },
    { value: "fall", label: "Fall" },
  ];
  const reservationOptions = [
    { value: null, label: "All" },
    { value: "True", label: "Reservation required" },
    { value: "False", label: "Reservation not required" },
  ];
  const feesOptions = [
    { value: null, label: "All" },
    { value: "True", label: "Fees required" },
    { value: "False", label: "Fees not required" },
  ];
  const petsOptions = [
    { value: null, label: "All" },
    { value: "True", label: "Pets allowed" },
    { value: "False", label: "Pets not allowed" },
  ];

  const parseFilterValues = (selected: any) => {
    let val = "";
    if (selected.length !== 0) {
      for (let i = 0; i < selected.length - 1; i++) {
        val += selected[i].value;
        val += ",";
      }
      val += selected[selected.length - 1].value;
    }
    return val;
  };

  const handleTitleChange = (selected: any) => {
    const newFilters: ThingsToDoParamType = props.thingsToDoFilters;
    newFilters.title = selected.value;
    props.setFilter(newFilters);
  };

  const handleSeasonsChange = (selected: any) => {
    const newFilters: ThingsToDoParamType = props.thingsToDoFilters;
    newFilters.seasons = parseFilterValues(selected);
    props.setFilter(newFilters);
  };

  const handleReservationChange = (selected: any) => {
    const newFilters: ThingsToDoParamType = props.thingsToDoFilters;
    newFilters.reservation = selected.value;
    props.setFilter(newFilters);
  };

  const handleFeesChange = (selected: any) => {
    const newFilters: ThingsToDoParamType = props.thingsToDoFilters;
    newFilters.fees = selected.value;
    props.setFilter(newFilters);
  };

  const handlePetsChange = (selected: any) => {
    const newFilters: ThingsToDoParamType = props.thingsToDoFilters;
    newFilters.pets = selected.value;
    props.setFilter(newFilters);
  };

  return (
    <div>
      <b>Sort by...</b>
      <Select className="sortSize" options={titleOptions} onChange={handleTitleChange} />
      <br />
      <b>Filter by...</b>
      <br />
      <Row>
        <Col>
          Seasons:
          <Select options={seasonsOptions} isMulti onChange={handleSeasonsChange} />
        </Col>
        <Col>
          Reservation:
          <Select options={reservationOptions} onChange={handleReservationChange} />
        </Col>
        <Col>
          Fees:
          <Select options={feesOptions} onChange={handleFeesChange} />
        </Col>
        <Col>
          Pets:
          <Select options={petsOptions} onChange={handlePetsChange} />
        </Col>
      </Row>
    </div>
  );
}

export default ThingstodoFilter;
