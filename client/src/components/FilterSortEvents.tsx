import React from "react";
import { Row, Col } from "react-bootstrap";
import Select from "react-select";
import "../styles/pages.css";
import { EventParamType } from "./Types";

/* eslint-disable  @typescript-eslint/no-explicit-any */
/* eslint-disable  @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable  no-unused-vars */

function EventsFilter(props: { eventFilters: EventParamType; setFilter: (newFilters: EventParamType) => void }) {
  const sortOptions = [
    { value: "title-asc", label: "Title, A-Z" },
    { value: "title-desc", label: "Title, Z-A" },
    { value: "date-asc", label: "Date, ascending" },
    { value: "date-desc", label: "Date, descending" },
  ];
  const categoryOptions = [
    { value: null, label: "All" },
    { value: "True", label: "Regular" },
    { value: "False", label: "Special" },
  ];
  const freeOptions = [
    { value: null, label: "All" },
    { value: "True", label: "Free" },
    { value: "False", label: "Not free" },
  ];
  const registrationOptions = [
    { value: null, label: "All" },
    { value: "True", label: "Registration required" },
    { value: "False", label: "Registration not required" },
  ];

  const handleSortChange = (selected: any): void => {
    const newFilters: EventParamType = props.eventFilters;
    const fullVal: Array<string> = selected.value.split("-");
    const attribute: string = fullVal[0];
    const query: string = fullVal[1];
    if (attribute === "title") {
      newFilters.title = query;
      newFilters.date = null;
    } else {
      newFilters.date = query;
      newFilters.title = null;
    }
    props.setFilter(newFilters);
  };

  const handleCategoryChange = (selected: any): void => {
    const newFilters: EventParamType = props.eventFilters;
    newFilters.category = selected.value;
    props.setFilter(newFilters);
  };

  const handleFreeChange = (selected: any): void => {
    const newFilters: EventParamType = props.eventFilters;
    newFilters.free = selected.value;
    props.setFilter(newFilters);
  };

  const handleRegistrationChange = (selected: any): void => {
    const newFilters: EventParamType = props.eventFilters;
    newFilters.registration = selected.value;
    props.setFilter(newFilters);
  };

  return (
    <div>
      <b>Sort by...</b>
      <Select className="sortSize" options={sortOptions} onChange={handleSortChange} />
      <br />
      <b>Filter by...</b>
      <br />
      <Row>
        <Col>
          Category:
          <Select options={categoryOptions} onChange={handleCategoryChange} />
        </Col>
        <Col>
          Free:
          <Select options={freeOptions} onChange={handleFreeChange} />
        </Col>
        <Col>
          Registration:
          <Select options={registrationOptions} onChange={handleRegistrationChange} />
        </Col>
      </Row>
    </div>
  );
}

export default EventsFilter;
