import React, { ReactElement } from "react";
/* eslint-disable-next-line import/no-unresolved */
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from "recharts";
import visualizationStylesheet from "../styles/visualization.module.css";

function IncomeBarChart(): ReactElement {
  const data = [
    { name: "$10,000-$19,999", "Number of Districts": 1 },
    { name: "$20,000-$29,999", "Number of Districts": 137 },
    { name: "$30,000-$39,999", "Number of Districts": 227 },
    { name: "$40,000-$49,999", "Number of Districts": 50 },
    { name: "$50,000-$59,999", "Number of Districts": 14 },
    { name: "$60,000-$69,999", "Number of Districts": 5 },
    { name: "$70,000-$79,999", "Number of Districts": 1 },
  ];

  return (
    <div className={visualizationStylesheet.center}>
      <BarChart
        width={1100}
        height={500}
        data={data}
        margin={{
          top: 10,
          right: 10,
          left: 10,
          bottom: 10,
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="name" />
        <YAxis />
        <Tooltip />
        <Legend />
        <Bar dataKey="Number of Districts" fill="#e87c10" />
      </BarChart>
    </div>
  );
}

export default IncomeBarChart;
