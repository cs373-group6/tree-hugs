import React, { ReactElement } from "react";
import { Nav, Navbar } from "react-bootstrap";
import { Link } from "react-router-dom";

function Footer(): ReactElement {
  return (
    <div>
      <Navbar expand="lg" style={{ background: "rgb(183, 196, 188)", position: "relative" }} fixed="bottom">
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Nav className="mr-auto" style={{ margin: "auto" }}>
          <Nav.Link as={Link} to="/about">
            About Us
          </Nav.Link>
          <Nav.Link href="https://gitlab.com/cs373-group6/tree-hugs">GitLab</Nav.Link>
        </Nav>
      </Navbar>
    </div>
  );
}

export default Footer;
