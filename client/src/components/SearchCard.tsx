import React, { ReactElement } from "react";
import { Highlight } from "react-instantsearch-dom";
import "../styles/pages.css";
import searchStylesheet from "../styles/search.module.css";
import { SearchAttributeType } from "./Types";

// eslint-disable-next-line  @typescript-eslint/no-explicit-any
function SearchCard(props: { hit: any; path: string; attributes: Array<SearchAttributeType> }): ReactElement {
  return (
    <div>
      <a href={`${props.path}/${props.hit.id}`}>
        <h5 className={searchStylesheet.header}>{props.hit.title ? props.hit.title : props.hit.name}</h5>
      </a>
      {props.attributes.map((attribute) => (
        <p key={attribute.attribute_id}>
          <b>{attribute.name}</b>
          <Highlight attribute={attribute.attribute} tagName="mark" hit={props.hit} />
        </p>
      ))}
      <hr />
    </div>
  );
}

export default SearchCard;
