import React, { ReactElement } from "react";
import { Switch, Route } from "react-router-dom";
import Navbar from "./components/NavBar";
import Footer from "./components/Footer";
import LandingPage from "./pages/LandingPage";
import Parks from "./pages/ParksModelPage";
import ParkInstance from "./pages/ParksInstance";
import Events from "./pages/EventsModelPage";
import ThingsToDo from "./pages/ThingstodoModelPage";
import ThingsToDoInstance from "./pages/ThingstodoInstancePage";
import AboutPage from "./pages/AboutPage";
import EventInstance from "./pages/EventInstance";
import SearchPage from "./pages/SearchPage";
import Visualizations from "./pages/VisualizationsPage";

function App(): ReactElement {
  return (
    <div className="App">
      <Navbar />
      <Switch>
        <Route exact path="/" component={LandingPage} />
        <Route exact path="/parks" component={Parks} />
        <Route exact path="/events" component={Events} />
        <Route exact path="/thingstodo" component={ThingsToDo} />
        <Route exact path="/about" component={AboutPage} />
        <Route exact path="/visualizations" component={Visualizations} />
        <Route exact path="/parks/:id" component={ParkInstance} />
        <Route exact path="/thingstodo/:id" component={ThingsToDoInstance} />
        <Route exact path="/events/:id" component={EventInstance} />
        {/* eslint-disable-next-line react/prop-types */}
        <Route path="/search/q=:q" render={(props) => <SearchPage q={props.match.params.q} />} />
      </Switch>
      <Footer />
    </div>
  );
}

export default App;
