import React, { useState, useEffect, ReactElement } from "react";
import { Card, CardColumns } from "react-bootstrap";
import { Link } from "react-router-dom";
import { ParkType, ParkParamType } from "../components/Types";
import "../styles/pages.css";
import parksStylesheet from "../styles/parks.module.css";
import { getParks } from "../data/api";
import LoadingPage from "./LoadingPage";
import PaginationBar from "../components/PaginationBar";
import ParksFilter from "../components/FilterSortParks";
import defaultImg from "../images/HomeBackground.jpg";

export function Park(park: ParkType): ReactElement {
  const link = `/parks/${park?.id}`;
  let isOpen = true;
  const d = new Date();
  const currentDay = d.getDay();
  const currentHour = d.getHours();
  if (park?.standard_hours.length === 0) isOpen = false;
  else if (park?.standard_hours[currentDay].time === "All Day") isOpen = true;
  else if (park?.standard_hours[currentDay].time === "Closed") isOpen = false;
  else if (park?.standard_hours[currentDay].time === "Sunrise to Sunset")
    isOpen = currentHour >= 6 && currentHour <= 18;
  else if (park?.standard_hours[currentDay].time.includes("-")) {
    const openTime = parseInt(park?.standard_hours[currentDay].time.split("-")[0].split(":")[0], 10);
    const closeTime = parseInt(park?.standard_hours[currentDay].time.split("-")[1].split(":")[0], 10);
    isOpen = currentHour >= openTime && currentHour <= closeTime;
  } else isOpen = true;

  return (
    <Card className={parksStylesheet.ParkCard} key={park?.id}>
      <Card.Img
        variant="top"
        className={parksStylesheet.parkCardImg}
        src={park?.media?.[0]?.url ? park?.media?.[0]?.url : defaultImg}
      />
      <Card.Body>
        <Card.Title>
          <Link to={link}>{park?.name}</Link>
        </Card.Title>
        <Card.Text>
          State(s): {park?.state}
          <br />
          Cost: {park?.max_fee !== 0 && park?.max_fee !== null ? `$${park?.min_fee} - $${park?.max_fee}` : "No Cost"}
          <br />
          Park Code: {park?.park_code}
          <br />
          {isOpen ? " The park is open now." : " The park is closed now."}
        </Card.Text>
      </Card.Body>
    </Card>
  );
}

function Parks(): ReactElement {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const [parksData, setParksData] = useState<any>({});
  const [currentPage, setCurrentPage] = useState(1);
  const [dataLength, setDataLength] = useState(0);
  const [reload, setReload] = useState(false);
  const [loading, setLoading] = useState<boolean>(true);
  const [parkFilters, setParkFilters] = useState<ParkParamType>({
    day: null,
    world_heritage: null,
    min_budget: null,
    max_budget: null,
    name: null,
    state: null,
    page_size: null,
  });

  const setFilter = (newFilters: ParkParamType) => {
    setParkFilters(newFilters);
    setReload(!reload);
  };

  useEffect(() => {
    const getData = async () => {
      const apiParkData = await getParks(currentPage, parkFilters);
      setParksData(apiParkData);
      setDataLength(apiParkData.num_results);
      setLoading(false);
    };
    getData();
  }, [currentPage, reload]);

  return loading ? (
    <LoadingPage />
  ) : (
    <div className="page">
      <header>
        <h1 className="title">Parks</h1>
      </header>
      {/* <Button onClick={resetAll} >reset all</Button> */}
      <ParksFilter parkFilters={parkFilters} setFilter={setFilter} />
      <br />
      <CardColumns>{parksData.objects.map((park: ParkType) => Park(park))}</CardColumns>
      Number of Parks: {dataLength}
      <PaginationBar
        currentPage={currentPage}
        totalPages={parksData.total_pages}
        range={5}
        onChange={(newPage: number) => setCurrentPage(newPage)}
      />
    </div>
  );
}

export default Parks;
