import React, { ReactElement } from "react";
import { CardDeck, Button } from "react-bootstrap";
import "../styles/pages.css";
import landingStylesheet from "../styles/landing.module.css";
import ModelCard, { Model } from "../components/ModelCard";
import TreeHugLogo from "../images/TreeHugLogo.svg";

function LandingPage(): ReactElement {
  const models: Array<Model> = [
    {
      link: "/parks",
      src: "https://www.nps.gov/common/uploads/structured_data/3C861078-1DD8-B71B-0B774A242EF6A706.jpg",
      title: "National Parks",
    },
    {
      link: "/events",
      src: "https://www.nps.gov/common/uploads/event_calendar/5884976D-B7EC-5D10-ADFB75EE3BDEEF6D.jpg",
      title: "Events",
    },
    {
      link: "/thingstodo",
      src: "https://www.nps.gov/common/uploads/cropped_image/928D157F-BB27-6614-8C0833EE8C35672E.jpg",
      title: "Things to do",
    },
  ];

  return (
    <div id="home" className={landingStylesheet.landing}>
      <div id={landingStylesheet.overlay}>
        <div className={landingStylesheet.row}>
          <img src={TreeHugLogo} alt="logo" width="50" height="50" />
          <h1>Welcome to Tree Hugs</h1>
          <img src={TreeHugLogo} alt="logo" width="50" height="50" />
        </div>
        <p>Find a tree to hug</p>
        <div>
          <CardDeck>
            <ModelCard model={models[0]} />
            <ModelCard model={models[1]} />
            <ModelCard model={models[2]} />
          </CardDeck>
        </div>
        <br />
        <div className={landingStylesheet.row}>
          <Button href="/about" size="lg" className={landingStylesheet.button}>
            About
          </Button>
        </div>
      </div>
    </div>
  );
}

export default LandingPage;
