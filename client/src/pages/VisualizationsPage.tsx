import React, { ReactElement } from "react";
import "../styles/pages.css";
import visualizationStylesheet from "../styles/visualization.module.css";
import MapChart from "../components/VisualizationParkMap";
import BubbleChart from "../components/VisualizationThingsTodoBubbleChart";
import EventsLineChart from "../components/VisualizationEventsLineChart";
import BillsMap from "../components/VisualizationBillsMap";
import PoliticianPieChart from "../components/VisualizationPoliticianPieChart";
import IncomeBarChart from "../components/VisualizationIncomeBarChart";
import MapKey from "../images/ParksKey.jpg";
import BillsKey from "../images/BillsKey.jpg";

function VisualizationsPage(): ReactElement {
  return (
    <div className="page">
      <h1 className="title">Our Visualizations</h1>
      <h4 className="title">Parks Distribution Choropleth Map</h4>
      <div className={visualizationStylesheet.center}>
        <MapChart />
        <img src={MapKey} alt="MapKey" height="70" />
      </div>
      <h4 className="title">Things To Do Attributes</h4>
      <BubbleChart />
      <h4 className="title">Number of Events on Certain Dates</h4>
      <EventsLineChart />
      <h1 className="title">
        Provider's Visualizations:
        <a href="https://www.dolladollabills.me/"> DollaDollaBills</a>
      </h1>
      <h4 className="title">Bills Distribution Choropleth Map</h4>
      <div className={visualizationStylesheet.center}>
        <BillsMap />
        <img src={BillsKey} alt="BillsKey" height="70" />
      </div>
      <h4 className="title">Politicians Age Distribution Pie Chart</h4>
      <PoliticianPieChart />
      <h4 className="title">Congressional Districts Median HH Income Distribution Histogram</h4>
      <IncomeBarChart />
    </div>
  );
}

export default VisualizationsPage;
