import React, { useState, useEffect, ReactElement } from "react";
import { Link } from "react-router-dom";
import { Carousel, Row, Col } from "react-bootstrap";
import { GoogleMap, useLoadScript } from "@react-google-maps/api";
import "../styles/pages.css";
import parksStylesheet from "../styles/parks.module.css";
import LoadingPage from "./LoadingPage";
import ImageCarousel from "../components/ImageCarousel";
import defaultImg from "../images/HomeBackground.jpg";
import { getParkById, getParks } from "../data/api";
import { Park } from "./ParksModelPage";
import { ParkType, EventType, ThingsToDoType, ResponseType, StandardHoursType, TagsType } from "../components/Types";

const mapContainerStyle = {
  margin: "auto",
  width: "90%",
  height: "50vh",
};

function capitalizeFirstLetter(string: string): string {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

function RelatedParks(parks: Array<ParkType>): ReactElement {
  // group related parks
  const groupSize = 3;
  const groupedParks = [];
  let temp = [];
  for (let i = 0; i < parks.length; i++) {
    temp.push(parks[i]);
    if (i % groupSize === groupSize - 1) {
      groupedParks.push(temp);
      temp = [];
    }
  }
  if (temp.length > 0) {
    groupedParks.push(temp);
  }
  return (
    <Carousel className={parksStylesheet.relatedParksDesk}>
      {groupedParks.map((group: Array<ParkType>) => (
        <Carousel.Item interval={10000} key={group[0].id}>
          <div className={parksStylesheet.parkSlide}>
            {group.map((park: ParkType) => (
              <div className={parksStylesheet.parkCardContainer} key={park.id}>
                {Park(park)}
              </div>
            ))}
          </div>
        </Carousel.Item>
      ))}
    </Carousel>
  );
}

function ParksInstance(props: { match: { params: { id: number } } }): ReactElement {
  const parkId: number = props.match.params.id;
  /* eslint-disable @typescript-eslint/no-explicit-any */
  const [park, setPark] = useState<ParkType | any>({});
  const [parks, setParks] = useState<ResponseType | any>({});
  /* eslint-enable @typescript-eslint/no-explicit-any */
  const [loading, setLoading] = useState(true);
  const { isLoaded } = useLoadScript({
    googleMapsApiKey: "AIzaSyDZfonsQI2mQYnOfYJEYQEIyO1QOR9AODY",
  });
  const center = {
    lat: parseFloat(park.latitude),
    lng: parseFloat(park.longitude),
  };

  useEffect(() => {
    const getData = async () => {
      setLoading(true);
      const apiParkData = await getParkById(parkId);
      const apiParksData = await getParks(1, {
        day: null,
        world_heritage: null,
        min_budget: null,
        max_budget: null,
        name: null,
        state: apiParkData.state,
        page_size: 50,
      });
      setPark(apiParkData);
      setParks(apiParksData);
      setLoading(false);
      window.scrollTo(0, 0);
    };
    getData();
  }, [parkId]);

  return loading ? (
    <LoadingPage />
  ) : (
    <div className="page">
      <h1 className="title">{park.name}</h1>
      Tags: {park.tags.map((tag: TagsType) => `${tag?.name}, `)}
      <br />
      {ImageCarousel(park.media, defaultImg)}
      {park.description.replace(/<\/?[^>]+>/gi, "")}
      <Row>
        <Col>
          <h5 className="title">Basic Information</h5>
          State(s): {park.state}
          <br />
          Code: {park.park_code}
          <br />
          Cost: {park.max_fee !== 0 && park.max_fee !== null ? `$${park.min_fee} - $${park.max_fee}` : "No Cost"}
          <br />
          Email: {park.email}
          <br />
          Official website: <a href={park.url}>{park.url}</a>
        </Col>
        <Col>
          <h5 className="title">Regular Hours</h5>
          {park.standard_hours.map((standardHour: StandardHoursType) => (
            <div key={standardHour.day}>
              {capitalizeFirstLetter(standardHour.day)}: {standardHour.time}
            </div>
          ))}
          <br />
          <p>Note: {park.stdhour_description !== null ? park.stdhour_description : "Not Applicable"}</p>
        </Col>
      </Row>
      <Row>
        <Col>
          <h5 className="title">Location</h5>
          {isLoaded ? (
            <GoogleMap mapContainerStyle={mapContainerStyle} zoom={10} center={center} />
          ) : (
            <p>Not Applicable</p>
          )}
          <br />
          For more information regarding the directions, please refer to this <a href={park.directions_url}>link</a>.
        </Col>
        <Col>
          <h5 className="title">Weather Information</h5>
          <p>{park.weather}</p>
        </Col>
      </Row>
      <h5 className="title">{parks.num_results} parks within in the same state:</h5>
      {RelatedParks(parks.objects)}
      <Row>
        <Col>
          <h5 className="title">Related Events</h5>
          {park.events.map((event: EventType) => (
            <div key={event.id}>
              <Link to={`/events/${event.id}`}>{event.title}</Link>
            </div>
          ))}
        </Col>
        <Col>
          <h5 className="title">Related Things To Do</h5>
          {park.thingstodo.map((ttd: ThingsToDoType) => (
            <div key={ttd.id}>
              <Link to={`/thingstodo/${ttd.id}`}>{ttd.title}</Link>
            </div>
          ))}
        </Col>
      </Row>
    </div>
  );
}

export default ParksInstance;
