import React, { useState, ReactElement, useEffect } from "react";
import { Table } from "react-bootstrap";
import { Link } from "react-router-dom";
import { Close, Check } from "@material-ui/icons";
import { ThingsToDoType, ResponseType, ThingsToDoParamType } from "../components/Types";
import "../styles/pages.css";
import { getThingsToDo } from "../data/api";
import LoadingPage from "./LoadingPage";
import PaginationBar from "../components/PaginationBar";
import ThingstodoFilter from "../components/FilterSortThingstodo";

function ThingToDo(thingsToDo: ThingsToDoType): ReactElement {
  const link = `/thingstodo/${thingsToDo?.id}`;
  const winter: ReactElement = thingsToDo?.seasons.includes("winter") ? (
    <Check className="check" />
  ) : (
    <Close className="cross" />
  );
  const spring: ReactElement = thingsToDo?.seasons.includes("spring") ? (
    <Check className="check" />
  ) : (
    <Close className="cross" />
  );
  const summer: ReactElement = thingsToDo?.seasons.includes("summer") ? (
    <Check className="check" />
  ) : (
    <Close className="cross" />
  );
  const fall: ReactElement = thingsToDo?.seasons.includes("fall") ? (
    <Check className="check" />
  ) : (
    <Close className="cross" />
  );
  const hasFee: ReactElement = thingsToDo?.has_fees ? <Check className="check" /> : <Close className="cross" />;
  const reservation: ReactElement = thingsToDo?.require_reservation ? (
    <Check className="check" />
  ) : (
    <Close className="cross" />
  );
  const pets: ReactElement = thingsToDo?.pets_allowed ? <Check className="check" /> : <Close className="cross" />;

  return (
    <tr>
      <td>
        <Link to={link}>{thingsToDo?.title}</Link>
      </td>
      <td>{winter}</td>
      <td>{spring}</td>
      <td>{summer}</td>
      <td>{fall}</td>
      <td>{reservation}</td>
      <td>{hasFee}</td>
      <td>{pets}</td>
    </tr>
  );
}

function ThingsToDo(): ReactElement {
  // eslint-disable-next-line  @typescript-eslint/no-explicit-any
  const [thingsToDosData, setThingsToDoData] = useState<ResponseType | any>({});
  const [dataLength, setDataLength] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const [reload, setReload] = useState(false);
  const [loading, setLoading] = useState<boolean>(true);
  const [thingsToDoFilters, setThingsToDoFilters] = useState<ThingsToDoParamType>({
    seasons: null,
    reservation: null,
    fees: null,
    pets: null,
    title: null,
  });

  const setFilter = (newFilters: ThingsToDoParamType) => {
    setThingsToDoFilters(newFilters);
    setReload(!reload);
  };

  useEffect(() => {
    const getData = async () => {
      const apiThingsToDoData = await getThingsToDo(currentPage, thingsToDoFilters);
      setThingsToDoData(apiThingsToDoData);
      setDataLength(apiThingsToDoData.num_results);
      setLoading(false);
    };
    getData();
  }, [currentPage, reload]);

  return loading ? (
    <LoadingPage />
  ) : (
    <div className="page">
      <h1 className="title">Things To Do</h1>
      <ThingstodoFilter thingsToDoFilters={thingsToDoFilters} setFilter={setFilter} />
      <br />
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Title</th>
            <th colSpan={4}>Season</th>
            <th>Reservation Required</th>
            <th>Fees Required</th>
            <th>Pets allowed</th>
          </tr>
          <tr>
            <th>&nbsp;</th>
            <th>Winter</th>
            <th>Spring</th>
            <th>Summer</th>
            <th>Fall</th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        {thingsToDosData.objects.map((ttd: ThingsToDoType) => (
          <tbody key={ttd.id}>{ThingToDo(ttd)}</tbody>
        ))}
      </Table>
      Number of ThingsTodo: {dataLength}
      <br />
      <PaginationBar
        currentPage={currentPage}
        totalPages={thingsToDosData.total_pages}
        range={5}
        onChange={(newPage: number) => setCurrentPage(newPage)}
      />
    </div>
  );
}

export default ThingsToDo;
