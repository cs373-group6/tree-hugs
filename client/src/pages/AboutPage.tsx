import React, { ReactElement, useState, useEffect } from "react";
import ReactPlayer from "react-player";
import "../styles/pages.css";
import aboutStylesheet from "../styles/about.module.css";
import { getGitLabCommits, getGitLabIssues } from "../data/api";
import TeamInfo from "../components/TeamInfo";
import ToolList from "../components/ToolList";
import ApiList from "../components/ApiList";
import LoadingPage from "./LoadingPage";

function AboutPage(): ReactElement {
  const [loading, setLoading] = useState<boolean>(true);
  /* eslint-disable @typescript-eslint/no-explicit-any */
  const [commitData, setCommits] = useState<Array<any>>([]);
  const [issueData, setIssues] = useState<Array<any>>([]);
  /* eslint-enable @typescript-eslint/no-explicit-any */

  useEffect(() => {
    const getData = async () => {
      setCommits(await getGitLabCommits());
      setIssues(await getGitLabIssues());
      setLoading(false);
    };
    getData();
  }, []);

  return loading ? (
    <LoadingPage />
  ) : (
    <div id={aboutStylesheet.aboutBackground}>
      <div id={aboutStylesheet.aboutPage} className="page">
        <h1 className="title">About Us</h1>
        <br />
        <h4>
          Tree Hugs is a website for travelers to find information about national parks in United States, and the
          related events and things to do in the park.
        </h4>
        <br />
        <div className={aboutStylesheet.video}>
          <ReactPlayer url="https://www.youtube.com/watch?v=mcVf2Jc5Px4" />
        </div>
        <br />
        <TeamInfo commitData={commitData} issueData={issueData} />
        <br />
        <br />
        <ApiList />
        <ToolList />
        <br />
        <h3 className="title">Source Code</h3>
        <br />
        <p>
          Tree Hugs is an open source project. Here's the link to our{" "}
          <a href="https://gitlab.com/cs373-group6/tree-hugs">GitLab repository</a> .
        </p>
        <p>
          Our APIs are free to use, check our{" "}
          <a href="https://documenter.getpostman.com/view/12084061/Tz5jdKfE">API documentation</a> for more details.
        </p>
      </div>
    </div>
  );
}

export default AboutPage;
