import React, { ReactElement } from "react";
import { Spinner } from "react-bootstrap";
import "../styles/pages.css";

function LoadingPage(): ReactElement {
  return (
    <div id="loading" className="page">
      <div>
        <Spinner animation="border" />
      </div>
    </div>
  );
}

export default LoadingPage;
