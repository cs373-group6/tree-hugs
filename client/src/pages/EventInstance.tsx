import React, { ReactElement, useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { GoogleMap, useLoadScript } from "@react-google-maps/api";
import { Row, Col } from "react-bootstrap";
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import ReactWeather, { useOpenWeather } from "react-open-weather";
import "../styles/pages.css";
import LoadingPage from "./LoadingPage";
import ImageCarousel from "../components/ImageCarousel";
import defaultImg from "../images/defaultEventImg.jpeg";
import { getEventById } from "../data/api";
import { ParkType, EventType, ThingsToDoType, TagsType } from "../components/Types";

const mapContainerStyle = {
  margin: "auto",
  width: "90%",
  height: "50vh",
};

function EventInstance(props: { match: { params: { id: number } } }): ReactElement {
  const eventId: number = props.match.params.id;
  // eslint-disable-next-line  @typescript-eslint/no-explicit-any
  const [event, setEvent] = useState<EventType | any>({});
  const [loading, setLoading] = useState<boolean>(true);
  const longitude = parseFloat(event.longitude);
  const latitude = parseFloat(event.latitude);
  const center = {
    lat: latitude,
    lng: longitude,
  };
  const { data, isLoading, errorMessage } = useOpenWeather({
    key: "7cd1ed052a91998ef45c0637b3c00f5e",
    lat: latitude,
    lon: longitude,
    lang: "en",
    unit: "imperial", // values are (metric, standard, imperial)
  });
  const { isLoaded } = useLoadScript({
    googleMapsApiKey: "AIzaSyDZfonsQI2mQYnOfYJEYQEIyO1QOR9AODY",
  });

  useEffect(() => {
    const getEventData = async () => {
      const apiEventData = await getEventById(eventId);
      setEvent(apiEventData);
      setLoading(false);
    };
    getEventData();
  }, []);

  return loading ? (
    <LoadingPage />
  ) : (
    <div className="page">
      <h1 className="title">{event.title}</h1>
      Tags: {event.tags.map((tag: TagsType) => `${tag?.name}, `)}
      <Row>
        <Col>
          <br />
          <br />
          Category: {event.category ? "Regular" : "Special"}
          <br />
          Date: {event.date}
          <br />
          Fee Information: {event.is_free ? "This event is free." : event.fee_info?.replace(/<\/?[^>]+>/gi, "")}
          <br />
          <br />
          <a href={event.url}>More Information On This Event</a>
        </Col>
        <Col>
          <h5 className="title">Contact Information</h5>
          Name: {event.contact_name ? event.contact_name : "Not Applicable"}
          <br />
          Cell: {event.contact_cell ? event.contact_cell : "Not Applicable"}
          <br />
          Email: {event.contact_email ? event.contact_email : "Not Applicable"}
        </Col>
      </Row>
      <br />
      <Row>
        {ImageCarousel(event.media, defaultImg)}
        {event.description?.replace(/<\/?[^>]+>/gi, "")}
      </Row>
      <Row>
        <Col>
          <h5 className="title">Location</h5>
          {isLoaded ? <GoogleMap mapContainerStyle={mapContainerStyle} zoom={10} center={center} /> : <br />}
        </Col>
        <Col>
          <h5 className="title">Weather Information</h5>
          <ReactWeather
            isLoading={isLoading}
            errorMessage={errorMessage}
            data={data}
            lang="en"
            locationLabel={event.park?.name}
            unitsLabels={{ temperature: "°F", windSpeed: "mph" }}
            showForecast
          />
        </Col>
      </Row>
      <Row>
        <Col>
          <h5 className="title">Related parks</h5>
          {event.parks.map((park: ParkType) => (
            <div key={park.id}>
              <Link to={`/parks/${park.id}`}>{park.name}</Link>
            </div>
          ))}
        </Col>
        <Col>
          <h5 className="title">Related things to do</h5>
          {event.thingstodo.map((ttd: ThingsToDoType) => (
            <div key={ttd.id}>
              <Link to={`/thingstodo/${ttd.id}`}>{ttd.title}</Link>
            </div>
          ))}
        </Col>
      </Row>
    </div>
  );
}

export default EventInstance;
