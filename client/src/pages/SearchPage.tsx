import React, { ReactElement } from "react";
import { InstantSearch, SearchBox } from "react-instantsearch-dom";
import algoliasearch from "algoliasearch";
import SearchContent from "../components/SearchContent";
import "../styles/pages.css";
import { parkAttributes, eventAttributes, thingstodoAttributes } from "../components/SearchAttributes";

// eslint-disable-next-line  @typescript-eslint/no-explicit-any
function SearchPage(props: { q: any }): ReactElement {
  const searchClient = algoliasearch("B95EAU2PHN", "fd1111b96c48a0839759d76900113bde");

  return (
    <div className="page">
      <h1 className="title">{`Results for query: ${props.q}`}</h1>
      <InstantSearch indexName="Parks" searchClient={searchClient} searchState={{ query: props.q }}>
        <div style={{ display: "none" }}>
          <SearchBox />
        </div>
        <SearchContent title="Parks" path="/parks" attributes={parkAttributes} />
        <SearchContent title="Events" path="/events" attributes={eventAttributes} />
        <SearchContent title="ThingsToDo" path="/thingstodo" attributes={thingstodoAttributes} />
      </InstantSearch>
    </div>
  );
}

export default SearchPage;
