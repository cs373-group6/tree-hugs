import React, { useState, ReactElement, useEffect } from "react";
import { Table } from "react-bootstrap";
import { Link } from "react-router-dom";
import { Close, Check } from "@material-ui/icons";
import { EventType, ResponseType, EventParamType } from "../components/Types";
import "../styles/pages.css";
import { getEvents } from "../data/api";
import LoadingPage from "./LoadingPage";
import PaginationBar from "../components/PaginationBar";
import EventsFilter from "../components/FilterSortEvents";

function Event(event: EventType): ReactElement {
  const link = `/events/${event?.id}`;
  const eventCategory: string = event?.category ? "Regular" : "Special";
  // eslint-disable-next-line  @typescript-eslint/no-explicit-any
  const isFree: any = event?.is_free ? <Check className="check" /> : <Close className="cross" />;
  // eslint-disable-next-line  @typescript-eslint/no-explicit-any
  const registration: any = event?.require_registration ? <Check className="check" /> : <Close className="cross" />;
  return (
    <tr>
      <td>
        <Link to={link}>{event?.title}</Link>
      </td>
      <td>{registration}</td>
      <td>{eventCategory}</td>
      <td>{event?.date}</td>
      <td>{isFree}</td>
    </tr>
  );
}

function Events(): ReactElement {
  // eslint-disable-next-line  @typescript-eslint/no-explicit-any
  const [eventsData, setEventsData] = useState<ResponseType | any>({});
  const [dataLength, setDataLength] = useState<number>(0);
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [reload, setReload] = useState<boolean>(false);
  const [loading, setLoading] = useState<boolean>(true);
  const [eventFilters, setEventFilters] = useState<EventParamType>({
    category: null,
    free: null,
    registration: null,
    date: null,
    title: null,
  });

  const setFilter = (newFilters: EventParamType) => {
    setEventFilters(newFilters);
    setReload(!reload);
  };

  useEffect(() => {
    const getData = async () => {
      const apiEventsData = await getEvents(currentPage, eventFilters);
      setEventsData(apiEventsData);
      setDataLength(apiEventsData.num_results);
      setLoading(false);
    };
    getData();
  }, [currentPage, reload]);

  return loading ? (
    <LoadingPage />
  ) : (
    <div className="page">
      <h1 className="title">Events</h1>
      <EventsFilter eventFilters={eventFilters} setFilter={setFilter} />
      <br />
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Title</th>
            <th>Registration Required?</th>
            <th>Category</th>
            <th>Date Begin</th>
            <th>Free Event?</th>
          </tr>
        </thead>
        {eventsData.objects.map((event: EventType) => (
          <tbody key={event.id}>{Event(event)}</tbody>
        ))}
      </Table>
      Number of Events: {dataLength}
      <PaginationBar
        currentPage={currentPage}
        totalPages={eventsData.total_pages}
        range={5}
        onChange={(newPage: number) => setCurrentPage(newPage)}
      />
    </div>
  );
}

export default Events;
