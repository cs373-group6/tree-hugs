import React, { ReactElement, useState, useEffect } from "react";
import { Link } from "react-router-dom";
import ReactPlayer from "react-player";
import { Row, Col } from "react-bootstrap";
import "../styles/pages.css";
import LoadingPage from "./LoadingPage";
import ImageCarousel from "../components/ImageCarousel";
import defaultImg from "../images/HomeBackground.jpg";
import { getThingsToDoById } from "../data/api";
import { ParkType, EventType, ThingsToDoType, TagsType } from "../components/Types";

function ThingsToDoInstance(props: { match: { params: { id: number } } }): ReactElement {
  const thingsToDoId: number = props.match.params.id;
  // eslint-disable-next-line  @typescript-eslint/no-explicit-any
  const [thingsToDo, setThingsToDo] = useState<ThingsToDoType | any>({});
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const getThingsToDoData = async () => {
      const apiThingsToDoData = await getThingsToDoById(thingsToDoId);
      setThingsToDo(apiThingsToDoData);
      setLoading(false);
    };
    getThingsToDoData();
  }, []);

  return loading ? (
    <LoadingPage />
  ) : (
    <div className="page">
      <h1 className="title">{thingsToDo.title}</h1>
      Tags: {thingsToDo.tags?.map((tag: TagsType) => `${tag?.name}, `)}
      <br />
      <br />
      Duration: {thingsToDo.duration ? thingsToDo.duration : "Not Applicable"}
      <br />
      Suitable Seasons: {thingsToDo.seasons ? thingsToDo.seasons : "Any season"}
      <br />
      Suitable Time of Day: {thingsToDo.time_of_day ? thingsToDo.time_of_day : "Not Applicable"}
      <br />
      <br />
      {ImageCarousel(thingsToDo.media, defaultImg)}
      {thingsToDo.description?.replace(/<\/?[^>]+>/gi, "")}
      <Row>
        <Col>
          <h5 className="title">Fee Description</h5>
          {thingsToDo.fee_description ? thingsToDo.fee_description.replace(/<\/?[^>]+>/gi, "") : "Not Applicable"}
        </Col>
        <Col>
          <h5 className="title">Accessibility Information</h5>
          {thingsToDo.accessibility_info?.replace(/<\/?[^>]+>/gi, "")}
        </Col>
      </Row>
      <h5 className="title">Related Video</h5>
      <ReactPlayer style={{ alignItems: "center" }} url={thingsToDo.video_url} />
      <Row>
        <Col>
          <h5 className="title">Related Parks</h5>
          {thingsToDo.parks.map((park: ParkType) => (
            <div key={park.id}>
              <Link to={`/events/${park.id}`}>{park.name}</Link>
            </div>
          ))}
        </Col>
        <Col>
          <h5 className="title">Related Events</h5>
          {thingsToDo.events.map((event: EventType) => (
            <div key={event.id}>
              <Link to={`/events/${event.id}`}>{event.title}</Link>
            </div>
          ))}
        </Col>
      </Row>
    </div>
  );
}

export default ThingsToDoInstance;
