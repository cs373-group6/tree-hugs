import unittest
import warnings
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options

def ignore_warnings(test_func):
    def do_test(self, *args, **kwargs):
        with warnings.catch_warnings():
            warnings.simplefilter("ignore", ResourceWarning)
            test_func(self, *args, **kwargs)
    return do_test

class guiTests(unittest.TestCase):
    @ignore_warnings
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--disable-dev-shm-usage")
        chrome_options.add_argument("window-size=1920x1480")
        chrome_prefs = {}
        chrome_options.experimental_options["prefs"] = chrome_prefs
        chrome_prefs["profile.default_content_settings"] = {"images": 2}
        self.driver = webdriver.Chrome(options=chrome_options)

    def test_home(self):
        self.driver.get("https://treehugs.me")
        assert self.driver.title == "Tree Hugs"
        assert "treehugs" in self.driver.current_url
        assert "Welcome to Tree Hugs" in self.driver.page_source
        assert "Find a tree to hug" in self.driver.page_source

    def test_homeCards(self):
        self.driver.get("https://treehugs.me")
        self.driver.find_element_by_xpath("/html/body/div/div/div[2]/div/div[2]/div/div[1]/a").click()
        assert self.driver.current_url == "https://treehugs.me/parks"
        self.driver.find_element_by_xpath("/html/body/div/div/div[1]/nav/a").click()
        self.driver.find_element_by_xpath("/html/body/div/div/div[2]/div/div[2]/div/div[2]/a").click()
        assert self.driver.current_url == "https://treehugs.me/events"
        self.driver.back()
        self.driver.find_element_by_xpath("/html/body/div/div/div[2]/div/div[2]/div/div[3]/a").click()
        assert self.driver.current_url == "https://treehugs.me/thingstodo"
        self.driver.back()
        self.driver.find_element_by_xpath("/html/body/div/div/div[2]/div/div[3]/a").click()
        assert self.driver.current_url == "https://treehugs.me/about"
        self.driver.back()

    def test_nav(self):
        self.driver.get("https://treehugs.me")
        self.driver.find_element_by_xpath("/html/body/div/div/div[1]/nav/a").click()
        self.driver.find_element_by_xpath("/html/body/div/div/div[1]/nav/div/div/a[1]").click()
        assert self.driver.current_url == "https://treehugs.me/parks"
        self.driver.find_element_by_xpath("/html/body/div/div/div[1]/nav/div/div/a[2]").click()
        assert self.driver.current_url == "https://treehugs.me/events"
        self.driver.find_element_by_xpath("/html/body/div/div/div[1]/nav/div/div/a[3]").click()
        assert self.driver.current_url == "https://treehugs.me/thingstodo"
        self.driver.find_element_by_xpath("/html/body/div/div/div[1]/nav/div/div/a[4]").click()
        assert self.driver.current_url == "https://treehugs.me/about"
        self.driver.find_element_by_xpath("/html/body/div/div/div[1]/nav/div/div/a[5]").click()
        assert self.driver.current_url == "https://treehugs.me/visualizations"
    
    def test_footer(self):
        self.driver.get("https://treehugs.me")
        self.driver.find_element_by_xpath("/html/body/div/div/div[3]/nav/div/a[1]").click()
        assert self.driver.current_url == "https://treehugs.me/about"
        self.driver.back()
        self.driver.find_element_by_xpath("/html/body/div/div/div[3]/nav/div/a[2]").click()
        assert self.driver.current_url == "https://gitlab.com/cs373-group6/tree-hugs"

    def test_searchbar(self):
        self.driver.get("https://treehugs.me")
        self.driver.find_element_by_xpath("/html/body/div/div/div[1]/nav/div/form/div/div/button").click()
        assert self.driver.current_url == "https://treehugs.me/search/q=Yellowstone"
    
    def test_searchPage(self):
        self.driver.get("https://treehugs.me")
        self.driver.find_element_by_xpath("/html/body/div/div/div[1]/nav/div/form/div/div/button").click()
        assert "Results for query" in self.driver.find_element_by_xpath("/html/body/div/div/div[2]/h1").text
        assert "Search Results For Parks" in self.driver.find_element_by_xpath("/html/body/div/div/div[2]/div[2]/h2").text
        assert "Search Results For Events" in self.driver.find_element_by_xpath("/html/body/div/div/div[2]/div[3]/h2").text
        assert "Search Results For ThingsToDo" in self.driver.find_element_by_xpath("/html/body/div/div/div[2]/div[4]/h2").text

    def test_parksModel(self):
        self.driver.get("https://treehugs.me")
        self.driver.implicitly_wait(6)
        self.driver.find_element_by_xpath("/html/body/div/div/div[1]/nav/div/div/a[1]").click()
        self.driver.implicitly_wait(6)
        assert "parks" in self.driver.current_url
        self.driver.get("https://treehugs.me/parks")
        self.driver.implicitly_wait(6)
        assert self.driver.current_url == "https://treehugs.me/parks"
        assert "Parks" in self.driver.find_element_by_xpath("/html/body/div/div/div[2]/header").text

    def test_eventsModel(self):
        self.driver.get("https://treehugs.me")
        self.driver.implicitly_wait(4)
        self.driver.find_element_by_xpath("/html/body/div/div/div[1]/nav/div/div/a[2]").click()
        self.driver.implicitly_wait(4)
        assert "events" in self.driver.current_url
        self.driver.get("https://treehugs.me/events")
        self.driver.implicitly_wait(4)
        assert self.driver.current_url == "https://treehugs.me/events"
        assert "Events" in self.driver.find_element_by_xpath("/html/body/div/div/div[2]/h1").text

    def test_thingsToDoModel(self):
        self.driver.get("https://treehugs.me")
        self.driver.implicitly_wait(4)
        self.driver.find_element_by_xpath("/html/body/div/div/div[1]/nav/div/div/a[3]").click()
        self.driver.implicitly_wait(4)
        assert "thingstodo" in self.driver.current_url
        self.driver.get("https://treehugs.me/thingstodo")
        self.driver.implicitly_wait(4)
        assert self.driver.current_url == "https://treehugs.me/thingstodo"
        assert "Things To Do" in self.driver.find_element_by_xpath("/html/body/div/div/div[2]/h1").text
    
    def test_about(self):
        self.driver.get("https://treehugs.me")
        self.driver.implicitly_wait(4)
        self.driver.find_element_by_xpath("/html/body/div/div/div[1]/nav/div/div/a[4]").click()
        self.driver.implicitly_wait(4)
        assert "about" in self.driver.current_url
        self.driver.get("https://treehugs.me/about")
        self.driver.implicitly_wait(4)
        assert self.driver.current_url == "https://treehugs.me/about"
        assert "About Us" in self.driver.find_element_by_xpath("/html/body/div/div/div[2]/div/h1").text
        self.driver.find_element_by_xpath("/html/body/div/div/div[2]/div/div[3]/div/div[1]/div/div/a").click()
        self.driver.implicitly_wait(10)
        assert self.driver.current_url == "https://www.nps.gov/subjects/developer/api-documentation.htm"
        self.driver.back()
        
    def test_aboutTools(self):
        self.driver.get("https://treehugs.me/about")
        self.driver.implicitly_wait(4)
        self.driver.find_element_by_xpath("/html/body/div/div/div[2]/div/p[1]/a").click()
        self.driver.implicitly_wait(6)
        assert self.driver.current_url == "https://gitlab.com/cs373-group6/tree-hugs"
        self.driver.back()
        self.driver.implicitly_wait(6)
        self.driver.find_element_by_xpath("/html/body/div/div/div[2]/div/p[2]/a").click()
        self.driver.implicitly_wait(10)
        assert self.driver.current_url == "https://documenter.getpostman.com/view/12084061/Tz5jdKfE"
        self.driver.back()

    def test_visPage(self):
        self.driver.get("https://treehugs.me")
        self.driver.implicitly_wait(6)
        self.driver.find_element_by_xpath("/html/body/div/div/div[1]/nav/div/div/a[5]").click()
        self.driver.implicitly_wait(6)
        assert "visualizations" in self.driver.current_url
        assert self.driver.current_url == "https://treehugs.me/visualizations"
        assert "Our Visualizations" in self.driver.find_element_by_xpath("/html/body/div/div/div[2]/h1[1]").text
        assert "Provider's Visualizations:" in self.driver.find_element_by_xpath("/html/body/div/div/div[2]/h1[2]").text
        self.driver.find_element_by_xpath("/html/body/div/div/div[2]/h1[2]/a").click()
        self.driver.implicitly_wait(10)
        assert self.driver.current_url == "https://www.dolladollabills.me/"

    def tearDown(self):
        self.driver.close()

if __name__ == "__main__":
    unittest.main()
