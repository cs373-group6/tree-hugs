# Tree Hugs

Please refer to Phase4-snapshot branch to see phase 4 code.

## Website

[treehugs.me](https://treehugs.me)

## Presentation Video

[TreeHugs Presentation Video](https://www.youtube.com/watch?v=mcVf2Jc5Px4)

## Postman Documentation
[TreeHugs API Documentation](https://documenter.getpostman.com/view/12084061/Tz5jdKfE)

## Team

### Phase 4

|      Name      |   EID   |   GitLab ID   | Estimated completion time (hours) | Actual completion time (hours) | Leadership |
| :------------: | :-----: | :-----------: | :-------------------------------: | :----------------------------: | :--------: |
|  Eugene Chang  | ec25432 |  @eugeneccc   |                15                 |               11               |  Phase 3   |
|  Regina Chen   | rc45845 | @reginaxchen  |                15                 |               18               |  Phase 4   |
|   Yichen Liu   | yl33325 |   @tt0suzy    |                10                 |               15               |            |
| Stephanie Tran | st32986 | @stephoknees  |                10                 |               15               |  Phase 2   |
|  Xuefei Zhao   | xz6784  | @SophieZhao00 |                20                 |               18               |  Phase 1   |

## Git

link to GitLab pipelines: https://gitlab.com/cs373-group6/tree-hugs/-/pipelines

### Git SHA

Phase 1: 0da74de896a34650d66aff834089e27936e59581  
Phase 2: e95a75d794b80a49a0086e239d703faf2d52c8ad  
Phase 3: 7aecde7613d8b437c7676390739a6863d07a83e3  
Phase 4: 796e5987c3c6838afd1782fb1d11760e0ffb0f85  

## Develop

### `make frontend-build`

Build the latest frontend docker image

### `make frontend`

Open frontend docker environment

### `make backend-build`

Build the latest backend docker image

### `make backend`

Open backend docker environment
