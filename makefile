# develop
frontend-build:
	docker rmi -f frontend
	docker build --no-cache -t frontend ./client
frontend:
	docker run -it -v $(PWD)/client:/tree-hugs/client -v /tree-hugs/client/node_modules -p 3000:3000 -w /tree-hugs/client frontend
backend-build:
	docker rmi -f backend
	docker build --no-cache -t backend ./server
backend:
	docker run -it -v $(PWD)/server:/tree-hugs/server -p 8080:8080 -w /tree-hugs/server backend
prune:
	docker container prune
	docker volume prune
	docker system prune

# deploy
deploy-frontend:
	echo "Deploying Frontend..."
	cd client && export REACT_APP_API_URL=/api
	cd client && npm i && npm run build
	aws s3 sync client/build/ s3://treehugs.me --acl public-read
deploy-backend:
	echo "Deploying Backend..."
	aws ecr get-login-password --region us-east-2 | docker login --username AWS --password-stdin 449611945624.dkr.ecr.us-east-2.amazonaws.com
	docker build -f server/Dockerfile.prod -t treehugs-backend ./server
	docker tag treehugs-backend:latest 449611945624.dkr.ecr.us-east-2.amazonaws.com/treehugs-backend:latest
	docker push 449611945624.dkr.ecr.us-east-2.amazonaws.com/treehugs-backend:latest
	docker image prune
	cd server/aws_deploy && eb use Treehugs-env && eb deploy