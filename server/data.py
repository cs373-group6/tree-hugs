"""Scrap data from National Parks API and store them in database"""
# pylint: disable=no-member

import urllib.request
import re
import random
import requests
import models
from init import app, db

states = [
    "AZ",
    "AR",
    "CA",
    "CO",
    "DC",
    "DE",
    "FL",
    "GA",
    "ID",
    "IL",
    "IN",
    "IA",
    "KS",
    "KY",
    "LA",
    "MD",
    "MA",
    "MN",
    "MO",
    "MT",
    "NE",
    "NV",
    "NJ",
    "NM",
    "NY",
    "NC",
    "ND",
    "OH",
    "OK",
    "OR",
    "PA",
    "SC",
    "SD",
    "TN",
    "TX",
    "UT",
    "VT",
    "VA",
    "WA",
    "WV",
    "WI",
]

whs = [
    "cave",
    "ever",
    "glac",
    "glba",
    "grca",
    "grsm",
    "havo",
    "maca",
    "meve",
    "olym",
    "redw",
    "wrst",
    "yell",
    "yose",
]

PAGE_SIZE = 50
PARKS_URL = "https://developer.nps.gov/api/v1/parks"
EVENTS_URL = "https://developer.nps.gov/api/v1/events"
TTD_URL = "https://developer.nps.gov/api/v1/thingstodo"


def post_parks():
    """Collect all parks data from National Park Service API"""
    park_id = 0
    filtered_parks = set()
    for state in states:
        start = 0
        while True:
            res = requests.get(
                PARKS_URL
                + f"?start={start}&api_key="
                # + "?statecode=CT&api_key="
                + app.config["API_KEY"]
                + f"&stateCode={state}"
            )
            parks = res.json()
            # post data to db
            park_id = get_parks(parks["data"], park_id, filtered_parks)
            print(f"Committing through park {park_id}")
            start += int(parks["limit"])
            if start >= int(parks["total"]):
                break


def get_parks(parks, park_id, filtered_parks):
    """Post a list of parks with ids starting by park_id"""
    for park in parks:
        if park["parkCode"] in filtered_parks:
            continue
        filtered_parks.add(park["parkCode"])
        park_id += 1
        # prepare data
        min_fee, max_fee = get_fees(park)
        phone = (
            park["contacts"]["phoneNumbers"][0]["phoneNumber"]
            if len(park["contacts"]["phoneNumbers"]) > 0
            else None
        )
        email = (
            park["contacts"]["emailAddresses"][0]["emailAddress"]
            if len(park["contacts"]["emailAddresses"]) > 0
            else None
        )
        stdhour_description = (
            park["operatingHours"][0]["description"]
            if len(park["operatingHours"]) > 0
            else None
        )
        # create instance
        park_instance = models.Parks(
            id=park_id,
            name=park["fullName"],
            state=park["states"],
            park_code=park["parkCode"],
            min_fee=min_fee,
            max_fee=max_fee,
            directions_url=park["directionsUrl"],
            longitude=float(park["longitude"]),
            latitude=float(park["latitude"]),
            description=park["description"],
            phone=phone,
            email=email,
            url=park["url"],
            weather=park["weatherInfo"],
            stdhour_description=stdhour_description,
            world_heritage=park["parkCode"] in whs,
        )
        # add relationships
        post_media(park["images"], park_id=park_id)
        post_topics(park["topics"], park_id)
        if len(park["operatingHours"]) > 0:
            post_standard_hours(park["operatingHours"][0]["standardHours"], park_id)
        # update
        db.session.add(park_instance)
    # commit data
    db.session.commit()
    return park_id


def post_events():
    """Collect all events data from National Park Service API"""
    event_id = 0
    no_park = 0
    state_list = {}  # keep track of events in each state
    for state in states:
        state_list[state] = set()
        start = 1
        while True:
            res = requests.get(
                EVENTS_URL
                + f"?pageNumber={start}&pageSize={PAGE_SIZE}&api_key="
                # + "?parkCode=wefa&api_key="
                + app.config["API_KEY"]
                + f"&stateCode={state}"
            )
            events = res.json()
            # post data to db
            event_id, no_park = get_events(
                events["data"], event_id, no_park, state_list[state]
            )
            print(
                f"Page number: {events['pagenumber']} Page size: {PAGE_SIZE} Total: {events['total']}"
            )
            if (int(events["pagenumber"]) * PAGE_SIZE) >= int(events["total"]):
                break
            start += 1
    for state in states:
        parks_in_state = (
            models.Parks.query.filter(models.Parks.state.contains(state))
            .filter(~models.Parks.events.any())
            .all()
        )
        for park in parks_in_state:
            park.events = random.sample(
                list(state_list[state]), min(len(state_list[state]), 10)
            )
    print("events without park: ", no_park)


def get_events(events, event_id, no_park, state_list):
    """Post a list of events with ids starting by event_id"""
    for event in events:
        # check if event is already loaded
        prev_event = models.Events.query.filter_by(title=event["title"]).first()
        if prev_event:
            state_list.add(prev_event)
            continue
        # prepare data
        park = models.Parks.query.filter_by(name=event["parkfullname"]).first()
        # skip events with no related park
        if not park:
            no_park += 1
            continue
        event_id += 1
        # create instance
        event_instance = models.Events(
            id=event_id,
            title=event["title"],
            category=(event["category"] == "Regular Event"),
            date=event["date"],
            is_free=(event["isfree"] == "true"),
            fee_info=event["feeinfo"],
            longitude=float(event["longitude"])
            if event["longitude"]
            else park.longitude,
            latitude=float(event["latitude"]) if event["latitude"] else park.latitude,
            description=event["description"],
            url=event["infourl"],
            contact_name=event["contactname"],
            contact_cell=event["contacttelephonenumber"],
            contact_email=event["contactemailaddress"],
            require_registration=event["isregresrequired"] == "true",
        )
        # add relationships
        post_media(event["images"], event_id=event_id)
        post_times(event["times"], event_id)
        post_event_tags(event["tags"], event_id)
        # update
        db.session.add(event_instance)
        # connect to related park
        park.events.append(event_instance)
        # add event to list of events in the state
        state_list.add(event_instance)
    # commit data
    db.session.commit()
    return (event_id, no_park)


def post_thingstodo():
    """Collect all things-to-do data from National Park Service API"""
    ttd_id = 0
    no_park = 0
    state_list = {}  # keep track of things to do in each state
    for state in states:
        state_list[state] = set()
        start = 2 if state == "LA" else 0
        while True:
            res = requests.get(
                TTD_URL
                + f"?start={start}&api_key="
                # + "?parkCode=wefa&api_key="
                + app.config["API_KEY"]
                + f"&stateCode={state}"
            )
            thingstodo = res.json()
            # post data to db
            ttd_id, no_park = get_ttd(
                thingstodo["data"], ttd_id, no_park, state_list[state]
            )
            print(f"Committing through ttd {ttd_id}")
            start += int(thingstodo["limit"])
            if start >= int(thingstodo["total"]):
                break
    for state in states:
        parks_in_state = (
            models.Parks.query.filter(models.Parks.state.contains(state))
            .filter(~models.Parks.thingstodo.any())
            .all()
        )
        for park in parks_in_state:
            park.thingstodo = random.sample(
                list(state_list[state]), min(len(state_list[state]), 10)
            )
    print("things-to-do without park: ", no_park)
    load_ttd_videos()


def get_ttd(thingstodo, ttd_id, no_park, state_list):
    """Post a list of things-to-do with ids starting by ttd_id"""
    for ttd in thingstodo:
        # skip things-to-do with no related park
        if len(ttd["relatedParks"]) == 0:
            no_park += 1
            continue
        # check if current ttd is loaded
        prev_ttd = models.Thingstodo.query.filter_by(title=ttd["title"]).first()
        if prev_ttd:
            state_list.add(prev_ttd)
            continue
        video_url = ""
        ttd_id += 1
        # create instance
        ttd_instance = models.Thingstodo(
            id=ttd_id,
            title=ttd["title"],
            seasons=",".join([x.lower() for x in ttd["season"]]),
            require_reservation=(ttd["isReservationRequired"] == "true"),
            has_fees=(ttd["doFeesApply"] == "true"),
            fee_description=ttd["feeDescription"],
            pets_allowed=(ttd["arePetsPermitted"] == "true"),
            duration=ttd["duration"],
            description=ttd["longDescription"],
            accessibility_info=ttd["accessibilityInformation"],
            time_of_day=",".join([x.lower() for x in ttd["timeOfDay"]]),
            video_url=video_url,
        )
        # add relationships
        post_media(ttd["images"], thingstodo_id=ttd_id)
        post_ttd_tags(ttd["tags"], ttd_id)
        # update
        db.session.add(ttd_instance)
        # connect many-many relationship
        connect_parks_thingstodo(ttd["relatedParks"], ttd_instance)
        # add ttd to list of events in state
        state_list.add(ttd_instance)
    # commit data
    db.session.commit()
    return (ttd_id, no_park)


def connect_parks_thingstodo(related_parks, ttd):
    """Add many-to-many relationship between parks and things-to-do"""
    assert len(related_parks) > 0
    list_parks = []
    for park in related_parks:
        list_parks.append(park["parkCode"])
    parks = models.Parks.query.filter(models.Parks.park_code.in_(list_parks)).all()
    if parks:
        for query_parks in parks:
            query_parks.thingstodo.append(ttd)
    else:
        print("no related ttd-parks found")


def get_fees(park):
    """Get minimum and maximum fee of a park"""
    entrance_fees = park["entranceFees"]
    entrance_passes = park["entrancePasses"]
    min_fee = float("inf")
    max_fee = -1.0
    for fee in entrance_fees:
        min_fee = min(min_fee, float(fee["cost"]))
        max_fee = max(max_fee, float(fee["cost"]))
    for fee in entrance_passes:
        min_fee = min(min_fee, float(fee["cost"]))
        max_fee = max(max_fee, float(fee["cost"]))
    if min_fee == float("inf"):
        min_fee = 0.0
    if max_fee == -1.0:
        max_fee = 0.0
    assert min_fee is not None
    assert max_fee is not None
    assert 0 <= min_fee <= max_fee < float("inf")
    return min_fee, max_fee


def post_standard_hours(hours, park_id):
    """Post standard hours information of a park"""
    days = [
        "sunday",
        "monday",
        "tuesday",
        "wednesday",
        "thursday",
        "friday",
        "saturday",
    ]
    for day in days:
        hour_instance = models.StandardHours(day=day, time=hours[day], park_id=park_id)
        db.session.add(hour_instance)


def post_topics(topics, park_id):
    """Post topics/tags of a park"""
    for topic in topics:
        tag_instance = models.Tags(name=topic["name"], park_id=park_id)
        db.session.add(tag_instance)


def post_event_tags(tags, event_id):
    """Post tags of an event"""
    for tag in tags:
        tag_instance = models.Tags(name=tag.strip(), event_id=event_id)
        db.session.add(tag_instance)


def post_media(images, park_id=None, event_id=None, thingstodo_id=None):
    """Post images of an instance"""
    for image in images:
        url = (
            image["url"]
            if "https://www.nps.gov" in image["url"]
            else "https://www.nps.gov" + image["url"]
        )
        image_instance = models.Media(
            url=url,
            title=image["title"],
            alt_text=image["altText"],
            caption=image["caption"],
            credit=image["credit"],
            park_id=park_id,
            event_id=event_id,
            thingstodo_id=thingstodo_id,
        )
        db.session.add(image_instance)


def post_times(times, event_id):
    """Post time information (start time and end time) of an event"""
    for time in times:
        start = time["timestart"] if time["sunrisestart"] == "false" else "sunrise"
        end = time["timeend"] if time["sunsetend"] == "false" else "sunset"
        time_instance = models.Time(start=start, end=end, event_id=event_id)
        db.session.add(time_instance)


# def get_seasons(seasons):
#     """Represent seasons in binary int"""
#     seasons = [x.lower() for x in seasons]
#     base_seasons = ["winter", "spring", "summer", "fall"]
#     binary = ""
#     for season in base_seasons:
#         binary += "1" if season in seasons else "0"
#     return int(binary, 2)


# def get_time_of_day(times):
#     """Represent time of day in binary int"""
#     times = [x.lower() for x in times]
#     base_times = ["dawn", "day", "dusk", "night"]
#     binary = ""
#     for time in base_times:
#         binary += "1" if time in times else "0"
#     return int(binary, 2)


def post_ttd_tags(tags, ttd_id):
    """Post tags of a things-to-do instance"""
    for tag in tags:
        tag_instance = models.Tags(name=tag.strip(), thingstodo_id=ttd_id)
        db.session.add(tag_instance)


def load_ttd_videos():
    """Update videos_url for things-to-do instances"""
    missing = (
        db.session.query(models.Thingstodo)
        .filter(models.Thingstodo.video_url == "")
        .all()
    )
    for query in missing:
        # pylint: disable=line-too-long
        # print(type(query.parks[0].name))
        search_keyword = f"{query.parks[0].name.replace(' ', '+')}+{query.title.replace(' ','+')}".encode(
            "ascii", "ignore"
        ).decode(
            "ascii"
        )
        # print(search_keyword)
        print("making request...", end="")
        html = urllib.request.urlopen(
            ("https://www.youtube.com/results?search_query=" + search_keyword)
        )
        video_ids = re.findall(r"watch\?v=(\S{11})", html.read().decode())
        query.video_url = f"https://youtube.com/watch?v={video_ids[0]}"
        db.session.commit()
        print("updated")


def relate_events_ttd():
    """Add many-to-many relationship between events and things-to-do"""
    query = (
        db.session.query(models.ParksToThingsToDo, models.ParksToEvents)
        .join(
            models.ParksToEvents,
            models.ParksToEvents.c.park_id == models.ParksToThingsToDo.c.park_id,
        )
        .with_entities(
            models.ParksToEvents.c.event_id, models.ParksToThingsToDo.c.thingstodo_id
        )
        .all()
    )
    num = 0
    for row in query:
        event = models.Events.query.get(row[0])
        ttd = models.Thingstodo.query.get(row[1])
        if ttd not in event.thingstodo:
            event.thingstodo.append(ttd)
            num += 1
        if num % 100 == 0 and num > 0:
            db.session.commit()
    db.session.commit()
