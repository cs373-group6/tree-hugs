"""Backend tests"""
# pylint: disable = too-many-statements
# pylint: disable = too-many-public-methods
# pylint: disable = too-many-instance-attributes

from unittest import main, TestCase
import json
import random
from init import app
from index import PARKS_PER_PAGE, EVENTS_PER_PAGE, TTD_PER_PAGE
import models
import data
import mockdata

app.config.from_object("appconfig.TestingConfig")

PARKS = "/api/parks"
EVENTS = "/api/events"
TTD = "/api/thingstodo"
EVENTS_DATES = "/api/events-dates"


class Tests(TestCase):
    """Unit test class"""

    def setUp(self):
        self.app = app.test_client(self)
        # create mock database
        models.create()
        # post mock data
        filtered_parks = set()
        events_state_lst = set()
        ttd_state_lst = set()
        data.get_parks(mockdata.parks, 0, filtered_parks)
        data.get_events(mockdata.events, 0, 0, events_state_lst)
        data.get_ttd(mockdata.thingstodo, 0, 0, ttd_state_lst)
        data.relate_events_ttd()
        # important numbers from mock data
        self.num_parks = len(mockdata.parks)
        self.num_events = len(mockdata.events)
        self.num_ttd = len(mockdata.thingstodo)
        self.num_events_dates = len(mockdata.events)
        self.park_pages = self.num_parks // PARKS_PER_PAGE
        if self.num_parks % PARKS_PER_PAGE:
            self.park_pages += 1
        self.event_pages = self.num_events // EVENTS_PER_PAGE
        if self.num_events % EVENTS_PER_PAGE:
            self.event_pages += 1
        self.ttd_pages = self.num_ttd // TTD_PER_PAGE
        if self.num_ttd % TTD_PER_PAGE:
            self.ttd_pages += 1

    def tearDown(self):
        models.drop()

    # pylint: disable = missing-docstring

    # Index Tests

    def test_index_status(self):
        self.check_status("/")

    # Parks Tests

    def test_parks_status(self):
        self.check_status(PARKS)

    def test_parks_content_type(self):
        self.check_content_type(PARKS)

    def test_parks_fields(self):
        res = self.app.get(PARKS)
        self.check_data_fields(res.data)

    def test_parks_num(self):
        self.check_instance_num(PARKS, self.num_parks)

    def test_parks_total_pages(self):
        self.check_total_pages(PARKS, self.park_pages)

    def test_parks_objects(self):
        self.check_objects(PARKS)

    def test_parks_pagination(self):
        self.check_pagination(PARKS, self.num_parks, self.park_pages, PARKS_PER_PAGE)

    def test_parks_instance(self):
        self.check_instance(
            PARKS, self.park_pages, mockdata.parks, self.check_park_instance
        )

    def test_parks_instance_by_id(self):
        self.check_instance_by_id(
            PARKS, self.num_parks, mockdata.parks, self.check_park_instance
        )

    def test_parks_filter_by_state(self):
        res = self.app.get(f"{PARKS}?state=SD")
        self.assertEqual(res.status_code, 200)
        self.check_data_fields(res.data)
        res_data = json.loads(res.data.decode("utf-8"))
        self.assertEqual(res_data["num_results"], 1)

    def test_parks_filter_by_state_multiple(self):
        res = self.app.get(f"{PARKS}?state=SD,VA")
        self.assertEqual(res.status_code, 200)
        self.check_data_fields(res.data)
        res_data = json.loads(res.data.decode("utf-8"))
        self.assertEqual(res_data["num_results"], 2)

    def test_parks_filter_by_world_heritage(self):
        res = self.app.get(f"{PARKS}?world_heritage=True")
        self.assertEqual(res.status_code, 200)
        self.check_data_fields(res.data)
        res_data = json.loads(res.data.decode("utf-8"))
        self.assertEqual(res_data["num_results"], 0)

    def test_parks_filter_by_not_world_heritage(self):
        res = self.app.get(f"{PARKS}?world_heritage=False")
        self.assertEqual(res.status_code, 200)
        self.check_data_fields(res.data)
        res_data = json.loads(res.data.decode("utf-8"))
        self.assertEqual(res_data["num_results"], 3)

    def test_parks_filter_by_min_budget(self):
        res = self.app.get(f"{PARKS}?min_budget=5")
        self.assertEqual(res.status_code, 200)
        self.check_data_fields(res.data)
        res_data = json.loads(res.data.decode("utf-8"))
        self.assertEqual(res_data["num_results"], 1)

    def test_parks_filter_by_zero_min_budget(self):
        res = self.app.get(f"{PARKS}?min_budget=0")
        self.assertEqual(res.status_code, 200)
        self.check_data_fields(res.data)
        res_data = json.loads(res.data.decode("utf-8"))
        self.assertEqual(res_data["num_results"], self.num_parks)

    def test_parks_filter_by_max_budget(self):
        res = self.app.get(f"{PARKS}?max_budget=10")
        self.assertEqual(res.status_code, 200)
        self.check_data_fields(res.data)
        res_data = json.loads(res.data.decode("utf-8"))
        self.assertEqual(res_data["num_results"], 2)

    def test_parks_filter_by_day(self):
        res = self.app.get(f"{PARKS}?day=sunday")
        self.assertEqual(res.status_code, 200)
        self.check_data_fields(res.data)
        res_data = json.loads(res.data.decode("utf-8"))
        self.assertEqual(res_data["num_results"], 2)

    def test_parks_sort_by_name_asc(self):
        res = self.app.get(f"{PARKS}?name=asc")
        self.assertEqual(res.status_code, 200)
        self.check_data_fields(res.data)
        res_data = json.loads(res.data.decode("utf-8"))
        self.assertEqual(res_data["num_results"], self.num_parks)
        prev_park = ""
        for park in res_data["objects"]:
            self.assertTrue(prev_park <= park["name"])
            prev_park = park["name"]

    def test_parks_sort_by_name_desc(self):
        res = self.app.get(f"{PARKS}?name=desc")
        self.assertEqual(res.status_code, 200)
        self.check_data_fields(res.data)
        res_data = json.loads(res.data.decode("utf-8"))
        self.assertEqual(res_data["num_results"], self.num_parks)
        prev_park = "~"
        for park in res_data["objects"]:
            self.assertTrue(prev_park >= park["name"])
            prev_park = park["name"]

    # Events Tests

    def test_events_status(self):
        self.check_status(EVENTS)

    def test_events_content_type(self):
        self.check_content_type(EVENTS)

    def test_events_fields(self):
        res = self.app.get(EVENTS)
        self.check_data_fields(res.data)

    def test_events_num(self):
        self.check_instance_num(EVENTS, self.num_events)

    def test_events_total_pages(self):
        self.check_total_pages(EVENTS, self.event_pages)

    def test_events_objects(self):
        self.check_objects(EVENTS)

    def test_events_pagination(self):
        self.check_pagination(
            EVENTS, self.num_events, self.event_pages, EVENTS_PER_PAGE
        )

    def test_events_instance(self):
        self.check_instance(
            EVENTS, self.event_pages, mockdata.events, self.check_event_instance
        )

    def test_events_instance_by_id(self):
        self.check_instance_by_id(
            EVENTS, self.num_events, mockdata.events, self.check_event_instance
        )

    def test_events_filter_by_category_regular(self):
        res = self.app.get(f"{EVENTS}?category=True")
        self.assertEqual(res.status_code, 200)
        self.check_data_fields(res.data)
        res_data = json.loads(res.data.decode("utf-8"))
        self.assertEqual(res_data["num_results"], 1)

    def test_events_filter_by_category_special(self):
        res = self.app.get(f"{EVENTS}?category=False")
        self.assertEqual(res.status_code, 200)
        self.check_data_fields(res.data)
        res_data = json.loads(res.data.decode("utf-8"))
        self.assertEqual(res_data["num_results"], 2)

    def test_events_filter_by_free(self):
        res = self.app.get(f"{EVENTS}?free=True")
        self.assertEqual(res.status_code, 200)
        self.check_data_fields(res.data)
        res_data = json.loads(res.data.decode("utf-8"))
        self.assertEqual(res_data["num_results"], 3)

    def test_events_filter_by_not_free(self):
        res = self.app.get(f"{EVENTS}?free=False")
        self.assertEqual(res.status_code, 200)
        self.check_data_fields(res.data)
        res_data = json.loads(res.data.decode("utf-8"))
        self.assertEqual(res_data["num_results"], 0)

    def test_events_filter_by_registration(self):
        res = self.app.get(f"{EVENTS}?registration=True")
        self.assertEqual(res.status_code, 200)
        self.check_data_fields(res.data)
        res_data = json.loads(res.data.decode("utf-8"))
        self.assertEqual(res_data["num_results"], 2)

    def test_events_filter_by_not_registration(self):
        res = self.app.get(f"{EVENTS}?registration=False")
        self.assertEqual(res.status_code, 200)
        self.check_data_fields(res.data)
        res_data = json.loads(res.data.decode("utf-8"))
        self.assertEqual(res_data["num_results"], 1)

    def test_events_sort_by_date_asc(self):
        res = self.app.get(f"{EVENTS}?date=asc")
        self.assertEqual(res.status_code, 200)
        self.check_data_fields(res.data)
        res_data = json.loads(res.data.decode("utf-8"))
        self.assertEqual(res_data["num_results"], self.num_events)
        prev_event = ""
        for event in res_data["objects"]:
            self.assertTrue(prev_event <= event["date"])
            prev_event = event["date"]

    def test_events_sort_by_date_desc(self):
        res = self.app.get(f"{EVENTS}?date=desc")
        self.assertEqual(res.status_code, 200)
        self.check_data_fields(res.data)
        res_data = json.loads(res.data.decode("utf-8"))
        self.assertEqual(res_data["num_results"], self.num_events)
        prev_event = "~"
        for event in res_data["objects"]:
            self.assertTrue(prev_event >= event["date"])
            prev_event = event["date"]

    def test_events_sort_by_title_asc(self):
        res = self.app.get(f"{EVENTS}?title=asc")
        self.assertEqual(res.status_code, 200)
        self.check_data_fields(res.data)
        res_data = json.loads(res.data.decode("utf-8"))
        self.assertEqual(res_data["num_results"], self.num_events)
        prev_event = ""
        for event in res_data["objects"]:
            self.assertTrue(prev_event <= event["title"])
            prev_event = event["title"]

    def test_events_sort_by_title_desc(self):
        res = self.app.get(f"{EVENTS}?title=desc")
        self.assertEqual(res.status_code, 200)
        self.check_data_fields(res.data)
        res_data = json.loads(res.data.decode("utf-8"))
        self.assertEqual(res_data["num_results"], self.num_events)
        prev_event = "~"
        for event in res_data["objects"]:
            self.assertTrue(prev_event >= event["title"])
            prev_event = event["title"]

    # ThingsToDo Tests

    def test_ttd_status(self):
        self.check_status(TTD)

    def test_ttd_content_type(self):
        self.check_content_type(TTD)

    def test_ttd_fields(self):
        res = self.app.get(TTD)
        self.check_data_fields(res.data)

    def test_ttd_num(self):
        self.check_instance_num(TTD, self.num_ttd)

    def test_ttd_total_pages(self):
        self.check_total_pages(TTD, self.ttd_pages)

    def test_ttd_objects(self):
        self.check_objects(TTD)

    def test_ttd_pagination(self):
        self.check_pagination(TTD, self.num_ttd, self.ttd_pages, TTD_PER_PAGE)

    def test_ttd_instance(self):
        self.check_instance(
            TTD, self.ttd_pages, mockdata.thingstodo, self.check_ttd_instance
        )

    def test_ttd_instance_by_id(self):
        self.check_instance_by_id(
            TTD, self.num_ttd, mockdata.thingstodo, self.check_ttd_instance
        )

    def test_ttd_filter_by_seasons(self):
        res = self.app.get(f"{TTD}?seasons=winter")
        self.assertEqual(res.status_code, 200)
        self.check_data_fields(res.data)
        res_data = json.loads(res.data.decode("utf-8"))
        self.assertEqual(res_data["num_results"], 2)

    def test_ttd_filter_by_seasons_multiple(self):
        res = self.app.get(f"{TTD}?seasons=winter,spring")
        self.assertEqual(res.status_code, 200)
        self.check_data_fields(res.data)
        res_data = json.loads(res.data.decode("utf-8"))
        self.assertEqual(res_data["num_results"], 3)

    def test_ttd_filter_by_reservation(self):
        res = self.app.get(f"{TTD}?reservation=True")
        self.assertEqual(res.status_code, 200)
        self.check_data_fields(res.data)
        res_data = json.loads(res.data.decode("utf-8"))
        self.assertEqual(res_data["num_results"], 0)

    def test_ttd_filter_by_not_reservation(self):
        res = self.app.get(f"{TTD}?reservation=False")
        self.assertEqual(res.status_code, 200)
        self.check_data_fields(res.data)
        res_data = json.loads(res.data.decode("utf-8"))
        self.assertEqual(res_data["num_results"], 3)

    def test_ttd_filter_by_fees(self):
        res = self.app.get(f"{TTD}?fees=True")
        self.assertEqual(res.status_code, 200)
        self.check_data_fields(res.data)
        res_data = json.loads(res.data.decode("utf-8"))
        self.assertEqual(res_data["num_results"], 0)

    def test_ttd_filter_by_not_fees(self):
        res = self.app.get(f"{TTD}?fees=False")
        self.assertEqual(res.status_code, 200)
        self.check_data_fields(res.data)
        res_data = json.loads(res.data.decode("utf-8"))
        self.assertEqual(res_data["num_results"], 3)

    def test_ttd_filter_by_pets(self):
        res = self.app.get(f"{TTD}?pets=True")
        self.assertEqual(res.status_code, 200)
        self.check_data_fields(res.data)
        res_data = json.loads(res.data.decode("utf-8"))
        self.assertEqual(res_data["num_results"], 2)

    def test_ttd_filter_by_not_pets(self):
        res = self.app.get(f"{TTD}?pets=False")
        self.assertEqual(res.status_code, 200)
        self.check_data_fields(res.data)
        res_data = json.loads(res.data.decode("utf-8"))
        self.assertEqual(res_data["num_results"], 1)

    def test_ttd_sort_by_title_asc(self):
        res = self.app.get(f"{TTD}?title=asc")
        self.assertEqual(res.status_code, 200)
        self.check_data_fields(res.data)
        res_data = json.loads(res.data.decode("utf-8"))
        self.assertEqual(res_data["num_results"], self.num_ttd)
        prev_ttd = ""
        for event in res_data["objects"]:
            self.assertTrue(prev_ttd <= event["title"])
            prev_ttd = event["title"]

    def test_ttd_sort_by_title_desc(self):
        res = self.app.get(f"{TTD}?title=desc")
        self.assertEqual(res.status_code, 200)
        self.check_data_fields(res.data)
        res_data = json.loads(res.data.decode("utf-8"))
        self.assertEqual(res_data["num_results"], self.num_ttd)
        prev_ttd = "~"
        for event in res_data["objects"]:
            self.assertTrue(prev_ttd >= event["title"])
            prev_ttd = event["title"]

    # Events Dates Tests

    def test_events_dates_status(self):
        self.check_status(EVENTS_DATES)

    def test_events_dates_content_type(self):
        self.check_content_type(EVENTS_DATES)

    def test_events_dates_fields(self):
        res = self.app.get(EVENTS_DATES)
        self.assertTrue(b"num_results" in res.data)
        self.assertTrue(b"objects" in res.data)

    def test_events_dates_num(self):
        self.check_instance_num(EVENTS_DATES, self.num_events_dates)

    def test_events_dates_objects(self):
        self.check_objects(EVENTS_DATES)

    def test_events_dates_instance(self):
        res = self.app.get(EVENTS_DATES)
        res_data = json.loads(res.data.decode("utf-8"))
        count = 0
        for instance in res_data["objects"]:
            self.assertTrue("date" in instance)
            self.assertTrue(isinstance(instance["date"], str))
            self.assertTrue("num_events" in instance)
            self.assertTrue(isinstance(instance["num_events"], int))
            count += instance["num_events"]
        self.assertEqual(count, self.num_events)

    # pylint: enable = missing-docstring

    # Helper Functions

    def check_status(self, url):
        """Check url return status"""
        res = self.app.get(url)
        self.assertEqual(res.status_code, 200)

    def check_content_type(self, url):
        """Check if content type is application/json"""
        res = self.app.get(url)
        self.assertEqual(res.content_type, "application/json")

    def check_data_fields(self, res_data):
        """Check data fields"""
        self.assertTrue(b"num_results" in res_data)
        self.assertTrue(b"objects" in res_data)
        self.assertTrue(b"page" in res_data)
        self.assertTrue(b"total_pages" in res_data)

    def check_instance_num(self, url, expect_num):
        """Check if total number of instances meet expectation"""
        res = self.app.get(url)
        res_data = json.loads(res.data.decode("utf-8"))
        self.assertEqual(res_data["num_results"], expect_num)

    def check_total_pages(self, url, expect_pages):
        """Check if the total pages of an API meet expectation"""
        res = self.app.get(url)
        res_data = json.loads(res.data.decode("utf-8"))
        self.assertEqual(res_data["total_pages"], expect_pages)

    def check_objects(self, url):
        """Check if API returns valid data"""
        res = self.app.get(url)
        res_data = json.loads(res.data.decode("utf-8"))
        self.assertTrue(len(res_data["objects"]) >= 0)

    def check_pagination(self, url, num_results, num_pages, page_size):
        """Check pagination"""
        page_min = 1
        page_max = num_pages
        page = random.randint(page_min, page_max)
        res = self.app.get(url + "?page=" + str(page))
        self.check_data_fields(res.data)
        # check data is correct
        res_data = json.loads(res.data.decode("utf-8"))
        self.assertEqual(res_data["num_results"], num_results)
        self.assertEqual(res_data["total_pages"], num_pages)
        self.assertEqual(res_data["page"], page)
        if -num_pages < page <= num_pages and page != 0:
            self.assertTrue(0 < len(res_data["objects"]) <= page_size)
        else:
            self.assertTrue(len(res_data["objects"]) == 0)

    def check_instance(self, url, page_limit, expected, check):
        """Check a random instances"""
        page = random.randint(1, page_limit)
        res = self.app.get(url + "?page=" + str(page))
        res_data = json.loads(res.data.decode("utf-8"))
        item = random.randint(0, len(res_data["objects"]) - 1)
        instance = res_data["objects"][item]
        index = (page - 1) * PARKS_PER_PAGE + item
        check(instance, expected[index])

    def check_instance_by_id(self, url, limit, expected, check):
        """Check a random instances by id"""
        instance_id = random.randint(1, limit)
        res = self.app.get(url + "/id=" + str(instance_id))
        # test api
        self.assertEqual(res.status_code, 200)
        self.assertEqual(res.content_type, "application/json")
        # test data
        res_data = json.loads(res.data.decode("utf-8"))
        check(res_data, expected[instance_id - 1])

    def check_park_instance(self, instance, expected):
        """Check parks data schema"""
        self.assertTrue("id" in instance)
        self.assertTrue(isinstance(instance["id"], int))
        self.assertTrue("name" in instance)
        self.assertTrue(isinstance(instance["name"], str))
        self.assertEqual(instance["name"], expected["fullName"])
        self.assertTrue("state" in instance)
        self.assertTrue(isinstance(instance["state"], str))
        self.assertEqual(instance["state"], expected["states"])
        self.assertTrue("park_code" in instance)
        self.assertTrue(isinstance(instance["park_code"], str))
        self.assertTrue(len(instance["park_code"]) == 4)
        self.assertEqual(instance["park_code"], expected["parkCode"])
        self.assertTrue("min_fee" in instance)
        self.assertTrue(isinstance(instance["min_fee"], float))
        self.assertTrue("max_fee" in instance)
        self.assertTrue(isinstance(instance["max_fee"], float))
        self.assertTrue("directions_url" in instance)
        self.assertTrue(isinstance(instance["directions_url"], str))
        self.assertEqual(instance["directions_url"], expected["directionsUrl"])
        self.assertTrue("longitude" in instance)
        self.assertTrue(isinstance(instance["longitude"], float))
        self.assertEqual(instance["longitude"], float(expected["longitude"]))
        self.assertTrue("latitude" in instance)
        self.assertTrue(isinstance(instance["latitude"], float))
        self.assertEqual(instance["latitude"], float(expected["latitude"]))
        self.assertTrue("description" in instance)
        self.assertTrue(isinstance(instance["description"], str))
        self.assertEqual(instance["description"], expected["description"])
        self.assertTrue("phone" in instance)
        self.assertTrue(isinstance(instance["phone"], str))
        self.assertTrue("email" in instance)
        self.assertTrue(isinstance(instance["email"], str))
        self.assertTrue("url" in instance)
        self.assertTrue(isinstance(instance["url"], str))
        self.assertEqual(instance["url"], expected["url"])
        self.assertTrue("weather" in instance)
        self.assertTrue(isinstance(instance["weather"], str))
        self.assertEqual(instance["weather"], expected["weatherInfo"])
        self.assertTrue("stdhour_description" in instance)
        self.assertTrue(isinstance(instance["stdhour_description"], str))
        self.assertTrue("world_heritage" in instance)
        self.assertTrue(isinstance(instance["world_heritage"], bool))
        self.assertTrue("media" in instance)
        self.assertTrue(isinstance(instance["media"], list))
        if len(instance["media"]) > 0:
            index = random.randint(0, len(instance["media"]) - 1)
            self.check_media(instance["media"][index])
        self.assertTrue("standard_hours" in instance)
        self.assertTrue(isinstance(instance["standard_hours"], list))
        if len(instance["standard_hours"]) > 0:
            index = random.randint(0, len(instance["standard_hours"]) - 1)
            self.check_standard_hours(instance["standard_hours"][index])
        self.assertTrue("tags" in instance)
        self.assertTrue(isinstance(instance["tags"], list))
        if len(instance["tags"]) > 0:
            index = random.randint(0, len(instance["tags"]) - 1)
            self.check_tags(instance["tags"][index])
        self.assertTrue("events" in instance)
        self.assertTrue(isinstance(instance["events"], list))
        self.assertTrue(len(instance["events"]) > 0)
        index = random.randint(0, len(instance["events"]) - 1)
        self.check_events(instance["events"][index])
        res = self.app.get(EVENTS + "/id=" + str(instance["events"][index]["id"]))
        self.assertEqual(res.status_code, 200)
        res_data = json.loads(res.data.decode("utf-8"))
        self.assertTrue(
            any(
                instance["id"] == res_data["parks"][i]["id"]
                for i in range(len(res_data["parks"]))
            )
        )
        self.assertTrue("thingstodo" in instance)
        self.assertTrue(isinstance(instance["thingstodo"], list))
        self.assertTrue(len(instance["thingstodo"]) > 0)
        index = random.randint(0, len(instance["thingstodo"]) - 1)
        self.check_ttd(instance["thingstodo"][index])
        res = self.app.get(TTD + "/id=" + str(instance["thingstodo"][index]["id"]))
        self.assertEqual(res.status_code, 200)
        res_data = json.loads(res.data.decode("utf-8"))
        self.assertTrue(
            any(
                instance["id"] == res_data["parks"][i]["id"]
                for i in range(len(res_data["parks"]))
            )
        )

    def check_event_instance(self, instance, expected):
        """Check events data schema"""
        self.assertTrue("id" in instance)
        self.assertTrue(isinstance(instance["id"], int))
        self.assertTrue("title" in instance)
        self.assertTrue(isinstance(instance["title"], str))
        self.assertEqual(instance["title"], expected["title"])
        self.assertTrue("category" in instance)
        self.assertTrue(isinstance(instance["category"], bool))
        self.assertTrue(
            expected["category"] == "Regular Event"
            if instance["category"]
            else expected["category"] == "Special Event"
        )
        self.assertTrue("date" in instance)
        self.assertTrue(isinstance(instance["date"], str))
        self.assertEqual(instance["date"], expected["date"])
        self.assertTrue("is_free" in instance)
        self.assertTrue(isinstance(instance["is_free"], bool))
        self.assertTrue(
            expected["isfree"] == "true"
            if instance["is_free"]
            else expected["isfree"] == "false"
        )
        self.assertTrue("fee_info" in instance)
        self.assertTrue(isinstance(instance["fee_info"], str))
        self.assertEqual(instance["fee_info"], expected["feeinfo"])
        self.assertTrue("longitude" in instance)
        self.assertTrue(isinstance(instance["longitude"], float))
        self.assertEqual(instance["longitude"], float(expected["longitude"]))
        self.assertTrue("latitude" in instance)
        self.assertTrue(isinstance(instance["latitude"], float))
        self.assertEqual(instance["latitude"], float(expected["latitude"]))
        self.assertTrue("description" in instance)
        self.assertTrue(isinstance(instance["description"], str))
        self.assertEqual(instance["description"], expected["description"])
        self.assertTrue("url" in instance)
        self.assertTrue(isinstance(instance["url"], str))
        self.assertEqual(instance["url"], expected["infourl"])
        self.assertTrue("contact_name" in instance)
        self.assertTrue(isinstance(instance["contact_name"], str))
        self.assertEqual(instance["contact_name"], expected["contactname"])
        self.assertTrue("contact_cell" in instance)
        self.assertTrue(isinstance(instance["contact_cell"], str))
        self.assertEqual(instance["contact_cell"], expected["contacttelephonenumber"])
        self.assertTrue("contact_email" in instance)
        self.assertTrue(isinstance(instance["contact_email"], str))
        self.assertEqual(instance["contact_email"], expected["contactemailaddress"])
        self.assertTrue(isinstance(instance["require_registration"], bool))
        self.assertTrue(
            expected["isregresrequired"] == "true"
            if instance["require_registration"]
            else expected["isregresrequired"] == "false"
        )
        self.assertTrue("media" in instance)
        self.assertTrue(isinstance(instance["media"], list))
        if len(instance["media"]) > 0:
            index = random.randint(0, len(instance["media"]) - 1)
            self.check_media(instance["media"][index])
        self.assertTrue("times" in instance)
        self.assertTrue(isinstance(instance["times"], list))
        if len(instance["times"]) > 0:
            index = random.randint(0, len(instance["times"]) - 1)
            self.check_time(instance["times"][index])
        self.assertTrue("tags" in instance)
        self.assertTrue(isinstance(instance["tags"], list))
        if len(instance["tags"]) > 0:
            index = random.randint(0, len(instance["tags"]) - 1)
            self.check_tags(instance["tags"][index])
        self.assertTrue("parks" in instance)
        self.assertTrue(isinstance(instance["parks"], list))
        self.assertTrue(len(instance["parks"]) > 0)
        index = random.randint(0, len(instance["parks"]) - 1)
        self.check_parks(instance["parks"][index])
        res = self.app.get(PARKS + "/id=" + str(instance["parks"][index]["id"]))
        self.assertEqual(res.status_code, 200)
        res_data = json.loads(res.data.decode("utf-8"))
        self.assertTrue(
            any(
                instance["id"] == res_data["events"][i]["id"]
                for i in range(len(res_data["events"]))
            )
        )
        self.assertTrue("thingstodo" in instance)
        self.assertTrue(isinstance(instance["thingstodo"], list))
        self.assertTrue(len(instance["thingstodo"]) > 0)
        index = random.randint(0, len(instance["thingstodo"]) - 1)
        self.check_ttd(instance["thingstodo"][index])
        res = self.app.get(TTD + "/id=" + str(instance["thingstodo"][index]["id"]))
        self.assertEqual(res.status_code, 200)
        res_data = json.loads(res.data.decode("utf-8"))
        self.assertTrue(
            any(
                instance["id"] == res_data["events"][i]["id"]
                for i in range(len(res_data["events"]))
            )
        )

    def check_ttd_instance(self, instance, expected):
        """Check things-to-do data schema"""
        self.assertTrue("id" in instance)
        self.assertTrue(isinstance(instance["id"], int))
        self.assertTrue("title" in instance)
        self.assertTrue(isinstance(instance["title"], str))
        self.assertEqual(instance["title"], expected["title"])
        self.assertTrue("seasons" in instance)
        self.assertTrue(isinstance(instance["seasons"], str))
        self.assertTrue("require_reservation" in instance)
        self.assertTrue(isinstance(instance["require_reservation"], bool))
        self.assertTrue(
            expected["isReservationRequired"] == "true"
            if instance["require_reservation"]
            else expected["isReservationRequired"] == "false"
        )
        self.assertTrue("has_fees" in instance)
        self.assertTrue(isinstance(instance["has_fees"], bool))
        self.assertTrue(
            expected["doFeesApply"] == "true"
            if instance["has_fees"]
            else expected["doFeesApply"] == "false"
        )
        self.assertTrue("fee_description" in instance)
        self.assertTrue(isinstance(instance["fee_description"], str))
        self.assertEqual(instance["fee_description"], expected["feeDescription"])
        self.assertTrue("pets_allowed" in instance)
        self.assertTrue(isinstance(instance["pets_allowed"], bool))
        self.assertTrue(
            expected["arePetsPermitted"] == "true"
            if instance["pets_allowed"]
            else expected["arePetsPermitted"] == "false"
        )
        self.assertTrue("duration" in instance)
        self.assertTrue(isinstance(instance["duration"], str))
        self.assertEqual(instance["duration"], expected["duration"])
        self.assertTrue("description" in instance)
        self.assertTrue(isinstance(instance["description"], str))
        self.assertEqual(instance["description"], expected["longDescription"])
        self.assertTrue("accessibility_info" in instance)
        self.assertTrue(isinstance(instance["accessibility_info"], str))
        self.assertEqual(
            instance["accessibility_info"], expected["accessibilityInformation"]
        )
        self.assertTrue("time_of_day" in instance)
        self.assertTrue(isinstance(instance["time_of_day"], str))
        self.assertTrue("video_url" in instance)
        self.assertTrue(isinstance(instance["video_url"], str))
        self.assertTrue("media" in instance)
        self.assertTrue(isinstance(instance["media"], list))
        if len(instance["media"]) > 0:
            index = random.randint(0, len(instance["media"]) - 1)
            self.check_media(instance["media"][index])
        self.assertTrue("tags" in instance)
        self.assertTrue(isinstance(instance["tags"], list))
        if len(instance["tags"]) > 0:
            index = random.randint(0, len(instance["tags"]) - 1)
            self.check_tags(instance["tags"][index])
        self.assertTrue("parks" in instance)
        self.assertTrue(isinstance(instance["parks"], list))
        self.assertTrue(len(instance["parks"]) > 0)
        index = random.randint(0, len(instance["parks"]) - 1)
        self.check_parks(instance["parks"][index])
        res = self.app.get(PARKS + "/id=" + str(instance["parks"][index]["id"]))
        self.assertEqual(res.status_code, 200)
        res_data = json.loads(res.data.decode("utf-8"))
        self.assertTrue(
            any(
                instance["id"] == res_data["thingstodo"][i]["id"]
                for i in range(len(res_data["thingstodo"]))
            )
        )
        self.assertTrue("events" in instance)
        self.assertTrue(isinstance(instance["events"], list))
        self.assertTrue(len(instance["events"]) > 0)
        index = random.randint(0, len(instance["events"]) - 1)
        self.check_events(instance["events"][index])
        res = self.app.get(EVENTS + "/id=" + str(instance["events"][index]["id"]))
        self.assertEqual(res.status_code, 200)
        res_data = json.loads(res.data.decode("utf-8"))
        self.assertTrue(
            any(
                instance["id"] == res_data["thingstodo"][i]["id"]
                for i in range(len(res_data["thingstodo"]))
            )
        )

    def check_media(self, media):
        """Check media data schema"""
        self.assertTrue("id" not in media)
        self.assertTrue("url" in media)
        self.assertTrue(isinstance(media["url"], str))
        self.assertTrue("title" in media)
        self.assertTrue(isinstance(media["title"], str))
        self.assertTrue("alt_text" in media)
        self.assertTrue(isinstance(media["alt_text"], str))
        self.assertTrue("caption" in media)
        self.assertTrue(isinstance(media["caption"], str))
        self.assertTrue("credit" in media)
        self.assertTrue(isinstance(media["credit"], str))
        self.assertTrue("park_id" not in media)
        self.assertTrue("event_id" not in media)
        self.assertTrue("thingstodo_id" not in media)

    def check_standard_hours(self, standard_hours):
        """Check standard hours data schema"""
        self.assertTrue("id" not in standard_hours)
        self.assertTrue("day" in standard_hours)
        self.assertTrue(isinstance(standard_hours["day"], str))
        self.assertTrue("time" in standard_hours)
        self.assertTrue(isinstance(standard_hours["time"], str))
        self.assertTrue("park_id" not in standard_hours)

    def check_tags(self, tag):
        """Check tags data schema"""
        self.assertTrue("id" not in tag)
        self.assertTrue("name" in tag)
        self.assertTrue(isinstance(tag["name"], str))
        self.assertTrue("park_id" not in tag)
        self.assertTrue("event_id" not in tag)
        self.assertTrue("thingstodo_id" not in tag)

    def check_time(self, time):
        """Check time data schema"""
        self.assertTrue("id" not in time)
        self.assertTrue("start" in time)
        self.assertTrue(isinstance(time["start"], str))
        self.assertTrue("end" in time)
        self.assertTrue(isinstance(time["end"], str))
        self.assertTrue("event_id" not in time)

    def check_events(self, event):
        """Check events data schema in parks instance"""
        self.assertTrue("id" in event)
        self.assertTrue(isinstance(event["id"], int))
        self.assertTrue("title" in event)
        self.assertTrue(isinstance(event["title"], str))
        self.assertTrue("category" not in event)
        self.assertTrue("date" not in event)
        self.assertTrue("is_free" not in event)
        self.assertTrue("fee_info" not in event)
        self.assertTrue("longitude" not in event)
        self.assertTrue("latitude" not in event)
        self.assertTrue("description" not in event)
        self.assertTrue("url" not in event)
        self.assertTrue("contact_name" not in event)
        self.assertTrue("contact_cell" not in event)
        self.assertTrue("contact_email" not in event)
        self.assertTrue("media" not in event)
        self.assertTrue("times" not in event)
        self.assertTrue("tags" not in event)
        self.assertTrue("park_id" not in event)

    def check_ttd(self, ttd):
        """Check things-to-do data schema in parks instance"""
        self.assertTrue("id" in ttd)
        self.assertTrue(isinstance(ttd["id"], int))
        self.assertTrue("title" in ttd)
        self.assertTrue(isinstance(ttd["title"], str))
        self.assertTrue("seasons" not in ttd)
        self.assertTrue("require_reservation" not in ttd)
        self.assertTrue("has_fees" not in ttd)
        self.assertTrue("fee_description" not in ttd)
        self.assertTrue("pets_allowed" not in ttd)
        self.assertTrue("duration" not in ttd)
        self.assertTrue("description" not in ttd)
        self.assertTrue("accessibility_info" not in ttd)
        self.assertTrue("time_of_day" not in ttd)
        self.assertTrue("media" not in ttd)
        self.assertTrue("tags" not in ttd)
        self.assertTrue("parks" not in ttd)

    def check_parks(self, park):
        """Check things-to-do data schema in parks instance"""
        self.assertTrue("id" in park)
        self.assertTrue(isinstance(park["id"], int))
        self.assertTrue("name" in park)
        self.assertTrue(isinstance(park["name"], str))
        self.assertTrue("state" not in park)
        self.assertTrue("park_code" not in park)
        self.assertTrue("min_fee" not in park)
        self.assertTrue("max_fee" not in park)
        self.assertTrue("directions_url" not in park)
        self.assertTrue("longitude" not in park)
        self.assertTrue("latitude" not in park)
        self.assertTrue("description" not in park)
        self.assertTrue("phone" not in park)
        self.assertTrue("email" not in park)
        self.assertTrue("url" not in park)
        self.assertTrue("weather" not in park)
        self.assertTrue("stdhour_description" not in park)
        self.assertTrue("world_heritage" not in park)
        self.assertTrue("media" not in park)
        self.assertTrue("standard_hours" not in park)
        self.assertTrue("tags" not in park)
        self.assertTrue("events" not in park)
        self.assertTrue("thingstodo" not in park)


if __name__ == "__main__":
    main()
