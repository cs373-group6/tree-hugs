"""Configuration file for Tree Hugs backend"""
# pylint: disable = too-few-public-methods


class Config:
    """General configuration"""

    ENV = "development"
    DEBUG = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class ProductionConfig(Config):
    """Production mode"""

    ENV = "production"
    DEBUG = False


class DevelopmentConfig(Config):
    """Development mode"""

    ENV = "development"
    DEBUG = True


class TestingConfig(Config):
    """Testing mode"""

    TESTING = True
    SQLALCHEMY_DATABASE_URI = "sqlite:///:memory:"
