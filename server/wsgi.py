"""Production"""

from index import app

if __name__ == "__main__":
    app.config.from_object("appconfig.ProductionConfig")
    app.run()
