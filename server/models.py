"""Model definitions"""
# pylint: disable=too-few-public-methods

from init import db, BaseModel


ParksToThingsToDo = db.Table(
    "ParksToThingsToDo",
    db.Column(
        "park_id",
        db.Integer,
        db.ForeignKey("parks.id", ondelete="SET NULL"),
        primary_key=True,
    ),
    db.Column(
        "thingstodo_id",
        db.Integer,
        db.ForeignKey("thingstodo.id", ondelete="SET NULL"),
        primary_key=True,
    ),
)

ParksToEvents = db.Table(
    "ParksToEvents",
    db.Column(
        "park_id",
        db.Integer,
        db.ForeignKey("parks.id", ondelete="SET NULL"),
        primary_key=True,
    ),
    db.Column(
        "event_id",
        db.Integer,
        db.ForeignKey("events.id", ondelete="SET NULL"),
        primary_key=True,
    ),
)


EventsToThingsToDo = db.Table(
    "EventsToThingsToDo",
    db.Column(
        "event_id",
        db.Integer,
        db.ForeignKey("events.id", ondelete="SET NULL"),
        primary_key=True,
    ),
    db.Column(
        "thingstodo_id",
        db.Integer,
        db.ForeignKey("thingstodo.id", ondelete="SET NULL"),
        primary_key=True,
    ),
)


class Parks(BaseModel):
    """Parks model"""

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    state = db.Column(db.String)
    park_code = db.Column(db.String(4))
    min_fee = db.Column(db.Float)
    max_fee = db.Column(db.Float)
    directions_url = db.Column(db.String)
    longitude = db.Column(db.Float)
    latitude = db.Column(db.Float)
    description = db.Column(db.String)
    phone = db.Column(db.String)
    email = db.Column(db.String)
    url = db.Column(db.String)
    weather = db.Column(db.String)
    stdhour_description = db.Column(db.String)
    world_heritage = db.Column(db.Boolean)
    media = db.relationship("Media", cascade="all")
    standard_hours = db.relationship("StandardHours", cascade="all")
    tags = db.relationship("Tags", cascade="all")
    # events = db.relationship("Events", backref="park", cascade="all")


class Events(BaseModel):
    """Events model"""

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String)
    category = db.Column(db.Boolean)
    date = db.Column(db.String)
    is_free = db.Column(db.Boolean)
    fee_info = db.Column(db.String)
    longitude = db.Column(db.Float)
    latitude = db.Column(db.Float)
    description = db.Column(db.String)
    url = db.Column(db.String)
    contact_name = db.Column(db.String)
    contact_cell = db.Column(db.String)
    contact_email = db.Column(db.String)
    require_registration = db.Column(db.Boolean)
    media = db.relationship("Media", cascade="all")
    times = db.relationship("Time", cascade="all")
    tags = db.relationship("Tags", cascade="all")
    parks = db.relationship(
        "Parks", secondary=ParksToEvents, backref="events", cascade="all"
    )
    # park_id = db.Column(db.Integer, db.ForeignKey("parks.id", ondelete="SET NULL"))


class Thingstodo(BaseModel):
    """Things-to-do model"""

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String)
    seasons = db.Column(db.String)
    require_reservation = db.Column(db.Boolean)
    has_fees = db.Column(db.Boolean)
    fee_description = db.Column(db.String)
    pets_allowed = db.Column(db.Boolean)
    duration = db.Column(db.String)
    description = db.Column(db.String)
    accessibility_info = db.Column(db.String)
    time_of_day = db.Column(db.String)
    video_url = db.Column(db.String)
    media = db.relationship("Media", cascade="all")
    tags = db.relationship("Tags", cascade="all")
    parks = db.relationship(
        "Parks", secondary=ParksToThingsToDo, backref="thingstodo", cascade="all"
    )
    events = db.relationship(
        "Events", secondary=EventsToThingsToDo, backref="thingstodo", cascade="all"
    )


class Media(BaseModel):
    """Images"""

    id = db.Column(db.Integer, primary_key=True)
    url = db.Column(db.String)
    title = db.Column(db.String)
    alt_text = db.Column(db.String)
    caption = db.Column(db.String)
    credit = db.Column(db.String)
    park_id = db.Column(db.Integer, db.ForeignKey("parks.id", ondelete="SET NULL"))
    event_id = db.Column(db.Integer, db.ForeignKey("events.id", ondelete="SET NULL"))
    thingstodo_id = db.Column(
        db.Integer, db.ForeignKey("thingstodo.id", ondelete="SET NULL")
    )


class StandardHours(BaseModel):
    """Standard operating hours for parks"""

    id = db.Column(db.Integer, primary_key=True)
    day = db.Column(db.String)
    time = db.Column(db.String)
    park_id = db.Column(db.Integer, db.ForeignKey("parks.id", ondelete="SET NULL"))


class Tags(BaseModel):
    """Tags"""

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    park_id = db.Column(db.Integer, db.ForeignKey("parks.id", ondelete="SET NULL"))
    event_id = db.Column(db.Integer, db.ForeignKey("events.id", ondelete="SET NULL"))
    thingstodo_id = db.Column(
        db.Integer, db.ForeignKey("thingstodo.id", ondelete="SET NULL")
    )


class Time(BaseModel):
    """Time for events"""

    id = db.Column(db.Integer, primary_key=True)
    start = db.Column(db.String)
    end = db.Column(db.String)
    event_id = db.Column(db.Integer, db.ForeignKey("events.id", ondelete="SET NULL"))


def drop():
    """Drop all tables from db"""
    db.drop_all(bind=None)


def create():
    """Create all tables in db"""
    db.create_all()
