# Quick Start

### `python index.py`
Runs treehugs backend in the development mode.  
Open [http://localhost:8080](http://localhost:8080) to view it in the browser.  
The server will reload itself on code changes.

### `pip freeze -> requirements.txt`
Update flask packages