"""API Schema: specifies what fields are included in the API responses"""
# pylint: disable = missing-class-docstring
# pylint: disable = too-few-public-methods
# pylint: disable = no-member

from flask_marshmallow import Marshmallow  # type: ignore
from init import app


ma = Marshmallow(app)


class MediaSchema(ma.Schema):  # type: ignore
    class Meta:
        fields = ("url", "title", "alt_text", "caption", "credit")


class StandardHoursSchema(ma.Schema):  # type: ignore
    class Meta:
        fields = ("day", "time")


class TimesSchema(ma.Schema):  # type: ignore
    class Meta:
        fields = ("start", "end")


class TagsSchema(ma.Schema):  # type: ignore
    class Meta:
        fields = ("name",)


class RelatedParksSchema(ma.Schema):  # type: ignore
    class Meta:
        fields = ("id", "name")


class RelatedEventsSchema(ma.Schema):  # type: ignore
    class Meta:
        fields = ("id", "title")


class RelatedTTDSchema(ma.Schema):  # type: ignore
    class Meta:
        fields = ("id", "title")


class ParkSchema(ma.Schema):  # type: ignore
    class Meta:
        fields = (
            "id",
            "name",
            "state",
            "park_code",
            "min_fee",
            "max_fee",
            "directions_url",
            "longitude",
            "latitude",
            "description",
            "phone",
            "email",
            "url",
            "weather",
            "stdhour_description",
            "world_heritage",
            "media",
            "standard_hours",
            "tags",
            "events",
            "thingstodo",
        )

    media = ma.List(ma.Nested(MediaSchema))
    standard_hours = ma.List(ma.Nested(StandardHoursSchema))
    tags = ma.List(ma.Nested(TagsSchema))
    events = ma.List(ma.Nested(RelatedEventsSchema))
    thingstodo = ma.List(ma.Nested(RelatedTTDSchema))


class EventSchema(ma.Schema):  # type: ignore
    class Meta:
        fields = (
            "id",
            "title",
            "category",
            "date",
            "is_free",
            "fee_info",
            "longitude",
            "latitude",
            "description",
            "url",
            "contact_name",
            "contact_cell",
            "contact_email",
            "require_registration",
            "media",
            "times",
            "tags",
            "parks",
            "thingstodo",
        )

    media = ma.List(ma.Nested(MediaSchema))
    times = ma.List(ma.Nested(TimesSchema))
    tags = ma.List(ma.Nested(TagsSchema))
    parks = ma.List(ma.Nested(RelatedParksSchema))
    thingstodo = ma.List(ma.Nested(RelatedTTDSchema))


class TTDSchema(ma.Schema):  # type: ignore
    class Meta:
        fields = (
            "id",
            "title",
            "seasons",
            "require_reservation",
            "has_fees",
            "fee_description",
            "pets_allowed",
            "duration",
            "description",
            "accessibility_info",
            "time_of_day",
            "video_url",
            "media",
            "tags",
            "parks",
            "events",
        )

    media = ma.List(ma.Nested(MediaSchema))
    tags = ma.List(ma.Nested(TagsSchema))
    parks = ma.List(ma.Nested(RelatedParksSchema))
    events = ma.List(ma.Nested(RelatedEventsSchema))
