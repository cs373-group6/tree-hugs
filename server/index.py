"""The root file of TreeHugs API"""
# pylint: disable = too-many-branches

from flask import jsonify, request
from sqlalchemy import and_, or_, func  # type: ignore
from init import app, db
import models
import api_schema
import data


PARKS_PER_PAGE = 12
EVENTS_PER_PAGE = 25
TTD_PER_PAGE = 25

# API schema
park_schema = api_schema.ParkSchema()
parks_schema = api_schema.ParkSchema(many=True)
event_schema = api_schema.EventSchema()
events_schema = api_schema.EventSchema(many=True)
ttd_schema = api_schema.TTDSchema()
ttds_schema = api_schema.TTDSchema(many=True)

# API


@app.route("/api/parks", methods=["GET"])
def parks():
    """Get all parks"""
    # takes page, per_page, or max_per_page from the request query
    query = models.Parks.query
    if request.args.get("state"):
        query = query.filter(
            or_(
                models.Parks.state.like(f"%{state}%")
                for state in request.args.get("state").split(",")
            )
        )
    if request.args.get("world_heritage"):
        if request.args.get("world_heritage") == "True":
            query = query.filter_by(world_heritage=(True))
        elif request.args.get("world_heritage") == "False":
            query = query.filter_by(world_heritage=(False))

    if request.args.get("min_budget"):
        query = query.filter(
            models.Parks.max_fee >= float(request.args.get("min_budget"))
        )
    if request.args.get("max_budget"):
        query = query.filter(
            models.Parks.min_fee <= float(request.args.get("max_budget"))
        )
    if request.args.get("day"):
        query = query.filter(
            models.Parks.standard_hours.any(
                and_(
                    models.StandardHours.day == request.args.get("day"),
                    models.StandardHours.time != "Closed",
                )
            )
        )
    if request.args.get("name"):
        if request.args.get("name") == "asc":
            query = query.order_by(models.Parks.name)
        elif request.args.get("name") == "desc":
            query = query.order_by(models.Parks.name.desc())
    page_size = (
        int(request.args.get("page_size"))
        if request.args.get("page_size")
        else PARKS_PER_PAGE
    )
    res = query.paginate(per_page=page_size)
    obj = parks_schema.dump(res.items)
    result = {
        "num_results": res.total,
        "objects": obj,
        "page": res.page,
        "page_size": page_size,
        "total_pages": res.pages,
    }
    return jsonify(result)


@app.route("/api/parks/id=<park_id>", methods=["GET"])
def park(park_id):
    """Get a single park by id"""
    res = models.Parks.query.get(park_id)
    obj = park_schema.dump(res)
    return jsonify(obj)


@app.route("/api/events", methods=["GET"])
def events():
    """Get all events"""
    query = models.Events.query

    if request.args.get("category"):
        if request.args.get("category") == "True":
            query = query.filter_by(category=(True))
        elif request.args.get("category") == "False":
            query = query.filter_by(category=(False))

    if request.args.get("free"):
        if request.args.get("free") == "True":
            query = query.filter_by(is_free=(True))
        elif request.args.get("free") == "False":
            query = query.filter_by(is_free=(False))

    if request.args.get("registration"):
        if request.args.get("registration") == "True":
            query = query.filter_by(require_registration=(True))
        elif request.args.get("registration") == "False":
            query = query.filter_by(require_registration=(False))

    if request.args.get("date"):
        if request.args.get("date") == "asc":
            query = query.order_by(models.Events.date)
        elif request.args.get("date") == "desc":
            query = query.order_by(models.Events.date.desc())

    if request.args.get("title"):
        if request.args.get("title") == "asc":
            query = query.order_by(models.Events.title)
        elif request.args.get("title") == "desc":
            query = query.order_by(models.Events.title.desc())

    # takes page, per_page, or max_per_page from the request query
    page_size = (
        int(request.args.get("page_size"))
        if request.args.get("page_size")
        else EVENTS_PER_PAGE
    )
    res = query.paginate(per_page=page_size)
    obj = events_schema.dump(res.items)
    result = {
        "num_results": res.total,
        "objects": obj,
        "page": res.page,
        "page_size": page_size,
        "total_pages": res.pages,
    }
    return jsonify(result)


@app.route("/api/events/id=<event_id>", methods=["GET"])
def event(event_id):
    """Get a single event by id"""
    res = models.Events.query.get(event_id)
    obj = event_schema.dump(res)
    return jsonify(obj)


@app.route("/api/thingstodo", methods=["GET"])
def ttds():
    """Get all things-to-do"""
    query = models.Thingstodo.query

    # if request.args.get("seasons"):
    #     filter_seasons = []
    #     for i in range(4):
    #         if BASE_SEASONS[i] in request.args.get("seasons").lower():
    #             filter_seasons.append(1 << i)

    #     query = query.filter(
    #         or_(
    #             models.Thingstodo.seasons.op("&")(season) > 0
    #             for season in filter_seasons
    #         )
    #     )

    if request.args.get("seasons"):
        query = query.filter(
            or_(
                models.Thingstodo.seasons.like(f"%{season}%")
                for season in request.args.get("seasons").split(",")
            )
        )

    if request.args.get("reservation"):
        if request.args.get("reservation") == "True":
            query = query.filter_by(require_reservation=(True))
        elif request.args.get("reservation") == "False":
            query = query.filter_by(require_reservation=(False))

    if request.args.get("fees"):
        if request.args.get("fees") == "True":
            query = query.filter_by(has_fees=(True))
        elif request.args.get("fees") == "False":
            query = query.filter_by(has_fees=(False))

    if request.args.get("pets"):
        if request.args.get("pets") == "True":
            query = query.filter_by(pets_allowed=(True))
        elif request.args.get("pets") == "False":
            query = query.filter_by(pets_allowed=(False))

    if request.args.get("title"):
        if request.args.get("title") == "asc":
            query = query.order_by(models.Thingstodo.title)
        elif request.args.get("title") == "desc":
            query = query.order_by(models.Thingstodo.title.desc())

    # takes page, per_page, or max_per_page from the request query
    page_size = (
        int(request.args.get("page_size"))
        if request.args.get("page_size")
        else TTD_PER_PAGE
    )
    res = query.paginate(per_page=page_size)
    obj = ttds_schema.dump(res.items)
    result = {
        "num_results": res.total,
        "objects": obj,
        "page": res.page,
        "page_size": page_size,
        "total_pages": res.pages,
    }
    return jsonify(result)


@app.route("/api/thingstodo/id=<ttd_id>", methods=["GET"])
def ttd(ttd_id):
    """Get a single things-to-do by id"""
    res = models.Thingstodo.query.get(ttd_id)
    obj = ttd_schema.dump(res)
    return jsonify(obj)


@app.route("/api/events-dates", methods=["GET"])
def events_dates():
    """Get all events dates and number of events on each date"""
    # pylint: disable = no-member
    res = (
        db.session.query(models.Events.date, func.count(models.Events.date))
        .group_by(models.Events.date)
        .order_by(models.Events.date)
        .all()
    )
    obj = []
    for row in res:
        obj.append({"date": row[0], "num_events": row[1]})
    result = {
        "num_results": len(obj),
        "objects": obj,
    }
    return result


def post_data():
    """Load data to DB"""
    models.drop()
    models.create()

    data.post_parks()
    data.post_events()
    data.post_thingstodo()

    data.load_ttd_videos()

    data.relate_events_ttd()


@app.route("/")
def index():
    """Home page"""
    return "You are now connected to Tree Hugs API."


if __name__ == "__main__":
    app.config.from_object("appconfig.DevelopmentConfig")
    # post_data()
    app.run(port=8080, host="0.0.0.0")
