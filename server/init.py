"""Init treehugs backend"""
# pylint: disable = broad-except

from flask import Flask
from flask_sqlalchemy import SQLAlchemy  # type: ignore
from flask_sqlalchemy.model import DefaultMeta  # type: ignore
from flask_cors import CORS  # type: ignore

app = Flask(__name__, instance_relative_config=True)
app.config.from_object("appconfig.Config")
try:
    app.config.from_pyfile("config.py")
except Exception:
    pass
cors = CORS(app)

db = SQLAlchemy(app)
BaseModel: DefaultMeta = db.Model
